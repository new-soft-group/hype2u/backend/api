<?php

return [

    'code' => [
        'invalid' => 'This discount code is invalid or has expired.'
    ]
];
