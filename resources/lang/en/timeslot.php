<?php

return [
    'has' => [
        'allocation' => 'The given timeslot has allocations.'
    ],
    'duplicate' => 'Order has already selected timeslot.',
    'full' => 'This timeslot is fully booked.',
    'holiday' => 'The selected date is public holiday.',
    'weekend' => 'The selected date is weekend.',
];
