<?php

return [
    'not_found' => [
        'stock' => 'Product :name, :variant stock not found.'
    ],

    'out_of_stock' => 'Product :name, :variant is currently out of stock.',

    'insufficient_stock' => 'Product :name, :variant left :quantity quantity.',

    'tag' => [
        'insufficient_stock' => 'Product :variant left :quantity quantity.'
    ],

    'variant' => [
        'update' => [
            'failed' => 'The selected variant option has associated stocks. Please remove the associated product stock first.'
        ]
    ],

    'stock' => [
        'delete' => [
            'failed' => 'The selected stock has associated tags. Please remove the tags first.'
        ],
        'invalid' => 'Something unexpected happen. Kindly try again.'
    ],

    'type' => [
        'delete' => [
            'failed' => 'Please remove type from product first.'
        ]
    ],

    'delete' => [
        'failed' => 'This product have been used in orders.',
    ],

    'business_type' => [
        'invalid' => 'This product business type is invalid.'
    ]
];
