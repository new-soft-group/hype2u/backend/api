<?php

return [
    'failed' => [
        'update' => "This coupon can't be update because it has been used in a invoice."
    ],
    'plan' => [
        'not_belong' => "This promo code does not belong to the selected plan."
    ],
    'not_valid' => "This promo code is not valid"
];
