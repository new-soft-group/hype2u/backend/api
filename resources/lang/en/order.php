<?php

return [
    'failed' => [
        'mark_delivered' => 'Order is allowed to mark as delivered when payment was made.',
        'update' => 'Modification to this order is not allowed because status is :status.',
        'cancel' => 'This order failed to cancel because status is :status.',
        'delete' => "This order failed to delete because status is :status.",
        'delete_item' => "This order's item failed to delete because status is :status.",
        'ongoing' => 'You currently have :count ongoing orders.',
    ],
    'has' => [
        'charge' => 'This order has a paid charge payment.'
    ],
    'tracks_quantity_full' => 'This order only have :quantity items. Please remove some scanned tags of the order for adding new tags.',
    'tracks_quantity_not_match' => 'Quantity of scanned tags is not match with quantity of order items.',

];
