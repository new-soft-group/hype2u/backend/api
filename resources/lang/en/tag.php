<?php

return [
    'required' => 'Required exactly :number tags for verification.',
    'not_match' => 'Tags :tags not match.',
    'scanned' => 'Tag :tag has already been scanned.',
];
