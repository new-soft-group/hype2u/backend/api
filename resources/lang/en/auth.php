<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'required' => 'Unauthenticated',
    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'logout' => 'You have been logout from the system.',
    'login' => [
        'title' => 'Account Login',
        'email' => 'Email',
        'password' => 'Password',
        'confirm' => [
            'password' => 'Confirm Password',
        ],
        'button' => [
            'login' => 'Login',
        ],
        'remember' => 'Remember Me',
        'forgot' => 'Forgot Your Password?',
    ],
    'register' => [
        'title' => 'Register',
        'first_name' => 'First Name',
        'last_name' => 'Last Name',
        'mobile' => 'Mobile Number',
    ],
    'forget' => [
        'title' => 'Forget Password',
        'button' => [
            'send' => 'Send Email',
        ],
    ],
    'reset' => [
        'title' => 'Reset Password',
        'button' => [
            'send' => 'Confirm Reset',
        ]
    ],
    'lang' => [
        'title' => 'Language',
        'langs' => [
            'en' => 'English',
            'zh' => 'Chinese Simplified',
        ],
    ],
    'links' => [
        'login' => 'Home'
    ],
    'provider' => [
        'failed' => 'The given provider token is invalid.'
    ]

];
