<?php

return [
    'threshold' => [
        'invalid' => 'Threshold value must greater than plan amount.'
    ],
    'violate' => 'Current :plan plan only allow :quantity items from :type.',
    'policies' => 'Current plan only allows :message to be added in your order. Kindly contact Customer Support for verification.',
    'exceed' => [
        'exchange' => 'Current :plan plan only allow :limit order per month. You already have :count order within this month.',
        'amount' => 'Current :plan plan only allow total RM:amount amount per order.',
        'quantity' => 'Current :plan plan only allow maximum :quantity items per order.',
        'threshold' => 'Current :plan maximum purchase amount can up to MYR:maximum only.'
    ],
    'policy' => [
        'delete' => [
            'failed' => 'Current plan has existing subscriptions.'
        ]
    ]
];
