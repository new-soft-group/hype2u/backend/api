<?php

return [

    'not_found' => [
        'shipping_address' => "User doesn't have default shipping address."
    ]
];
