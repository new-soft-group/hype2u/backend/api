<?php

return [

    'item' => [
        'not_exist' => "Cart item not exist.",
        'not_belong_cart' => "Cart item does not belongs to user's cart.",
        'quantity_exceeded' => "Cart item quantity exceeded the stock count.",
        'duplicate' => 'Cart item already exist.'
    ],

    'empty' => "Your cart don't have any items.",

    'failed' => [
        'session' => 'Required cart session header is missing.',
        'invalid' => 'Auth checkout must have a valid cart with items.'
    ]
];
