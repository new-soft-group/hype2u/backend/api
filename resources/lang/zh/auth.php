<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '所填写资料并不符合我们系统的资料。',
    'throttle' => '登录尝试次数过多。请:seconds秒过后再重试。',
    'login' => [
        'title' => '登录账号',
        'email' => '电子邮件',
        'password' => '密码',
        'confirm' => [
            'password' => '确定密码',
        ],
        'button' => [
            'login' => '登录',
        ],
        'remember' => '记住我',
        'forgot' => '忘记您的密码？',
    ],
    'register' => [
        'title' => '注册',
        'first_name' => '名字',
        'last_name' => '姓氏',
        'mobile' => '手机号码',
    ],
    'forget' => [
        'title' => '忘记密码',
        'button' => [
            'send' => '发送邮件',
        ],
    ],
    'reset' => [
        'title' => '重置密码',
        'button' => [
            'send' => '确认重置',
        ]
    ],
    'lang' => [
        'title' => '语言',
        'langs' => [
            'en' => '英文',
            'zh' => '中文',
        ],
    ],
    'links' => [
        'login' => '首页'
    ],

];
