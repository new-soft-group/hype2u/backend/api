<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => '您的密码已被重置！',
    'sent' => '我们已经发送了密码重置连接到您的电子邮件！',
    'throttled' => '请稍后再重试。',
    'token' => '此重置密码连接无效。',
    'user' => "我们无法找寻此电子邮件的注册用户。",
    'password' => '密码必须是最少8个字节，并且需要符合确定密码。',
];
