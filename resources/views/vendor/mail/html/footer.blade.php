<tr>
<td>
<table class="footer" align="center" width="570" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td class="content-cell" align="center">
<div class="footer-container">
<div class="icon-container">
<a href="https://www.facebook.com/hype2u.streetwear/" target="_blank"><img src="{{ asset('images/dashboard/facebook.png') }}" class="facebook-icon"></a> <span class="horizon-spacing"> </span>
<a href="https://www.instagram.com/hype2u.streetwear/" target="_blank"><img src="{{ asset('images/dashboard/instagram.png') }}" class="instagram-icon"></a>
</div>
<div class="footer-navigation-container">
<a href="http://hype2u.newsoft.com.my/?scroll=howitworks">How it works</a><span class="horizon-spacing">|</span><a href="http://hype2u.newsoft.com.my/privacy">Rental policy</a><span class="horizon-spacing">|</span><a href="http://hype2u.newsoft.com.my">Contact Us</a>
</div>
</div>

{{ Illuminate\Mail\Markdown::parse($slot) }}
{{ Illuminate\Mail\Markdown::parse('33, Grove Lakefields, Sungai Besi, Kuala Lumpur 57000, MY.')}}
</td>
</tr>
</table>
</td>
</tr>
