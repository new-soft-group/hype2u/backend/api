<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
<link rel="preconnect" href="https://fonts.gstatic.com">
{{--<link href="https://fonts.googleapis.com/css2?family=Syne:wght@700&display=swap" rel="stylesheet">--}}
{{--<link href="http://fonts.cdnfonts.com/css/metropolis-2" rel="stylesheet">--}}
<style>
{{--@font-face {--}}
{{--font-family: SyneBold;--}}
{{--src: url('{{asset('/fonts/syne/Syne-Bold.ttf')}}');--}}
{{--}--}}

{{--@font-face {--}}
{{--font-family: MetropolisRegular;--}}
{{--src: url('{{asset('/fonts/metropolis/Metropolis-Regular.otf')}}');--}}
{{--}--}}


@media only screen and (max-width: 600px) {
.inner-body {
width: 100% !important;
}

.content-cell{
padding: 2rem 1rem 2rem 1rem !important;
}

.footer {
width: 100% !important;
}
}

@media only screen and (max-width: 500px) {
.button {
width: 100% !important;
}
}
</style>

<table class="wrapper" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<tr>
<td align="center">
<table class="content" width="100%" cellpadding="0" cellspacing="0" role="presentation">
<table class="inner-body" align="center" width="550" cellpadding="0" cellspacing="0" role="presentation">
{{ $header ?? '' }}
</table>

    <!-- Email Body -->
<tr>
<td class="body" width="100%" cellpadding="0" cellspacing="0">
<table class="inner-body" align="center" width="550" cellpadding="0" cellspacing="0" role="presentation">
<!-- Body content -->
<tr>
<td class="content-cell">
    {{ Illuminate\Mail\Markdown::parse($slot) }}

    {{ $subcopy ?? '' }}
</td>
</tr>
</table>
</td>
</tr>

{{ $footer ?? '' }}
</table>
</td>
</tr>
</table>
</body>
</html>
