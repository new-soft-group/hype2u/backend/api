<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">

        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <meta name="secret" content="{{ config('stripe.key') }}">

        <link rel="icon" type="image/png" href="{{ asset('/images/web/favicon.png') }}">

        <title>{{ config('app.name', 'CircleDna') }}</title>

        <!-- Fonts -->
        <link rel="dns-prefetch" href="//fonts.gstatic.com">

        <script src="https://js.stripe.com/v3/"></script>

        <link href="{{ mix('css/checkout.css') }}" rel="stylesheet">

        <script src="{{ mix('js/checkout.js') }}" defer></script>
    </head>
    <body>
        <div class="wrapper container full-height">

            <div class="row">
                <div class="col pb-5">
                    <div class="logo">
                        <img src="{{ asset('images/dasbboard/h2u.png') }}" class="logo" alt="Hype2U Logo">
                    </div>
                </div>
            </div>

            <div class="row full-height">
                <div class="col-md-6">
                    <form id="payment-form">
                        <div id="card-element"><!--Stripe.js injects the Card Element--></div>
                        <button class="mt-4" id="submit" type="submit" disabled>Pay</button>
                        <p id="card-errors" role="alert"></p>
                    </form>
                </div>

                <div class="col-md-6">
                    <div class="card border-0 ">
                        <div class="card-body pt-0">
                            @foreach($order->items as $item)
                                <div class="row justify-content-between">
                                    <div class="col-auto col-md-7">
                                        <div class="media flex-column flex-sm-row">
                                            <img class="img-fluid mr-4"
                                                 src="{{ \Storage::disk('product')->url($item->product->images->first()->path) }}"
                                                 width="62" height="62"
                                                 alt="product">
                                            <div class="media-body my-auto">
                                                <div class="row">
                                                    <div class="col-auto">
                                                        <p class="mb-0"><b>{{ $item->product->name }}</b></p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class=" pl-0 flex-sm-col col-auto my-auto">
                                        <p class="boxed-1">{{ $item->quantity }}</p>
                                    </div>
                                    <div class=" pl-0 flex-sm-col col-auto my-auto ">
                                        <p><b>{{ $item->currency }} ${{ number_format((float)$item->converted_unit_price, 2, '.', '') }}</b></p>
                                    </div>
                                </div>
                                <hr class="my-2">
                            @endforeach

                            <div class="row ">
                                <div class="col">
                                    <div class="row justify-content-between">
                                        <div class="col-4">
                                            <p><b>Total</b></p>
                                        </div>
                                        <div class="flex-sm-col col-auto">
                                            <p class="mb-1"><b>{{ $order->currency }} ${{ number_format((float)$order->converted_amount, 2, '.', '') }}</b></p>
                                        </div>
                                    </div>
                                    <hr class="my-0">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="modal fade" id="dialog" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title text-black-50">Redirect</h5>
                        </div>
                        <div class="modal-body dialog-message">
                            Thanks for your purchase, invoice will send directly to your email shorty.

                            <div class="dialog-message pt-2">
                                You will be redirect back to home page shortly. <span id="countdown">10</span> seconds.
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

<script>
    document.addEventListener('DOMContentLoaded', function () {

        const dialog = $('#dialog');

        dialog.on('hide.bs.modal', function () {
            return false
        });

        const startCountdown = function () {
            dialog.modal('show');
            let seconds = document.getElementById("countdown").textContent;
            let countdown = setInterval(function() {
                seconds--;
                document.getElementById("countdown").textContent = seconds;
                if (seconds <= 0) {
                    clearInterval(countdown);

                    window.location.href = '/';
                }
            }, 1000);
        };

        // Create references to the main form and its submit button.
        const form = document.getElementById('payment-form');
        const submitButton = document.getElementById('submit');

        const processing = (isProcessing) => {
            if(isProcessing) {
                submitButton.textContent = 'Processing...';
                submitButton.disabled = true;
            }
            else {
                submitButton.textContent = 'Pay';
                submitButton.disabled = false;
            }
        };

        const clearError = function() {
            let cardErrors = document.querySelector("#card-errors");
            cardErrors.textContent = "";
        };

        const showError = function(errorMsgText) {
            let cardErrors = document.querySelector("#card-errors");
            cardErrors.textContent = errorMsgText;
        };

        /**
         * Setup Stripe Elements.
         */

        // Create a Stripe client.
        const stripe = Stripe('{{ config('stripe.key') }}');

        // Create an instance of Elements.
        const elements = stripe.elements();

        // Prepare the styles for Elements.
        const style = {
            base: {
                iconColor: '#666ee8',
                color: '#31325f',
                fontWeight: 400,
                fontFamily:
                    '-apple-system, BlinkMacSystemFont, "Segoe UI", Roboto, Oxygen-Sans, Ubuntu, Cantarell, "Helvetica Neue", sans-serif',
                fontSmoothing: 'antialiased',
                fontSize: '15px',
                '::placeholder': {
                    color: '#aab7c4',
                },
                ':-webkit-autofill': {
                    color: '#666ee8',
                },
            },
        };

        /**
         * Implement a Stripe Card Element that matches the look-and-feel of the app.
         *
         * This makes it easy to collect debit and credit card payments information.
         */

        // Create a Card Element and pass some custom styles to it.
        const card = elements.create('card', {style, hidePostalCode: true});

        // Mount the Card Element on the page.
        card.mount('#card-element');

        // Monitor change events on the Card Element to display any errors.
        card.on('change', (event) => {
            if (event.error) {
                showError(event.error.message);
            } else {
                clearError();
            }

            // Disable the Pay button if there are no card details in the Element
            submitButton.disabled = event.empty;
        });

        form.addEventListener("submit", function(event) {
            event.preventDefault();

            clearError();
            processing(true);

            stripe
                .confirmCardPayment('{{ $client_secret }}', {
                    payment_method: {
                        card: card
                    }
                })
                .then(function(result) {
                    if (result.error) {
                        processing(false);
                        showError(result.error.message);
                    } else {
                        startCountdown();
                    }
                });
        });
    });
</script>
