@component('mail::message')

<p class="text-left">Name - {{ $firstName }} {{ $lastName }}</p>

<p class="text-left">Email - {{ $email }}</p>

<p class="text-left">Question - {{ $question }}</p>

<div class="text-left">
Thanks,<br>
{{ config('app.name') }}
</div>
@endcomponent
