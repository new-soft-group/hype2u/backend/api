@component('mail::message')
<p class="text-center header-font">Thanks for joining Hype2u</p>

<p class="text-center content-spacing">Hi {{$user->profile->name}},</p>

<p class="text-center content-spacing">You're all set. Choose your favorite styles and place your reservation. We pack, ship and deliver within 2-3 business days.</p>

@component('mail::button', ['url' => $url, 'color' => 'red'])
{{$button}}
@endcomponent

<div class="user-info-container">
<p class="text-left">Your account information:</p>

<div class="text-left text-gray">Email</div>
<div class="text-left">{{$user->email}}</div>
<br>

<div class="text-left text-gray">Plan details</div>
<div class="text-left">{{$subscription->plan->name}} RM{{$subscription->plan->amount}} / {{$subscription->plan->interval}}</div>
<div class="text-left">Your next payment of RM{{ $subscription->plan->amount }} will be charged on {{ date('d M Y',strtotime($subscription->nextBillingCycle())) }}</div>
<br>

<div class="text-left text-gray">Payment</div>
<div class="flex-container flex-start align-center">
<img src="{{ asset('images/dashboard/visa.png') }}" class="visa-icon">
<div class="text-padding-left">**** **** ****{{$user->card_last_four}}</div>
</div>

</div>

@endcomponent
