@component('mail::message')
<p class="text-center header-font">Thanks for submitting pick for me request</p>

<p class="text-center content-spacing">Hi {{ $user->profile->name }},</p>

<p class="text-center content-spacing">Thank you for submitting pick for me request with Hype2U! Your request will be process in shortly. Our Hype2u team will contact you after request is being processed!</p>

@endcomponent
