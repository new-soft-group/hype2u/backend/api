@component('mail::message')
<p class="text-center header-font">Thanks for registering Hype2u</p>

<p class="text-center content-spacing">Hi {{ $user->profile->name }},</p>

<p class="text-center content-spacing">Thank you for registering with Hype2U! You can now start browsing our plans!</p>

@component('mail::button', ['url' => $url, 'color' => 'red'])
{{$button}}
@endcomponent

@endcomponent
