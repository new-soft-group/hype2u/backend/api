@component('mail::message')
<p class="text-center header-font">Stock arrived</p>

<p class="text-center content-spacing">{{ $stock->product->name }} {{ $stock->combination }} has arrived. Grab it now!</p>

@component('mail::button', ['url' => $url, 'color' => 'red'])
{{$button}}
@endcomponent

@endcomponent
