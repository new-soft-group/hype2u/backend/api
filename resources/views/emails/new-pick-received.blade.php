@component('mail::message')
<p class="text-center header-font">New Pick For Me Request</p>

<p class="text-center content-spacing">Hi admin,</p>

<p class="text-center content-spacing">There is a new pick for me request from {{ $user->profile->name }}. Kindly check customer requests from admin dashboard.</p>

@endcomponent
