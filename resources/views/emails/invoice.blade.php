@component('mail::message')

    Invoice Number - INV{{ $order->id }}

@component('mail::table')
| Product Name  | Quantity | Unit Price | Discount | Total |
|:--------------|---------:|-----------:|---------:|------:|
@foreach($order->items as $item)
| {{ $item->product->name }} | {{ $item->quantity }} | {{ $item->converted_unit_price }} {{ $item->currency }} | {{ $item->discount_type === 1 ? "$item->discount%" : "$item->discount $item->currency" }} | {{ $item->converted_amount }} {{ $item->currency }} |
@endforeach
|               |          |            |          |       |
|               |          |            | Discount | {{ $order->discount_type === 1 ? "$order->discount%" : "$order->discount $order->currency" }} |
|               |          |            | Total    | {{ $order->converted_amount }} {{ $order->currency }} |
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
