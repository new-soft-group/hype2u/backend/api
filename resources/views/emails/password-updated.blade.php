@component('mail::message')
<p class="text-center header-font">Password changed successfully</p>

<p class="text-center content-spacing">Hi {{ $user->profile->name }},</p>

<p class="text-center content-spacing">Your password has been changed successfully. Please login with new password!</p>

@component('mail::button', ['url' => $url, 'color' => 'red'])
{{$button}}
@endcomponent

@endcomponent
