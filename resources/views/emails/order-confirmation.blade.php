@component('mail::message')
<p class="text-center header-font">Order confirmed</p>

<p class="text-center content-spacing">Hi {{$user->profile->name}},</p>

<p class="text-center content-spacing">Thank You {{$user->profile->name}}! Your order is confirmed, and will be shipped in 1-2 days time!</p>

<p class="text-center order-header-spacing subheader-font">Order Details</p>

<div class="text-left text-gray">Order ID</div>
<div class="text-left">{{ $order->id }}</div>
<br>

<div class="text-left text-gray">Order At</div>
<div class="text-left">{{ date('d M Y',strtotime($order->created_at)) }}</div>
<br>

<div class="text-left text-gray">Email</div>
<div class="text-left">{{$user->email}}</div>
<br>

<div class="text-left text-gray">Shipping Address</div>
<div class="text-left">{{ $order->shippingAddress->address_1 }}</div>
<div class="text-left">{{$order->shippingAddress->address_2}}</div>
<div class="text-left">{{ $order->shippingAddress->postcode }} , {{$order->shippingAddress->city}} , {{ $order->shippingAddress->state }}</div>
<div class="text-left">{{ $order->shippingAddress->country }}</div>
<br>

<div class="product-container">
@foreach($items as $item)

<div class="flex-container flex-start align-center vertical-spacing">
<div class="product-image-container"><img src="{{$item->product->images->first()->url}}" class="product-image"></div>
<div class="product-description-container">
<div class="text-left">{{$item->product->brand->name}} {{$item->product->name}}</div>
<div class="text-left text-gray">{{$item->variant}}</div>
<div class="text-left text-gray"><span class="text-uppercase">{{$order->currency}}</span> {{$item->unit_price}}</div>
</div>
</div>

@endforeach
</div>

@component('mail::button', ['url' => $url, 'color' => 'red'])
{{$button}}
@endcomponent

@endcomponent
