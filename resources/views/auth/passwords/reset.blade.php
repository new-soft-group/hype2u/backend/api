@extends('web.layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('auth.reset.title') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('password.update') }}">
                        @csrf

                        <input type="hidden" name="token" value="{{ $token }}">
                        
                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right"><i class="material-icons prefix">email</i></label>

                            <div class="col-md-6">
                                <input id="email" type="email" placeholder="{{ __('auth.login.email') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ $email ?? old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right"><i class="material-icons prefix">lock</i></label>

                            <div class="col-md-6">
                                <input id="password" type="password" placeholder="{{ __('auth.login.password') }}" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password-confirm" class="col-md-4 col-form-label text-md-right"><i class="material-icons prefix">lock</i></label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" placeholder="{{ __('auth.login.confirm.password') }}" class="form-control" name="password_confirmation" required autocomplete="new-password">
                            </div>
                        </div>


                        <div class="col-md-8 offset-md-4">
                             <div class="btn text-center">
                                <button type="submit" class="mdc-button mdc-button--raised primary">
                                    <div class="mdc-button__ripple"></div>
                                    <i class="material-icons mdc-button__icon" aria-hidden="true">send</i>
                                    <span class="mdc-button__label">{{ __('auth.reset.button.send') }}</span>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
