@extends('web.layouts.auth')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">{{ __('auth.login.title') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">
                                <i class="material-icons prefix">account_circle</i>
                            </label>

                            <div class="col-md-6">
                                <input id="email" type="email" placeholder="{{ __('auth.login.email') }}" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">
                                <i class="material-icons prefix">lock</i>
                            </label>

                            <div class="col-md-6">
                                <input id="password" type="password" placeholder="{{ __('auth.login.password') }}" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('auth.login.remember') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row justify-content-center">
                            <div class="pl-5">
                                <button type="submit" class="mdc-button mdc-button--raised primary">
                                    <div class="mdc-button__ripple"></div>
                                    <i class="material-icons mdc-button__icon" aria-hidden="true">login</i>
                                    <span class="mdc-button__label">{{ __('auth.login.button.login') }}</span>
                                </button>
                            </div>

{{--                            <div class="pl-3">--}}
{{--                                <a class="btn mdc-button mdc-button--raised primary" role="button" href="{{ route('register') }}">--}}
{{--                                    <div class="mdc-button__ripple"></div>--}}
{{--                                    <i class="material-icons mdc-button__icon" aria-hidden="true">how_to_reg</i>--}}
{{--                                    <span class="mdc-button__label">{{ __('auth.register.title') }}</span>--}}
{{--                                </a>--}}
{{--                            </div>--}}
                        </div>

                        <div class="row justify-content-center">
                            @if (Route::has('password.request'))
                                <div class="offset-md-1">
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('auth.login.forgot') }}
                                    </a>
                                </div>
                            @endif
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
