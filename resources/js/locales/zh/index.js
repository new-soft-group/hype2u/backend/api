import message from './message.json'
import plan from './plan.json'
import validation from 'vee-validate/dist/locale/en'
import customValidation from './validation.json'

export default {
    ...message,
    ...plan,
    validation: {
        ...validation.messages,
        ...customValidation
    }
}
