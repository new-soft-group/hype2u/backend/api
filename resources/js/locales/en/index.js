import common from './common.json'
import profile from './profile.json'
import account from './account.json'
import warehouse from './warehouse.json'
import product from './product.json'
import vendor from './vendor.json'
import banner from './banner.json'
import plan from './plan.json'
import payment from './payment.json'
import wishlist from './wishlist.json'
import brand from './brand.json'
import order from './order.json'
import pick from './pick.json'
import report from './report.json'
import coupon from './coupon.json'
import timeslot from './timeslot.json'
import holiday from './holiday.json'
import validation from 'vee-validate/dist/locale/en'
import customValidation from './validation.json'

export default {
    ...common,
    ...profile,
    ...account,
    ...warehouse,
    ...report,
    ...product,
    ...vendor,
    ...banner,
    ...plan,
    ...payment,
    ...wishlist,
    ...brand,
    ...order,
    ...coupon,
    ...timeslot,
    ...holiday,
    ...pick,
    validation: {
        ...validation.messages,
        ...customValidation
    }
}
