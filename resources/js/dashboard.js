import './bootstrap';
import Vue from 'vue'
import vuetify from './plugins/vuetify'
import router from './plugins/router'
import store from './plugins/store/store'
import i18n from './plugins/i18n'
import Clipboard from 'v-clipboard'
import JsonExcel from 'vue-json-excel'
import countryLookup from './plugins/countries'
import App from './components/dashboard/App'
import './plugins/vee-validate'
import './plugins/filter'
import StripeComponent from "./components/stripe/StripeComponent";

Vue.countryLookup = Vue.prototype.countryLookup = countryLookup;

Vue.component('stripe-component', StripeComponent);
Vue.component('app', App);

Vue.use(Clipboard);

Vue.component('downloadExcel', JsonExcel);

const app = new Vue({
    vuetify,
    router,
    store,
    i18n,
    axios,
    el: '#app'
});
