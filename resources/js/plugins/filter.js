import moment from 'moment';
import numeral from 'numeral';
import i18n from './i18n';
import Vue from 'vue';

Vue.filter('capitalize', function (value) {
    if (!value) return '';
    value = value.toString();
    return value.charAt(0).toUpperCase() + value.slice(1);
});

Vue.filter('datetime', function (value, format = 'MMMM Do YYYY, h:mm:ss a') {
    if (!value) return i18n.t('tbd');

    return moment(value)
        .local()
        .format(format);
});

Vue.filter('date', function (value, format = 'MMM Do YYYY', locale = 'en') {
    if (value) {
        return moment(value).format(format);
    } else {
        return '-';
    }
});

Vue.filter('monthYear', function (value, locale = 'en') {
    if (value) {
        return moment(value)
            .locale(locale)
            .format("MMM YYYY");
    } else {
        return '-';
    }
});

Vue.filter('shortMonth', (value) => {
    if (value) {
        return moment()
            .month(value - 1)
            .format('MMM');
    } else {
        return '-';
    }
});

Vue.filter('time', function (value, format = 'H:mm') {
    return moment.utc(value, "H:i").local().format(format);
});

Vue.filter('currency', function (value, format = '0,0.00') {
    const number = Number(value);
    return numeral(number).format(format);
});

Vue.filter('percentage', function (value) {

    return numeral(value).format('0.00%');
});

Vue.filter('discount', function (value, type, currency = 'MYR', rate = 1) {

    if (type === 1) {
        return numeral(value * rate).format('0,0.00') + ` ${currency}`;
    } else if (type === 2) {
        return numeral(value / 100).format('0.00%');
    } else {
        return 'N/A';
    }
});
