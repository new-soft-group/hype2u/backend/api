import Vue from 'vue';
import store from "./store/store";
import VueRouter from 'vue-router';
import Dashboard from '../components/dashboard/Dashboard'
import Account from '../components/dashboard/account/Account'
import AccountCreate from '../components/dashboard/account/AccountCreate'
import AccountEdit from "../components/dashboard/account/AccountEdit";
import Product from "../components/dashboard/product/Product";
import ProductCreate from "../components/dashboard/product/ProductCreate";
import ProductEdit from "../components/dashboard/product/ProductEdit";
import UserProfile from "../components/dashboard/profile/UserProfile";
import Setting from "../components/dashboard/setting/Setting";
import Payout from "../components/dashboard/payout/Payout";
import Order from "../components/dashboard/order/Order";
import ChangePasswordForm from "../components/dashboard/password/ChangePasswordForm";
import AffiliateMember from "../components/dashboard/affiliate/Member";
import ProductStockForm from "../components/dashboard/product/ProductStockForm";
import ProductVariant from "../components/dashboard/product/ProductVariant";
import Subscriptions from "../components/dashboard/reports/Subscriptions";
import Revenue from "../components/dashboard/reports/Revenue";
import Vendor from "../components/dashboard/vendor/Vendor";
import VendorEdit from "../components/dashboard/vendor/VendorEdit";
import VendorCreate from "../components/dashboard/vendor/VendorCreate";
import Warehouse from "../components/dashboard/warehouse/Warehouse";
import WarehouseEdit from "../components/dashboard/warehouse/WarehouseEdit";
import WarehouseCreate from "../components/dashboard/warehouse/WarehouseCreate";
import WarehouseItems from "../components/dashboard/warehouse/WarehouseItems";
import Wishlist from "../components/dashboard/wishlist/Wishlist";
import ProductBrand from "../components/dashboard/product/ProductBrand";
import ProductType from "../components/dashboard/product/ProductType";
import Banner from "../components/dashboard/banner/Banner";
import Plan from "../components/dashboard/plan/Plan";
import PlanCreate from "../components/dashboard/plan/PlanCreate";
import PlanEdit from "../components/dashboard/plan/PlanEdit";
import User from "../components/dashboard/user/User";
import UserInfo from "../components/dashboard/user/UserInfo";
import UserAddress from "../components/dashboard/user/UserAddress";
import UserOrder from "../components/dashboard/user/UserOrder";
import UserPayment from "../components/dashboard/user/UserPayment";
import OrderShow from "../components/dashboard/order/OrderShow";
import Coupon from "../components/dashboard/coupon/Coupon";
import Timeslot from "../components/dashboard/timeslot/Timeslot";
import Holiday from "../components/dashboard/holiday/Holiday";
import Category from "../components/dashboard/category/Category";
import Pick from "../components/dashboard/pick/Pick";
import PickEdit from "../components/dashboard/pick/PickEdit";

Vue.use(VueRouter);

const routes = [
    { path: "/", component: Dashboard },
    {
        path: "/member",
        component: AffiliateMember,
        meta: {
            permission: "affiliate.member.view"
        }
    },
    { path: "/profile", component: UserProfile },
    {
        path: "/account",
        component: Account,
        meta: {
            permission: "account.view"
        }
    },
    {
        path: "/account/create",
        component: AccountCreate,
        meta: {
            permission: "account.create"
        }
    },
    {
        path: "/account/edit/:id",
        component: AccountEdit,
        props: true,
        meta: {
            permission: "account.edit"
        }
    },
    {
        path: "/product",
        component: Product,
        meta: {
            permission: "product.view"
        }
    },
    {
        path: "/product/create",
        component: ProductCreate,
        meta: {
            permission: "product.create"
        }
    },
    {
        path: "/variant",
        name: "productVariant",
        component: ProductVariant,
        meta: {
            permission: "product.edit"
        }
    },
    {
        path: "/subscriptions",
        name: "Subscriptions",
        component: Subscriptions,
        meta: {
            permission: "product.view"
        }
    },
    {
        path: "/revenue",
        name: "Revenue",
        component: Revenue,
        meta: {
            permission: "product.view"
        }
    },
    {
        path: "/vendor",
        name: "Vendor",
        component: Vendor,
        meta: {
            permission: "product.view"
        }
    },
    {
        path: "/vendor/create",
        component: VendorCreate,
        meta: {
            permission: "product.create"
        }
    },
    {
        path: "/vendor/edit/:id",
        name: "vendorEdit",
        component: VendorEdit,
        props: true,
        meta: {
            permission: "product.edit"
        }
    },
    {
        path: "/warehouse",
        name: "Warehouse",
        component: Warehouse,
        meta: {
            permission: "product.view"
        }
    },
    {
        path: "/warehouse/edit/:id",
        name: "warehouseEdit",
        component: WarehouseEdit,
        props: true,
        meta: {
            permission: "product.edit"
        }
    },
    {
        path: "/warehouse/create",
        component: WarehouseCreate,
        meta: {
            permission: "product.create"
        }
    },
    {
        path: "/items",
        name: "WarehouseItems",
        component: WarehouseItems,
        meta: {
            permission: "product.view"
        }
    },
    {
        path: "/wishlist",
        name: "Wishlist",
        component: Wishlist,
        meta: {
            permission: "product.view"
        }
    },
    {
        path: "/brand",
        name: "productBrand",
        component: ProductBrand,
        meta: {
            permission: "product.edit"
        }
    },
    {
        path: "/category",
        name: "category",
        component: Category,
        meta: {
            permission: "product.edit"
        }
    },
    {
        path: "/collection",
        name: "productType",
        component: ProductType,
        meta: {
            permission: "product.edit"
        }
    },
    {
        path: "/product/edit/:id",
        name: "productEdit",
        component: ProductEdit,
        props: true,
        meta: {
            permission: "product.edit"
        }
    },
    {
        path: "/product/stock/:id",
        name: "productStock",
        component: ProductStockForm,
        props: true,
        meta: {
            permission: "product.view"
        }
    },
    {
        path: "/order",
        component: Order,
        meta: {
            permission: 'order.view'
        }
    },
    {
        path: "/order/:id",
        component: OrderShow,
        meta: {
            permission: 'order.view'
        },
        props: true
    },
    {
        path: "/payout",
        component: Payout,
        meta: {
            permission: 'payout.view'
        }
    },
    {
        path: "/banner",
        component: Banner,
        meta: {
            permission: "banner.create"
        }
    },
    {
        path: "/setting",
        component: Setting,
        meta: {
            permission: "system.editor"
        }
    },
    {
        path: "/change/password",
        component: ChangePasswordForm,
        meta: {
            permission: "dashboard.access"
        }
    },
    {
        path: "/plans",
        component: Plan,
        meta: {
            permission: "plan.view"
        }
    },
    {
        path: "/plans/create",
        component: PlanCreate,
        meta: {
            permission: "plan.create"
        }
    },
    {
        path: "/plans/edit/:id",
        component: PlanEdit,
        props: true,
        meta: {
            permission: "plan.create"
        }
    },
    {
        path: "/user/:id",
        component: User,
        props: true,
        children: [
            { path: 'info', component: UserInfo, props: true },
            { path: 'address', component: UserAddress, props: true },
            { path: 'order', component: UserOrder, props: true },
            { path: 'payment', component: UserPayment, props: true },
        ]
    },
    {
        path: '/coupons',
        component: Coupon,
        meta: {
            permission: 'system.editor'
        }
    },
    {
        path: '/timeslots',
        component: Timeslot,
        meta: {
            permission: 'system.editor'
        }
    },
    {
        path: '/holidays',
        component: Holiday,
        meta: {
            permission: 'system.editor'
        }
    },
    {
        path: "/pick",
        component: Pick,
        meta: {
            permission: "product.view"
        }
    },
    {
        path: "/pick/edit/:id",
        name: "pickEdit",
        component: PickEdit,
        props: true,
        meta: {
            permission: "product.create"
        }
    },
];

const router = new VueRouter({
    routes: routes
});

/** Global permissions based middleware */
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => !!record.meta.role)) {

        let is = store.getters['auth/is'](to.meta.role);

        is ? next() : next(false)
    }
    else if (to.matched.some(record => !!record.meta.permission)) {

        let can = store.getters['auth/can'](to.meta.permission);

        can ? next() : next(false)
    }
    else {
        next()
    }
});

export default router;
