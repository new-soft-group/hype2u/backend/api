import { Bar, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins;
import numeral from 'numeral';

export default {
    extends: Bar,
    mixins: [reactiveProp],
    data: () => ({
        options: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            scales: {
                xAxes: [{
                    offset: true,
                    stacked: true,
                    display: true,
                    gridLines: {
                        display: false,
                        drawBorder: false,
                    },
                    type: 'time',
                    distribution: 'series',
                    ticks: {
                        fontFamily: 'Roboto,sans-serif',
                        padding: 10,
                        fontColor: '#525f7f'
                    },
                    barPercentage: 0.1,
                }],
                yAxes: [{
                    stacked: true,
                    display: true,
                    gridLines: {
                        display: true,
                        drawBorder: false,
                        drawTicks: false,
                        borderDash: [3],
                        zeroLineWidth: 0
                    },
                    ticks: {
                        fontFamily: 'Roboto,sans-serif',
                        padding: 10,
                        fontColor: '#525f7f'
                    },
                }]
            }
        }
    }),
    mounted () {
        // this.chartData is created in the mixin.
        // If you want to pass options please create a local options object
        this.renderChart(this.chartData, this.options)
    }
}
