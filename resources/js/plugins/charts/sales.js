import { Line, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins;
import numeral from 'numeral';

export default {
    extends: Line,
    mixins: [reactiveProp],
    props: {
        options: {
            type: Object,
            default: () => ({})
        }
    },
    data: () => ({
        mergedOptions: {},
        defaultOptions: {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            elements: {
                point: {
                    radius: 6
                }
            },
            scales: {
                xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    },
                    type: 'time',
                    distribution: 'series',
                    ticks: {
                        fontFamily: 'Roboto,sans-serif',
                        padding: 10,
                        fontColor: '#a4a6a8'
                    }
                }],
                yAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    },
                    ticks: {
                        beginAtZero: false,
                        fontFamily: 'Roboto,sans-serif',
                        padding: 10,
                        fontColor: '#a4a6a8',
                        callback: (label) => {
                            return 'MYR' + numeral(label).format('(0.00a)');
                        }
                    },
                }]
            }
        }
    }),

    mounted () {
        this.options = _.merge(this.options, this.defaultOptions);

        this.renderChart(this.chartData, this.options);
    }
}
