import Vue from 'vue'
import Vuex from 'vuex'
import { auth } from './module/auth'
import { snackbar } from './module/snackbar'
import { locale } from './module/locale'
import { orderCount } from './module/orderCount'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
		auth: auth,
        snackbar: snackbar,
		locale: locale,
        orderCount: orderCount
    }
})
