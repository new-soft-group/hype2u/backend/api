export const auth = {
    namespaced: true,

    state: () => ({
        user: null,
        isAdmin: false,
        profile: null,
        roles: [],
    }),

    getters: {
        getUser(state) {
            return state.user;
        },

        getRoles(state) {
            return state.roles.length
                ? state.roles
                : JSON.parse(sessionStorage.getItem('roles'));
        },

        can: (state, getters) => (names) => {
            const roles = getters.getRoles;

            return roles.some(r => {
                if(names instanceof Array) {
                    return names.some(name => {
                        const result = r.permissions.filter(p => {
                            return p.name === name
                        });

                        return result.length > 0;
                    });
                }
                else {
                    const result = r.permissions.filter(p => {
                        return p.name === names
                    });

                    return result.length > 0;
                }
            });
        },

        is: (state, getters) => (names) => {
            const roles = getters.getRoles;

            if(names instanceof Array) {
                return names.some(name => {
                    return roles.some(r => r.name === name);
                });
            }
            else {
                return roles.some(r => r.name === names);
            }
        }
    },

    mutations: {
        setUser(state, user) {
            sessionStorage.removeItem('roles');
            sessionStorage.setItem('roles', JSON.stringify(user.roles));
            state.user = user;
            state.profile = user.profile;
            state.roles = user.roles;
            state.isAdmin = user.roles.name === 'admin';
        },

        setProfile(state, profile) {
            state.profile = profile;
        },

        clearUser(state) {
            state.user = null;
            sessionStorage.removeItem('role');
        }
    },

    actions: {
        updateProfile({ commit }, payload) {
            return new Promise((resolve, reject) => {
                axios.post('api/user', payload)
                    .then(({ data }) => {
                        commit('setProfile', payload);
                        resolve(data)
                    })
                    .catch(error => {
                        reject(error.response.data)
                    })
            });
        },
    }
};
