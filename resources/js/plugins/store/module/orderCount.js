export const orderCount = {
    namespaced: true,

    state: () => ({
        message: ''
    }),

    getters: {
        getMessage(state) {
            return state.message;
        },
    },

    mutations: {
        count(state, message) {
            state.message = message;
        },
    }
};
