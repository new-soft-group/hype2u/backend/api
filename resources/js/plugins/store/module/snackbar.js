export const snackbar = {
    namespaced: true,

    state: () => ({
        show: false,
        color: null,
        timeout: 6000,
        message: ''
    }),

    getters: {
        getShow(state) {
            return state.show;
        },
        getColor(state) {
            return state.color;
        },
        getTimeout(state) {
            return state.timeout;
        },
        getMessage(state) {
            return state.message;
        },
    },

    mutations: {
        setShow(state, show) {
            state.show = show
        },

        dismiss(state) {
            state.show = false
        },

        success(state, message) {
            state.color = 'success';
            state.message = message;
            state.show = true;
        },

        error(state, message) {
            state.color = 'error';
            state.message = message;
            state.show = true;
        },
    }
};
