import i18n from "../../i18n";

export const locale = {
    namespaced: true,

    state: () => ({
        locale: 'en',
    }),

    getters: {
        getLocale(state) {
            return state.locale;
        }
    },

    mutations: {
        setLocale(state, locale) {
            localStorage.setItem('locale', locale);
            window.axios.defaults.headers.common['X-Locale'] = locale;
            i18n.locale = locale;
            state.locale = locale;
        }
    },

    actions: {
        initLocale({ commit }) {
            const locale = localStorage.getItem('locale') || 'en';
            commit('setLocale', locale);
        }
    }
};
