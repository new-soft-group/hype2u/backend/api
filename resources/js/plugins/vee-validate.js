import { extend, configure } from 'vee-validate'
import { parsePhoneNumberFromString } from 'libphonenumber-js';
import i18n from './i18n'
import moment from 'moment'
import {
    required,
    required_if,
    email,
    between,
    integer,
    numeric,
    min,
    max,
    min_value,
} from 'vee-validate/dist/rules';

configure({
    defaultMessage: (field, values) => {
        if(values._rule_ === 'decimal') {
            values.places = values.places ?? 2;
            values._field_ = i18n.t(`${field}`);
            return i18n.t(`validation.${values._rule_}`, values);
        }

        values._field_ = i18n.te(`${field}`) ? i18n.t(`${field}`) : field;
        return i18n.t(`validation.${values._rule_}`, values);
    }
});

extend('required_if', {
    ...required_if
});

extend('max', {
    ...max
});

extend('min', {
    ...min
});

extend('numeric', {
    ...numeric
});

extend('integer', {
    ...integer
});

extend('email', {
    ...email
});

extend('between', {
    ...between
});

extend('min_value', {
    ...min_value
});


extend('dimensions', {
    params: ['width', 'height'],
    validate(files, { width, height }) {
        const validateImage = (file, width, height) => {
            const URL = window.URL || window.webkitURL;
            return new Promise(resolve => {
                const image = new Image();
                image.onerror = () => resolve(false);
                image.onload = () => resolve(image.width >= Number(width) && image.height >= Number(height));
                image.src = URL.createObjectURL(file);
            });
        };

        const list = [];
        for (let i = 0; i < files.length; i++) {
            // if file is not an image, reject.
            if (! /\.(jpg|svg|jpeg|png|bmp|gif)$/i.test(files[i].name)) {
                return false;
            }

            list.push(files[i]);
        }

        return Promise.all(list.map(file => validateImage(file, width, height)))
            .then(value => {
                return { valid: value.reduce((a, v) => a && v) };
            });
    }
});

extend('required', {
    validate(value) {
        let result = {
            valid: false,
            required: true
        };

        if(typeof value === 'string') {
            result.valid = !!String(value).trim().length;
        }

        else if(typeof value === 'number') {
            result.valid = value > 0;
        }

        else if(value instanceof Array) {
            result.valid = !!value.length;
        }

        else if(value instanceof Object) {
            result.valid = !_.isEmpty(value);
        }

        return result;
    },
    computesRequired: true,
});

extend('password', {
    params: ['target'],
    validate(value, { target }) {
        return value === target;
    }
});

extend('mobile', {
    validate(value) {
        let result = {
            valid: false
        };

        if(value) {
            const phone = parsePhoneNumberFromString(value);
            result.valid = phone ? phone.isPossible() : false;
        }

        return result;
    },
});

extend('decimal', {
    params: ['places'],
    validate(value, { places }) {
        let result = {
            valid: false
        };

        const decimalPlaces = places ? Number(places) : 2;

        let regex = new RegExp('^\\d+(\\.\\d{0,' + decimalPlaces + '})?$');

        result.valid = regex.test(String(value));

        return result;
    },
});

extend('unique-email', {
    validate(value) {
        return new Promise((resolve, reject) => {
            axios.post('api/unique/email', {email: value})
                .then(({ data }) => {
                    resolve(data);
                })
                .catch(() => {
                    reject({ valid: false })
                })
        })
    }
});

extend('gt', {
    params: ['target'],
    validate(value, { target }) {
        let result = {
            valid: false
        };

        result.valid = Number(value) > Number(target);

        return result;
    },
});

extend('future', {
    validate(value) {
        let result = {
            valid: false
        };

        result.valid = moment(value).isAfter();

        return result;
    },
});
