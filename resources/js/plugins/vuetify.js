import Vue from 'vue';
import Vuetify from 'vuetify';

Vue.use(Vuetify);

export default new Vuetify({
    icons: {
        iconfont: 'mdi',
    },
    theme: {
        options: {
            customProperties: true
        },
        light: true,
        themes: {
            light: {
                primary: '#5e72e4',
                secondary: '#f7fafc',
                error: '#f5365c',
                info: '#11cdef',
                success: '#2dce89',
                warning: '#fb6340',
            }
        }
    }
});
