import { mapGetters } from 'vuex'

export default {
    computed: {
        // mix the getters into computed with object spread operator
        ...mapGetters({
            can: 'auth/can',
            is: 'auth/is',
        })
    }
}
