export default {
    PENDING: 1,
    COMPLETED: 2,
    REFUND: 3,
    CANCELLED: 4,
    DELIVERED: 5,
    FAILED: 6,
    REJECTED: 7,
    PARTIAL_RETURNED: 8,
    APPROVED: 9,
    SHIPPING : 10
}
