export default {
    IN_STOCK: 1,
    OUT_OF_STOCK: 2,
    DRY_CLEANING: 3,
    RESERVED: 4,
    SHIPPED_OUT: 5,
    DAMAGED: 6,
    LOST: 7
}
