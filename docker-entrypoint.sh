#!/bin/sh
cd /app \
    && composer dump-autoload \
    && php artisan config:clear \
    && php artisan cache:clear \
    && php artisan storage:link \
    && php artisan queue:restart \
    && supervisord \
    && php artisan schedule:work
