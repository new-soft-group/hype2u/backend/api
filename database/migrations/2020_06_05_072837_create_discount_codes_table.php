<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDiscountCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('discount_codes', function (Blueprint $table) {
    //         $table->id();
    //         $table->morphs('discountable');
    //         $table->string('code');
    //         $table->foreignId('discount_type_id');
    //         $table->foreignId('discount_method_id');
    //         $table->unsignedDecimal('discount')->default(0);
    //         $table->timestamp('expired_at')->nullable();
    //         $table->timestamps();
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('discount_codes');
    }
}
