<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePaymentSummariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payment_summaries', function (Blueprint $table) {
            $table->id();
            $table->string('charge_id');
            $table->string('invoice_id')->nullable();
            $table->string('payment_intent')->nullable();
            $table->decimal('amount');
            $table->decimal('amount_refunded')->nullable();
            $table->decimal('application_fee')->nullable();
            $table->decimal('application_fee_amount')->nullable();
            $table->decimal('dispute')->nullable();
            $table->boolean('disputed')->default(false);
            $table->string('type')->nullable();
            $table->string('failure_code')->nullable();
            $table->string('failure_message')->nullable();
            $table->string('customer_stripe_id');
            $table->string('status')->nullable();
            $table->string('currency')->nullable();
            $table->unsignedInteger('payment_date')->nullable();
            $table->boolean('paid')->default(false);
            $table->foreignId('user_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payment_summaries');
    }
}
