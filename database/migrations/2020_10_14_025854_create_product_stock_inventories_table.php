<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductStockInventoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_stock_inventories', function (Blueprint $table) {
            $table->id();
            $table->string('tag');
            $table->foreignId('warehouse_id')->constrained();
            $table->foreignId('product_stock_id')->constrained();
            $table->foreignId('stock_status_id');
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_stock_inventories');
    }
}
