<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('stripe_plan');
            $table->string('stripe_product');
            $table->unsignedDecimal('amount');
            $table->unsignedDecimal('purchase_amount');
            $table->string('interval');
            $table->string('slug');
            $table->longText('description');
            $table->integer('exchange_limit');
            $table->unsignedInteger('sort');
            $table->boolean('enabled')->default(true);
            $table->boolean('sms')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
