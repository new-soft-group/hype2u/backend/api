<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVendorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vendors', function (Blueprint $table) {
            $table->id();
            $table->string('company_name',255);
            $table->string('contact_name',45)->nullable();
            $table->string('contact_title',45)->nullable();
            $table->string('address1',255);
            $table->string('address2',255)->nullable();
            $table->string('city',45);
            $table->string('country',45);
            $table->string('phone',15)->nullable();
            $table->string('fax',15)->nullable();
            $table->string('email',45)->nullable();
            $table->string('site_url',200)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vendors');
    }
}
