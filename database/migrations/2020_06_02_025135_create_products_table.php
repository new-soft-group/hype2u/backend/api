<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->text('description');
            $table->string('sku');
            $table->string('slug');
            $table->foreignId('gender_id');
            $table->foreignId('brand_id');
            $table->foreignId('product_type_id');
            $table->unsignedDecimal('price');
            $table->unsignedDecimal('cost_price');
            $table->boolean('featured')->default(false);
            $table->boolean('enabled')->default(true);
            $table->timestamp('available_on')->nullable();
            $table->boolean('most_popular')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
