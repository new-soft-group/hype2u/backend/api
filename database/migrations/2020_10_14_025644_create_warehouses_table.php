<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWarehousesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('warehouses', function (Blueprint $table) {
            $table->id();
            $table->string('country', 45);
            $table->string('name', 200);
            $table->string('slug', 255);
            $table->string('address1', 200);
            $table->string('address2', 200)->nullable();
            $table->string('city', 45);
            $table->string('phone', 15)->nullable();
            $table->string('fax', 15)->nullable();
            $table->tinyInteger('default')->default('0');
            $table->boolean('enabled')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('warehouses');
    }
}
