<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    // public function up()
    // {
    //     Schema::create('transactions', function (Blueprint $table) {
    //         $table->uuid('id')->primary();
    //         $table->foreignId('order_id');
    //         $table->foreignId('transaction_status_id');
    //         $table->unsignedDecimal('amount');
    //         $table->string('currency');
    //         $table->unsignedDecimal('currency_rate');
    //         $table->timestamps();
    //     });
    // }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
