<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Permission;
use Spatie\Permission\PermissionRegistrar;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        /** Banner Permission */
        Permission::create(['name' => 'banner.create']);
        Permission::create(['name' => 'banner.edit']);
        Permission::create(['name' => 'banner.delete']);

        /** User CRUD */
        Permission::create(['name' => 'profile.view']);
        Permission::create(['name' => 'profile.create']);
        Permission::create(['name' => 'profile.edit']);
        Permission::create(['name' => 'profile.delete']);

        /** Dashboard access */
        Permission::create(['name' => 'dashboard.access']);

        /** Account CRUD */
        Permission::create(['name' => 'account.view']);
        Permission::create(['name' => 'account.create']);
        Permission::create(['name' => 'account.edit']);
        Permission::create(['name' => 'account.delete']);

        /** Product CRUD */
        Permission::create(['name' => 'product.view']);
        Permission::create(['name' => 'product.create']);
        Permission::create(['name' => 'product.edit']);
        Permission::create(['name' => 'product.delete']);

        /** Category CRUD */
        Permission::create(['name' => 'category.view']);
        Permission::create(['name' => 'category.create']);
        Permission::create(['name' => 'category.edit']);
        Permission::create(['name' => 'category.delete']);

        /** Order CRUD */
        Permission::create(['name' => 'order.editor']);
        Permission::create(['name' => 'order.view']);
        Permission::create(['name' => 'order.create']);
        Permission::create(['name' => 'order.edit']);
        Permission::create(['name' => 'order.delete']);

        /** Plan CRUD */
        Permission::create(['name' => 'plan.view']);
        Permission::create(['name' => 'plan.create']);
        Permission::create(['name' => 'plan.edit']);
        Permission::create(['name' => 'plan.delete']);

        /** Affiliate CRUD */
        Permission::create(['name' => 'affiliate.view']);
        Permission::create(['name' => 'affiliate.superior.view']);
        Permission::create(['name' => 'affiliate.member.view']);
        Permission::create(['name' => 'affiliate.sales']);
        Permission::create(['name' => 'affiliate.member.sales']);
        Permission::create(['name' => 'affiliate.create']);
        Permission::create(['name' => 'affiliate.edit']);
        Permission::create(['name' => 'affiliate.edit.code']);
        Permission::create(['name' => 'affiliate.delete']);

        /** Sales Chart */
        Permission::create(['name' => 'sales.view']);

        Permission::create(['name' => 'payout.view']);
        Permission::create(['name' => 'payout.edit']);

        /** Wishlist Permission */
        Permission::create(['name' => 'wishlist.view']);
        Permission::create(['name' => 'wishlist.create']);
        Permission::create(['name' => 'wishlist.edit']);
        Permission::create(['name' => 'wishlist.delete']);

        /** Cart Permission */
        Permission::create(['name' => 'cart.view']);
        Permission::create(['name' => 'cart.create']);
        Permission::create(['name' => 'cart.edit']);
        Permission::create(['name' => 'cart.delete']);

        /** Payment Methods Permission */
        Permission::create(['name' => 'payment.methods.remove']);

        /** Subscription Permission */
        Permission::create(['name' => 'subscription.create']);
        Permission::create(['name' => 'subscription.edit']);
        Permission::create(['name' => 'subscription.cancel']);

        Permission::create(['name' => 'payment.checkout']);

        /** Master permission for system update */
        Permission::create(['name' => 'system.editor']);

        /** @var Role $role */
        $role = Role::create(['name' => Role::ADMIN]);
        $role->givePermissionTo(Permission::all());

        $role = Role::create(['name' => Role::USER]);
        $role->givePermissionTo([
            'profile.view',
            'profile.create',
            'profile.edit',
            'profile.delete',
            'wishlist.view',
            'wishlist.create',
            'wishlist.edit',
            'wishlist.delete',
            'cart.view',
            'cart.create',
            'cart.edit',
            'cart.delete',
            'order.view',
            'subscription.create',
            'subscription.edit',
            'plan.view',
            'payment.checkout'
        ]);
    }
}
