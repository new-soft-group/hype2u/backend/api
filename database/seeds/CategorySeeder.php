<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::insert($this->categories());
    }

    public function categories()
    {
        return [
            ['name' => 'Hoodie' ],
            ['name' => 'Shirt' ],
            ['name' => 'T-shirt' ],
            ['name' => 'Long Sleeve' ],
            ['name' => 'Jacket' ],
            ['name' => 'Windbreaker' ],
            ['name' => 'Crewneck' ],
            ['name' => 'Cap' ],
            ['name' => 'Pants' ],
            ['name' => 'Dress' ],
            ['name' => 'Crop Top' ],
            ['name' => 'Others' ],
        ];
    }
}
