<?php

namespace Database\Seeders;

use App\Models\Variant;
use Illuminate\Database\Seeder;

class VariantSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (($sizesCsv = fopen('public/csv/sizes.csv', 'r')) === false) {
            die('Error opening file');
        }

        if (($colorsCsv = fopen('public/csv/colors.csv', 'r')) === false) {
            die('Error opening file');
        }

        $sizesHeaders = fgetcsv($sizesCsv, 4096, ',');
        $colorsHeaders = fgetcsv($colorsCsv, 4096, ',');

        $sizesRecords = array();
        $colorsRecords = array();

        while ($sizesRow = fgetcsv($sizesCsv, 4096, ',')) {
            $sizesRecords[] = array_combine($sizesHeaders, $sizesRow);
        }

        while ($colorsRow = fgetcsv($colorsCsv, 4096, ',')) {
            $colorsRecords[] = array_combine($colorsHeaders, $colorsRow);
        }

        fclose($sizesCsv);
        fclose($colorsCsv);

        $sizesOptions = [];
        $colorsOptions = [];

        foreach ($sizesRecords as $rec) {
            $sizeRecord = [];
            foreach ($rec as $key => $test){
                $sizeRecord[trim($key, '﻿')] = trim($test);
            }
            $sizeRecord['removable'] = false;
            array_push($sizesOptions, $sizeRecord);
        }

        foreach ($colorsRecords as $rec) {
            $colorRecord=[];
            foreach ($rec as $key => $test){
                $colorRecord[trim($key, '﻿')] = trim($test);
            }
            array_push($colorsOptions, $colorRecord);
        }

        Variant::reguard();

        Variant::createMany([
            [
                'name' => 'Sizes',
                'removable' => false,
                'options' => $sizesOptions
            ],
            [
                'name' => 'Colors',
                'options' => $colorsOptions
            ]
        ]);
    }
}
