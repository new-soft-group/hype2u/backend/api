<?php

namespace Database\Seeders;

use App\Models\Vendor;
use Illuminate\Database\Seeder;

class VendorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Vendor::insert($this->vendor());
    }

    public function vendor()
    {
        return [
            [
                'company_name' => 'HYPE2U',
//                'contact_name' => 'Bob',
//                'contact_title' => 'Hypebeast Manager',
                'address1' => 'A',
//                'address2' => '58200 Kuala Lumpur',
                'city' => 'A',
                'country' => 'Malaysia',
//                'phone' => '+60120122123',
//                'fax' => '+60380122123',
//                'email' => 'bob@gmail.com',
//                'site_url' => 'https://hypebeast.com/'
            ]
        ];
    }
}
