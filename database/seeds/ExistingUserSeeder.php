<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use App\Models\VariantOption;
use Carbon\Carbon;
use Database\Factories\UserFactory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Arr;

class ExistingUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (($handle = fopen('public/csv/customers.csv', 'r')) === false) {
            die('Error opening file');
        }

        $headers = fgetcsv($handle, 4096, ',');
        $records = array();

        while ($row = fgetcsv($handle, 4096, ',')) {
            $records[] = array_combine($headers, $row);
        }
        fclose($handle);

        foreach ($records as $rec) {
            $record = [];
            foreach ($rec as $key => $test){
                $record[trim($key)] = trim($test);
            }
            $userData = \Arr::only($record, ['email', 'email_verified_at', 'password']);
            $profileData = \Arr::only($record, ['first_name', 'last_name', 'mobile', 'dob', 'gender_id']);
            $addressData = \Arr::only($record, ['address_first_name', 'address_last_name', 'address_mobile', 'company', 'address_1', 'address_2', 'city', 'postcode']);

            $userData = $this->stripEmptyCustom($userData);
            $profileData = $this->stripEmptyCustom($profileData);
            $addressData = $this->stripEmptyCustom($addressData);


            $addressData = $this->replaceKeys('address_first_name', 'first_name', $addressData);
            $addressData = $this->replaceKeys('address_last_name', 'last_name', $addressData);
            $addressData = $this->replaceKeys('address_mobile', 'mobile', $addressData);

            /** Get Size Id */
            if($record['size'] != "NULL"){
                $sizeId = VariantOption::firstWhere('name',$record['size']);
                $profileData['size_id'] = $sizeId->id;
            }

            /** Get Mobile Number and work validation */
            if( array_key_exists('mobile',$profileData) && $profileData['mobile'] != "NULL"){
                $mobilefirstNumber = substr($profileData['mobile'],0,1);
                if(!str_contains($profileData['mobile'],'+60') && $mobilefirstNumber == "1"){
                    $profileData['mobile'] = "+60".$profileData['mobile'];
                }elseif (!str_contains($profileData['mobile'],'+60') && $mobilefirstNumber == "6"){
                    $profileData['mobile'] = "+".$profileData['mobile'];
                }elseif (!str_contains($profileData['mobile'],'+60') && $mobilefirstNumber == "0"){
                    $profileData['mobile'] = "+6".$profileData['mobile'];
                }
            }

            if( array_key_exists('mobile',$addressData) && $addressData['mobile'] != "NULL"){
                $mobilefirstNumber = substr($addressData['mobile'],0,1);
                if(!str_contains($addressData['mobile'],'+60') && $mobilefirstNumber == "1"){
                    $addressData['mobile'] = "+60".$addressData['mobile'];
                }elseif (!str_contains($addressData['mobile'],'+60') && $mobilefirstNumber == "6"){
                    $addressData['mobile'] = "+".$addressData['mobile'];
                }elseif (!str_contains($addressData['mobile'],'+60') && $mobilefirstNumber == "0"){
                    $addressData['mobile'] = "+6".$addressData['mobile'];
                }
            }

            if(!empty($addressData)){
                $addressData['state'] = $addressData['city'];
                $addressData['country'] = "Malaysia";
                $addressData['addressable_type'] = Profile::class;
                if(!Arr::has($addressData,'postcode')){
                    $addressData['postcode'] = "-";
                }

                if(User::where('email',$userData['email'])->count() == 0){
                    $user = User::create($userData);
                    $user->assignRole(Role::USER);
                    $user->profile()->create($profileData);
                    $user->profile->addresses()->create($addressData);
                };


            }else {
                if (!empty($profileData)) {
                    if(User::where('email',$userData['email'])->count() == 0){
                        $user = User::create($userData);
                        $user->assignRole(Role::USER);
                        $user->profile()->create($profileData);
                    }
                }else{
                    if(User::where('email',$userData['email'])->count() == 0) {
                        $user = User::create($userData);
                        $user->assignRole(Role::USER);
                    }

                }
            }


        }
    }

    public function stripEmptyCustom($data) {
        foreach ($data as $key => $value) {
            if (is_array($data[$key])){
                $data[$key] = stripEmptyCustom($data[$key]);
            }

            if ($value == "NULL"){
                unset($data[$key]);
            }
        }

        return $data;
    }

    public function replaceKeys($oldKey, $newKey, array $input){
        $return = array();
        foreach ($input as $key => $value) {
            if ($key===$oldKey)
                $key = $newKey;

            if (is_array($value))
                $value = $this->replaceKeys( $oldKey, $newKey, $value);

            $return[$key] = $value;
        }
        return $return;
    }
}
