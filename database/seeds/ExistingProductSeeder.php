<?php

namespace Database\Seeders;

use App\Models\Address;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\ProductVariant;
use App\Models\Profile;
use App\Models\Role;
use App\Models\StockStatus;
use App\Models\User;
use App\Models\Variant;
use App\Models\VariantOption;
use Illuminate\Database\Seeder;
use Illuminate\Support\Collection;

class ExistingProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (($productsCsv = fopen('public/csv/products_table.csv', 'r')) === false) {
            die('Error opening file');
        }

        if (($productStockCsv = fopen('public/csv/variants_table.csv', 'r')) === false) {
            die('Error opening file');
        }

        if (($stockInventoriesCsv = fopen('public/csv/tag_ids.csv', 'r')) === false) {
            die('Error opening file');
        }

        if (($imagesCsv = fopen('public/csv/images_table.csv', 'r')) === false) {
            die('Error opening file');
        }

        $productsHeaders = fgetcsv($productsCsv, 4096, ',');
        $productStockHeaders = fgetcsv($productStockCsv, 4096, ',');
        $stockInventoriesHeaders = fgetcsv($stockInventoriesCsv, 4096, ',');
        $imagesHeaders = fgetcsv($imagesCsv, 4096, ',');

        $productsRecords = array();
        $productStockRecords = array();
        $stockInventoriesRecords = array();
        $imagesRecords = array();

        while ($row = fgetcsv($productsCsv, 4096, ',')) {
            $productsRecords[] = array_combine($productsHeaders, $row);
        }

        while ($row = fgetcsv($productStockCsv, 4096, ',')) {
            $productStockRecords[] = array_combine($productStockHeaders, $row);
        }

        while ($row = fgetcsv($stockInventoriesCsv, 4096, ',')) {
            $stockInventoriesRecords[] = array_combine($stockInventoriesHeaders, $row);
        }

        while ($row = fgetcsv($imagesCsv, 4096, ',')) {
            $imagesRecords[] = array_combine($imagesHeaders, $row);
        }

        fclose($productsCsv);
        fclose($productStockCsv);
        fclose($stockInventoriesCsv);
        fclose($imagesCsv);

        $productStockCollection = new Collection();
        $stockInventoriesCollection = new Collection();
        $imagesCollection = new Collection();

        foreach ($productStockRecords as $rec) {
            $record = [];
            foreach ($rec as $key => $test){
                $record[trim($key)] = trim($test);
            }
            $productStockData = \Arr::only($record, [ 'id', 'product_id','sku', 'price','cost_price','size','color']);
            $productStockCollection->push($productStockData);
        }

        foreach ($stockInventoriesRecords as $rec) {
            $record = [];
            foreach ($rec as $key => $test){
                $record[trim($key)] = trim($test);
            }
            $inventoriesData = \Arr::only($record, ['value','variant_id']);
            $stockInventoriesCollection->push($inventoriesData);
        }

        foreach ($imagesRecords as $rec) {
            $record = [];
            foreach ($rec as $key => $test){
                $record[trim($key)] = trim($test);
            }
            $imagesData = \Arr::only($record, ['id','original','imageable_id','imageable_type']);
            $imagesCollection->push($imagesData);
        }



        foreach ($productsRecords as $rec) {
            $record = [];
            foreach ($rec as $key => $test){
                $record[trim($key)] = trim($test);
            }
            $productData = \Arr::only($record, ['name', 'description','slug','price','cost_price','most_popular']);
            $productData = $this->stripEmptyCustom($productData);

            $productType = ProductType::firstWhere('name', $record['product_type_name']);
            $productBrand = Brand::firstWhere('name', $record['brand']);
            $productCategories = Category::firstWhere('name', $record['categories_name']);

//            /** if product type not found, ignore the record */
//            if(!$productType){
//                continue;
//            }
//
//            /** Check for duplicated product slug*/
//            $duplicatedSlug = Product::where('slug',$productData['slug']);
//            if($duplicatedSlug->count() > 0){
//                continue;
//            }

            /** if product categories not found assign it to other */
            if(!$productCategories){
                $productCategories = Category::firstWhere('name', 'Others');
            }

            /** check if most popular is null then unset*/

            $productData['brand_id'] = $productBrand->id;
            $productData['gender_id'] = 1;
            $productData['product_type_id'] = $productType->id;

            $product = Product::create($productData);
            $product->assignCategories($productCategories->id);

            /** Product Stock */
            $productStock = $productStockCollection->where('product_id',$record['id']);

            /** Variant and option creation */
            $sizeId = Variant::firstWhere('name','Sizes')->id;
            $colorId = Variant::firstWhere('name','Colors')->id;

            $sizeOptionNames = $productStock->pluck('size');
            $colorOptionNames = $productStock->pluck('color');

            $sizeOptionIds = VariantOption::whereIn('name' , $sizeOptionNames)->pluck('id');
            $colorOptionIds = VariantOption::whereIn('name' , $colorOptionNames)->pluck('id');

            if(count($sizeOptionIds)>0) {
                $sizeVariant = $product->variants()->make();
                $sizeVariant->lookup()->associate($sizeId)->save();
                $sizeVariant->pivotOptions()->sync($sizeOptionIds);
            }

            if(count($colorOptionIds)>0){
                $colorVariant = $product->variants()->make();
                $colorVariant->lookup()->associate($colorId)->save();
                $colorVariant->pivotOptions()->sync($colorOptionIds);
            }

            $productStockImages = new Collection();


                /** Create stock with combination */
            foreach ($productStock as $stock){
                $stockData = \Arr::only($stock, ['sku', 'price','cost_price']);

                $stockInstance = $product->stocks()->make($stockData);
                $stockInstance->vendor()->associate(1);
                $stockInstance->save();

                $combinationIds= [];

                if($stock['size']){
                   $productSizeVariants = $product->variants->where('variant_id', $sizeId);
                    $currentStockSizeId =  VariantOption::firstWhere('name' , $stock['size'])->id;

                    foreach ($productSizeVariants as $sizeVariant){
                        $sizeCombinationId = $sizeVariant->options->where('variant_option_id', $currentStockSizeId)->pluck('id');
                        array_push($combinationIds , $sizeCombinationId->first());
                    }
                }

                if($stock['color']){
                    $productColorVariants = $product->variants->where('variant_id', $colorId);
                    $currentStockColorId = VariantOption::firstWhere('name' , $stock['color'])->id;

                    foreach ($productColorVariants as $colorVariant){
                        $sizeCombinationId = $colorVariant->options->where('variant_option_id', $currentStockColorId)->pluck('id');
                        array_push($combinationIds , $sizeCombinationId->first());
                    }
                }

                $stockInstance->combinations()->sync($combinationIds);

                /** Import tags for stock */
                $tags = $stockInventoriesCollection->where('variant_id' , $stock['id'])->pluck('value')->toArray();
                if(count($tags)>0){
                    $stockInstance->restocks($tags,1,StockStatus::IN_STOCK);
                }


            }

            /** get first product stock images */
            if($productStock->count()){
                $firstProductStockImages = $imagesCollection->where('imageable_id', $productStock->first()['id'])->where('imageable_type','App\Models\Variant');
                foreach ($firstProductStockImages as $image){
                    $productStockImages->push($image);
                }
            }

            /** Save image */
            foreach ($productStockImages as $image){
                $s3FilePath = 'https://hype2u.s3-ap-southeast-1.amazonaws.com/';
                if(str_contains($image['original'] , $s3FilePath)){
                    $newS3filePath = str_replace($s3FilePath,"",$image['original']);

                    $productImage =$product->images()->make([
                       'disk' => 's3',
                       'path' => $newS3filePath,
                       'width' => 410,
                       'height' => 560,
                       'sort' => 0
                    ]);
                    $product->images()->save($productImage);
                }
            }
        }
    }

    public function stripEmptyCustom($data) {
        foreach ($data as $key => $value) {
            if (is_array($data[$key])){
                $data[$key] = stripEmptyCustom($data[$key]);
            }

            if ($value == "NULL"){
                unset($data[$key]);
            }
        }

        return $data;
    }
}
