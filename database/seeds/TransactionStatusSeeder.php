<?php

use App\Models\TransactionStatus;
use Illuminate\Database\Seeder;

class TransactionStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TransactionStatus::create(['id' => TransactionStatus::SUCCEED, 'name' => 'succeed']);
        TransactionStatus::create(['id' => TransactionStatus::FAILED, 'name' => 'failed']);
    }
}
