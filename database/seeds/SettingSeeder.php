<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Setting::create(['key' => 'currencies.rate.myr', 'value' => 1]);
        // Setting::create(['key' => 'currencies.rate.sgd', 'value' => 0.34]);
        // Setting::create(['key' => 'currencies.rate.usd', 'value' => 0.24]);
    }
}
