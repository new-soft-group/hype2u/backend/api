<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (($handle = fopen('public/csv/brands.csv', 'r')) === false) {
            die('Error opening file');
        }

        $headers = fgetcsv($handle, 4096, ',');
        $records = array();

        while ($row = fgetcsv($handle, 4096, ',')) {
            $records[] = array_combine($headers, $row);
        }
        fclose($handle);

        $brands = [];

        foreach ($records as $rec) {
            $record = [];
            foreach ($rec as $key => $test){
                $record[trim($key, '﻿')] = trim($test);
            }
            $record['enabled'] = true;
            array_push($brands , $record);
        }

        Brand::insert($brands);
    }
}
