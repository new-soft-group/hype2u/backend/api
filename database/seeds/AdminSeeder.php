<?php

namespace Database\Seeders;

use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Str;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if(app()->environment() === 'production') {
            $user = User::create([
                            'email' => 'admin@hype2u.com',
                            'password' => bcrypt('Hype2U@ssw0rd123'),
                            'email_verified_at' => now(),
                            'remember_token' => Str::random(10),
                        ]);

            $user->profile()->create([
                'first_name' => 'Admin',
                'last_name' => 'Admin',
                'mobile' => '+60121234567',
                'dob' => Carbon::parse('1997-01-01'),
                'gender_id' => '1',
                'size_id' => '1',
            ]);
        }
        else {
            $user = User::factory()
                        ->has(Profile::factory()->state([
                            'first_name' => 'Admin',
                            'last_name' => 'Admin',
                            'mobile' => '+60121234567',
                            'dob' => Carbon::parse('1997-01-01'),
                            'gender_id' => '1',
                            'size_id' => '1',
                        ]))
                        ->create([
                            'email' => 'admin@hype2u.com'
                        ]);
        }

        $user->assignRole(Role::ADMIN);
    }
}
