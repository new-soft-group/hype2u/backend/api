<?php

namespace Database\Seeders;

use App\Models\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::create(['id' => OrderStatus::PENDING, 'name' => 'pending']);
        OrderStatus::create(['id' => OrderStatus::REFUND, 'name' => 'refund']);
        OrderStatus::create(['id' => OrderStatus::CANCELLED, 'name' => 'cancelled']);
        OrderStatus::create(['id' => OrderStatus::DELIVERED, 'name' => 'delivered']);
        OrderStatus::create(['id' => OrderStatus::FAILED, 'name' => 'failed']);
        OrderStatus::create(['id' => OrderStatus::REJECTED, 'name' => 'rejected']);
        OrderStatus::create(['id' => OrderStatus::COMPLETED, 'name' => 'completed']);
        OrderStatus::create(['id' => OrderStatus::PARTIAL_RETURNED, 'name' => 'partial_returned']);
    }
}
