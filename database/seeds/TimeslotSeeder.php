<?php

namespace Database\Seeders;

use App\Models\TimeslotType;
use Illuminate\Database\Seeder;

class TimeslotSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        TimeslotType::insert([
            ['id' => TimeslotType::DELIVER, 'name' => 'deliver'],
            ['id' => TimeslotType::RETURN_BACK, 'name' => 'return']
        ]);
    }
}
