<?php

namespace Database\Seeders;

use App\Models\ProductType;
use Illuminate\Database\Seeder;

class ProductTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProductType::insert($this->types());
    }

    public function types()
    {
        return [
            ['name' => 'VIP' , 'enabled' => true ],
            ['name' => 'NEW ARRIVALS' , 'enabled' => true ],
            ['name' => 'PREMIUM' , 'enabled' => true ],
            ['name' => 'LUXURY' , 'enabled' => true ],
            ['name' => 'STREET' , 'enabled' => true ],
        ];
    }
}
