<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call([
             PermissionSeeder::class,
             OrderStatusSeeder::class,
             OrderStatusApproveShippingSeeder::class,
             StockStatusSeeder::class,
             SettingSeeder::class,
             AdminSeeder::class,
             CategorySeeder::class,
             ProductTypeSeeder::class,
             VendorSeeder::class,
             WarehouseSeeder::class,
             GenderSeeder::class,
             BrandSeeder::class,
             VariantSeeder::class,
             BannerPositionSeeder::class,
             TimeslotSeeder::class,
             BusinessTypeSeeder::class
         ]);
    }
}
