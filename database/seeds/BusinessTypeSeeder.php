<?php

namespace Database\Seeders;

use App\Models\BusinessType;
use Illuminate\Database\Seeder;

class BusinessTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        BusinessType::insert($this->types());
    }

    public function types()
    {
        return [
            ['name' => 'Rent' ],
            ['name' => 'Shop' ]
        ];
    }
}
