<?php

namespace Database\Seeders;

use App\Models\BannerPosition;
use Illuminate\Database\Seeder;

class BannerPositionSeeder extends Seeder
{
    private $seeds = [
        ['id' => 1, 'name' => 'Home Banner']
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->seeds as $seed) {
            $target = BannerPosition::find($seed['id']);

            if(is_null($target)) {
                BannerPosition::create($seed);
            }
            else {
                $target->update(['name' => $seed['name']]);
            }
        }
    }
}
