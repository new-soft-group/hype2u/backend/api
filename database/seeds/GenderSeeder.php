<?php

namespace Database\Seeders;

use App\Models\Gender;
use Illuminate\Database\Seeder;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $genders = [
            ['name' => 'Men'],
            ['name' => 'Women'],
            ['name' => 'Unisex']
        ];

        foreach ($genders as $gender) {
            Gender::make($gender)->setRemovable(false)->save();
        }
    }
}
