<?php

namespace Database\Seeders;

use App\Models\StockStatus;
use Illuminate\Database\Seeder;

class StockStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        StockStatus::create(['id' => StockStatus::IN_STOCK, 'name' => 'in_stock']);
        StockStatus::create(['id' => StockStatus::OUT_OF_STOCK, 'name' => 'out_of_stock']);
        StockStatus::create(['id' => StockStatus::DRY_CLEANING, 'name' => 'dry_cleaning']);
        StockStatus::create(['id' => StockStatus::RESERVED, 'name' => 'reserved']);
        StockStatus::create(['id' => StockStatus::SHIPPED_OUT, 'name' => 'shipped_out']);
        StockStatus::create(['id' => StockStatus::DAMAGED, 'name' => 'damaged']);
        StockStatus::create(['id' => StockStatus::LOST, 'name' => 'lost']);
    }
}
