<?php

namespace Database\Seeders;

use App\Models\Warehouse;
use Illuminate\Database\Seeder;

class WarehouseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Warehouse::insert($this->warehouses());
    }

    public function warehouses()
    {
        return [
            [
                'country' => 'Malaysia',
                'name' => 'HYPE2U',
                'slug' => 'hype2u-1',
                'address1' => '-',
                'address2' => '-',
                'city' => '-',
                'phone' => '+60123456789',
                'fax' => '+60123456789',
                'default' => 0,
                'enabled' => 1
            ]
        ];
    }
}
