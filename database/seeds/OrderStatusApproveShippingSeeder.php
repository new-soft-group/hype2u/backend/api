<?php

namespace Database\Seeders;

use App\Models\OrderStatus;
use Illuminate\Database\Seeder;

class OrderStatusApproveShippingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        OrderStatus::create(['id' => OrderStatus::APPROVED, 'name' => 'approved']);
        OrderStatus::create(['id' => OrderStatus::SHIPPING, 'name' => 'shipping']);
    }
}
