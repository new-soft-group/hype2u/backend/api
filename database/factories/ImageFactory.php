<?php

namespace Database\Factories;

use App\Models\Image;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Http\UploadedFile;
use Storage;

class ImageFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Image::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'disk' => 'public',
            'path' => function() {
                return Storage::disk('public')->put(
                    '/',
                    UploadedFile::fake()->image('image.png', 640, 480)
                );
            },
            'width' => 640,
            'height' => 480
        ];
    }

    /**
     * Define this image is used for product
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function product()
    {
        return $this->state(function (array $attributes) {
            return [
                'disk' => 'product',
                'path' => Storage::disk('product')->put(
                    '/',
                    UploadedFile::fake()->image('image.png', 640, 480)
                )
            ];
        });
    }

    /**
     * Define this image is used for banner
     *
     * @return \Illuminate\Database\Eloquent\Factories\Factory
     */
    public function banner()
    {
        return $this->state(function (array $attributes) {
            return [
                'disk' => 'banner',
                'path' => Storage::disk('banner')->put(
                    '/',
                    UploadedFile::fake()->image('image.png', 640, 480)
                )
            ];
        });
    }
}
