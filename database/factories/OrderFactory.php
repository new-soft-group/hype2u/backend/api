<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory()->has(Profile::factory()->has(Address::factory())),
            'order_status_id' => OrderStatus::PENDING,
            'currency' => 'myr',
            'currency_rate' => 1
        ];
    }
}
