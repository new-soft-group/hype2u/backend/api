<?php

namespace Database\Factories;

use App\Models\Image;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\ProductVariantOption;
use Illuminate\Database\Eloquent\Factories\Factory;

class OrderItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_stock_id' => ProductStock::factory()
                                              ->hasAttached(ProductVariantOption::factory(), [], 'combinations')
                                              ->has(ProductStockInventory::factory()->count(5), 'inventories'),
            'product_id' => function(array $item) {
                return ProductStock::find($item['product_stock_id'])->product_id;
            },
            'unit_price' => function(array $item) {
                return Product::find($item['product_id'])->price;
            },
            'quantity' => $this->faker->numberBetween(1, 5)
        ];
    }
}
