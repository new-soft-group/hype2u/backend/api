<?php

namespace Database\Factories;

use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\ProductType;
use App\Models\ProductVariantOption;
use Illuminate\Database\Eloquent\Factories\Factory;

class CartItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = CartItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'cart_id' => Cart::factory(),
            'product_id' => Product::factory()
                                   ->has(ProductType::factory(), 'type'),
            'product_stock_id' => function(array $attributes) {
                return ProductStock::factory()
                                   ->state(['product_id' => $attributes['product_id']])
                                   ->hasAttached(
                                       ProductVariantOption::factory()->product(), [],
                                       'combinations'
                                   )
                                   ->has(ProductStockInventory::factory()->count(10), 'inventories')
                                   ->create();
            },
            'quantity' => $this->faker->randomNumber(1),
        ];
    }
}
