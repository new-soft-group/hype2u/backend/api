<?php

namespace Database\Factories;

use App\Models\Plan;
use App\Models\PlanPolicy;
use App\Models\ProductType;
use Illuminate\Database\Eloquent\Factories\Factory;

class PlanPolicyFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = PlanPolicy::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'plan_id' => Plan::factory(),
            'quantity' => $this->faker->randomNumber(1),
            'logical' => PlanPolicy::LOGICAL_OR
        ];
    }

    public function logicalOr()
    {
        return $this->state(function (array $policy) {
            return ['logical' => PlanPolicy::LOGICAL_OR];
        });
    }

    public function logicalAnd()
    {
        return $this->state(function (array $policy) {
            return ['logical' => PlanPolicy::LOGICAL_AND];
        });
    }

    public function logicalNone()
    {
        return $this->state(function (array $policy) {
            return ['logical' => PlanPolicy::LOGICAL_NONE];
        });
    }
}
