<?php

namespace Database\Factories;

use App\Models\Image;
use App\Models\Product;
use App\Models\WishlistItem;
use Illuminate\Database\Eloquent\Factories\Factory;

class WishlistItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = WishlistItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => Product::factory()
        ];
    }

    public function image()
    {
        return $this->state(function (array $attribute) {
            return ['product_id' => Product::factory()->has(Image::factory()->product())];
        });
    }
}
