<?php

namespace Database\Factories;

use App\Models\Brand;
use App\Models\BusinessType;
use App\Models\Gender;
use App\Models\Product;
use App\Models\ProductType;
use App\Models\StockStatus;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'description' => $this->faker->sentence,
            'slug' => function($product) {
                return Str::slug($product['name']);
            },
            'gender_id' => Gender::factory(),
            'brand_id' => Brand::factory(),
            'price' => $this->faker->numberBetween(1000, 3000),
            'cost_price' => $this->faker->numberBetween(1000, 3000),
            'product_type_id' => ProductType::factory(),
            'featured' => false,
            'enabled' => true,
            'business_type_id' => BusinessType::factory()
        ];
    }

    public function enableFeatured()
    {
        return $this->state(function (array $attributes) {
            return [
                'featured' => true
            ];
        });
    }

    public function disabled()
    {
        return $this->state(function (array $attributes) {
            return [
                'enabled' => false
            ];
        });
    }
}
