<?php

namespace Database\Factories;

use App\Models\ProductType;
use App\Models\TargetType;
use Illuminate\Database\Eloquent\Factories\Factory;

class TargetTypeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TargetType::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_type_id' => ProductType::factory(),
            'quantity' => $this->faker->randomNumber(1),
            'prime' => false
        ];
    }
}
