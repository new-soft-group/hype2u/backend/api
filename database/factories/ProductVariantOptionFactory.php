<?php

namespace Database\Factories;

use App\Models\ProductStock;
use App\Models\ProductVariant;
use App\Models\ProductVariantOption;
use App\Models\VariantOption;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductVariantOptionFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductVariantOption::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_variant_id' => ProductVariant::factory(),
            'variant_option_id' => function($attribute) {
                $variant = ProductVariant::find($attribute['product_variant_id'])->variant_id;
                return VariantOption::factory()->state(['variant_id' => $variant])->create();
            }
        ];
    }

    public function product()
    {
        return $this->state(function (array $attributes, ProductStock $stock) {
            return ['product_variant_id' => ProductVariant::factory()->state(['product_id' => $stock->product_id])];
        });
    }
}
