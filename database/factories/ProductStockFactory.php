<?php

namespace Database\Factories;

use App\Models\Product;
use App\Models\ProductStock;
use App\Models\Vendor;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductStockFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductStock::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'product_id' => Product::factory(),
            'price' => $this->faker->numberBetween(1000, 3000),
            'cost_price' => $this->faker->numberBetween(1000, 3000),
            'vendor_id' => Vendor::factory(),
            'enabled' => true
        ];
    }

    public function product()
    {
        return $this->state(function (array $attributes, Product $product) {
            return ['product_id' => $product->id];
        });
    }
}
