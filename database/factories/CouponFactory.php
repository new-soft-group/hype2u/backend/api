<?php

namespace Database\Factories;

use App\Models\Address;
use App\Models\Coupon;
use App\Models\Invoice;
use App\Models\Profile;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class CouponFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Coupon::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name,
            'stripe_id' => Str::random(),
            'code' => Str::random(),
            'duration' => Coupon::DURATION_ONCE,
            'enabled' => true
        ];
    }

    public function amount()
    {
        return $this->state(function (array $coupon) {
            return [
                'amount_off' => $this->faker->randomNumber(3),
                'currency' => 'myr'
            ];
        });
    }

    public function percentage()
    {
        return $this->state(function (array $coupon) {
            return [
                'percent_off' => $this->faker->randomNumber(2)
            ];
        });
    }
}
