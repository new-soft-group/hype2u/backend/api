<?php

namespace Database\Factories;

use App\Models\NotifyQueue;
use App\Models\Queuer;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class QueuerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Queuer::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'user_id' => User::factory(),
            'queue_id' => NotifyQueue::factory()
        ];
    }
}
