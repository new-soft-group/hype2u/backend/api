<?php

namespace Database\Factories;

use App\Models\Coupon;
use App\Models\Invoice;
use Illuminate\Database\Eloquent\Factories\Factory;
use Str;

class InvoiceFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Invoice::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'stripe_id' => 'in_'.Str::random(),
            'payment_intent' => 'pm_'.Str::random(),
            'amount' => $this->faker->randomNumber(3),
            'currency' => 'myr',
            'status' => 'succeeded',
        ];
    }
}
