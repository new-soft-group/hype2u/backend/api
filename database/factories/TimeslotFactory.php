<?php

namespace Database\Factories;

use App\Models\Timeslot;
use App\Models\TimeslotType;
use Illuminate\Database\Eloquent\Factories\Factory;

class TimeslotFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Timeslot::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'start_time' => now()->format('H:i'),
            'end_time' => now()->addHour()->format('H:i'),
            'type_id' => TimeslotType::factory(),
            'limit' => $this->faker->numberBetween(1, 10),
            'enabled' => true
        ];
    }
}
