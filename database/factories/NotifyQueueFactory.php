<?php

namespace Database\Factories;

use App\Models\NotifyQueue;
use App\Models\ProductStock;
use Illuminate\Database\Eloquent\Factories\Factory;

class NotifyQueueFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = NotifyQueue::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'stock_id' => ProductStock::factory()
        ];
    }
}
