<?php

namespace Database\Factories;

use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\ProductVariantOption;
use App\Models\StockStatus;
use App\Models\Warehouse;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductStockInventoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = ProductStockInventory::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'warehouse_id' => Warehouse::factory(),
            'product_stock_id' => ProductStock::factory()
                                              ->hasAttached(
                                                  ProductVariantOption::factory(),
                                                  [],
                                                  'combinations'
                                              ),
            'tag' => $this->faker->uuid,
            'stock_status_id' => StockStatus::IN_STOCK
        ];
    }

    public function shippedOut()
    {
        return $this->state(function ($attributes) {
            return ['stock_status_id' => StockStatus::SHIPPED_OUT];
        });
    }
}
