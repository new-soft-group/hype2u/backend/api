<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Timeslot;
use App\Models\TimeslotAllocation;
use App\Models\TimeslotType;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class TimeslotAllocationFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = TimeslotAllocation::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $today = now()->format('Y-m-d');

        return [
            'timeslot_id' => Timeslot::factory(),
            'order_id' => Order::factory(),
            'start_at' => function($attributes) use ($today) {
                $timeslot = Timeslot::find($attributes['timeslot_id']);
                return Carbon::parse(strtotime("$today $timeslot->start_time"))
                             ->format('Y-m-d H:i:s');
            },
            'end_at' => function($attributes) use ($today) {
                $timeslot = Timeslot::find($attributes['timeslot_id']);
                return Carbon::parse(strtotime("$today $timeslot->end_time"))
                             ->format('Y-m-d H:i:s');
            },
        ];
    }
}
