<?php

namespace Database\Factories;

use App\Models\Banner;
use App\Models\BannerPosition;
use Illuminate\Database\Eloquent\Factories\Factory;

class BannerFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Banner::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'viewport' => $this->faker->randomElement([
                Banner::MOBILE_VIEWPORT,
                Banner::DESKTOP_VIEWPORT
            ]),
            'sort' => 0,
            'banner_position_id' => BannerPosition::factory()
        ];
    }
}
