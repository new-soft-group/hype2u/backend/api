<?php

namespace Database\Factories;

use App\Models\Plan;
use App\Models\Subscription;
use App\Models\Surcharge;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class PlanFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Plan::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->name,
            'description' => $this->faker->sentence,
            'stripe_plan' => 'plan_'.Str::random(),
            'stripe_product' => 'prod_'.Str::random(),
            'amount' => $this->faker->randomFloat(2, 200, 500),
            'purchase_amount' => $this->faker->randomFloat(2, 1000, 9999),
            'interval' => 'month',
            'exchange_limit' => $this->faker->randomNumber(2),
            'sort' => 1
        ];
    }

    public function disabled()
    {
        return $this->state(function (array $attributes) {
            return [
                'enabled' => false
            ];
        });
    }
}
