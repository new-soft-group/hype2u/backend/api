<?php

namespace Database\Factories;

use App\Models\Plan;
use App\Models\Surcharge;
use Illuminate\Database\Eloquent\Factories\Factory;

class SurchargeFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Surcharge::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'threshold' => $this->faker->randomNumber(4),
            'charge' => $this->faker->randomNumber(3)
        ];
    }
}
