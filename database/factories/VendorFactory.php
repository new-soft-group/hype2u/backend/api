<?php

namespace Database\Factories;

use App\Models\Vendor;
use Illuminate\Database\Eloquent\Factories\Factory;

class VendorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Vendor::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'company_name' => $this->faker->company,
            'contact_name' => $this->faker->name,
            'contact_title' => $this->faker->name,
            'address1' => $this->faker->address,
            'address2' => $this->faker->address,
            'city' => $this->faker->city,
            'country' => $this->faker->state,
            'phone' => $this->faker->e164PhoneNumber,
            'fax' => $this->faker->e164PhoneNumber,
            'email' => $this->faker->email,
            'site_url' => $this->faker->url
        ];
    }
}
