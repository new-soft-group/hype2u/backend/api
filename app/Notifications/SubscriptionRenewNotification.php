<?php

namespace App\Notifications;

use App\Broadcasting\SmsChannel;
use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class SubscriptionRenewNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Subscription
     */
    public $subscription;

    /**
     * Create a new notification instance.
     *
     * @param Subscription $subscription
     */
    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return "Hi {$this->subscription->user->profile->name}, your {$this->subscription->plan->name} has renewed! You can now pick a new set of style to be delivered to you! hype2u.com";
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->subscription->toArray();
    }
}
