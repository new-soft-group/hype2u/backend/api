<?php

namespace App\Notifications;

use App\Broadcasting\SmsChannel;
use App\Models\ProductStock;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class StockArrivalNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var ProductStock
     */
    public $stock;

    /**
     * Create a new notification instance.
     *
     * @param ProductStock $stock
     */
    public function __construct(ProductStock $stock)
    {
        $this->stock = $stock;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', SmsChannel::class];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject('Stock Arrived')
                    ->markdown('emails.stock-arrival', [
                        'stock' => $this->stock,
                        'url' => 'http://hype2u.newsoft.com.my/browse',
                        'button' => 'Explore styles'
                    ]);
    }

    public function toSms($notifiable)
    {
        return $this->stock->product->name." ".$this->stock->combination." has arrived. Grab it now!";
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
