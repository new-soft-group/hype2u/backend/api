<?php

namespace App\Notifications;

use App\Broadcasting\SmsChannel;
use App\Models\Order;
use App\Models\TimeslotType;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class OrderPickUpReminderNotification extends Notification
{
    use Queueable;

    public $order;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return "Hi {$this->order->user->profile->name}, your pickup is scheduled for today at {$this->order->timeslots->where('timeslot_id', TimeslotType::RETURN_BACK)->first()->start_at->format('g:i A')}. Kindly contact support for any changes. TQ.";
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->order->toArray();
    }
}
