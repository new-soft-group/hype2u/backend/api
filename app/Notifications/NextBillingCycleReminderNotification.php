<?php

namespace App\Notifications;

use App\Broadcasting\SmsChannel;
use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class NextBillingCycleReminderNotification extends Notification implements ShouldQueue
{
    use Queueable;

    /**
     * @var Subscription
     */
    public $subscription;

    /**
     * Create a new notification instance.
     *
     * @param Subscription $subscription
     */
    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return "Hi {$this->subscription->user->profile->name}, your items are about to expire in 7 days! Kindly arrange for a pickup by {$this->subscription->nextBillingCycle()->format('Y/m/d')} and start a new set of style!";
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->order->toArray();
    }
}
