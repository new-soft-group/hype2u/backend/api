<?php

namespace App\Notifications;

use App\Broadcasting\SmsChannel;
use App\Models\Order;
use App\Models\TimeslotType;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class OrderDeliverReminderNotification extends Notification
{
    use Queueable;

    public $order;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [SmsChannel::class];
    }

    public function toSms($notifiable)
    {
        return "Hi {$this->order->user->profile->name}, your order is scheduled for delivery today at {$this->order->timeslots->where('timeslot_id', TimeslotType::DELIVER)->first()->start_at->format('g:i A')}. Kindly contact support for any changes. TQ.";
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return $this->order->toArray();
    }
}
