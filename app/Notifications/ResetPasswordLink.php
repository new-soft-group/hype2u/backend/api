<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Auth\Notifications\ResetPassword as ResetPasswordNotification;

class ResetPasswordLink extends ResetPasswordNotification implements ShouldQueue
{
    use Queueable;
    
    public static $createUrlCallback = [self::class, 'createActionUrl'];
    
    /**
     * @param mixed $notifiable
     * @param $token
     * @return \Illuminate\Contracts\Routing\UrlGenerator|string
     */
    public static function createActionUrl($notifiable, $token)
    {
        return config('password.reset.domain').'/'.$token.'?email='.urlencode($notifiable->getEmailForPasswordReset());
    }
}
