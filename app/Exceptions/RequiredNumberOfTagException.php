<?php


namespace App\Exceptions;


class RequiredNumberOfTagException extends JsonException
{
    public function __construct($number)
    {
        $message = __('tag.required', [
            'number' => $number
        ]);

        parent::__construct($message, 400, null);
    }
}
