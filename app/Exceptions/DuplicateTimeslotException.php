<?php


namespace App\Exceptions;

class DuplicateTimeslotException extends JsonException
{
    public function __construct()
    {
        parent::__construct(__('timeslot.duplicate'), 400, null);
    }
}
