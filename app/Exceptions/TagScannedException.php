<?php


namespace App\Exceptions;


class TagScannedException extends JsonException
{
    public function __construct($tag)
    {
        $message = __('tag.scanned', ['tag' => $tag]);

        parent::__construct($message, 400, null);
    }
}
