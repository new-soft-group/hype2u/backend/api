<?php


namespace App\Exceptions;


use Throwable;

class ViolatePolicyException extends JsonException
{
    public function __construct($message = "")
    {
        parent::__construct($message, 400, null);
    }
}
