<?php

namespace App\Exceptions;

class PickExistException extends JsonException
{
    public function __construct()
    {
        parent::__construct(__('pick.exist'), 400, null);
    }
}
