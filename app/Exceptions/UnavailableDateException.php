<?php


namespace App\Exceptions;


class UnavailableDateException extends JsonException
{
    public function __construct($message = "")
    {
        parent::__construct($message, 400, null);
    }
}
