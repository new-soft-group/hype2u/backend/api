<?php

namespace App\Exceptions;

class CouponNotValidException extends JsonException
{
    public function __construct()
    {
        $message = __('coupon.not_valid');

        parent::__construct($message, 422, null);
    }
}
