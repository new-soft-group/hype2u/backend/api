<?php


namespace App\Exceptions;


use Exception;
use Illuminate\Http\Request;
use Throwable;

class JsonException extends Exception
{
    public function __construct($message = "", $code = 500, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

    public function render(Request $request)
    {
        return response()->json(
            [
                'message' => $this->getMessage(),
                'status' => $this->getCode()
            ],
            $this->getCode()
        );
    }
}
