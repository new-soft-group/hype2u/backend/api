<?php

namespace App\Exceptions;

class InvalidProductBusinessType extends JsonException
{
    public function __construct()
    {
        parent::__construct(__('product.business_type.invalid'), 400, null);
    }
}
