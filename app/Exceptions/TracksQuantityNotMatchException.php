<?php

namespace App\Exceptions;

class TracksQuantityNotMatchException extends JsonException
{
}
