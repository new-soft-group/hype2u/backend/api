<?php


namespace App\Exceptions;

class ShippingAddressNotFound extends JsonException
{
    public function __construct()
    {
        parent::__construct(__('profile.not_found.shipping_address'), 400, null);
    }
}
