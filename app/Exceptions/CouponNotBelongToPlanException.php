<?php

namespace App\Exceptions;

class CouponNotBelongToPlanException extends JsonException
{
    public function __construct()
    {
        $message = __('coupon.plan.not_belong');

        parent::__construct($message, 422, null);
    }
}
