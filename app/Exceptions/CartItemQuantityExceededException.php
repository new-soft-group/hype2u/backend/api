<?php

namespace App\Exceptions;

class CartItemQuantityExceededException extends JsonException
{
    public function __construct()
    {
        $message = __('cart.item.quantity_exceeded');

        parent::__construct($message, 422, null);
    }
}
