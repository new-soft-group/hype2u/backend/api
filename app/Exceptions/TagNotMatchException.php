<?php


namespace App\Exceptions;


class TagNotMatchException extends JsonException
{
    public function __construct(array $tags)
    {
        $message = __('tag.not_match', [
            'tags' => implode(', ', $tags)
        ]);

        parent::__construct($message, 400, null);
    }
}
