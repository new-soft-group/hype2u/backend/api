<?php


namespace App\Exceptions;


class RequireSubscriptionException extends JsonException
{
    public function __construct()
    {
        parent::__construct(__('middleware.subscribed'), 400, null);
    }
}
