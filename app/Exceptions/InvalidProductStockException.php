<?php

namespace App\Exceptions;


class InvalidProductStockException extends JsonException
{
    public function __construct()
    {
        parent::__construct(__('product.stock.invalid'), 400, null);
    }
}
