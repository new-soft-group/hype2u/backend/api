<?php


namespace App\Exceptions;


class InsufficientStockException extends JsonException
{
}
