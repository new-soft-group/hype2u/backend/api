<?php


namespace App\Exceptions;


class OngoingOrderException extends JsonException
{
    public function __construct(int $count)
    {
        $message = __('order.failed.ongoing', ['count' => $count]);

        parent::__construct($message, 400, null);
    }
}
