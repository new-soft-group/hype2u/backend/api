<?php


namespace App\Exceptions;

class DuplicateQueueEntryException extends JsonException
{
    public function __construct()
    {
        parent::__construct(__('queue.duplicate'), 400, null);
    }
}
