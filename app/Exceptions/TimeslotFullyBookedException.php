<?php


namespace App\Exceptions;

class TimeslotFullyBookedException extends JsonException
{
    public function __construct()
    {
        parent::__construct(__('timeslot.full'), 400, null);
    }
}
