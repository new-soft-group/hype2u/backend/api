<?php


namespace App\Exceptions;


class InvalidSubscriptionException extends JsonException
{
    public function __construct($status = "")
    {
        $message = __('subscription.invalid', ['status' => $status]);

        parent::__construct($message, 400, null);
    }
}
