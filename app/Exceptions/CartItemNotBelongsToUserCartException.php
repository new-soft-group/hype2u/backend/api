<?php

namespace App\Exceptions;

class CartItemNotBelongsToUserCartException extends JsonException
{
    public function __construct()
    {
        $message = __('cart.item.not_belong_cart');

        parent::__construct($message, 422, null);
    }
}
