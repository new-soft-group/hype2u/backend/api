<?php

namespace App\Exceptions;

class DuplicateCartItemException extends JsonException
{
    public function __construct()
    {
        $message = __('cart.item.duplicate');

        parent::__construct($message, 400, null);
    }
}
