<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class AllowedLanguage implements Rule
{
    private $language = '';
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        foreach (array_keys($value) as $key) {
            if(!in_array($key, ['en'])) {
                $this->language = $key;
                return false;
            }
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.language', ['language' => $this->language]);
    }
}
