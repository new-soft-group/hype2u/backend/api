<?php

namespace App\Rules;

use \App\Models\DiscountType as Type;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class Discount implements Rule
{
    /**
     * @var int
     */
    private $discountType;
    
    /**
     * Create a new rule instance.
     *
     * @param int $discountType
     */
    public function __construct(int $discountType)
    {
        $this->discountType = $discountType;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if($this->discountType === 0) {
            return false;
        }
        
        if($this->discountType === Type::PERCENTAGE) {
            return (float)$value <= 100;
        }
        
        return true;
    }
    
    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return $this->discountType === 0
            ? trans('validation.discount.type_absence')
            : trans('validation.discount.exceed_limit');
    }
}
