<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class AllowedPaymentMethod implements Rule
{
    const CARD_METHOD = 'card';
    
    const FPX_METHOD = 'fpx';
    
    /**
     * @var string
     */
    private $currency;
    /**
     * @var bool
     */
    private $fpxError;
    
    public function __construct(string $currency)
    {
        $this->currency = strtoupper($currency);
        
        $this->fpxError = false;
    }
    
    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validator = Validator::make(
            [$attribute => $value],
            [$attribute => 'in:'.self::CARD_METHOD.','.self::FPX_METHOD]
        );
        
        if($validator->passes()) {
            if($value === self::FPX_METHOD and $this->currency !== AllowedCurrencies::MYR) {
                $this->fpxError = true;
                return false;
            }
        }
        
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return $this->fpxError
            ? trans('validation.payment_method.fpx')
            : trans('validation.payment_method.invalid');
    }
}
