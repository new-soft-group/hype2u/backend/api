<?php

namespace App\Rules;

use App\Models\Affiliate;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Http\Request;

class MinMaxCommission implements Rule
{
    /** @var Affiliate $affiliate */
    private $affiliate;
    /**
     * @var float
     */
    private $max;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->affiliate = $request->route('member');
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if(is_null($this->affiliate)) return false;
        
        if($this->affiliate->isRoot()) return true;
        
        $this->max = (float)$this->affiliate->parent->commission;
        
        return (float)$value < $this->max;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.min_max_commission', ['max' => $this->max]);
    }
}
