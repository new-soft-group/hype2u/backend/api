<?php

namespace App\Rules;

use App\Models\DiscountType as Type;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class DiscountType implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $restriction = 'in:'.Type::AMOUNT.','.Type::PERCENTAGE;
        
        $validator = Validator::make([$attribute => $value], [
            $attribute => $restriction
        ]);
        
        return $validator->passes();
    }
    
    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return trans('validation.discount.invalid_type');
    }
}
