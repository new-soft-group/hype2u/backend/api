<?php

namespace App\Rules;

use App\Models\DiscountMethod as Method;
use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class DiscountMethod implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $methods = 'in:'.Method::APPLY_TO_ITEMS.','.Method::APPLY_TO_FINAL_AMOUNT;
        
        $validator = Validator::make([$attribute => $value], [
            $attribute => $methods
        ]);
        
        return $validator->passes();
    }
    
    /**
     * Get the validation error message.
     *
     * @return string|array
     */
    public function message()
    {
        return trans('validation.discount.invalid_method');
    }
}
