<?php

namespace App\Rules;

use App\Models\Wishlist;
use Illuminate\Contracts\Validation\Rule;

class DuplicateWishlistItem implements Rule
{
    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable|null
     */
    private $user;
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->user = auth()->user();
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        /** @var Wishlist $wishlist */
        $wishlist = $this->user->wishlist;
        
        if(is_null($wishlist)) return true;
        
        $exist = $wishlist->items()
                          ->whereKey($value)
                          ->exists();
        
        return !$exist;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.wishlist_duplicate');
    }
}
