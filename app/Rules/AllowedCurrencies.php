<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Facades\Validator;

class AllowedCurrencies implements Rule
{
    const MYR = 'myr';
    const SGD = 'sgd';
    const USD = 'usd';

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $validator = Validator::make(
            [$attribute => $value],
            [$attribute => 'in:'.self::MYR.','.self::SGD.','.self::USD]
        );

        return $validator->passes();
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.currencies');
    }
}
