<?php

namespace App\Rules;

use App\Models\Image;
use App\Models\ProductOptionValue;
use Illuminate\Contracts\Validation\Rule;

class ExistProductOptionImage implements Rule
{
    private $language = '';
    
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $result = Image::whereKey($value)
                       ->where('imageable_type', ProductOptionValue::class)
                       ->exists();
        
        return $result;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.exists');
    }
}
