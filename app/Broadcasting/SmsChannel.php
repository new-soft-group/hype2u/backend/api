<?php

namespace App\Broadcasting;

use App\Contracts\Sms\SmsContract;
use Illuminate\Notifications\Notification;

class SmsChannel
{
    /**
     * @var SmsContract
     */
    private $sms;

    /**
     * Create a new channel instance.
     *
     * @param SmsContract $sms
     */
    public function __construct(SmsContract $sms)
    {
        $this->sms = $sms;
    }

    /**
     * Send the given notification.
     *
     * @param  mixed  $notifiable
     * @param  \Illuminate\Notifications\Notification  $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $mobile = null;

        if(method_exists($notifiable, 'getPhoneNumberForSms')) {
            $mobile = call_user_func([$notifiable, 'getPhoneNumberForSms']);
        }

        if(!is_null($mobile)) {
            $message = $notification->toSms($notifiable);

            $withPrefix = "{$this->getContentPrefix()} $message";

            $this->sms->send($mobile, $withPrefix);
        }
    }

    public function getContentPrefix()
    {
        return config('sms.prefix', 'RM0.00');
    }
}
