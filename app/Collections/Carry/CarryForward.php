<?php


namespace App\Collections\Carry;

use App\Models\Order;
use App\Models\Product;

class CarryForward
{
    /**
     * @var Product
     */
    protected $product = null;
    /**
     * @var int
     */
    protected $orderKey = null;
    /**
     * @var int
     */
    protected $orderStatusId = null;
    /**
     * @var int
     */
    protected $stockKey = null;
    /**
     * @var string
     */
    protected $variant = null;
    /**
     * @var float
     */
    protected $variantPrice = 0;
    /**
     * @var float
     */
    protected $unitPrice = 0;
    /**
     * @var int
     */
    protected $quantity = 0;

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return CarryForward
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return string
     */
    public function getVariant(): ?string
    {
        return $this->variant;
    }

    /**
     * @param string|null $variant
     * @return CarryForward
     */
    public function setVariant(?string $variant)
    {
        $this->variant = $variant;

        return $this;
    }

    /**
     * @return float
     */
    public function getVariantPrice(): float
    {
        return $this->variantPrice;
    }

    /**
     * @param float $variantPrice
     * @return CarryForward
     */
    public function setVariantPrice(float $variantPrice)
    {
        $this->variantPrice = $variantPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getUnitPrice(): float
    {
        return $this->unitPrice;
    }

    /**
     * @param float $unitPrice
     * @return CarryForward
     */
    public function setUnitPrice(float $unitPrice)
    {
        $this->unitPrice = $unitPrice;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     * @return CarryForward
     */
    public function setQuantity(int $quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return int
     */
    public function getStockKey(): int
    {
        return $this->stockKey;
    }

    /**
     * @param int $stockKey
     * @return CarryForward
     */
    public function setStockKey(int $stockKey)
    {
        $this->stockKey = $stockKey;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderKey(): int
    {
        return $this->orderKey;
    }

    /**
     * @param int $orderKey
     * @return CarryForward
     */
    public function setOrderKey(int $orderKey)
    {
        $this->orderKey = $orderKey;

        return $this;
    }

    /**
     * @return int
     */
    public function getOrderStatusId(): int
    {
        return $this->orderStatusId;
    }

    /**
     * @param int $orderKey
     * @return CarryForward
     */
    public function setOrderStatusId(int $orderId)
    {
        $order = Order::find($orderId);

        $this->orderStatusId = $order->order_status_id;

        return $this;
    }

    public function getAmount(): float
    {
        $price = $this->unitPrice;

        return $price * $this->quantity;
    }
}
