<?php


namespace App\Collections\Carry;

use App\Models\Cart;
use Illuminate\Database\Eloquent\Collection;

class CarryForwardCollection extends Collection
{
    public function moveToCart(Cart $cart)
    {
        return $cart->setCarryForward($this);
    }

    public function totalAmount(array $exclude = [])
    {
        return $this->sum(function (CarryForward $carry) use ($exclude) {
            if(in_array($carry->getProduct()->product_type_id, $exclude)) return 0;

            return $carry->getAmount();
        });
    }

    public function totalQuantity()
    {
        return $this->sum(function (CarryForward $carry) {
            return $carry->getQuantity();
        });
    }
}
