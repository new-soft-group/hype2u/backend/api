<?php


namespace App\Collections;

use App\Exceptions\TagScannedException;
use App\Models\ProductStockInventory;
use App\Models\StockStatus;
use Illuminate\Database\Eloquent\Collection;

class ProductStockInventoryCollection extends Collection
{
    public function tags(): array
    {
        return $this->pluck('tag')
                    ->toArray();
    }

    public function markAsShippedOut()
    {
        ProductStockInventory::markTagsAs($this->tags(), StockStatus::SHIPPED_OUT);
    }

    public function markAsReserved()
    {
        ProductStockInventory::markTagsAs($this->tags(), StockStatus::RESERVED);
    }

    public function markAsInStock()
    {
        ProductStockInventory::markTagsAs($this->tags(), StockStatus::IN_STOCK);
    }

    public function intersectTags(array $tags)
    {
        return $this->whereIn('tag', $tags);
    }

    public function diffTags(array $tags)
    {
        return array_diff($tags, $this->tags());
    }

    public function validateScannedTags(array $tags)
    {
        $this->map(function ($track) use ($tags) {
            if(in_array($track->tag, $tags)){
                if($track->pivot->scanned_at) throw new TagScannedException($track->tag);
            }
        });
    }
}
