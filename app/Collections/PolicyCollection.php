<?php


namespace App\Collections;

use App\Models\PlanPolicy;
use Illuminate\Database\Eloquent\Collection;

class PolicyCollection extends Collection
{
    public function getPrimeRules(bool $prime = true)
    {
        return $this->map(function (PlanPolicy $policy) use ($prime) {
            return $policy->rules->where('pivot.prime', $prime);
        })->flatten();
    }

    public function primeTargetIds(): array
    {
        return $this->getPrimeRules()
                    ->pluck('pivot.product_type_id')
                    ->toArray();
    }

    public function totalQuantity(): int
    {
        return $this->sum('quantity');
    }
}
