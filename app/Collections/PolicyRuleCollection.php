<?php


namespace App\Collections;

use App\Validators\Plan\Policy\PolicyRule;
use Illuminate\Support\Collection;

class PolicyRuleCollection extends Collection
{
    public function wherePrime(bool $is = false)
    {
        return $this->filter(function (PolicyRule $rule) use ($is) {
            dd($rule->getPolicy()->toArray());

            return $rule->getPolicy()->where('rules.pivot.prime', $is);
        })->map(function (PolicyRule $rule) {
            return $rule->getPolicy()->rules;
        })->flatten();
    }

    public function totalTargetQuantity()
    {
        return $this->sum(function (PolicyRule $rule) {
            return $rule->getTarget()->totalQuantity();
        });
    }

    public function allTargetIds(): array
    {
        return $this->map(function (PolicyRule $rule) {
            return $rule->getPolicy()->targetIds();
        })->flatten()->toArray();
    }
}
