<?php


namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class CartItemCollection extends Collection
{
    public function excludeTypeIn(array $ids)
    {
        return $this->whereNotIn('product_type_id', $ids)->values();
    }

    public function filterByType(array $ids)
    {
        return $this->whereIn($key = 'product_type_id', $ids)->values();
    }

    public function groupByType()
    {
        return $this->groupBy($key = 'product_type_id');
    }

    public function totalQuantity(): int
    {
        return $this->sum('quantity');
    }
}
