<?php


namespace App\Collections;

use App\Models\ProductStock;
use App\Models\ProductVariantOption;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

class StockCollection extends Collection
{
    /**
     * @param $variant
     * @return ProductVariantOption|null
     */
    public function findByVariant($variant)
    {
        /** @var ProductStock $stock */
        foreach ($this->items as $stock) {
            $variant = $stock->combinations->firstWhere('lookup.variant_id', $variant);

            if(!is_null($variant)) return $variant;
        }

        return null;
    }

    public function missingOptions($variant, array $options)
    {
        $keys = Arr::pluck($options, 'variant_option_id');

        $existed = $this->pluck('combinations')
                        ->flatten()
                        ->where('lookup.variant_id', $variant)
                        ->unique('variant_option_id');

        /**
         * @var $isAttachedOptionsMissing
         * Find missing attached stock variant option
         */
        $isAttachedOptionsMissing = $this->optionsAttachedMissing($existed , $keys);

        return $isAttachedOptionsMissing;
    }

    public function optionsAttachedMissing($existed, array $keys)
    {
        $existedOptionIds = $existed->pluck('variant_option_id');

        /** Loop attached stock variant option id in input key and check if it missing */
        foreach ($existedOptionIds as $optionId){
            if(!in_array($optionId, $keys)){
                return true;
            };
        }

        return false;
    }
}
