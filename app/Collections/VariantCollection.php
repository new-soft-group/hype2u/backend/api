<?php


namespace App\Collections;

use Illuminate\Database\Eloquent\Collection;

class VariantCollection extends Collection
{
    public function getVariantIdByName($name)
    {
        if(is_array($name)) {
            return $this->whereIn('name', $name)
                        ->pluck('id')
                        ->toArray();
        }
        else {
            return $this->where('name', $name)
                        ->pluck('id')
                        ->first();
        }
    }

    public function getOptionIdByName($name, $withKey = false)
    {
        $options = $this->pluck('options')->flatten();

        if(is_array($name)) {
            $ids = $options->whereIn('name', $name)
                           ->pluck('id')
                           ->toArray();
        }
        else {
            $ids = $options->where('name', $name)
                           ->pluck('id')
                           ->first();
        }

        if($withKey) {
            if(is_array($ids)) {
                return array_map(function ($id) {
                    return ['variant_option_id' => $id];
                    }, $ids);
            }
            else {
                return ['variant_option_id' => $ids];
            }
        }

        return $ids;
    }

    public function getAllOptionName(): array
    {
        return $this->pluck('options')
                    ->flatten()
                    ->map(function ($options) {
                        return $options->name;
                    })
                    ->toArray();
    }
}
