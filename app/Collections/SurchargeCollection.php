<?php


namespace App\Collections;

use App\Models\Surcharge;
use Illuminate\Database\Eloquent\Collection;

class SurchargeCollection extends Collection
{
    public function updateOrCreate($foreignKey, array $attributes)
    {
        foreach ($attributes as $attribute) {
            if(array_key_exists('id', $attribute)) {
                $this->update($attribute);
            }
            else {
                $this->create($foreignKey, $attribute);
            }
        }
    }

    public function create($foreignKey, array $attribute)
    {
        $surcharge = new Surcharge();
        $surcharge->plan()->associate($foreignKey);
        $surcharge->setThreshold($attribute['threshold'])
                  ->setCharge($attribute['charge'])
                  ->save();

        return $surcharge;
    }

    public function update(array $attribute)
    {
        /** @var Surcharge $surcharge */
        $surcharge = $this->find($attribute['id']);

        $surcharge->setThreshold($attribute['threshold'])
                  ->setCharge($attribute['charge']);

        if($surcharge->isDirty(['threshold', 'charge'])) {
            $surcharge->save();
        }

        return $surcharge;
    }
}
