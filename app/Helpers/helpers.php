<?php

if (! function_exists('amountToCent')) {
    /**
     * Convert the amount to cent.
     *
     * @param float $amount
     * @return int
     */
    function amountToCent(float $amount): int
    {
        return (float)$amount * 100;
    }
}

if (! function_exists('centToAmount')) {
    /**
     * Convert the cent to amount.
     *
     * @param int $cent
     * @return float
     */
    function centToAmount(int $cent): float
    {
        return $cent / 100;
    }
}

if (! function_exists('convertToStripeObject')) {
    /**
     * Convert the cent to amount.
     *
     * @param array $payload
     * @return \Stripe\StripeObject
     */
    function convertToStripeObject(array $payload): \Stripe\StripeObject
    {
        $stripeObject = $payload['data']['object']['object'];
        $stripeClass = str_replace('_', '', ucwords($stripeObject, '_'));
        $stripePayload = $payload['data']['object'];

        return call_user_func("\\Stripe\\${stripeClass}::constructFrom", $stripePayload);
    }
}
