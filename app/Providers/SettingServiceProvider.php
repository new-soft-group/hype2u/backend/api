<?php

namespace App\Providers;

use App\Models\Setting;
use Cache;
use Illuminate\Support\ServiceProvider;
use Schema;

class SettingServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        try {
            if(Schema::hasTable('settings')) {
                $settings = Cache::rememberForever('settings', function() {
                    return Setting::all()->pluck('value', 'key')->toArray();
                });
                config()->set($settings);
            }
        } catch (\Exception $exception) {
            echo "Database connection not found.\n";
        }
    }
}
