<?php

namespace App\Providers;

use Illuminate\Contracts\Routing\UrlGenerator;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to your controller routes.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $webNS = 'App\Http\Controllers\Web';

    protected $apiNS = 'App\Http\Controllers\Api';

    /**
     * The path to the "home" route for your application.
     *
     * @var string
     */
    public const HOME = '/dashboard';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        if($this->app->environment() === 'production') {
            resolve(UrlGenerator::class)->forceScheme('https');
        }

        parent::boot();
    }

    /**
     * Define the routes for the application.
     *
     * @return void
     */
    public function map()
    {
        $this->mapPublicRoutes();
        $this->mapAuthRoutes();

        $this->mapWebRoutes();
    }

    /**
     * Define the "web" routes for the application.
     *
     * These routes all receive session state, CSRF protection, etc.
     *
     * @return void
     */
    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->webNS)
            ->group(base_path('routes/web.php'));
    }

    /**
     * Define the "api" routes for the application.
     *
     * These routes are typically stateless.
     *
     * @return void
     */
    protected function mapApiRoutes()
    {
        Route::prefix('api')
            ->middleware('api')
            ->namespace($this->apiNS)
            ->group(base_path('routes/api.php'));
    }

    protected function mapPublicRoutes()
    {
        foreach (glob(base_path('routes/api/public/*.php')) as $file) {
            Route::prefix('api')
                 ->middleware('api')
                 ->namespace($this->apiNS)
                 ->group($file);
        }
    }

    protected function mapAuthRoutes()
    {
        foreach (glob(base_path('routes/api/auth/*.php')) as $file) {
            Route::prefix('api')
                 ->middleware(['api', 'auth:api'])
                 ->namespace($this->apiNS)
                 ->group($file);
        }
    }
}
