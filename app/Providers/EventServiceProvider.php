<?php

namespace App\Providers;

use App\Events\NewPickReceivedEvent;
use App\Events\NewRegistrationEvent;
use App\Events\NewSubscriptionEvent;
use App\Events\NextBillingCycleReminderEvent;
use App\Events\OrderConfirmationEvent;
use App\Events\ReturnCompletedEvent;
use App\Events\OrderDeliverReminderEvent;
use App\Events\OrderPickUpReminderEvent;
use App\Events\ShippingConfirmationEvent;
use App\Events\ShopOrderCompletedEvent;
use App\Events\ShopOrderConfirmationEvent;
use App\Events\ShopOrderRejectedEvent;
use App\Events\StockArrival;
use App\Events\SubscriptionRenewEvent;
use App\Events\UserLoggedIn;
use App\Events\UserPasswordUpdatedEvent;
use App\Events\UserPickSubmittedEvent;
use App\Listeners\NewPickReceivedListener;
use App\Listeners\NewRegistrationListener;
use App\Listeners\NewSubscriptionNotificationListener;
use App\Listeners\NextBillingCycleReminderListener;
use App\Listeners\OrderConfirmationListener;
use App\Listeners\ReturnCompletedListener;
use App\Listeners\OrderDeliverReminderListener;
use App\Listeners\OrderPickUpReminderListener;
use App\Listeners\RenewSubscriptionNotificationListener;
use App\Listeners\SendStockArrivalNotification;
use App\Listeners\MergeWithGuestCart;
use App\Listeners\ShippingConfirmationListener;
use App\Listeners\ShopOrderCompletedListener;
use App\Listeners\ShopOrderConfirmationListener;
use App\Listeners\ShopOrderRejectedListener;
use App\Listeners\UserPasswordUpdatedListener;
use App\Listeners\UserPickSubmittedListener;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        Registered::class => [
            SendEmailVerificationNotification::class,
        ],
        UserLoggedIn::class => [
            MergeWithGuestCart::class,
        ],
        NewRegistrationEvent::class => [
            NewRegistrationListener::class,
        ],
        StockArrival::class => [
            SendStockArrivalNotification::class,
        ],
        NewSubscriptionEvent::class => [
            NewSubscriptionNotificationListener::class,
        ],
        SubscriptionRenewEvent::class => [
            RenewSubscriptionNotificationListener::class,
        ],
        OrderConfirmationEvent::class => [
            OrderConfirmationListener::class,
        ],
        ShopOrderConfirmationEvent::class => [
            ShopOrderConfirmationListener::class,
        ],
        ShopOrderRejectedEvent::class =>[
          ShopOrderRejectedListener::class,
        ],
        ReturnCompletedEvent::class => [
            ReturnCompletedListener::class,
        ],
        ShopOrderCompletedEvent::class =>[
          ShopOrderCompletedListener::class,
        ],
        ShippingConfirmationEvent::class => [
            ShippingConfirmationListener::class,
        ],
        NextBillingCycleReminderEvent::class => [
            NextBillingCycleReminderListener::class,
        ],
        UserPasswordUpdatedEvent::class => [
            UserPasswordUpdatedListener::class,
        ],
        OrderDeliverReminderEvent::class => [
            OrderDeliverReminderListener::class,
        ],
        OrderPickUpReminderEvent::class => [
            OrderPickUpReminderListener::class,
        ],
        UserPickSubmittedEvent::class => [
            UserPickSubmittedListener::class,
        ],
        NewPickReceivedEvent::class => [
            NewPickReceivedListener::class,
        ]
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
