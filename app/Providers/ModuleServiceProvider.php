<?php

namespace App\Providers;

use App\Services\AffiliateService;
use App\Services\UserService;
use Illuminate\Support\ServiceProvider;

class ModuleServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
//        $this->app->bind('App\Services\AffiliateService', function ($app) {
//            return new AffiliateService();
//        });
//
//        $this->app->bind('App\Services\UserService', function ($app) {
//            return $app->make(UserService::class);
//        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
