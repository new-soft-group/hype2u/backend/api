<?php

namespace App\Providers;

use App\Contracts\Sms\SmsContract;
use App\Services\Sms\LocalShortMessageService;
use App\Services\Sms\ShortMessageService;
use GuzzleHttp\Client;
use Illuminate\Support\ServiceProvider;

class ShortMessageServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        if($this->app->isLocal() || $this->app->environment() === 'testing') {
            $this->app->singleton(SmsContract::class, function () {
                return new LocalShortMessageService();
            });
        }
        else {
            $this->app->singleton(SmsContract::class, function () {
                $client = new Client([
                    'verify' => false,
                    'base_uri' => config('sms.endpoint'),
                    'defaults' => [
                        'headers' => [
                            'Content-Type' => 'application/x-www-form-urlencoded'
                        ]
                    ]
                ]);
                return new ShortMessageService(
                    $client,
                    config('sms.auth.username', 'none'),
                    config('sms.auth.password', 'none'),
                    config('sms.options.from', 'none')
                );
            });
        }
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
