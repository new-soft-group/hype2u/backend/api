<?php

namespace App\Providers;

use App\Gateways\PaymentGateway;
use Illuminate\Support\ServiceProvider;

class PaymentGatewayServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(PaymentGateway::class, function () {
            return new PaymentGateway(config('stripe.secret'));
        });
    }
}
