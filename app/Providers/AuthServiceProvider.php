<?php

namespace App\Providers;

use App\Models\Address;
use App\Models\Coupon;
use App\Models\Order;
use App\Models\Timeslot;
use App\Policies\AddressPolicy;
use App\Policies\CouponPolicy;
use App\Policies\OrderPolicy;
use App\Policies\TimeslotPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Laravel\Passport\Passport;

class AuthServiceProvider extends ServiceProvider
{

    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
         Address::class => AddressPolicy::class,
         Order::class => OrderPolicy::class,
         Coupon::class => CouponPolicy::class,
         Timeslot::class => TimeslotPolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Passport::routes();

        Passport::cookie('hype2u_token');
    }
}
