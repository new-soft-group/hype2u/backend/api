<?php


namespace App\Builders;


use App\Contracts\Criteria\CriteriaContract;
use App\Contracts\Datatable\Searchable;
use Closure;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class DatatableBuilder
{
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * @var Searchable
     */
    private $model;
    /**
     * @var string
     */
    private $defaultSort = null;
    /**
     * @var CriteriaContract[]
     */
    private $criteria = [];

    /**
     * @param \Illuminate\Http\Request $request
     * @return $this
     */
    public function setRequest(Request $request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * @param Searchable $model
     * @return $this
     */
    public function setModel(Searchable $model)
    {
        $this->model = $model;

        return $this;
    }

    public function getRequest(): Request
    {
        return $this->request;
    }

    public function getSortBy(): string
    {
        return $this->request->get('sort_by', '');
    }

    public function getFilter()
    {
        $filter = $this->request->get('filter', null);
        return $filter ? json_decode($filter, true) : [];
    }

    public function getSortDirection(): string
    {
        $desc = $this->request->get('sort_desc', false);

        $desc = filter_var($desc, FILTER_VALIDATE_BOOLEAN);

        return $desc ? 'desc' : 'asc';
    }

    public function getSearch(): string
    {
        $search = $this->request->get('search', '');

        return trim($search);
    }

    public function getPerPage(): int
    {
        return $this->request->get('per_page', 18);
    }

    protected function isFilterable(string $field, array $filters): bool
    {
        return array_key_exists($field, $filters);
    }

    protected function isSortable(string $field, array $sorts): bool
    {
        return array_key_exists($field, $sorts);
    }

    public function setDefaultSort(string $column = 'created_at')
    {
        $this->defaultSort = $column;

        return $this;
    }

    public function setCriteria(...$criteria)
    {
        $this->criteria = $criteria;

        return $this;
    }

    protected function executeCallback(Builder $builder, $method, $value)
    {
        if($method instanceof Closure) {
            $method($builder, $value);
        }
        else if(is_string($method) && method_exists($this->model, $method)) {
            $this->model->{$method}($builder, $value);
        }
    }

    protected function buildSortQuery(Builder $builder)
    {
        $sorts = $this->model->sortCriteria();
        $field = $this->getSortBy();

        if($this->isSortable($field, $sorts)) {
            $method = $sorts[$field];
            $this->executeCallback($builder, $method, $this->getSortDirection());
        }
    }

    protected function buildFilterQuery(Builder $builder)
    {
        $filters = $this->model->filterCriteria();

        foreach ($this->getFilter() as $field => $value) {
            if($this->isFilterable($field, $filters) and !empty($value)) {
                $method = $filters[$field];
                $this->executeCallback($builder, $method, $value);
            }
        }
    }

    protected function buildSearchQuery(Builder $builder)
    {
        $keyword = $this->getSearch();

        if($keyword) {
            $caseInsensitive = '%'.strtolower($keyword).'%';

            $this->model->searchCriterion($builder, $caseInsensitive);
        }
    }

    protected function applyCriteria(Builder $builder)
    {
        foreach ($this->criteria as $criterion) {
            if($criterion instanceof CriteriaContract) {
                $criterion->apply($builder);
            }
        }

        return $builder;
    }

    protected function applyDefaultSort(Builder $builder)
    {
        if($this->defaultSort) {
            $builder->latest($this->defaultSort);
        }
    }

    protected function withRelations(Builder $builder)
    {
        $relations = $this->model->withRelations();

        if(count($relations)) {
            $builder->with($relations);
        }
    }

    public function getLimit(Builder $builder): int
    {
        $perPage = $this->getPerPage();

        return $perPage === -1 ? $builder->count() : $perPage;
    }

    public function get(): LengthAwarePaginator
    {
        /** @var Builder $builder */
        $builder = $this->model->newQuery();

        $this->buildFilterQuery($builder);
        $this->buildSearchQuery($builder);
        $this->buildSortQuery($builder);
        $this->withRelations($builder);
        $this->applyCriteria($builder);
        $this->applyDefaultSort($builder);

        return $builder->paginate($this->getLimit($builder));
    }
}
