<?php

namespace App\Observers;

use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStatus;

class OrderObserver
{
    public function updated(Order $order)
    {
        if($order->isDirty('order_status_id')) {
            if($order->isInvalided()) {
                $order->items->each(function (OrderItem $item) {
                    $stock = $item->stock;
                    $stock->getLock()->block(5, function () use ($stock, $item) {
                        $stock->unreserved($item->quantity);
                    });
                });
            }
        }
    }
}
