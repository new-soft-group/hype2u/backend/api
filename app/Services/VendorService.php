<?php

namespace App\Services;

use App\Models\Vendor;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class VendorService
{
    /**
     * @param array $attributes
     * @return Vendor
     */
    public function create(array $attributes): Vendor
    {
        $vendor = new Vendor($attributes);
        
        $vendor->save();
        
        return $vendor;
    }
    
    /**
     * @param Vendor $vendor
     * @param array $attributes
     * @return bool
     */
    public function update(Vendor $vendor, array $attributes = [])
    {
        if(!isset($vendor->id)) return false;
    
        return $vendor->fill($attributes)->save();
    }

    /**
     * @param Vendor $vendor
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Vendor $vendor)
    {
        $result = $vendor->safeDelete();
        return response()->json(['result' => $result]);
    }
}
