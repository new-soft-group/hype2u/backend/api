<?php


namespace App\Services;

use App\Models\Order;
use App\Models\Transaction;
use Ramsey\Uuid\Uuid;

class TransactionService
{
    /**
     * Create customer order
     *
     * @param Order $order
     * @param int $status
     * @return Transaction
     */
    public function create(Order $order, int $status): Transaction
    {
        $transaction = new Transaction();
        $transaction->id = Uuid::uuid4();
        $transaction->order()->associate($order);
        $transaction->status()->associate($status);
        $transaction->amount = $order->amount;
        $transaction->currency = $order->currency;
        $transaction->currency_rate = $order->currency_rate;
        $transaction->save();
        
        return $transaction;
    }
}
