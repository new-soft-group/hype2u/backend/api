<?php

namespace App\Services;

use App\Models\Warehouse;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class WarehouseService
{
    /**
     * @param array $attributes
     * @return Warehouse
     */
    public function create(array $attributes): Warehouse
    {
        $warehouse = new Warehouse($attributes);
        
        $warehouse->save();
        
        return $warehouse;
    }
    
    /**
     * @param Warehouse $warehouse
     * @param array $attributes
     * @return bool
     */
    public function update(Warehouse $warehouse, array $attributes = [])
    {
        if(!isset($warehouse->id)) return false;
    
        return $warehouse->fill($attributes)->save();
    }

    /**
     * @param Warehouse $warehouse
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Warehouse $warehouse)
    {
        $result = $warehouse->safeDelete();
        return response()->json(['result' => $result]);
    }
}
