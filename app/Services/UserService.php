<?php

namespace App\Services;

use App\Exceptions\AuthMismatchException;
use App\Models\User;
use App\Models\Role;
use \Laravel\Socialite\Two\User as SocialUser;
use Hash;

class UserService
{
    /**
     * @var AffiliateService
     */
    private $affiliateService;

    public function __construct(AffiliateService $affiliateService)
    {
        $this->affiliateService = $affiliateService;
    }

    public function create(array $data, $role): User
    {
        $user = User::create([
            'email' => $data['email'],
            'password' => Hash::make($data['password'])
        ]);

        $user->setRelation('profile', $user->profile()->create($data));

        $user->assignRole($role);

        return $user;
    }

    public function emailExist(string $email)
    {
        $user = User::firstWhere(['email' => $email]);

        return !is_null($user);
    }

    /**
     * @param SocialUser $socialUser
     * @param string $provider
     * @return User
     * @throws AuthMismatchException
     */
    public function findOrRegister(SocialUser $socialUser, string $provider): User
    {
        $email = $socialUser->getEmail();

        if($this->emailExist($email)) {

            return $this->getSocialUser($socialUser, $provider);
        }
        else {

            $firstName = $socialUser->offsetExists('first_name')
                ? $socialUser->offsetGet('first_name')
                : null;

            $lastName = $socialUser->offsetExists('last_name')
                ? $socialUser->offsetGet('last_name')
                : null;

            $user = User::firstWhere(['email' => $email, 'provider' => $provider]);

            if(is_null($user)) {
                $user = new User([
                    'email' => $email,
                    'provider' => $provider
                ]);
                $user->password = bcrypt($email);
                $user->save();

                $user->assignRole(Role::USER);

                $user->profile()->create([
                    'first_name' => $firstName,
                    'last_name' => $lastName
                ]);
            }

            return $user;
        }
    }

    /**
     * @param SocialUser $socialUser
     * @param string $provider
     * @return User
     * @throws AuthMismatchException
     */
    public function getSocialUser(SocialUser $socialUser, string $provider): User
    {
        $user = User::firstWhere(['email' => $socialUser->getEmail(), 'provider' => $provider]);

        if(is_null($user)) {
            throw new AuthMismatchException(trans('auth.failed'));
        }

        return $user;
    }

    public function updateRole(User $user, int $roleId): bool
    {
        $role = Role::find($roleId);

        if(!$user->hasRole($role->name)) {
            $user->syncRoles($role->name);
        }

        return true;
    }
}
