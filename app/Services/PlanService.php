<?php


namespace App\Services;

use App\Gateways\PaymentGateway;
use App\Models\Plan;

class PlanService
{
    /**
     * @var PaymentGateway
     */
    private $gateway;

    public function __construct(PaymentGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    public function create(array $attributes, string $currency = 'myr'): Plan
    {
        /** Create new plan */
        $plan = new Plan($attributes);
        $plan->setAmount($attributes['amount'])
             ->setInterval($attributes['interval']);

        /** Create new plan in stripe */
        $response = $this->gateway->plans->create($plan->getStripeOption());

        $plan->setStripe($response)
             ->save();

        /** Create new extra surcharge */
        if(array_key_exists('surcharges', $attributes)) {
            $plan->newSurcharges($attributes['surcharges']);
        }

        /** Create new extra surcharge */
        if(array_key_exists('policies', $attributes)) {
            $plan->createPolicies($attributes['policies']);
        }

        return $plan;
    }

    public function update(Plan $plan, array $attributes = []): Plan
    {
        $plan->fill($attributes);

        if($plan->isDirty('name')) {
            $this->gateway->products->update($plan->stripe_product, [
                'name' => $plan->name
            ]);
        }

        if($plan->isDirty()) {
            $plan->save();
        }

        if(array_key_exists('surcharges', $attributes)) {
            $plan->surcharges->updateOrCreate($plan->id, $attributes['surcharges']);
        }

        if(array_key_exists('policies', $attributes)) {
            $plan->syncPolicies($attributes['policies']);
        }

        return $plan;
    }

    public function status(Plan $plan, bool $status): bool
    {
        $this->gateway->plans->update($plan->stripe_plan, [
            'active' => $status
        ]);

        return $plan->setEnabled($status)->save();
    }

    public function deletePricing(Plan $plan)
    {
        $this->gateway->plans->delete($plan->stripe_plan, []);

        $plan->stripe_plan = '';

        return $plan->save();
    }

    public function deactivate(Plan $plan): bool
    {
        return $this->status($plan, false);
    }

    public function activate(Plan $plan): bool
    {
        return $this->status($plan, true);
    }

    public function updateSort(Plan $plan, int $sort): Plan
    {
        $plan->sort = $sort;

        $plan->save();

        return $plan;
    }
}
