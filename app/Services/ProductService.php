<?php

namespace App\Services;

use App\Models\Product;
use App\Exceptions\ModelHasAssociate;
use App\Models\ProductStock;
use Exception;
use Illuminate\Support\Facades\Log;

class ProductService
{
    /**
     * @param array $attributes
     * @param array $images
     * @return Product
     */
    public function create(array $attributes, array $images): Product
    {
        $product = new Product($attributes);

        /** Set product status */
        if (array_key_exists('enabled', $attributes)) {
            $product->setEnabled($attributes['enabled']);
        }

        /** Create product in database in order to proceed to following steps */
        $product->save();

        /** Update product's images */
        $product->uploadImages($images);

        /** Assign categories to product */
        if (array_key_exists('category_ids', $attributes)) {
            $product->assignCategories($attributes['category_ids']);
        }

        /** Assign variants to product */
        if (array_key_exists('variants', $attributes)) {
            $product->assignVariants($attributes['variants']);
        }

        if ($product->relationLoaded('variants')) {
            $product->initializeStocks($product->variants, $attributes['vendor'] ?? null);
        }

        return $product;
    }

    /**
     * @param Product $product
     * @param array $attributes
     * @param array $images
     * @param array $uploadedImages
     * @param array $deleteImages
     * @return bool
     * @throws Exception
     */
    public function update(Product $product,
        array $attributes = [],
        array $images = [],
        array $uploadedImages = [],
        array $deleteImages = []
    )
    {
        if (!isset($product->id)) return false;

        if (array_key_exists('enabled', $attributes)) {
            $product->setEnabled($attributes['enabled']);
        }

        if (count($uploadedImages)) {
            $product->sortImage($uploadedImages);
        }

        if (count($deleteImages)) {
            $product->deleteImages($deleteImages);
        }

        if (count($images)) {
            $product->uploadImages($images);
        }

        if (array_key_exists('category_ids', $attributes)) {
            $product->assignCategories($attributes['category_ids']);
        }

        if (array_key_exists('variants', $attributes)) {
            $product->syncVariants($attributes['variants']);
        }

        return $product->fill($attributes)->save();
    }

    /**
     * Create stock with given variant combination and others attribute
     *1
     * @param Product $product
     * @param array $attribute
     * @return ProductStock
     */
    public function createStock(Product $product, $attribute = []): ProductStock
    {
        /** @var ProductStock $stock */
        $stock = $product->stocks()->make($attribute);

        if (array_key_exists('enabled', $attribute)) {
            $stock->setEnabled($attribute['enabled']);
        }

        if (array_key_exists('vendor', $attribute)) {
            $stock->vendor()->associate($attribute['vendor']);
        }

        $stock->save();

        if (array_key_exists('product_variant_option_ids', $attribute)) {
            $stock->combinations()->sync($attribute['product_variant_option_ids']);
        }

        return $stock;
    }

    /**
     * Update existing product's stock
     * @param ProductStock $stock
     * @param array $attribute
     * @return bool
     */
    public function updateStocks(ProductStock $stock, $attribute = []): bool
    {
        if (array_key_exists('enabled', $attribute)) {
            $stock->setEnabled($attribute['enabled']);
        }

        if (array_key_exists('vendor_id', $attribute)) {
            $stock->vendor()->associate($attribute['vendor_id']);
        }

        if (array_key_exists('price', $attribute)) {
            $stock->price = $attribute['price'];
        }

        if (array_key_exists('cost_price', $attribute)) {
            $stock->cost_price = $attribute['cost_price'];
        }

        return $stock->save();
    }

    /**
     * @param Product $product
     * @param $ids
     * @return bool
     * @throws ModelHasAssociate
     */
    public function deleteStocks(Product $product, $ids)
    {
        if ($product->inventories()->count()) {
            throw new ModelHasAssociate(__('product.stock.delete.failed'));
        }

        $result = true;

        try {
            if (is_array($ids)) {
                $stocks = $product->stocks()->whereKey($ids)->get();

                foreach ($stocks as $stock) {
                    /** @var ProductStock $stock */
                    $stock->combinations()->detach();
                    $stock->notifyQueue()->delete();
                    $stock->queuers()->delete();

                    $stock->delete();
                }
            }
            else {
                /** @var ProductStock $stock */
                $stock = $product->stocks()->find($ids);
                $stock->combinations()->detach();
                $stock->notifyQueue()->delete();
                $stock->queuers()->delete();

                $stock->delete();
            }
        }
        catch (Exception $exception) {
            $result = false;

            Log::warning('Failed to delete product stock', [
                'message' => $exception->getMessage()
            ]);
        }

        return $result;
    }
}
