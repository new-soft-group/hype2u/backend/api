<?php


namespace App\Services;

use App\Events\ShopOrderConfirmationEvent;
use App\Models\Guest;
use App\Models\Order;
use App\Models\User;
use Laravel\Cashier\Exceptions\PaymentActionRequired;
use Stripe\PaymentIntent;

class CheckoutService
{
    public function dollarToCent($amount)
    {
        return (float)$amount * 100;
    }

    public function centToDollar(int $amount)
    {
        return $amount / 100;
    }

    public function isValidForFpxMethod(string $currency)
    {
        return $currency === 'MYR';
    }

    /**
     * @param Order $order
     * @param array $method
     * @throws PaymentActionRequired
     * @throws \Laravel\Cashier\Exceptions\PaymentFailure
     * @throws \Laravel\Cashier\Exceptions\CustomerAlreadyCreated
     */
    public function shopCheckout(Order $order, array $method)
    {
        if($order->user->hasPaymentMethod()){
            $invoice = $order->user->invoiceFor('Order Charges', amountToCent($order->converted_amount))
                ->asStripeInvoice();

            $chargeInvoice = $order->newInvoice($invoice);

            $order->setRelation('invoice', $order->invoice()->save($chargeInvoice));

        }else{
            $order->user->createAsStripeCustomer();
            $order->user->updateDefaultPaymentMethod($method['id']);
            $invoice = $order->user->invoiceFor('Order Charges', amountToCent($order->converted_amount))
                ->asStripeInvoice();

            $chargeInvoice = $order->newInvoice($invoice);

            $order->setRelation('invoice', $order->invoice()->save($chargeInvoice));
        }

        event(new ShopOrderConfirmationEvent($order));
    }
}
