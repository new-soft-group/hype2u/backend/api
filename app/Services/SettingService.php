<?php

namespace App\Services;

use App\Models\Setting;
use Cache;

class SettingService
{
    public function update(array $settings): bool {
        foreach ($settings as $setting) {
            Setting::where('key', $setting['key'])->update(['value' => $setting['value']]);
        }
        
        Cache::forget('settings');
        
        return true;
    }
}
