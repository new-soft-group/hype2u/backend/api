<?php


namespace App\Services;


use App\Gateways\PaymentGateway;
use App\Models\Coupon;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class CouponService
{
    /**
     * @var PaymentGateway
     */
    private $gateway;

    public function __construct(PaymentGateway $gateway)
    {
        $this->gateway = $gateway;
    }

    /**
     * @param string $name
     * @param string $code
     * @param string $duration
     * @param array $plan_ids
     * @param float $amount
     * @param string $currency
     * @param array $options
     * @return Coupon
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function create(
        string $name,
        string $code,
        string $duration,
        array $plan_ids,
        float $amount,
        string $currency = '',
        array $options = []): Coupon
    {
        $coupon = new Coupon();

        $coupon->setName($name)
               ->setCode($code)
               ->setDuration($duration)
               ->setEnabled($options['enabled'] ?? true)
               ->setDiscount($amount, $currency)
               ->setOptions(Arr::except($options, 'enabled'));

        $stripeCoupon = $this->gateway->coupons->create($coupon->getStripeOptions());

        $coupon->setStripeId($stripeCoupon->id)
               ->save();

        $coupon->setPlans($plan_ids)->save();

        return $coupon;
    }

    public function update(Coupon $coupon, array $attributes)
    {
       if(array_key_exists('plan_ids', $attributes)){
           $ids = [];
           foreach ($attributes['plan_ids'] as $plan){
               if (is_array($plan)){
                   if (array_key_exists('id', $plan)) array_push($ids, $plan['id']);
               }else{
                   array_push($ids, $plan);
               }
           }
           $coupon->setPlans($ids);
       }

       if(array_key_exists('name', $attributes)) {
           $coupon->setName($attributes['name']);
       }

       if(array_key_exists('code', $attributes)) {
           $coupon->setCode($attributes['code']);
       }

       if(array_key_exists('enabled', $attributes)) {
           $coupon->setEnabled($attributes['enabled']);
       }

       if($coupon->isDirty()) {

           if($coupon->isDirty('name')) {
               $this->gateway->coupons->update($coupon->stripe_id, [
                   'name' => $coupon->name
               ]);
           }

           $coupon->save();
       }

       return $coupon;
    }

    /**
     * @param Coupon $coupon
     * @return bool|null
     * @throws \Exception
     */
    public function delete(Coupon $coupon): bool
    {
        $this->gateway->coupons->delete($coupon->stripe_id);

        if(empty($coupon->invoices()->count())) {
            return $coupon->forceDelete();
        }

        $coupon->setEnabled(false)->save();

        return $coupon->delete();
    }
}
