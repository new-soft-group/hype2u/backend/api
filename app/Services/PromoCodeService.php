<?php


namespace App\Services;

use App\Models\Promotion;

class PromoCodeService
{
    public function create($name, array $data): Promotion
    {
        $model = new Promotion();
        
        $promotion = $model->create(['name' => $name]);
        
        $promotion->setDiscountCode($data);
        
        return $promotion;
    }
    
    public function update(Promotion $promotion, array $data = []): bool
    {
        $promotion->updateDiscountCode($data);
    
        $promotion->fill($data);
        
        return $promotion->save();
    }
}
