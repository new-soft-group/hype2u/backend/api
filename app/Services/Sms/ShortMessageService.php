<?php


namespace App\Services\Sms;


use App\Contracts\Sms\SmsContract;
use GuzzleHttp\Client;

class ShortMessageService implements SmsContract
{
    /**
     * @var Client
     */
    private $client;
    /**
     * @var string
     */
    private $username;
    /**
     * @var string
     */
    private $password;
    /**
     * @var string
     */
    private $from;

    public function __construct(
        Client $client,
        string $username,
        string $password,
        string $from
    )
    {
        $this->client = $client;
        $this->username = $username;
        $this->password = $password;
        $this->from = $from;
    }

    public function send(string $mobile, string $content)
    {
        $disabledSend = $this->username == 'none' || $this->password == 'none' || $this->from == 'none';
        if($disabledSend) return;

        $this->client->post('sendsms', [
            'form_params' => [
                'gw-username' => $this->username,
                'gw-password' => $this->password,
                'gw-from' => $this->from,
                'gw-to' => $mobile,
                'gw-text' => $content
            ]
        ]);
    }
}
