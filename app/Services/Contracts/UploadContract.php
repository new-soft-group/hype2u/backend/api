<?php


namespace App\Services\Contracts;

interface UploadContract
{
    /**
     * @param $disk
     * @param array $uploaded
     * @return mixed
     */
    public function upload($disk, array $uploaded);
    
    /**
     * @param $disk
     * @param array $paths
     * @return mixed
     */
    public function delete($disk, array $paths);
}
