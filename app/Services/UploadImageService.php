<?php

namespace App\Services;

use App\Models\Image;
use App\Services\Contracts\UploadContract;
use Illuminate\Database\Eloquent\Collection;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class UploadImageService implements UploadContract
{
    /**
     * @var Image
     */
    private $model;

    /**
     * UploadImageService constructor.
     * @param Image $model
     */
    public function __construct(Image $model) {
        $this->model = $model;
    }

    /**
     * Upload image to file system
     *
     * @param $disk
     * @param array $uploaded
     * @return Collection
     */
    public function upload($disk, array $uploaded): Collection {
        $collect = new Collection();

        foreach ($uploaded as $image) {
            if($image instanceof UploadedFile) {
                $imageSize = getimagesize($image);

                $model = $this->model->newInstance([
                    'path' => Storage::disk($disk)->put('/', $image),
                    'width' => $imageSize[0],
                    'height' => $imageSize[1]
                ]);

                $collect->push($model);
            }
        }

        return $collect;
    }

    /**
     * @param $disk
     * @param array $paths
     * @return bool
     */
    public function delete($disk, $paths): bool {

        $images = $this->images()->pluck('path')->toArray();
        $this->uploadImageService->delete('product', $paths);
        $this->images()->delete();

        $result = true;

        foreach ($paths as $path) {
            if(!Storage::disk($disk)->delete($path)) {
                $result = false;
            }
        }

        return $result;
    }
}
