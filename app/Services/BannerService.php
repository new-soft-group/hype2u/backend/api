<?php

namespace App\Services;

use App\Models\Banner;
use Illuminate\Database\Eloquent\Collection;

class BannerService
{
    /**
     * Create banners
     *
     * @param array $attributes
     * @return Collection
     */
    public function create(array $attributes): Collection
    {
        $result = new Collection();
        
        foreach ($attributes as $attribute) {
            $banner = new Banner($attribute);
            $banner->save();
            
            if(array_key_exists('image', $attribute)) {
                $banner->uploadImage($attribute['image']['file'], $attribute['image']['sort']);
            }
            
            $result->push($banner);
        }
        
        $result->load('position');
        
        return $result;
    }
    
    
    /**
     * Update banners
     *
     * @param array $attributes
     * @return bool
     */
    public function update(array $attributes)
    {
        foreach ($attributes as $attribute) {
            if(!array_key_exists('id', $attribute)) continue;
    
            $model = Banner::find($attribute['id']);
            $model->fill($attribute);
            $model->save();
        }
        
        return true;
    }
    
    public function delete(array $ids)
    {
        foreach ($ids as $id) {
            $banner = Banner::find($id);
            
            if(is_null($banner)) continue;
    
            try {
                $banner->deleteImage();
    
                $banner->delete();
                
            } catch (\Exception $e) {
                \Log::warning('Failed to delete banner', ['id' => $id]);
            }
        }
        
        return true;
    }
}
