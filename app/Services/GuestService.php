<?php


namespace App\Services;


use App\Models\Guest;
use App\Models\Profile;

class GuestService
{
    /**
     * @param array $attribute
     * @return Guest|\Illuminate\Database\Eloquent\Model
     */
    public function create(array $attribute): Guest {
        $guest = Guest::make($attribute);
        $guest->save();
    
        /** @var Profile $profile */
        $guest->profile()
              ->make($attribute['profile'])
              ->save();
        
        return $guest;
    }
}
