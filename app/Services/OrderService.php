<?php


namespace App\Services;

use App\Events\OrderConfirmationEvent;
use App\Exceptions\InsufficientStockException;
use App\Exceptions\OutOfStockException;
use App\Exceptions\ShippingAddressNotFound;
use App\Models\BusinessType;
use App\Models\CartItem;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStatus;
use App\Models\Pick;
use App\Models\PickStatus;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\User;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Laravel\Cashier\Exceptions\PaymentActionRequired;
use App\Options\OrderOptions as Options;

class OrderService
{
    /**
     * Create customer order
     *
     * @param User $user
     * @param \Illuminate\Support\Collection|Collection $items
     * @param Options $option
     * @param int $type
     * @param string $orderType
     * @return Order|Model
     * @throws InsufficientStockException
     * @throws OutOfStockException
     * @throws ShippingAddressNotFound
     * @throws \App\Exceptions\DuplicateTimeslotException
     */
    public function create(User $user, $items, Options $option, $type = BusinessType::RENT, $orderType = Order::ORDER_USER): Order
    {
        if(is_null($option->getShippingAddress()) && is_null($user->profile->defaultShippingAddress)) {
            throw new ShippingAddressNotFound();
        }
        /**
         * Ensure all the required items have stock available
         * before proceed to create order
         */
        $orderItems = $this->reserveItems($items);
        /** @var Order $order
         * Create new order
         */
        $order = $user->orders()->make();
        $order->setCurrency($option->getCurrency())
              ->setStatus(OrderStatus::PENDING)
              ->setBusinessType($type)
              ->save();
        /**
         * Override user default shipping address if arg 'address' has value
         */
        $order->newShippingAddress($option->getShippingAddress(), $user->profile->defaultShippingAddress);
        /**
         * Create new order's items record and set the relationship
         * to prevent further query
         */
        $order->setRelation('items', $order->items()->saveMany($orderItems));
        /**
         * Setup extra options during order creation
         */
        if($type == BusinessType::RENT) $this->attachTimeslot($option, $order);

        try {
            /**
             * Create new surcharge from user default payment method
             * This will result an exception if user don't have default payment method
             * This action will not require any interaction from user
             * as it will charge automatically on behalf of user
             */
            if($type == BusinessType::RENT && !empty($option->getCharge())) {
                $invoice = $this->processCharges($user, $option->getCharge());

                $chargeInvoice = $order->newInvoice($invoice);

                $order->setRelation('invoice', $order->invoice()->save($chargeInvoice));
            }

            /** Send notification to user after order confirmed */
            if ($type == BusinessType::RENT) event(new OrderConfirmationEvent($order));

            /** Empty cart after checkout success */
            if($orderType == Order::ORDER_USER) $type == BusinessType::RENT ? $user->cart->checkout() : $user->shopCart->checkout();
        }
        catch (Exception $exception) {
            /**
             * Mark order as failed if any exception throw during the charges process
             */
            $order->exceptionOccur($exception->getMessage());
        }
        finally {
            return $order;
        }
    }

    /**
     * Create customer order on behalf
     *
     * @param User $user
     * @param \Illuminate\Support\Collection|Collection $items
     * @param Options $option
     * @param $orderType
     * @return Order|Model
     * @throws InsufficientStockException
     * @throws OutOfStockException
     * @throws ShippingAddressNotFound
     * @throws \App\Exceptions\DuplicateTimeslotException
     */
    public function createOnBehalf(User $user, $items, Options $option, $orderType): Order
    {
        return $this->create($user, $items, $option, BusinessType::RENT, $orderType);
    }

    /**
     * @param \Illuminate\Support\Collection|Collection $items
     * @return Collection
     * @throws OutOfStockException
     * @throws InsufficientStockException
     */
    public function reserveItems($items): Collection
    {
        $collection = new Collection();

        try {
            /** @var CartItem $item */
            foreach ($items as $item) {
                /** @var Product $product
                 * Query ordered product and filter by 'enabled' true
                 */
                $product = Product::filterByEnabled(true)
                                  ->with('stocks')
                                  ->firstWhere('id', $item->product_id);
                /** @var ProductStock $stock
                 * Query ordered product's stock
                 */
                $stock = $product->stocks->firstWhere('id', $item->product_stock_id);

                /** If product stock price exist then use product stock price else product price */
                $price = $stock->count() && $stock->price > 0 ? $stock->price : $product->price;

                /**
                 * Prepare order's item
                 */
                $instance = new OrderItem();
                $instance->product()->associate($product);
                $instance->unit_price = $price;
                /**
                 * allocateStock() will try to reserved stock
                 * inventory by using atomic lock to limit concurrency
                 */
                $instance->allocateStock($stock, $item->quantity);
                /**
                 * Set variant combination name and price
                 */
                $instance->setVariant($stock);

                $collection->push($instance);
            }

            return $collection;
        }
        catch (Exception $exception) {
            /**
             * Recover inventory reservation
             */
            $collection->each(function (OrderItem $item) {
                $item->stock->unreserved($item->quantity);
            });

            throw $exception;
        }
    }

    /**
     * @param User $user
     * @param float $amount
     * @return \Stripe\Invoice
     * @throws PaymentActionRequired
     * @throws \Laravel\Cashier\Exceptions\PaymentFailure
     */
    public function processCharges(User $user, float $amount)
    {
        return $user->invoiceFor('Order Surcharge', amountToCent($amount))
                    ->asStripeInvoice();
    }

    /**
     * @param $option
     * @param Order $order
     * @throws \App\Exceptions\DuplicateTimeslotException
     */
    public function attachTimeslot($option, Order $order)
    {
        if(!empty($option->getTimeslot()) && !empty($option->getTimeslotDate())) {
            $order->timeslot($option->getTimeslot(), $option->getTimeslotDate());
        }
    }

    public function closePickRequest(Order $order)
    {
        $user = $order->user;

        $pick = $user->picks()->whereIn('status', [PickStatus::PENDING, PickStatus::ATTENDED]);

        if($pick->count()) {
            $pick->get()->map(function (Pick $pick){
                $pick->setClose();
            });
        }
    }

    public function updateDefaultShippingAddress(User $user)
    {
        // Check if user has address without assigned in profile default addresses. If exist, update to default
        if(!$user->profile->shipping_address_id && $user->profile->addresses->count() == 1 ){
            $user->profile->shipping_address_id = $user->profile->addresses->first()->id;

            return $user->profile->save();
        }

        return false;
    }
}
