<?php


namespace App\Services;

use App\Events\NewPickReceivedEvent;
use App\Events\UserPickSubmittedEvent;
use App\Exceptions\PickExistException;
use App\Models\Pick;
use App\Models\PickStatus;
use App\Models\User;

class PickService
{
    public function create(User $user, array $attributes)
    {
        // Check if user has pending picks
        if($this->validatePickRequest($user)) throw new PickExistException();

        $pick = $user->picks()->create([
            'gender_id' => $attributes['gender_id'],
            'sizes' => $this->transformArrayToCommaString($attributes['sizes']),
            'styles' => $this->transformArrayToCommaString($attributes['styles']),
            'fits' => $this->transformArrayToCommaString($attributes['fits']),
            'brands' => $this->transformArrayToCommaString($attributes['brands']),
            'colors' => $this->transformArrayToCommaString($attributes['colors']),
            'status' => PickStatus::PENDING,
        ]);

        $pick->load('gender', 'user');

        // Send notification to admin and customer
        event(new NewPickReceivedEvent($pick));
        event(new UserPickSubmittedEvent($pick));

        return $pick;
    }

    public function update(Pick $pick, array $attributes)
    {
        $pick->remark = $attributes['remark'];

        return $pick->save();
    }

    public function transformArrayToCommaString(array $array)
    {
        return join(",", $array);
    }

    public function validatePickRequest(User $user)
    {
        if($user->picks()->whereIn('status', [PickStatus::PENDING, PickStatus::ATTENDED])->count()) return true;

        return false;
    }
}
