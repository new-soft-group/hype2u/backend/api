<?php

namespace App\Services;

use App\Models\PaymentSummary;

class PaymentSummaryService
{
    /**
     * @param array $input
     * @return PaymentSummary
     */
    public function create(array $input): PaymentSummary
    {
        return PaymentSummary::create($input);
    }

}
