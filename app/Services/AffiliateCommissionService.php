<?php

namespace App\Services;

use App\Models\Affiliate;
use App\Models\AffiliateSale;
use App\Models\Order;

class AffiliateCommissionService
{
    public function createSalesRecord(Affiliate $affiliate, Order $order)
    {
        $affiliate->refreshNode();
        
        if($affiliate->isRoot()) {
    
            foreach ($order->items as $item) {
                $affiliate->sales()->create([
                    'order_id' => $order->id,
                    'commission' => $affiliate->commission * $item->quantity
                ]);
            }
        }
        else {
            $members = $affiliate->ancestors()->get();
    
            /** Maximum commission contribute */
            $commissionPool = $members->first()->commission;
    
            $members = $members->push($affiliate)->reverse();
            
            foreach ($order->items as $item) {
    
                foreach ($members as $member) {
                    $commissionPool -= $member->commission;
                    $takenFromPool = $member->commission;
        
                    /** When given commission is larger than pool amount */
                    if($commissionPool < 0) {
                        /** Calculate the possible amount able to get from poll */
                        $takenFromPool = max($member->commission - abs($commissionPool), 0);
                    }
        
                    $sales = new AffiliateSale();
                    $sales->order_id = $order->id;
                    $sales->commission = $takenFromPool * $item->quantity;
                    $sales->by_id = $member->id !== $affiliate->id ? $affiliate->id : null;
                    
                    /** @var Affiliate $member */
                    $member->sales()->save($sales);
                }
            }
        }
    }
}
