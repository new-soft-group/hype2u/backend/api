<?php

namespace App\Services;

use App\Models\Affiliate;
use App\Models\DiscountCode;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class AffiliateService
{
    public function create(User $user, array $data = []): Affiliate
    {
        /** @var Affiliate $affiliate */
        $affiliate = new Affiliate();
        
        $affiliate->user()->associate($user);
        $affiliate->parent_id = $data['parent_id'] ?? null;
        $affiliate->commission = $data['commission'] ?? 0;
        $affiliate->save();
        
        $affiliate->setDiscountCode($data['discount_code'] ?? []);
        
        return $affiliate;
    }
    
    /**
     * @param User $user
     * @param string $code
     * @return Affiliate
     * @throws \Exception
     */
    public function createByCode(User $user, string $code): Affiliate
    {
        $discountCode = DiscountCode::findByCode($code);;
        
        if(is_null($discountCode)) throw new \Exception('Code not exist');
    
        /** @var Affiliate $parent */
        $parent = $discountCode->discountable;
        
        /** @var Affiliate $affiliate */
        $affiliate = new Affiliate();
        $affiliate->user()->associate($user);
        $affiliate->parent_id = $parent->id;
        $affiliate->save();

        $affiliate->setDiscountCode([]);
        
        return $affiliate;
    }
    
    /**
     * @param Affiliate|Model $affiliate
     * @param array $data
     * @return bool
     */
    public function update(Affiliate $affiliate, array $data)
    {
        if(array_key_exists('parent_id', $data)) {
            $parentId = $data['parent_id'];
            
            if(isset($affiliate->parent_id) && $parentId) {
                /** Can't assign self as direct member */
                if($affiliate->id !== $parentId) {
                    $this->assignToParent($affiliate, Affiliate::findOrFail($parentId));
                }
            }
            else {
                $this->removeFromParent($affiliate);
            }
        }
    
        if(array_key_exists('discount_code', $data)) {
            $this->updateDiscountCode($affiliate, $data['discount_code']);
        }
        
        if(array_key_exists('commission', $data)) {
            $affiliate->commission = $data['commission'];
            return $affiliate->save();
        }
        
        return true;
    }
    
    /**
     * @param Affiliate|Model $member
     * @param Affiliate $parent
     * @return bool
     */
    private function assignToParent(Affiliate $member, Affiliate $parent)
    {
        return $member->appendToNode($parent)->save();
    }
    
    private function removeFromParent(Affiliate $member)
    {
        $member->parent_id = null;
        
        return $member->save();
    }
    
    public function updateDiscountCode(Affiliate $affiliate, array $data = []): bool
    {
        return $affiliate->updateDiscountCode($data);
    }
    
    public function updateReferralCode(Affiliate $affiliate, string $code): bool
    {
        if(empty($code)) return false;
        
        return $affiliate->discountCode()->update(['code' => $code]);
    }
    
    public function updateCommission(Affiliate $affiliate, float $commission): bool
    {
        $affiliate->commission = $commission;
        
        return $affiliate->save();
    }
    
    public function markSalesAsPaid(Affiliate $affiliate, Carbon $date, string $referenceNo): bool
    {
        if(is_null($date)) new \Exception('Date cant be null');
        
        return $affiliate->sales()
                         ->whereMonth('created_at', $date->month)
                         ->whereYear('created_at', $date->year)
                         ->update(['paid' => 1, 'reference_no' => $referenceNo]);
    }
}
