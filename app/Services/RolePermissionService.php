<?php

namespace App\Services;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;
use Spatie\Permission\Models\Role;

class RolePermissionService
{
    public function getRoles(): Collection {
        return Role::all();
    }
    
    public function getAllRoleWithPermissions(): Collection {
        return Role::with('permissions')->get();
    }
    
    public function updateRolePermissions(array $rolePermissions): bool {
        foreach ($rolePermissions as $_rolePermissions) {
            $role = Role::findByName($_rolePermissions['name'], 'web');
            $permissions = Arr::pluck($_rolePermissions['permissions'], 'id');
            $role->syncPermissions($permissions);
        }
        
        return true;
    }
}
