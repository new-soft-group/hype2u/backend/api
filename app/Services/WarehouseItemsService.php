<?php

namespace App\Services;

use App\Models\WarehouseItems;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;

class WarehouseItemsService
{
    /**
     * Get Warehouse Items Record
     */
    public function getRecord($warehouseId, $productStockId)
    {
        $result = WarehouseItems::where('warehouse_id', $warehouseId)->where('product_stock_id', $productStockId)->get();
        return $result;
    }

     /**
     * Create Warehouse Items Record
     */
    public function create($warehouseId, $productStockId)
    {
        $warehouseItems= new WarehouseItems();
        $warehouseItem = $warehouseItems->make();

        $warehouseItem->warehouse()
                    ->associate($warehouseId)
                    ->stock()
                    ->associate($productStockId)
                    ->save();
                    
        return $warehouseItem;
    }

    /**
     * Update Warehouse Items's Warehouse
     */
    public function updateWarehouseItemsWarehouse(WarehouseItems $warehouseItem , string $warehouseId)
    {
        $warehouseItem->setWarehouseId($warehouseId)
            ->save(); 
                    
        return $warehouseItem;
    }
}
