<?php

namespace App\Services;

use App\Models\Category;

class CategoryService
{
    public function create($input): Category
    {
        return Category::create($input);
    }

    public function update($input)
    {
        return Category::massUpdate($input->get('categories'));
    }
}
