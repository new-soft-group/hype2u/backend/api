<?php


namespace App\Validators\Plan\Policy;


use App\Contracts\Rule\PolicyRuleContract;

class PolicyRule extends PolicyRuleContract
{
    public function pass(): bool
    {
        return $this->getTarget()->totalQuantity() <= $this->getPolicy()->quantity;
    }
}
