<?php


namespace App\Validators\Plan\Policy;


use App\Collections\CartItemCollection;
use App\Collections\PolicyCollection;
use App\Collections\PolicyRuleCollection;
use App\Models\PlanPolicy;
use Illuminate\Support\Collection;

class PolicyValidator
{
    /**
     * @var PolicyCollection
     */
    protected $policies;
    /**
     * @var CartItemCollection
     */
    protected $items;
    /**
     * @var PolicyRuleCollection
     */
    protected $rules = null;
    /**
     * @var Collection
     */
    protected $itemsWithoutRule = null;

    /**
     * PolicyValidator constructor.
     * @param Collection|PolicyCollection $policies
     * @param Collection|CartItemCollection $items
     */
    public function __construct($policies, $items)
    {
        $this->policies = $policies;
        $this->items = $items;
        $this->rules = new PolicyRuleCollection();
        $this->itemsWithoutRule = new Collection();
    }

    protected function distributeRules()
    {
        $this->policies->each(function (PlanPolicy $policy) {
            $items = $this->items->filterByType($policy->targetIds());

            if($items->isNotEmpty()) {
                $this->rules->push(new PolicyRule($policy, $items));
            }
        });

        $withoutRule = $this->items->excludeTypeIn($this->rules->allTargetIds());

        $this->itemsWithoutRule->push(...$withoutRule);
    }

    public function hasItemWithoutRule()
    {
        return $this->itemsWithoutRule->isNotEmpty();
    }

    public function validate(): bool
    {
        $result = false;

        $this->distributeRules();

        if($this->rules->isNotEmpty() && !$this->hasItemWithoutRule()) {
            $this->rules->sortByDesc('logical');

            /** @var PolicyRule $rule */
            foreach ($this->rules as $rule) {
                $result = $rule->pass();

                if(!$result) {
                    return false;
                }
                elseif ($rule->isOr()) {
                    return $rule->getTarget()->count() === $this->items->count();
                }
            }
        }

        return $result;
    }

    public function getMessage()
    {
        $message = '';

        /** @var PlanPolicy $policy */
        foreach ($this->policies->sortByDesc('logical') as $policy) {
            $message .= "$policy->quantity ";
            $message .= $policy->rules->implode('name', '/') . " ";

            if($policy->isOr() || $policy->isAnd()) {
                $message .= " $policy->logical". " ";
            }
        }

        return __('plan.policies', ['message' => $message]);
    }
}
