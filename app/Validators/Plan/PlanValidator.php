<?php


namespace App\Validators\Plan;


use App\Models\Plan;
use App\Models\Surcharge;

class PlanValidator
{
    /**
     * @var Plan
     */
    private $plan;
    /**
     * @var int
     */
    private $exchange;
    /**
     * @var float
     */
    private $purchaseAmount;
    /**
     * @var bool
     */
    private $ignore;
    /**
     * @var string
     */
    private $message = '';

    /**
     * PlanValidator constructor.
     * @param Plan $plan
     */
    public function __construct(Plan $plan)
    {
        $this->plan = $plan;
    }

    /**
     * @return Plan
     */
    public function getPlan(): Plan
    {
        return $this->plan;
    }

    public function getPurchaseAmount(): float
    {
        return $this->purchaseAmount;
    }

    /**
     * @param float $amount
     * @return PlanValidator
     */
    public function setPurchaseAmount(float $amount)
    {
        $this->purchaseAmount = $amount;

        return $this;
    }

    public function getExchange(): int
    {
        return $this->exchange;
    }

    /**
     * @param int $exchange
     * @return PlanValidator
     */
    public function setExchange(int $exchange)
    {
        $this->exchange = $exchange;

        return $this;
    }

    public function ignorePurchaseAmount()
    {
        return $this->ignore = true;
    }

    public function isWithinPurchaseAmount(): bool
    {
        if($this->ignore) return true;

        if($this->plan->hasThreshold()) {
            return $this->isWithinThreshold();
        }

        return $this->purchaseAmount <= $this->plan->purchase_amount;
    }

    public function isWithinExchangeLimit(): bool
    {
        if($this->isUnlimitedExchange()) return true;

        return $this->exchange <= $this->plan->exchange_limit;
    }

    public function isWithinThreshold(): bool
    {
        $result = false;

        foreach($this->plan->surcharges as $surcharge) {
            /** @var Surcharge $surcharge */
            $result |= $this->purchaseAmount <= $surcharge->threshold;
        }

        return $result;
    }

    public function isUnlimitedExchange(): bool
    {
        return $this->plan->exchange_limit === 0;
    }

    public function validate(): bool
    {
        $exchangeValid = $this->isWithinExchangeLimit();

        if (!$exchangeValid) {
            $this->setMessage(__('plan.exceed.exchange', [
                'plan' => $this->plan->name,
                'limit' => $this->plan->exchange_limit,
                'count' => $this->exchange
            ]));
        }

        $amountValid = $this->isWithinPurchaseAmount();

        if (!$amountValid) {
            $this->setMessage(__('plan.exceed.amount', [
                'plan' => $this->plan->name,
                'amount' => $this->plan->getThreshold()
            ]));
        }

        return $exchangeValid && $amountValid;
    }

    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
