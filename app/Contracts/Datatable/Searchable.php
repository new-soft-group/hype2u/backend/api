<?php


namespace App\Contracts\Datatable;


use Illuminate\Database\Eloquent\Builder;

interface Searchable
{
    public function searchCriterion(Builder $builder, string $keyword): Builder;

    public function filterCriteria(): array;

    public function sortCriteria(): array;

    public function withRelations(): array;
}

