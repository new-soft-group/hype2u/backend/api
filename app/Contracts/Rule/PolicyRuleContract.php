<?php


namespace App\Contracts\Rule;

use App\Collections\CartItemCollection;
use App\Models\PlanPolicy;

abstract class PolicyRuleContract
{
    /**
     * @var PlanPolicy
     */
    private $policy;
    /**
     * @var CartItemCollection
     */
    private $target;

    /**
     * PolicyRule constructor.
     * @param PlanPolicy $policy
     * @param CartItemCollection $target
     */
    public function __construct(PlanPolicy $policy, CartItemCollection $target)
    {
        $this->policy = $policy;
        $this->target = $target;
    }

    public function getMaxQuantity(): int
    {
        return $this->policy->quantity;
    }

    public function getPolicy(): PlanPolicy
    {
        return $this->policy;
    }

    public function getTarget(): CartItemCollection
    {
        return $this->target;
    }

    public function isOr(): bool
    {
        return $this->policy->isOr();
    }

    public function isAnd(): bool
    {
        return $this->policy->isAnd();
    }

    public function isNone(): bool
    {
        return $this->policy->isNone();
    }

    abstract function pass(): bool;
}
