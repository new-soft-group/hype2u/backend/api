<?php


namespace App\Contracts\Sms;


interface SmsContract
{
    public function send(string $mobile, string $content);
}
