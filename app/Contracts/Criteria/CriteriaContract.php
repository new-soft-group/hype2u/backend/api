<?php


namespace App\Contracts\Criteria;


use Illuminate\Database\Eloquent\Builder;

interface CriteriaContract
{
    public function apply(Builder $builder);
}
