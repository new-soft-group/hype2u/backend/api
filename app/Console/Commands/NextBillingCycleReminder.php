<?php

namespace App\Console\Commands;

use App\Events\NextBillingCycleReminderEvent;
use App\Models\OrderStatus;
use App\Models\Plan;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class NextBillingCycleReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cycle:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Notify plan user for next billing cycle';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        /** Get Delivered Order Statuses Id */
        $orderDeliveredStatusId = OrderStatus::where('name', 'delivered')->first()->id;

        /** Get plan ids which has sms notification*/
        $planIds = Plan::where('sms', true)->pluck('id');

        /** Get subscriptions which under the plan that has sms notification */
        $filteredSubscriptions = Subscription::where('stripe_status', 'active')->whereHas('plan', function(Builder $builder) use ($planIds){
                                    return $builder->whereIn('id',$planIds);
                                })->get();

        /** For each subscription get user and check for notifyNextBillingCycleDate and orders that haven't return*/
        foreach($filteredSubscriptions as $subscription){
            $user = $subscription->user;
            $orderNotReturned = $user->orders->where('order_status_id',$orderDeliveredStatusId)->whereNull('returned_at');

            $notifyNextBillingCycleDate = $subscription->notifyNextBillingCycle()->format('Y/m/d');
            $today = Carbon::now()->format('Y/m/d');

            /** Notify user that haven't return order items if notifyNextBillingCycleDate is today */
            if(count($orderNotReturned)>0 && $today == $notifyNextBillingCycleDate){
                event(new NextBillingCycleReminderEvent($subscription));
            }
        }

    }
}
