<?php

namespace App\Console\Commands;

use App\Events\OrderDeliverReminderEvent;
use App\Events\OrderPickUpReminderEvent;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\TimeslotType;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class OrderReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'order:reminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     */
    public function handle()
    {
        /**
         * @var $today
         * Get today date
         */
        $today = Carbon::now()->format('Y/m/d');

        /** Get order status Ids where not pending  */
        $ids = [OrderStatus::APPROVED, OrderStatus::SHIPPING];

        $orders = Order::whereIn('order_status_id',$ids)->with('timeslots')->get();

        foreach ($orders as $order){
            $deliver = $order->timeslots->firstWhere('timeslot_id',TimeslotType::DELIVER);
            $return = $order->timeslots->firstWhere('timeslot_id',TimeslotType::RETURN_BACK);

            if($deliver){
                /** Get date */
                $deliverDate = $deliver->start_at->format('Y/m/d');

                if($deliverDate == $today) event(new OrderDeliverReminderEvent($order));
            }

            if($return){
                /** Get date */
                $returnDate = $return->start_at->format('Y/m/d');
                if($returnDate == $today) event(new OrderPickUpReminderEvent($order));
            }
        }

    }
}
