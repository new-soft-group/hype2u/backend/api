<?php


namespace App\Options;


use App\Models\Address;
use App\Rules\AllowedCurrencies;
use Illuminate\Support\Str;

class OrderOptions
{
    /**
     * @var string
     */
    protected $currency = AllowedCurrencies::MYR;
    /**
     * @var int
     */
    protected $charge = 0;
    /**
     * @var Address|null
     */
    protected $shippingAddress = null;
    /**
     * @var int|string|null
     */
    protected $timeslot = null;
    /**
     * @var string|null
     */
    protected $timeslotDate = null;

    public function __construct(array $option = [])
    {
        foreach ($option as $key => $value) {
            $method = "set".ucfirst(Str::camel($key));

            if(method_exists($this, $method)) {
                $this->{$method}($value);
            }
        }
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param string $currency
     */
    public function setCurrency(string $currency): void
    {
        $this->currency = $currency;
    }

    /**
     * @return int
     */
    public function getCharge(): int
    {
        return $this->charge;
    }

    /**
     * @param int $charge
     */
    public function setCharge(int $charge): void
    {
        $this->charge = $charge;
    }

    /**
     * @return Address|null
     */
    public function getShippingAddress(): ?Address
    {
        return $this->shippingAddress;
    }

    /**
     * @param array|null $shippingAddress
     */
    public function setShippingAddress(?array $shippingAddress): void
    {
        if(!is_null($shippingAddress)) {
            $this->shippingAddress = new Address($shippingAddress);
        }
    }

    /**
     * @return int|string|null
     */
    public function getTimeslot()
    {
        return $this->timeslot;
    }

    /**
     * @param int|string|null $timeslot
     */
    public function setTimeslot(int $timeslot): void
    {
        $this->timeslot = $timeslot;
    }

    /**
     * @return string|null
     */
    public function getTimeslotDate(): ?string
    {
        return $this->timeslotDate;
    }

    /**
     * @param string|null $timeslotDate
     */
    public function setTimeslotDate(string $timeslotDate): void
    {
        $this->timeslotDate = $timeslotDate;
    }
}
