<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Profile
 *
 * @property int $id
 * @property string $profileable_type
 * @property int $profileable_id
 * @property string|null $first_name
 * @property string|null $last_name
 * @property string|null $mobile
 * @property \Illuminate\Support\Carbon|null $dob
 * @property int|null $gender_id
 * @property int|null $size_id
 * @property int|null $shipping_address_id
 * @property int|null $billing_address_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Address[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \App\Models\Address|null $defaultBillingAddress
 * @property-read \App\Models\Address|null $defaultShippingAddress
 * @property-read \App\Models\Gender|null $gender
 * @property-read mixed $name
 * @property-read \App\Models\VariantOption|null $option
 * @property-read Model|\Eloquent $profileable
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Profile newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Profile newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Profile query()
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereBillingAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereDob($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereGenderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereMobile($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereProfileableId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereProfileableType($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereShippingAddressId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereSizeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Profile whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Profile extends Model
{
    use HasFactory;

    protected $fillable = [
        'first_name',
        'last_name',
        'mobile',
        'dob',
        'gender_id',
        'size_id'
    ];

    protected $dates = ['dob'];

    public function getNameAttribute()
    {
        return "{$this->first_name} {$this->last_name}";
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function option()
    {
        return $this->belongsTo(VariantOption::class, 'size_id', 'id');
    }

    public function profileable()
    {
        return $this->morphTo('profileable');
    }

    public function addresses()
    {
        return $this->morphMany(Address::class, 'addressable');
    }

    public function defaultShippingAddress()
    {
        return $this->belongsTo(Address::class, 'shipping_address_id', 'id');
    }

    public function defaultBillingAddress()
    {
        return $this->belongsTo(Address::class, 'billing_address_id', 'id');
    }

    public function isDefaultShippingAddress(Address $address)
    {
        return $this->shipping_address_id === $address->id;
    }

    public function isDefaultBillingAddress(Address $address)
    {
        return $this->billing_address_id === $address->id;
    }

    public function unsetShippingAddress()
    {
        $this->shipping_address_id = null;
    }

    public function unsetBillingAddress()
    {
        $this->billing_address_id = null;
    }
}
