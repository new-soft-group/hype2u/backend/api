<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PickStatus extends Model
{
    const PENDING = 'pending';
    const ATTENDED = 'attended';
    const CLOSE = 'close';
    const REJECTED = 'rejected';
}
