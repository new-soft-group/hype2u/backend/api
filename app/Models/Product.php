<?php

namespace App\Models;


use App\Contracts\Datatable\Searchable;
use App\Exceptions\ModelHasAssociate;
use App\Traits\Product\ExtraFeature;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Collection;
use Str;


/**
 * App\Models\Product
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property string $sku
 * @property string $slug
 * @property int $gender_id
 * @property int $brand_id
 * @property int $product_type_id
 * @property float $price
 * @property float $cost_price
 * @property bool $featured
 * @property bool $enabled
 * @property string|null $available_on
 * @property bool $most_popular
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property bool $new_arrival
 * @property-read \App\Models\Brand $brand
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Category[] $categories
 * @property-read int|null $categories_count
 * @property-read \App\Models\Gender $gender
 * @property-read mixed $disk
 * @property-read mixed $pricing
 * @property-read \App\Models\Image|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductVariantOption[] $options
 * @property-read int|null $options_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\OrderItem[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductStock[] $stocks
 * @property-read int|null $stocks_count
 * @property-read \App\Models\ProductType $type
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductVariant[] $variants
 * @property-read int|null $variants_count
 * @method static Builder|Product byCategory($category)
 * @method static Builder|Product enabled($state = true)
 * @method static Builder|Product filterByBrand($brandId)
 * @method static Builder|Product filterByCategories($categoriesId)
 * @method static Builder|Product filterByEnabled($enabled)
 * @method static Builder|Product filterByFeatured($featured)
 * @method static Builder|Product filterByGender($genderId)
 * @method static Builder|Product filterByMostPopular($mostPopular)
 * @method static Builder|Product filterByNewArrival($newArrival)
 * @method static Builder|Product filterByPrice($price)
 * @method static Builder|Product filterByType($typeId)
 * @method static Builder|Product filterByVariant($variantOptionId)
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereAvailableOn($value)
 * @method static Builder|Product whereBrandId($value)
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereDescription($value)
 * @method static Builder|Product whereEnabled($value)
 * @method static Builder|Product whereFeatured($value)
 * @method static Builder|Product whereGenderId($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereMostPopular($value)
 * @method static Builder|Product whereName($value)
 * @method static Builder|Product whereNewArrival($value)
 * @method static Builder|Product wherePrice($value)
 * @method static Builder|Product whereCostPrice($value)
 * @method static Builder|Product whereProductTypeId($value)
 * @method static Builder|Product whereSku($value)
 * @method static Builder|Product whereSlug($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductStockInventory[] $inventories
 * @property-read int|null $inventories_count
 */
class Product extends Model implements Searchable
{
    use HasFactory, ExtraFeature;

    const CURRENCIES = ['MYR', 'SGD', 'USD'];

    const DEFAULT_PREFIX = 'SKU';

    protected $with = [
        'gender',
        'brand',
        'type',
        'images',
        'categories',
        'variants',
        'stocks',
        'businessType',
        'type'
    ];

    protected $fillable = [
        'name',
        'description',
        'slug',
        'sku',
        'gender_id',
        'brand_id',
        'product_type_id',
        'price',
        'cost_price',
        'featured',
        'new_arrival',
        'most_popular',
        'available_on',
        'business_type_id'
    ];

    protected $casts = [
        'price' => 'float',
        'cost_price' => 'float',
        'new_arrival' => 'boolean',
        'most_popular' => 'boolean',
        'featured' => 'boolean',
        'enabled' => 'boolean',
    ];

    protected $disk = 'product';

    protected $priceField = 'price';

    protected $appends = ['disk'];

    protected static function booted()
    {
        static::creating(function (Product $product) {
            if(empty($product->sku)) {
                $product->sku = self::DEFAULT_PREFIX . strtoupper(Str::random(8));
            }
            if(empty($product->slug)) {
                $product->slug = Str::slug((string)$product->name, '-');
            }
        });
    }

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where('id', $value)
                    ->orWhere('slug', $value)
                    ->firstOrFail();
    }

    public function getDiskAttribute()
    {
        return $this->disk;
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function brand()
    {
        return $this->belongsTo(Brand::class);
    }

    public function type()
    {
        return $this->belongsTo(ProductType::class, 'product_type_id');
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class, 'business_type_id');
    }

    public static function filterOptions()
    {
        $result = new Collection();

        $categories = Category::withCount('products')->orderBy('name','asc')->get();

        $brands = Brand::withCount('products')->orderBy('name','asc')->get();

        $genders = Gender::withCount('products')->get();

        $types = ProductType::withCount('products')->get();

        $options = [
            'options' => function (HasMany $builder) {
                $builder->withCount('products');
            }
        ];

        $variants = Variant::with($options)
                           ->withCount('products')
                           ->get();

        $result->put('categories', $categories)
               ->put('brands', $brands)
               ->put('genders', $genders)
               ->put('types', $types)
               ->put('variants', $variants);

        return $result;
    }

    public static function getFeaturedByCategories($limit = 18)
    {
        /** Categories ids */
        $categories = Category::all();

        $firstCategory = $categories->splice(0, 1)->first();

        $baseQuery = Product::filterByFeatured(true)
                            ->filterByEnabled(true)
                            ->byCategory($firstCategory->name)
                            ->limit($limit);

        foreach ($categories as $index => $category) {

            $baseQuery->union(Product::filterByFeatured(true)
                                     ->filterByEnabled(true)
                                     ->byCategory($category->name)
                                     ->limit($limit));
        }

        return $baseQuery->get();
    }

    public function orders()
    {
        return $this->hasMany(OrderItem::class);
    }

    /**
     * @return bool|null
     * @throws ModelHasAssociate
     * @throws \Exception
     */
    public function safeDelete()
    {
        if($this->orders()->count()) {
            throw new ModelHasAssociate(__('product.delete.failed'));
        }

        /** @var ProductStock $stock */
        foreach ($this->stocks()->get() as $stock) {
            $stock->combinations()->detach();
            $stock->inventories()->delete();
            $stock->deleteImages();
            $stock->delete();
        }

        $this->options()->delete();
        $this->variants()->delete();
        $this->deleteImages();

        return $this->delete();
    }

    public function getTotalInventoriesCountAttribute()
    {
        $total = 0;

        $stocks = $this->stocks()->get();

        foreach ($stocks as $stock) {
            $total += $stock->inventoryOnHand()->count();
        }

        return $total;
    }

    public function scopeFilterByBrand(Builder $builder, array $brandId)
    {
        return $builder->whereHas(
            'brand',
            function (Builder $builder) use ($brandId) {
                $builder->whereIn('id',$brandId);
            });
    }

    public function scopeFilterByBusinessType(Builder $builder, array $businessTypeId)
    {
        return $builder->whereHas(
            'businessType',
            function (Builder $builder) use ($businessTypeId) {
                $builder->whereIn('id',$businessTypeId);
            });
    }

    public function scopeFilterByCategories(Builder $builder, array $categoriesId)
    {
        return $builder->whereHas(
            'categories',
            function (Builder $builder) use ($categoriesId) {
                $builder->whereIn('category_id',$categoriesId);
            });
    }

    public function scopeFilterByType(Builder $builder, array $typeId)
    {
        return $builder->whereHas(
            'type',
            function (Builder $builder) use ($typeId) {
                $builder->whereIn('id',$typeId);
            });
    }

    public function scopeFilterByGender(Builder $builder, array $genderId)
    {
        return $builder->whereHas(
            'gender',
            function (Builder $builder) use ($genderId) {
                $builder->whereIn('id', $genderId);
            });
    }

    public function scopeFilterByPrice(Builder $builder, array $price)
    {
        return $builder->whereBetween('price', $price);
    }

    public function scopeFilterByNewArrival(Builder $builder, bool $newArrival)
    {
        return $builder->where('new_arrival', $newArrival);
    }

    public function scopeFilterByMostPopular(Builder $builder, bool $mostPopular)
    {
        return $builder->where('most_popular', $mostPopular);
    }

    public function scopeFilterByEnabled(Builder $builder, bool $enabled)
    {
        return $builder->where('enabled', $enabled);
    }

    public function scopeFilterByFeatured(Builder $builder, bool $featured)
    {
        return $builder->where('featured', $featured);
    }

    public function scopeFilterByVariant(Builder $builder, array $variantOptionId)
    {
        return $builder->whereHas(
            'options',
            function (Builder $builder) use ($variantOptionId) {
                $builder->whereIn('variant_option_id', $variantOptionId);
            });
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder->where('name', 'like', $keyword);
    }

    public function filterCriteria(): array
    {
        return [
            'brand' => 'scopeFilterByBrand',
            'business_type' => 'scopeFilterByBusinessType',
            'type' => 'scopeFilterByType',
            'gender' => 'scopeFilterByGender',
            'price' => 'scopeFilterByPrice',
            'status' => 'scopeFilterByEnabled',
            'new_arrival' => 'scopeFilterByNewArrival',
            'most_popular' => 'scopeFilterByMostPopular',
            'variants' => 'scopeFilterByVariant',
            'categories' => 'scopeFilterByCategories'
        ];
    }

    public function sortCriteria(): array
    {
        return [
            'name' => function(Builder $builder, $direction) {
                return $builder->orderBy('name', $direction);
            },
            'created_at' => function(Builder $builder, $direction) {
                return $builder->orderBy('created_at', $direction);
            },
            'price' => function(Builder $builder, $direction) {
                return $builder->orderBy('price', $direction);
            },
            'new_arrival' => function(Builder $builder, $direction) {
                return $builder->orderBy('new_arrival', $direction);
            },
            'most_popular' => function(Builder $builder, $direction) {
                return $builder->orderBy('most_popular', $direction);
            }
        ];
    }

    public function withRelations(): array
    {
        return $this->with;
    }
}
