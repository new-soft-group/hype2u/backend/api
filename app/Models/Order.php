<?php

namespace App\Models;

use App\Collections\ProductStockInventoryCollection;
use App\Contracts\Datatable\Searchable;
use App\Events\ShippingConfirmationEvent;
use App\Events\ShopOrderCompletedEvent;
use App\Events\ShopOrderRejectedEvent;
use App\Events\StockArrival;
use App\Exceptions\InsufficientStockException;
use App\Exceptions\RequiredNumberOfTagException;
use App\Exceptions\StockNotFoundException;
use App\Exceptions\TagNotMatchException;
use App\Exceptions\TagScannedException;
use App\Exceptions\TracksFullException;
use App\Exceptions\TracksQuantityNotMatchException;
use App\Models\Pivots\Track;
use App\Traits\HasInvoice;
use Carbon\Carbon;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;


/**
 * App\Models\Order
 *
 * @property int $id
 * @property int $user_id
 * @property int $order_status_id
 * @property int $business_type_id
 * @property string $currency
 * @property float $currency_rate
 * @property string|null $shipped_at
 * @property string|null $approved_at
 * @property string|null $shipping_at
 * @property string|null $returned_at
 * @property string|null $reason
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $amount
 * @property-read mixed $converted_amount
 * @property-read \App\Models\Invoice|null $invoice
 * @property-read Collection|\App\Models\OrderItem[] $items
 * @property-read int|null $items_count
 * @property-read \App\Models\Address|null $shippingAddress
 * @property-read \App\Models\OrderStatus $status
 * @property-read Collection|\App\Models\Timeslot[] $timeslots
 * @property-read int|null $timeslots_count
 * @property-read ProductStockInventoryCollection|\App\Models\ProductStockInventory[] $tracks
 * @property-read int|null $tracks_count
 * @property-read \App\Models\User $user
 * @method static Builder|Order newModelQuery()
 * @method static Builder|Order newQuery()
 * @method static Builder|Order query()
 * @method static Builder|Order whereCreatedAt($value)
 * @method static Builder|Order whereCurrency($value)
 * @method static Builder|Order whereCurrencyRate($value)
 * @method static Builder|Order whereId($value)
 * @method static Builder|Order whereOrderStatusId($value)
 * @method static Builder|Order whereReason($value)
 * @method static Builder|Order whereReturnedAt($value)
 * @method static Builder|Order whereShippedAt($value)
 * @method static Builder|Order whereUpdatedAt($value)
 * @method static Builder|Order whereUserId($value)
 * @mixin \Eloquent
 */
class Order extends Model implements Searchable
{
    use HasFactory, HasInvoice;

    protected $with = ['status','businessType'];

    protected $casts = [
        'currency_rate' => 'float',
        'order_status_id' => 'int'
    ];

    protected $dates = [
        'shipped_at',
        'approved_at',
        'shipping_at',
        'returned_at',
        'pick_up_requested_at'
    ];

    const ORDER_USER = 'order_user';
    const ORDER_CUSTOM = 'order_custom';

    public function exceptionOccur(string $reason)
    {
        $this->reason = $reason;
        $this->setStatus(OrderStatus::FAILED)
             ->save();
    }

    public function setCurrency(string $currency)
    {
        $this->currency = $currency;
        $this->currency_rate = (float)config("currencies.rate.$currency");

        return $this;
    }

    public function setStatus($status)
    {
        $this->status()->associate($status);

        return $this;
    }

    public function setBusinessType($businessType)
    {
        $this->businessType()->associate($businessType);

        return $this;
    }

    /**
     * @param mixed $address
     * @param Address|null $default
     * @return Address|Model|false
     */
    public function newShippingAddress($address, Address $default = null)
    {
        if(!is_null($address) && $address instanceof Model) {
            return $this->shippingAddress()->create($address->toArray());
        }

        else {
            return $this->shippingAddress()->create($default->toArray());
        }
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function status()
    {
        return $this->belongsTo(OrderStatus::class, 'order_status_id');
    }

    public function items()
    {
        return $this->hasMany(OrderItem::class, 'order_id', 'id');
    }

    public function invoice()
    {
        return $this->morphOne(Invoice::class, 'invoiceable');
    }

    public function timeslots()
    {
        return $this->hasMany(TimeslotAllocation::class)
                    ->with('timeslot.type');
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class);
    }

    /**
     * @param mixed $timeslot
     * @param string $date
     * @return mixed|null
     * @throws \App\Exceptions\DuplicateTimeslotException
     */
    public function timeslot($timeslot, string $date)
    {
        if (is_int($timeslot) || is_string($timeslot)) {
            $timeslot = Timeslot::find($timeslot);
        }

        if(is_null($timeslot)) {
            return null;
        }

        return $timeslot->allocate($this, $date);
    }

    public function shippingAddress()
    {
        return $this->morphOne(Address::class, 'addressable');
    }

    public function tracks()
    {
        return $this->belongsToMany(
            ProductStockInventory::class,
            'inventory_order_tracks'
        )->withoutGlobalScopes()
         ->using(Track::class)
         ->withPivot(['id', 'scanned_at']);
    }

    public function getAmountAttribute()
    {
        return $this->items->sum(function (OrderItem $item) {
            return $item->getAmountAttribute();
        });
    }

    public function getConvertedAmountAttribute()
    {
        return round($this->getAmountAttribute() * $this->currency_rate);
    }

    public static function chart(string $from, string $to)
    {
        return self::selectRaw("count(id) as data, DATE_FORMAT(created_at, '%Y-%m') as date")
                   ->groupBy('date')
                   ->orderBy('date')
                   ->whereDate('created_at', '>=', $from)
                   ->whereDate('created_at', '<=', $to)
                   ->get();
    }

    public function canEdit(): bool
    {
        return $this->order_status_id === OrderStatus::PENDING || $this->order_status_id === OrderStatus::APPROVED || $this->order_status_id === OrderStatus::SHIPPING;
    }

    public function canDeliver(): bool
    {
        return is_null($this->invoice)
            ? $this->canEdit()
            : ($this->invoice->paid() && $this->canEdit());
    }

    public function canApprove(): bool
    {
        return is_null($this->invoice)
            ? $this->canEdit()
            : ($this->invoice->paid() && $this->canEdit());
    }

    public function canShip(): bool
    {
        return is_null($this->invoice)
            ? $this->canEdit()
            : ($this->invoice->paid() && $this->canEdit());
    }

    public function canReserveTags(): bool
    {
        return is_null($this->invoice)
            ? $this->canEdit()
            : ($this->invoice->paid() && $this->canEdit());
    }

    public function canRemoveReserveTag(): bool
    {
        return $this->canEdit();
    }

    public function canReturn(): bool
    {
        return $this->order_status_id === OrderStatus::DELIVERED ||  $this->order_status_id === OrderStatus::PARTIAL_RETURNED;
    }

    public function canReject(): bool
    {
        return $this->canEdit();
    }

    public function canDelete(): bool
    {
        return $this->canEdit();
    }

    public function canCancel(): bool
    {
        $status = $this->order_status_id;

        return $status === OrderStatus::PENDING || $status === OrderStatus::FAILED;
    }

    public function isInvalided(): bool
    {
        $status = $this->order_status_id;

        return $status === OrderStatus::CANCELLED
            || $status === OrderStatus::REJECTED
            || $status === OrderStatus::FAILED;
    }

    public function approve()
    {
        /**
         * Mark current order as delivered
         */
        $this->markAsApproved();
    }

    /**
     * @throws TracksQuantityNotMatchException
     */
    public function ship()
    {
        $totalOrderItemCount = 0;

        foreach ($this->items as $item){
            $totalOrderItemCount += $item->quantity;
        }

        if($this->tracks->count() != $totalOrderItemCount){
            throw new TracksQuantityNotMatchException(__('order.tracks_quantity_not_match'), 400);
        }
        /**
         * Mark current order as delivered
         */
        $this->markAsShipping();

        /** Notify user order items is shipping */
        event(new ShippingConfirmationEvent($this));
    }


    /**
     * @param array $tags
     * @return Collection
     * @throws InsufficientStockException
     * @throws StockNotFoundException
     * @throws TracksFullException
     */
    public function scanReservedTags(array $tags)
    {
        $totalOrderItemCount = 0;

        foreach ($this->items as $item){
            $totalOrderItemCount += $item->quantity;
        }

        /** check available count for tags to be scanned*/
        if($this->tracks->count() >= $totalOrderItemCount || ($totalOrderItemCount - $this->tracks->count()) < count($tags)){
            throw new TracksFullException(__('order.tracks_quantity_full',[
                'quantity' => $totalOrderItemCount
            ]), 400);
        }

        $inventories = new ProductStockInventoryCollection();

        $groupByStock = ProductStockInventory::inStockTags($tags)
            ->get()
            ->groupBy('product_stock_id');


        /** @var OrderItem $item */
        foreach ($this->items as $item) {
            /** @var \Illuminate\Support\Collection $groupedInventories */
            $groupedInventories = $groupByStock->get($item->product_stock_id);
//            $variantQuantity = $this->items->where('product_stock_id', $item->product_stock_id)->count();
            $variantQuantity = $this->items->firstWhere('product_stock_id', $item->product_stock_id)->quantity;
            $tracksItem = $this->tracks->where('product_stock_id', $item->product_stock_id)->count();

            /** If current tracks already meet the quantity of the product item, ignore and continue the loop */
            if($tracksItem){
                if($tracksItem == $variantQuantity){
                    continue;
                }
            }

            /**
             * If the ordered item not found in inventories
             */
            if(is_null($groupedInventories)) {
                if ($this->items->last() == $item && count($tags) >= $totalOrderItemCount){
                    throw new StockNotFoundException(__('product.not_found.stock', [
                        'name' => $item->product->name,
                        'variant' => $item->variant
                    ]), 400);
                }
                else{
                    continue;
                }
            }

            /**
             * Inventories quantity must meet the demand
             */
            if($groupedInventories->count() <= $item->quantity) {
                $inventories->push(...$groupedInventories->take($item->quantity));
            }
            else {
                throw new InsufficientStockException(__('product.insufficient_stock', [
                    'variant' => $item->variant,
                    'quantity' => $groupedInventories->count()
                ]), 400);
            }
        }

        /**
         * Mark all inventories as reserved
         */
        $inventories->markAsReserved();

        /**
         * Unreserved quantity on reserved items
         */
        /** @var OrderItem $item */
        foreach ($this->items as $item) {
            if($inventories->where('product_stock_id',$item->product_stock_id)->count() > 0){
                $item->stock->unreserved($item->quantity);
            }
        }

        /**
         * Keep track which tags has been ship out to this order
         */
        $this->tracks()->syncWithoutDetaching($inventories);

        return $inventories;
    }

    public function removeScannedReservedTag(array $tag): ProductStockInventoryCollection
    {
        $inventories = new ProductStockInventoryCollection();

        $stock = ProductStockInventory::reservedTag($tag)
            ->first();

        $inventories->push($stock);

        /**
         * Mark all inventories as in stock
         */
        $inventories->markAsInStock();

        /**
         * Reserved quantity on removed reserved items
         */
        /** @var OrderItem $item */
        foreach ($this->items as $item) {
            if($inventories->where('product_stock_id',$item->product_stock_id)->count() > 0){
                $item->stock->reserved($item->quantity);
            }
        }

        /** detach track as it is removed */
        $this->tracks()->detach($inventories);

        return $inventories;
    }

    /**
     * @throws TracksQuantityNotMatchException
     */
    public function deliver()
    {
        $totalOrderItemCount = 0;

        foreach ($this->items as $item){
            $totalOrderItemCount += $item->quantity;
        }

        if($this->tracks->count() != $totalOrderItemCount){
            throw new TracksQuantityNotMatchException(__('order.tracks_quantity_not_match'), 400);
        }

        $inventories = new ProductStockInventoryCollection();

        /** Attached inventory tracks */
        $tags = $this->tracks->pluck('tag')->toArray();

        $groupByStock = ProductStockInventory::reservedTags($tags)->get();

        foreach ($groupByStock as $stock){
            $inventories->push($stock);
        }

        /**
         * Mark all inventories as delivered
         */
        $inventories->markAsShippedOut();

        /**
         * Mark current order as delivered or shop completed depends on business type
         */
        if($this->business_type_id == BusinessType::RENT){
            $this->markAsDelivered();
        }
        else{
            $this->markAsCompleted();
            event(new ShopOrderCompletedEvent($this));
        }

        return $inventories;
    }

    /**
     * @param array $tags
     * @throws TagNotMatchException
     * @throws RequiredNumberOfTagException
     * @throws TagScannedException
     */
    public function returnBack(array $tags)
    {
        $returned = collect($tags);
        /** @var ProductStockInventoryCollection $tracks */
        $tracks = $this->tracks()->get();

        $partialReturnItemTags = $this->getPartialReturnItemTags($tracks);

        /** Get partial return items from input */
        $partialReturned = $returned->whereIn('tag',$partialReturnItemTags);

        /** Filtered non partial return items from input  */
        $returned = $returned->whereNotIn('tag',$partialReturnItemTags);

        /**
         * Shipped out tag and returned tag must exactly same
         */
        $diff = $tracks->diffTags($returned->pluck('tag')->toArray());
        if(count($diff)) {
            throw new TagNotMatchException($diff);
        }
        /**
         * Stamp returned tracked tags with timestamp
         */;
        $tracks->whereIn('tag', $returned->pluck('tag'))
               ->each(function ($track) {
                   if(!is_null($track->pivot->scanned_at)) {
                       throw new TagScannedException($track->tag);
                   }
                   $this->tracks()->updateExistingPivot($track->id, ['scanned_at' => now()]);
               });

        $this->updateReturnTagStatus($returned);

        /**
         * All the delivered tags has been scanned
         */
        if($this->scannedTags()->count() === $tracks->count()) {
            /**
             * Mark this order as completed
             */
            $this->markAsCompleted();
        }
        else {
            $this->markAs(OrderStatus::PARTIAL_RETURNED);
        }

        if($partialReturned->count()) $this->returnPartialReturnOrder($partialReturned);
    }

    /**
     * @param array $tags
     * @throws TagNotMatchException
     * @throws RequiredNumberOfTagException
     * @throws TagScannedException
     */
    public function returnBackPartial(array $tags)
    {
        $returned = collect($tags);
        /** @var ProductStockInventoryCollection $tracks */
        $tracks = $this->tracks()->get();

        $partialReturnItemTags = $this->getPartialReturnItemTags($tracks);

        /** Get partial return items from input */
        $partialReturned = $returned->whereIn('tag',$partialReturnItemTags);

        /**
         * Shipped out tag and partial return tag must exactly same
         */
        $diff = $tracks->diffTags($returned->pluck('tag')->toArray());
        if(count($diff)) {
            throw new TagNotMatchException($diff);
        }

        // Check if tags has been returned before
        $tracks->validateScannedTags($returned->pluck('tag')->toArray());

        if($partialReturned->count()) $this->returnPartialReturnOrder($partialReturned);
    }

    public function getPartialReturnItemTags($tracks)
    {
        /** Latest Order Tracks */
        $latestTags = $tracks->pluck('tag');

        /** Get User partial return order */
        $partialReturnOrders = $this->user->partialReturnedOrders()->get();
        $partialReturnItemTags = new Collection();

        /** Get partial return items tags */
        foreach ($partialReturnOrders as $order){
            $order->tracks->map(function ($track) use ($partialReturnItemTags,$order){
               if(!$track->pivot->scanned_at) $partialReturnItemTags->push($track->tag);
            });
        }

        return $partialReturnItemTags;
    }

    public function returnPartialReturnOrder($partialReturned)
    {
        $partialReturnInputOrder = new Collection();
        $this->user->partialReturnedOrders()->get()->map(function($order) use ($partialReturned, $partialReturnInputOrder){
            $order->tracks()->whereIn('tag', $partialReturned->pluck('tag'))->each(function ($track) use ($order, $partialReturnInputOrder){
                if(!is_null($track->pivot->scanned_at)) {
                    throw new TagScannedException($track->tag);
                }
                $partialReturnInputOrder->push($order);
                $order->tracks()->updateExistingPivot($track->id, ['scanned_at' => now()]);
            });;
        });

        $response = $this->updateReturnTagStatus($partialReturned);

        if($response){
            $partialReturnInputOrder->map(function(Order $order){
                if($order->tracks()->count() == $order->tracks()->where('stock_status_id',StockStatus::IN_STOCK)->count()){
                    $order->markAsCompleted();
                }
            });
        }
    }

    public function updateReturnTagStatus($returned)
    {
        /**
         * Update the returned tags status
         */
        $returned->groupBy('status')
            ->each(function ($item, $status) {
                $tags = $item->pluck('tag')->toArray();
                /**
                 * Mark the returned tags with the given status
                 */
                ProductStockInventory::markTagsAs($tags, $status);

                $inventories = ProductStockInventory::findByTags($tags)->get();
                /**
                 * Unreserve items and make it available for next order
                 */
                $inventories->each(function (ProductStockInventory $inventory) {
                    $stock = $inventory->stock;
                    $stock->getLock()
                        ->block(5, function () use ($stock) {
                            $stock->unreserved(1);
                        });
                });
                /**
                 * Returned tag with IN_STOCK status will trigger StockArrival
                 */
                if($status === StockStatus::IN_STOCK) {
                    $inventories->unique('product_stock_id')
                        ->each(function (ProductStockInventory $inventory) {
                            event(new StockArrival($inventory->stock));
                        });
                }
            });

        return true;
    }

    public function reject()
    {
        $this->setStatus(OrderStatus::REJECTED)
             ->save();
    }

    public function refund()
    {
        $this->user->refund($this->invoice->payment_intent);

        event(new ShopOrderRejectedEvent($this));
    }

    public function scannedTags()
    {
        return $this->tracks()->wherePivotNotNull('scanned_at');
    }

    private function markAsDelivered()
    {
        $this->shipped_at = Carbon::now();

        $this->markAs(OrderStatus::DELIVERED);
    }

    private function markAsApproved()
    {
        $this->approved_at = Carbon::now();

        $this->markAs(OrderStatus::APPROVED);
    }

    private function markAsShipping()
    {
        $this->shipping_at = Carbon::now();

        $this->markAs(OrderStatus::SHIPPING);
    }

    private function markAsCompleted()
    {
        $this->returned_at = Carbon::now();

        $this->markAs(OrderStatus::COMPLETED);
    }

    private function markAsShopCompleted()
    {
        $this->markAs(OrderStatus::COMPLETED);
    }

    private function markAs($status)
    {
        $this->setStatus($status)->save();
    }

    public function safeDelete(): bool
    {
        $result = false;
        /**
         * Unreserved all items belongs to this order
         */
        foreach ($this->items as $item) {
            $stock = $item->stock;

            $stock->getLock()
                  ->block(5, function () use ($item, $stock) {
                      $stock->unreserved($item->quantity);
                  });
        }
        /**
         * Delete all items belongs to this order
         */
        $this->items()->delete();

        try {
            $result = $this->delete();
        }
        catch (Exception $exception) {
            Log::error('Failed to delete order', ['message' => $exception->getMessage()]);
        }
        finally {
            return $result;
        }
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder->where('id', $keyword)
                       ->orWhereHas('tracks', function (Builder $query) use ($keyword) {
                           $query->where('product_stock_inventories.tag', 'like', $keyword);
                       });
    }

    public function filterCriteria(): array
    {
        return [];
    }

    public function sortCriteria(): array
    {
        return [];
    }

    public function withRelations(): array
    {
        return array_merge($this->with, [
            'user.profile'
        ]);
    }
}
