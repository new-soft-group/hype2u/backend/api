<?php

namespace App\Models;


use App\Collections\StockCollection;
use App\Models\Pivots\Combination;
use App\Traits\CanEnable;
use App\Traits\HasImage;
use App\Traits\Product\HasInventory;
use App\Traits\HasPricing;
use App\Traits\Product\StockQueue;
use Cache;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;



/**
 * App\Models\ProductStock
 *
 * @property int $id
 * @property int $product_id
 * @property int|null $vendor_id
 * @property string $sku
 * @property float $price
 * @property float $cost_price
 * @property int $reserved
 * @property int $enabled
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductVariantOption[] $combinations
 * @property-read int|null $combinations_count
 * @property-read string $combination
 * @property-read array $combination_ids
 * @property-read mixed $pricing
 * @property-read \App\Models\Image|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductStockInventory[] $inventories
 * @property-read int|null $inventories_count
 * @property-read \App\Models\NotifyQueue|null $notifyQueue
 * @property-read \App\Models\Product $product
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Queuer[] $queuers
 * @property-read int|null $queuers_count
 * @property-read \App\Models\Vendor|null $vendor
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock enabled($state = true)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock whereCostPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock whereEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock whereReserved($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock whereSku($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductStock whereVendorId($value)
 * @mixin \Eloquent
 */
class ProductStock extends Model
{
    use HasFactory,
        HasImage,
        CanEnable,
        HasPricing,
        HasInventory,
        StockQueue;

    const DEFAULT_PREFIX = 'SKU';

    protected $fillable = [
        'price',
        'cost_price',
        'vendor_id',
    ];

    protected $casts = [
        'enabled' => 'boolean',
        'price' => 'float',
        'cost_price' => 'float',
        'vendor_id' => 'integer',
        'reserved' => 'integer'
    ];

    protected $with = ['combinations'];

    protected $disk = 'product';

    protected static function booted()
    {
        static::creating(function (ProductStock $stock) {
            if(empty($stock->sku)) {
                $stock->sku = self::DEFAULT_PREFIX . strtoupper(Str::random(8));
            }
        });
    }

    public function resolveRouteBinding($value, $field = null)
    {
        return $this->where('id', $value)
                    ->firstOrFail();
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function vendor()
    {
        return $this->belongsTo(Vendor::class , 'vendor_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function combinations()
    {
        return $this->belongsToMany(ProductVariantOption::class,
            'stock_variant_option_combinations',
            'product_stock_id',
            'product_variant_option_id'
        )->with('lookup');
    }

    public function getCombinationAttribute(): string
    {
        return $this->combinations->pluck('lookup.name')->implode('/');
    }

    public function getCombinationIdsAttribute(): array
    {
        return $this->combinations->pluck('id')->toArray();
    }

    public function hasCombination(array $combinations)
    {
        $options = $this->combinations->pluck('lookup.name')->toArray();

        return count(array_intersect($options, $combinations)) === count($combinations);
    }

    /**
     * @param int|Vendor $vendor
     * @return bool
     */
    public function hasVendor($vendor)
    {
        if(is_int($vendor)) {
            return $this->vendor_id === $vendor;
        }

        return $this->vendor_id === (int)$vendor->id;
    }

    public function getCombinations(): array
    {
        return [
            'ids' => $this->getCombinationIdsAttribute(),
            'name' => $this->getCombinationAttribute()
        ];
    }

    public function getLock()
    {
        return Cache::lock("stock.$this->id", 10);
    }

    public function newCollection(array $models = [])
    {
        return new StockCollection($models);
    }
}
