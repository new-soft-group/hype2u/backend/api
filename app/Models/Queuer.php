<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Notification;


/**
 * App\Models\Queuer
 *
 * @property int $id
 * @property int $user_id
 * @property int $queue_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\NotifyQueue $queue
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Queuer newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Queuer newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Queuer query()
 * @method static \Illuminate\Database\Eloquent\Builder|Queuer whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Queuer whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Queuer whereQueueId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Queuer whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Queuer whereUserId($value)
 * @mixin \Eloquent
 */
class Queuer extends Model
{
    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function queue()
    {
        return $this->belongsTo(NotifyQueue::class, 'queue_id');
    }
}
