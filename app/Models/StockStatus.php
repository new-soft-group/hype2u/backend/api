<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\StockStatus
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StockStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StockStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StockStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StockStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StockStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StockStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\StockStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class StockStatus extends Model
{
    const IN_STOCK = 1;
    const OUT_OF_STOCK = 2;
    const DRY_CLEANING = 3;
    const RESERVED = 4;
    const SHIPPED_OUT = 5;
    const DAMAGED = 6;
    const LOST = 7;
}
