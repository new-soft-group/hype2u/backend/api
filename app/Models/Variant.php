<?php


namespace App\Models;


use App\Collections\VariantCollection;
use App\Traits\Removable;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Log;



/**
 * App\Models\Variant
 *
 * @property int $id
 * @property string $name
 * @property bool $removable
 * @property-read Collection|\App\Models\VariantOption[] $options
 * @property-read int|null $options_count
 * @property-read Collection|\App\Models\ProductVariant[] $products
 * @property-read int|null $products_count
 * @method static VariantCollection|static[] all($columns = ['*'])
 * @method static VariantCollection|static[] get($columns = ['*'])
 * @method static Builder|Variant newModelQuery()
 * @method static Builder|Variant newQuery()
 * @method static Builder|Variant query()
 * @method static Builder|Variant whereId($value)
 * @method static Builder|Variant whereName($value)
 * @method static Builder|Variant whereRemovable($value)
 * @mixin \Eloquent
 */
class Variant extends Model
{
    use HasFactory, Removable;

    public $timestamps = false;

    protected $fillable = ['name'];

    protected $casts = [
        'removable' => 'boolean'
    ];

    public function newCollection(array $models = [])
    {
        return new VariantCollection($models);
    }

    public static function create($attribute = []): Variant
    {
        $instance = new static($attribute);

        if(array_key_exists('removable', $attribute)) {
            $instance->setRemovable($attribute['removable']);
        }

        $instance->save();

        if(array_key_exists('options', $attribute)) {
            $options = new Collection();
            foreach ($attribute['options'] as $option) {
                /** @var VariantOption $model */
                $model = $instance->options()->make($option);
                if(array_key_exists('removable', $option)) {
                    $model->setRemovable($option['removable']);
                }
                $model->save();
                $options->push($model);
            }
            $instance->setRelation('options', $options);
        }

        return $instance;
    }

    public static function createMany($attributes = [])
    {
        $instances = new VariantCollection();

        foreach ($attributes as $attribute) {
            $instance = self::create($attribute);

            $instances->push($instance);
        }

        return $instances;
    }

    public function options()
    {
        return $this->hasMany(VariantOption::class);
    }

    public function products()
    {
        return $this->hasMany(ProductVariant::class, 'variant_id', 'id')
                    ->whereHas('product', function (Builder $query) {
                        $query->where('enabled', true);
                    });
    }

    public function updateVariant(array $attribute)
    {
        $this->fill($attribute)->save();

        if(array_key_exists('options', $attribute)) {
            foreach ($attribute['options'] as $option) {
                $model = $this->options()->firstOrCreate(['id' => $option['id'] ?? 0], $option);

                if(!$model->wasRecentlyCreated) {
                    $model->fill($option)->save();
                }
            }
        }

        if(array_key_exists('removed_options', $attribute)) {
            $selectedOptions = $this->options()
                                    ->whereKey($attribute['removed_options'])
                                    ->whereDoesntHave('products')
                                    ->get();

            foreach ($selectedOptions as $selected) {
                /** @var VariantOption $selected */
                if($selected->isRemovable()) {
                    try {
                        $selected->delete();
                    }
                    catch (Exception $exception) {
                        Log::error("Failed to delete variant's option", [
                            'message' => $exception->getMessage()
                        ]);
                    }
                }
            }
        }

        return $this->load('options');
    }

    /**
     * @return bool|null
     * @throws \Exception
     */
    public function safeDelete()
    {
        if($this->products()->count() || !$this->isRemovable()) {
            return false;
        }

        $this->options()->delete();

        return $this->delete();
    }

    public function getOptionForeignId(): array
    {
        return $this->options->pluck('id')->toArray();
    }
}
