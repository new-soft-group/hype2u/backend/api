<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\BannerPosition
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|BannerPosition newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerPosition newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerPosition query()
 * @method static \Illuminate\Database\Eloquent\Builder|BannerPosition whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|BannerPosition whereName($value)
 * @mixin \Eloquent
 */
class BannerPosition extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    public $timestamps = false;

    public function rename(string $name) {
        $this->name = $name;
        $this->save();
    }
}
