<?php

namespace App\Models;

use App\Collections\PolicyCollection;
use App\Models\Pivots\PolicyRule;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



/**
 * App\Models\PlanPolicy
 *
 * @property int $id
 * @property int $plan_id
 * @property int $quantity
 * @property string $logical
 * @property-read \App\Models\Plan $plan
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductType[] $rules
 * @property-read int|null $rules_count
 * @method static PolicyCollection|static[] all($columns = ['*'])
 * @method static PolicyCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPolicy newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPolicy newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPolicy query()
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPolicy whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPolicy whereLogical($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPolicy wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|PlanPolicy whereQuantity($value)
 * @mixin \Eloquent
 */
class PlanPolicy extends Model
{
    use HasFactory;

    const LOGICAL_OR = 'or';
    const LOGICAL_AND = 'and';
    const LOGICAL_NONE = 'none';

    protected $fillable = [
        'logical',
        'quantity',
        'prime'
    ];

    protected $casts = [
        'quantity' => 'integer',
        'prime' => 'boolean'
    ];

    public $timestamps = false;

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function rules()
    {
        return $this->belongsToMany(ProductType::class,
            'policy_rules',
            'plan_policy_id',
            'product_type_id'
        )->using(PolicyRule::class)
         ->withPivot(['prime']);
    }

    public function isOr()
    {
        return $this->logical === self::LOGICAL_OR;
    }

    public function isAnd()
    {
        return $this->logical === self::LOGICAL_AND;
    }

    public function isNone()
    {
        return $this->logical === self::LOGICAL_NONE;
    }

    public function targetIds(): array
    {
        return $this->rules
                    ->pluck('pivot.product_type_id')
                    ->toArray();
    }

    public function newCollection(array $models = [])
    {
        return new PolicyCollection($models);
    }
}
