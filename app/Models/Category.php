<?php

namespace App\Models;

use App\Exceptions\ModelHasAssociate;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;


/**
 * App\Models\Category
 *
 * @property int $id
 * @property string $name
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|Category newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Category query()
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Category whereName($value)
 * @mixin \Eloquent
 */
class Category extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['name', 'business_type_id'];

    public function products()
    {
        return $this->belongsToMany(Product::class,
                                    'product_categories',
                                    'category_id',
                                    'product_id')
                    ->where('enabled', true);
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class);
    }

    /**
     * @return bool
     * @throws ModelHasAssociate
     * @throws Exception
     */
    public function safeDelete(): bool
    {
        if($this->products()->count()) {
            throw new ModelHasAssociate(__('category.delete.failed'));
        }

        return $this->delete();
    }

    public static function massUpdate(array $attributes): Collection
    {
        $ids = Arr::pluck($attributes, 'id');

        $categories = self::whereKey($ids)->get();

        foreach ($categories as $index => $category) {
            /** @var Category $category */
            $category->fill($attributes[$index]);
            $category->save();
        }

        return $categories;
    }
}
