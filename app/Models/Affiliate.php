<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Kalnoy\Nestedset\NodeTrait;

/**
 * App\Models\Affiliate
 *
 * @property int $id
 * @property int $user_id
 * @property int $level
 * @property float $commission
 * @property int $_lft
 * @property int $_rgt
 * @property int|null $parent_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Kalnoy\Nestedset\Collection|\App\Models\Affiliate[] $children
 * @property-read int|null $children_count
 * @property-read \App\Models\DiscountCode|null $discountCode
 * @property-read mixed|string $code
 * @property-read float|mixed $discount
 * @property-read int|mixed $discount_code_id
 * @property-read int|mixed $discount_type_id
 * @property-read int|mixed $discount_type_name
 * @property-read mixed|string|null $expired_at
 * @property-read mixed $referral_code
 * @property-read \App\Models\Affiliate|null $parent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\AffiliateSale[] $sales
 * @property-read int|null $sales_count
 * @property-read \App\Models\User $user
 * @method static \Kalnoy\Nestedset\Collection|static[] all($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Affiliate d()
 * @method static \Kalnoy\Nestedset\Collection|static[] get($columns = ['*'])
 * @method static \Kalnoy\Nestedset\QueryBuilder|\App\Models\Affiliate newModelQuery()
 * @method static \Kalnoy\Nestedset\QueryBuilder|\App\Models\Affiliate newQuery()
 * @method static \Kalnoy\Nestedset\QueryBuilder|\App\Models\Affiliate query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Affiliate whereCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Affiliate whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Affiliate whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Affiliate whereLevel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Affiliate whereLft($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Affiliate whereParentId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Affiliate whereRgt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Affiliate whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Affiliate whereUserId($value)
 * @mixin \Eloquent
 * @property-read int|mixed $discount_method_id
 * @property-read int|mixed $discount_method_name
 */
class Affiliate extends Model
{
    use NodeTrait;

    const REQUIRED_ROLE = [];

    protected $casts = [
        'level' => 'int',
        'commission' => 'float'
    ];

    protected $with = ['discountCode'];

    public static function boot()
    {
        parent::boot();

        static::creating(function (Affiliate $affiliate) {
            if ($affiliate->parent_id) {
                $parent = self::find($affiliate->parent_id);
                $affiliate->level = $parent->level + 1;
            }
            else {
                $affiliate->level = 1;
            }
        });

        static::updating(function (Affiliate $affiliate) {
            if ($affiliate->isDirty('parent_id')) {
                if ($affiliate->parent_id === null) {
                    $affiliate->level = 1;
                }
                else {
                    $affiliate->level = $affiliate->parent->level + 1;
                }
            }
        });
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function sales()
    {
        return $this->hasMany(AffiliateSale::class, 'affiliate_id', 'id');
    }

    public function paid()
    {
        return $this->sales()->where('paid', '=', 1);
    }

    public function unpaid()
    {
        return $this->sales()->where('paid', '=', 0);
    }

    public function sumOfSales()
    {
        return $this->sales()
                    ->selectRaw("affiliate_id, SUM(commission) as commission_sum, DATE_FORMAT(created_at, '%Y-%m') as date")
                    ->groupBy('affiliate_id', 'date');
    }

    public function totalSales(Carbon $date)
    {
        return $this->sales()
                    ->selectRaw("SUM(commission) as data, DATE_FORMAT(created_at, '%Y-%m') as date")
                    ->groupBy('date')
                    ->orderBy('date')
                    ->whereYear('created_at', $date->year)
                    ->whereMonth('created_at', $date->month)
                    ->get();
    }

    public function totalMemberSales()
    {
        return $this->children()
                    ->join('affiliate_sales', 'affiliate_sales.affiliate_id', '=', 'affiliates.id')
                    ->join('users', 'users.id', '=', 'affiliates.user_id')
                    ->selectRaw("CONCAT(users.first_name, ' ', users.last_name) as name, " . "SUM(affiliate_sales.commission) as data, " . "DATE_FORMAT(affiliate_sales.created_at, '%Y-%m') as date")
                    ->groupBy('name', 'date')
                    ->orderBy('date')
                    ->get();
    }
}
