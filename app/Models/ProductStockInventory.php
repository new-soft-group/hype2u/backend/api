<?php

namespace App\Models;

use App\Collections\ProductStockInventoryCollection;
use App\Contracts\Datatable\Searchable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


/**
 * App\Models\ProductStockInventory
 *
 * @property int $id
 * @property string $tag
 * @property int $warehouse_id
 * @property int $product_stock_id
 * @property int $stock_status_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\StockStatus $status
 * @property-read \App\Models\ProductStock $stock
 * @property-read \App\Models\Warehouse $warehouse
 * @method static ProductStockInventoryCollection|static[] all($columns = ['*'])
 * @method static Builder|ProductStockInventory findByTags($tags)
 * @method static ProductStockInventoryCollection|static[] get($columns = ['*'])
 * @method static Builder|ProductStockInventory inStockTags($tags)
 * @method static Builder|ProductStockInventory reservedTags($tags)
 * @method static Builder|ProductStockInventory inStockTag($tags)
 * @method static Builder|ProductStockInventory reservedTag($tags)
 * @method static Builder|ProductStockInventory markTagsAs($tags, $status)
 * @method static Builder|ProductStockInventory newModelQuery()
 * @method static Builder|ProductStockInventory newQuery()
 * @method static Builder|ProductStockInventory query()
 * @method static Builder|ProductStockInventory whereCreatedAt($value)
 * @method static Builder|ProductStockInventory whereId($value)
 * @method static Builder|ProductStockInventory whereProductStockId($value)
 * @method static Builder|ProductStockInventory whereStockStatusId($value)
 * @method static Builder|ProductStockInventory whereTag($value)
 * @method static Builder|ProductStockInventory whereUpdatedAt($value)
 * @method static Builder|ProductStockInventory whereWarehouseId($value)
 * @mixin \Eloquent
 */
class ProductStockInventory extends Model implements Searchable
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'tag',
        'warehouse_id',
        'stock_status_id'
    ];

    public function setTag(string $tag)
    {
        $this->tag = $tag;

        return $this;
    }

    public function stock()
    {
        return $this->belongsTo(ProductStock::class, 'product_stock_id');
    }

    public function warehouse()
    {
        return $this->belongsTo(Warehouse::class);
    }

    public function tracks()
    {
        return $this->belongsToMany(Order::class, 'inventory_order_tracks');
    }

    public function shippedOutTracks()
    {
        return $this->belongsToMany(Order::class, 'inventory_order_tracks')->whereHas('status', function (Builder $builder){
            $builder->where('id',OrderStatus::DELIVERED);
        })->with('user');
    }

    public function status()
    {
        return $this->belongsTo(StockStatus::class, 'stock_status_id');
    }

    public function scopeInStockTags(Builder $builder, array $tags)
    {
        return $builder->whereIn('tag', $tags)
                       ->where('stock_status_id', StockStatus::IN_STOCK);
    }

    public function scopeReservedTags(Builder $builder, array $tags)
    {
        return $builder->whereIn('tag', $tags)
            ->where('stock_status_id', StockStatus::RESERVED);
    }

    public function scopeInStockTag(Builder $builder, array $tags)
    {
        return $builder->where('tag', $tags)
            ->where('stock_status_id', StockStatus::IN_STOCK);
    }

    public function scopeReservedTag(Builder $builder, array $tags)
    {
        return $builder->where('tag', $tags)
            ->where('stock_status_id', StockStatus::RESERVED);
    }

    public function scopeFindByTags(Builder $builder, array $tags)
    {
        return $builder->whereIn('tag', $tags);
    }

    public function scopeMarkTagsAs(Builder $builder, array $tags, int $status)
    {
        return $builder->whereIn('tag', $tags)
                       ->update(['stock_status_id' => $status]);
    }

    public function isInStock(): bool
    {
        return $this->stock_status_id === StockStatus::IN_STOCK;
    }

    public function isOutOfStock(): bool
    {
        return $this->stock_status_id === StockStatus::OUT_OF_STOCK;
    }

    public function isShippedOut(): bool
    {
        return $this->stock_status_id === StockStatus::SHIPPED_OUT;
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder->whereHas('stock.product', function (Builder $subQuery) use ($keyword) {
                           $subQuery->where('name', 'like', $keyword);
                       });
    }

    public function filterCriteria(): array
    {
        return [
            'warehouses' => function(Builder $builder, array $warehouses) {
                return $builder->whereIn('warehouse_id', $warehouses);
            }
        ];
    }

    public function sortCriteria(): array
    {
        return [];
    }

    public function withRelations(): array
    {
        return [];
    }

    public function newCollection(array $models = [])
    {
        return new ProductStockInventoryCollection($models);
    }
}
