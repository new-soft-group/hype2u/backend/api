<?php

namespace App\Models;

use App\Contracts\Datatable\Searchable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



/**
 * App\Models\Holiday
 *
 * @property int $id
 * @property string $label
 * @property string $from
 * @property string $to
 * @method static Builder|Holiday month($date)
 * @method static Builder|Holiday newModelQuery()
 * @method static Builder|Holiday newQuery()
 * @method static Builder|Holiday query()
 * @method static Builder|Holiday week($date)
 * @method static Builder|Holiday whereFrom($value)
 * @method static Builder|Holiday whereId($value)
 * @method static Builder|Holiday whereLabel($value)
 * @method static Builder|Holiday whereTo($value)
 * @method static Builder|Holiday year($date)
 * @mixin \Eloquent
 */
class Holiday extends Model implements Searchable
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'label',
        'from',
        'to'
    ];

    public static function crashOn(string $date): bool
    {
        return self::whereDate('from', '<=', $date)
                   ->whereDate('to', '>=', $date)
                   ->exists();
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder->where('label', 'like', $keyword);
    }

    public function scopeWeek(Builder $builder, string $date)
    {
        $carbon = Carbon::parse($date);

        $start = $carbon->startOfWeek()->toDateString();
        $end = $carbon->endOfWeek()->toDateString();

        return $builder->whereBetween('from', [$start, $end]);
    }

    public function scopeMonth(Builder $builder, string $date)
    {
        $carbon = Carbon::parse($date);

        $start = $carbon->startOfMonth()->toDateString();
        $end = $carbon->endOfMonth()->toDateString();

        return $builder->whereBetween('from', [$start, $end]);
    }

    public function scopeYear(Builder $builder, string $year)
    {
        return $builder->whereYear('from', $year);
    }

    public function filterCriteria(): array
    {
        return [
            'week' => 'scopeWeek',
            'month' => 'scopeMonth',
            'year' => 'scopeYear'
        ];
    }

    public function sortCriteria(): array
    {
        return [];
    }

    public function withRelations(): array
    {
        return [];
    }
}
