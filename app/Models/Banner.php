<?php

namespace App\Models;

use App\Traits\HasImage;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;


/**
 * App\Models\Banner
 *
 * @property int $id
 * @property string $viewport
 * @property int $banner_position_id
 * @property int $sort
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Image|null $image
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Image[] $images
 * @property-read int|null $images_count
 * @property-read \App\Models\BannerPosition $position
 * @method static Builder|Banner newModelQuery()
 * @method static Builder|Banner newQuery()
 * @method static Builder|Banner query()
 * @method static Builder|Banner sortOrder()
 * @method static Builder|Banner whereBannerPositionId($value)
 * @method static Builder|Banner whereCreatedAt($value)
 * @method static Builder|Banner whereId($value)
 * @method static Builder|Banner whereSort($value)
 * @method static Builder|Banner whereUpdatedAt($value)
 * @method static Builder|Banner whereViewport($value)
 * @mixin \Eloquent
 */
class Banner extends Model
{
    use HasFactory, HasImage;

    const DESKTOP_VIEWPORT = 'desktop';

    const MOBILE_VIEWPORT = 'mobile';

    protected $fillable = [
        'viewport',
        'banner_position_id',
        'sort'
    ];

    protected $casts = [
        'sort' => 'integer'
    ];

    protected $with = ['image', 'position'];

    protected $disk = 'banner';

    public function position()
    {
        return $this->belongsTo(BannerPosition::class, 'banner_position_id');
    }

    public function scopeSortOrder(Builder $query)
    {
        return $query->orderBy('sort');
    }
}
