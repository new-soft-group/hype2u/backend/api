<?php

namespace App\Models;

use App\Traits\HasInvoice;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use \Laravel\Cashier\Subscription as CashierSubscription;
use Illuminate\Database\Eloquent\Builder;
use App\Contracts\Datatable\Searchable;



/**
 * App\Models\Subscription
 *
 * @property int $id
 * @property int $user_id
 * @property string $name
 * @property string $stripe_id
 * @property string $stripe_status
 * @property string|null $stripe_plan
 * @property int|null $quantity
 * @property \Illuminate\Support\Carbon|null $trial_ends_at
 * @property \Illuminate\Support\Carbon|null $ends_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Cashier\SubscriptionItem[] $items
 * @property-read int|null $items_count
 * @property-read \App\Models\User $owner
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invoice[] $payment
 * @property-read int|null $payment_count
 * @property-read \App\Models\Plan|null $plan
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription active()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription cancelled()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription ended()
 * @method static Builder|Subscription findByStripeId($stripeId)
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription incomplete()
 * @method static Builder|Subscription newModelQuery()
 * @method static Builder|Subscription newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription notCancelled()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription notOnGracePeriod()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription notOnTrial()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription onGracePeriod()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription onTrial()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription pastDue()
 * @method static Builder|Subscription query()
 * @method static \Illuminate\Database\Eloquent\Builder|Subscription recurring()
 * @method static Builder|Subscription whereCreatedAt($value)
 * @method static Builder|Subscription whereEndsAt($value)
 * @method static Builder|Subscription whereId($value)
 * @method static Builder|Subscription whereName($value)
 * @method static Builder|Subscription whereQuantity($value)
 * @method static Builder|Subscription whereStripeId($value)
 * @method static Builder|Subscription whereStripePlan($value)
 * @method static Builder|Subscription whereStripeStatus($value)
 * @method static Builder|Subscription whereTrialEndsAt($value)
 * @method static Builder|Subscription whereUpdatedAt($value)
 * @method static Builder|Subscription whereUserId($value)
 * @mixin \Eloquent
 */
class Subscription extends CashierSubscription implements Searchable
{
    use HasFactory, HasInvoice;

    public function plan()
    {
        return $this->belongsTo(Plan::class, 'stripe_plan', 'stripe_plan');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function nextBillingCycle()
    {
        return $this->updated_at->add(1, $this->plan->interval);
    }

    public function notifyNextBillingCycle()
    {
        return $this->updated_at->add(1, $this->plan->interval)->sub(7, "day");
    }

    public function getNextCycleDateAttribute()
    {
        return $this->updated_at->add(1, $this->plan->interval);
    }

    public function invoices()
    {
        return $this->morphMany(Invoice::class, 'invoiceable');
    }

    public function scopeFindByStripeId(Builder $builder, string $stripeId)
    {
        return $builder->firstWhere('stripe_id', $stripeId);
    }

    public function scopeFilterByDate(Builder $builder, array $date)
    {
        $startDate = Carbon::createFromFormat('Y-m-d', min($date))->startOfDay();
        $endDate = Carbon::createFromFormat('Y-m-d', max($date))->endOfDay();

        return $builder->whereBetween('created_at', [$startDate,$endDate]);
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder->whereHas(
            'user',
            function (Builder $builder) use ($keyword) {
                $builder->whereHas('profile', function(Builder $builder) use ($keyword){
                    return $builder->where('first_name', 'like', $keyword)
                            ->orWhere('last_name', 'like', $keyword);
                });
            });
    }

    public function filterCriteria(): array
    {
        return [
            'date' => 'scopeFilterByDate'
        ];
    }

    public function sortCriteria(): array
    {
        return [];
    }

    public function withRelations(): array
    {
        return [];
    }
}
