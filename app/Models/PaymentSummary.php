<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PaymentSummary extends Model
{
    use HasFactory;

    protected $fillable = [
        'charge_id',
        'invoice_id',
        'payment_intent',
        'amount',
        'amount_refunded',
        'application_fee',
        'application_fee_amount',
        'dispute',
        'disputed',
        'type',
        'failure_code',
        'failure_message',
        'customer_stripe_id',
        'status',
        'payment_date',
        'paid',
        'user_id',
        'currency'
    ];

    public function user() {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

}
