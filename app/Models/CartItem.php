<?php


namespace App\Models;


use App\Collections\CartItemCollection;
use App\Exceptions\CartItemQuantityExceededException;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



/**
 * App\Models\CartItem
 *
 * @property int $id
 * @property int $cart_id
 * @property int $product_id
 * @property int $product_type_id
 * @property int|null $product_stock_id
 * @property int $quantity
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Cart $cart
 * @property-read \App\Models\ProductStock|null $option
 * @property-read \App\Models\Product $product
 * @method static CartItemCollection|static[] all($columns = ['*'])
 * @method static CartItemCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem whereCartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem whereProductStockId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem whereProductTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|CartItem whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class CartItem extends Model
{
    use HasFactory;

    protected $fillable = [
        'product_id',
        'product_stock_id',
        'product_type_id',
        'quantity',
    ];

    protected $casts = [
        'product_id' => 'int',
        'product_stock_id' => 'int',
        'quantity' => 'int',
    ];

    protected $with = ['product'];

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function option()
    {
        return $this->belongsTo(ProductStock::class, 'product_stock_id');
    }

    public function getProductType()
    {
        return $this->product->type;
    }

    public function newCollection(array $models = [])
    {
        return new CartItemCollection($models);
    }

    public function getAmount(array $exclude = [])
    {
        if(in_array($this->product_type_id, $exclude)) return 0;

        if($this->option->price > 0) return $this->option->price * $this->quantity;

        return $this->product->price * $this->quantity;

//        return ($this->product->price + $this->option->price) * $this->quantity;
    }

    public function incrementQuantity(){
        if($this->quantity >= $this->option->availableInventoryCount()) throw new CartItemQuantityExceededException();

        return $this->quantity += 1;
    }

    public function decrementQuantity(){
        return $this->quantity -= 1;
    }
}
