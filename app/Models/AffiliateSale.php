<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\AffiliateSale
 *
 * @property int $id
 * @property int $affiliate_id
 * @property int $order_id
 * @property float $commission
 * @property int $paid
 * @property string|null $reference_no
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Affiliate $affiliate
 * @property-read \App\Models\Order $order
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale whereAffiliateId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale whereCommission($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale wherePaid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale whereReferenceNo($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale whereUpdatedAt($value)
 * @mixin \Eloquent
 * @property int|null $by_id
 * @property-read \App\Models\Affiliate|null $by
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\AffiliateSale whereById($value)
 */
class AffiliateSale extends Model
{
    protected $fillable = [
        'order_id',
        'commission'
    ];
    
    public function order() {
        return $this->belongsTo('App\Models\Order');
    }
    
    public function affiliate() {
        return $this->belongsTo('App\Models\Affiliate');
    }
    
    public function by() {
        return $this->hasOne('App\Models\Affiliate', 'id', 'by_id')
                    ->with('user')
                    ->select('user_id', 'id', 'level');
    }
    
    public function payoutQuery(Builder $query) {
        return $query->selectRaw("affiliate_id, paid, reference_no, " .
                                 "SUM(commission) as commission_sum, " .
                                 "DATE_FORMAT(created_at, '%Y-%m') as date")
                     ->groupBy('affiliate_id', 'paid', 'reference_no', 'date');
    }
}
