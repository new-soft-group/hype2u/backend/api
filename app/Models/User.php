<?php

namespace App\Models;

use App\Contracts\Datatable\Searchable;
use App\Notifications\ResetPasswordLink;
use App\Traits\CanSMS;
use App\Traits\HasCart;
use App\Traits\HasOrder;
use App\Traits\HasProfile;
use App\Traits\HasWishlist;
use App\Traits\QueueForStock;
use Carbon\Carbon;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Cashier\Billable;
use Laravel\Passport\HasApiTokens;
use Spatie\Permission\Traits\HasRoles;


/**
 * App\Models\User
 *
 * @property int $id
 * @property string $email
 * @property \Illuminate\Support\Carbon|null $email_verified_at
 * @property string $password
 * @property string|null $provider
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $stripe_id
 * @property string|null $card_brand
 * @property string|null $card_last_four
 * @property string|null $trial_ends_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Address[] $addresses
 * @property-read int|null $addresses_count
 * @property-read \App\Models\Affiliate|null $affiliate
 * @property-read \App\Models\Cart|null $cart
 * @property-read \App\Models\Cart|null $shopCart
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Client[] $clients
 * @property-read int|null $clients_count
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @property-read int|null $notifications_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $orders
 * @property-read int|null $orders_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Permission[] $permissions
 * @property-read int|null $permissions_count
 * @property-read \App\Models\Profile|null $profile
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Queuer[] $queuer
 * @property-read int|null $queuer_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\NotifyQueue[] $queues
 * @property-read int|null $queues_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Spatie\Permission\Models\Role[] $roles
 * @property-read int|null $roles_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Subscription[] $subscriptions
 * @property-read int|null $subscriptions_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\Laravel\Passport\Token[] $tokens
 * @property-read int|null $tokens_count
 * @property-read \App\Models\Wishlist|null $wishlist
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User permission($permissions)
 * @method static Builder|User query()
 * @method static Builder|User role($roles, $guard = null)
 * @method static Builder|User whereCardBrand($value)
 * @method static Builder|User whereCardLastFour($value)
 * @method static Builder|User whereCreatedAt($value)
 * @method static Builder|User whereEmail($value)
 * @method static Builder|User whereEmailVerifiedAt($value)
 * @method static Builder|User whereId($value)
 * @method static Builder|User wherePassword($value)
 * @method static Builder|User whereProvider($value)
 * @method static Builder|User whereRememberToken($value)
 * @method static Builder|User whereStripeId($value)
 * @method static Builder|User whereTrialEndsAt($value)
 * @method static Builder|User whereUpdatedAt($value)
 * @property-read \App\Models\Pick $picks
 * @mixin \Eloquent
 */
class User extends Authenticatable implements Searchable
{
    use HasFactory,
        Notifiable,
        Billable,
        HasProfile,
        HasRoles,
        HasApiTokens,
        HasWishlist,
        HasCart,
        HasOrder,
        CanSMS,
        QueueForStock;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email',
        'password',
        'provider'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $with = ['profile', 'payments'];

    public function affiliate()
    {
        return $this->hasOne(Affiliate::class);
    }

    public function payments()
    {
        return $this->hasMany(PaymentSummary::class, 'user_id', 'id');
    }

    public function picks()
    {
        return $this->hasMany(Pick::class, 'user_id', 'id');
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPasswordLink($token));
    }

    public function resetPassword(string $password)
    {
        $this->password = bcrypt($password);

        $this->save();

        event(new PasswordReset($this));
    }

    public function changePassword(string $password): bool
    {
        $this->password = bcrypt($password);

        return $this->save();
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, $this->getForeignKey())
                    ->orderBy('created_at', 'desc')
                    ->with('plan');
    }

    public function scopeFilterByDate(Builder $builder, array $date)
    {
        $startDate = Carbon::createFromFormat('Y-m-d', min($date))->startOfDay();
        $endDate = Carbon::createFromFormat('Y-m-d', max($date))->endOfDay();

        return $builder->whereBetween('created_at', [$startDate,$endDate]);
    }

    /**
     * @return Subscription|Model|null
     */
    public function defaultSubscription(): ?Subscription
    {
        return $this->subscriptions()->with('plan.surcharges')->first();
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder->where('email', 'like', $keyword)
                       ->orWhereHas('profile', function (Builder $subQuery) use ($keyword) {
                           $subQuery->where('first_name', 'like', $keyword)
                                    ->orWhere('last_name', 'like', $keyword)
                                    ->orWhere('mobile', 'like', $keyword);
                       });
    }

    public function filterCriteria(): array
    {
        return [
            'roles' => 'scopeRole',
            'date' => 'scopeFilterByDate'
        ];
    }

    public function sortCriteria(): array
    {
        return [
            'name' => function(Builder $builder, $direction) {
                return $builder->orderBy('name', $direction);
            },
            'email' => function(Builder $builder, $direction) {
                return $builder->orderBy('email', $direction);
            }
        ];
    }

    public function withRelations(): array
    {
        return $this->with;
    }
}
