<?php

namespace App\Models;

use App\Exceptions\ModelHasAssociate;
use App\Traits\CanEnable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;


/**
 * App\Models\ProductType
 *
 * @property int $id
 * @property string $name
 * @property-read Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|ProductType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductType query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductType whereName($value)
 * @mixin \Eloquent
 */
class ProductType extends Model
{
    use HasFactory , CanEnable;

    protected $fillable = ['name' , 'enabled', 'business_type_id'];

    public $timestamps = false;


    public function products()
    {
        return $this->hasMany(Product::class, 'product_type_id', 'id')
                    ->where('enabled', true);
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class);
    }

    /**
     * @return bool|null
     * @throws ModelHasAssociate
     * @throws \Exception
     */
    public function safeDelete()
    {
        if($this->products()->count()) {
            throw new ModelHasAssociate(__('product.type.delete.failed'));
        }

        return $this->delete();
    }

    public static function massUpdate(array $attributes): Collection
    {
        $ids = Arr::pluck($attributes, 'id');

        $types = self::whereKey($ids)->get();

        foreach ($types as $index => $type) {
            /** @var ProductType $type */
            $type->fill($attributes[$index]);
            $type->save();
        }

        return $types;
    }
}
