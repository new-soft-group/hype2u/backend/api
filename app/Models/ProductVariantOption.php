<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\ProductVariantOption
 *
 * @property int $id
 * @property int $product_variant_id
 * @property int $variant_option_id
 * @property-read \App\Models\VariantOption $lookup
 * @property-read \App\Models\Product|null $product
 * @property-read \App\Models\ProductVariant $variant
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariantOption newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariantOption newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariantOption query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariantOption whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariantOption whereProductVariantId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariantOption whereVariantOptionId($value)
 * @mixin \Eloquent
 */
class ProductVariantOption extends Model
{
    use HasFactory;

    public $timestamps = false;

    public function lookup()
    {
        return $this->belongsTo(VariantOption::class, 'variant_option_id');
    }

    public function product()
    {
        return $this->hasOneThrough(Product::class, ProductVariant::class, 'product_id', 'id');
    }

    public function variant()
    {
        return $this->belongsTo(ProductVariant::class, 'product_variant_id');
    }
}
