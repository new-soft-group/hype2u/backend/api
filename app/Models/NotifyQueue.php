<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



/**
 * App\Models\NotifyQueue
 *
 * @property int $id
 * @property int $stock_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Queuer[] $queuers
 * @property-read int|null $queuers_count
 * @property-read \App\Models\ProductStock $stock
 * @method static Builder|NotifyQueue findByStock($stock)
 * @method static Builder|NotifyQueue newModelQuery()
 * @method static Builder|NotifyQueue newQuery()
 * @method static Builder|NotifyQueue query()
 * @method static Builder|NotifyQueue whereCreatedAt($value)
 * @method static Builder|NotifyQueue whereId($value)
 * @method static Builder|NotifyQueue whereStockId($value)
 * @method static Builder|NotifyQueue whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class NotifyQueue extends Model
{
    use HasFactory;

    protected $fillable = [
        'stock_id'
    ];

    public function scopeFindByStock(Builder $builder, int $stock)
    {
        return $builder->where('stock_id', $stock);
    }

    public function stock()
    {
        return $this->belongsTo(ProductStock::class, 'stock_id');
    }

    public function queuers()
    {
        return $this->hasMany(Queuer::class, 'queue_id');
    }
}
