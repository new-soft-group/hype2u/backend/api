<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Contracts\Datatable\Searchable;


/**
 * App\Models\Wishlist
 *
 * @property int $id
 * @property int $user_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\WishlistItem[] $items
 * @property-read int|null $items_count
 * @property-read \App\Models\User $user
 * @method static Builder|Wishlist newModelQuery()
 * @method static Builder|Wishlist newQuery()
 * @method static Builder|Wishlist query()
 * @method static Builder|Wishlist whereCreatedAt($value)
 * @method static Builder|Wishlist whereId($value)
 * @method static Builder|Wishlist whereUpdatedAt($value)
 * @method static Builder|Wishlist whereUserId($value)
 * @mixin \Eloquent
 */
class Wishlist extends Model implements Searchable
{
    use HasFactory;

    protected $with = ['items', 'user'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(WishlistItem::class);
    }

    /**
     * @param Product|int $product
     * @return bool
     */
    public function hasItem($product)
    {
        $id = $product instanceof Product ? $product->id : $product;

        $item = $this->getItem($id);

        return !is_null($item);
    }

    /**
     * @param Product|int $product
     * @return mixed
     */
    public function getItem($product)
    {
        $id = $product instanceof Product ? $product->id : $product;

        return $this->items->firstWhere('product_id', $id);
    }

    /**
     * @param Product|int $product
     * @return WishlistItem|Model
     */
    public function addItem($product): WishlistItem
    {
        $id = $product instanceof Product ? $product->id : $product;

        if($this->hasItem($id)) {
            return $this->getItem($product);
        }

        /** @var WishlistItem $item */
        $item = $this->items()->make();
        $item->product()->associate($product)->save();

        return $item;
    }

    /**
     * @param Product|int $product
     * @return bool
     */
    public function removeItem($product): bool
    {
        $id = $product instanceof Product ? $product->id : $product;

        return $this->items()
                    ->where('product_id', $id)
                    ->delete();
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $this->where('name', 'like', $keyword);
    }

    public function filterCriteria(): array
    {
        return [];
    }

    public function sortCriteria(): array
    {
        return [];
    }


    public function withRelations(): array
    {
        return $this->with;
    }
}
