<?php


namespace App\Models;


use App\Traits\Removable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\VariantOption
 *
 * @property int $id
 * @property int $variant_id
 * @property string $name
 * @property bool $removable
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductVariantOption[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Profile[] $profiles
 * @property-read int|null $profiles_count
 * @property-read \App\Models\Variant $variant
 * @method static Builder|VariantOption newModelQuery()
 * @method static Builder|VariantOption newQuery()
 * @method static Builder|VariantOption query()
 * @method static Builder|VariantOption whereId($value)
 * @method static Builder|VariantOption whereName($value)
 * @method static Builder|VariantOption whereRemovable($value)
 * @method static Builder|VariantOption whereVariantId($value)
 * @mixin \Eloquent
 */
class VariantOption extends Model
{
    use HasFactory, Removable;

    public $timestamps = false;

    protected $fillable = ['name'];

    protected $casts = [
        'removable' => 'boolean'
    ];

    public function variant()
    {
        return $this->belongsTo(Variant::class);
    }

    public function products()
    {
        return $this->hasMany(ProductVariantOption::class)
                    ->with([
                        'product' => function (Builder $query) {
                            return $query->where('enabled', true);
                        }
                    ]);
    }

    public function profiles()
    {
        return $this->hasMany(Profile::class);
    }
}
