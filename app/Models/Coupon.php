<?php

namespace App\Models;

use App\Contracts\Datatable\Searchable;
use App\Traits\CanEnable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Stripe\Coupon as StripeCoupon;


/**
 * App\Models\Coupon
 *
 * @property int $id
 * @property string $name
 * @property string $stripe_id
 * @property string $code
 * @property float|null $amount_off
 * @property string|null $currency
 * @property string|null $percent_off
 * @property string $duration
 * @property int|null $duration_in_months
 * @property int|null $max_redemptions
 * @property int|null $times_redeemed
 * @property string|null $redeem_by
 * @property bool $enabled
 * @property \Illuminate\Support\Carbon|null $deleted_at
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Invoice[] $invoices
 * @property-read int|null $invoices_count
 * @method static Builder|Coupon enabled($state = true)
 * @method static Builder|Coupon findByCode($code)
 * @method static Builder|Coupon findByStripeId($stripeId)
 * @method static Builder|Coupon newModelQuery()
 * @method static Builder|Coupon newQuery()
 * @method static \Illuminate\Database\Query\Builder|Coupon onlyTrashed()
 * @method static Builder|Coupon query()
 * @method static Builder|Coupon whereAmountOff($value)
 * @method static Builder|Coupon whereCode($value)
 * @method static Builder|Coupon whereCreatedAt($value)
 * @method static Builder|Coupon whereCurrency($value)
 * @method static Builder|Coupon whereDeletedAt($value)
 * @method static Builder|Coupon whereDuration($value)
 * @method static Builder|Coupon whereDurationInMonths($value)
 * @method static Builder|Coupon whereEnabled($value)
 * @method static Builder|Coupon whereId($value)
 * @method static Builder|Coupon whereMaxRedemptions($value)
 * @method static Builder|Coupon whereName($value)
 * @method static Builder|Coupon wherePercentOff($value)
 * @method static Builder|Coupon whereRedeemBy($value)
 * @method static Builder|Coupon whereStripeId($value)
 * @method static Builder|Coupon whereTimesRedeemed($value)
 * @method static Builder|Coupon whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|Coupon withTrashed()
 * @method static \Illuminate\Database\Query\Builder|Coupon withoutTrashed()
 * @mixin \Eloquent
 */
class Coupon extends Model implements Searchable
{
    use HasFactory, SoftDeletes, CanEnable;

    protected $fillable = [
        'amount_off',
        'currency',
        'percent_off',
        'duration',
        'duration_in_months',
        'max_redemptions',
        'redeem_by'
    ];

    const DURATION_ONCE = 'once';
    const DURATION_REPEATING = 'repeating';
    const DURATION_FOREVER = 'forever';

    protected $casts = [
        'enabled' => 'boolean',
        'amount_off' => 'float',
        'percentage' => 'float',
        'times_redeemed' => 'integer',
    ];

    public function scopeFindByCode(Builder $builder, string $code)
    {
        return $builder->firstWhere('code', $code);
    }

    public function amount(float $amount, string $currency)
    {
        $this->amount_off = $amount;
        $this->currency = $currency;

        return $this;
    }

    public function percentage(float $amount)
    {
        $this->percent_off = $amount;

        return $this;
    }

    public function isCodeBelongsPlan(string $stripe_plan) : bool
    {
        $plan = $this->plans()->where('stripe_plan', $stripe_plan)->get();

        if($plan->count()) return true;

        return false;
    }

    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    public function setCode(string $code)
    {
        $this->code = $code;

        return $this;
    }

    public function setStripeId(string $stripeId)
    {
        $this->stripe_id = $stripeId;

        return $this;
    }

    public function setDuration(string $duration)
    {
        $this->duration = $duration;

        return $this;
    }

    public function setPlans(array $ids)
    {
        $this->plans()->sync($ids);

        return $this;
    }

    public function plans()
    {
        return $this->belongsToMany(Plan::class, 'plan_coupons', 'plan_id', 'coupon_id');
    }

    public function setOptions(array $options)
    {
        return $this->fill($options);
    }

    public function setDiscount(float $amount, string $currency = '')
    {
        return empty($currency)
            ? $this->percentage($amount)
            : $this->amount($amount, $currency);
    }

    public function invoices()
    {
        return $this->hasMany(Invoice::class, 'stripe_coupon', 'stripe_id');
    }

    public function scopeFindByStripeId(Builder $builder, string $stripeId)
    {
        return $builder->firstWhere('stripe_id', $stripeId);
    }

    public function isFixedAmount(): bool
    {
        return !is_null($this->currency);
    }

    public function getStripeOptions(): array
    {
        $options = [
            'name' => $this->name,
            'duration' => $this->duration
        ];

        $options = $this->isFixedAmount()
            ? array_merge($options, ['amount_off' => amountToCent($this->amount_off), 'currency' => $this->currency])
            : array_merge($options, ['percent_off' => $this->percent_off]);

        if($this->duration_in_months) {
            $options = array_merge($options, ['duration_in_months' => $this->duration_in_months]);
        }
        if($this->max_redemptions) {
            $options = array_merge($options, ['max_redemptions' => $this->max_redemptions]);
        }
        if($this->redeem_by) {
            $options = array_merge($options, [
                'redeem_by' => Carbon::parse($this->redeem_by)->unix()
            ]);
        }

        return $options;
    }

    /**
     * @param StripeCoupon $coupon
     */
    public function syncWithStripeCoupon(StripeCoupon $coupon): void
    {
        $this->times_redeemed = $coupon->times_redeemed;

        $this->fill([
            'duration_in_months' => $coupon->duration_in_months,
            'max_redemptions' => $coupon->max_redemptions,
            'redeem_by' => $coupon->redeem_by,
        ])->save();
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder->where('name', 'like', $keyword)
                       ->orWhere('code', 'like', $keyword);
    }

    public function filterCriteria(): array
    {
        return [];
    }

    public function sortCriteria(): array
    {
        return [];
    }

    public function withRelations(): array
    {
        return ['plans'];
    }
}
