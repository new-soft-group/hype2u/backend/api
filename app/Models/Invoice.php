<?php

namespace App\Models;

use App\Contracts\Datatable\Searchable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;



/**
 * App\Models\Invoice
 *
 * @property int $id
 * @property string $invoiceable_type
 * @property int $invoiceable_id
 * @property string $stripe_id
 * @property string $payment_intent
 * @property string $amount
 * @property string $currency
 * @property string $status
 * @property string|null $stripe_coupon
 * @property string|null $pdf
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Coupon|null $coupon
 * @property-read Model|\Eloquent $invoiceable
 * @method static Builder|Invoice newModelQuery()
 * @method static Builder|Invoice newQuery()
 * @method static Builder|Invoice query()
 * @method static Builder|Invoice whereAmount($value)
 * @method static Builder|Invoice whereCreatedAt($value)
 * @method static Builder|Invoice whereCurrency($value)
 * @method static Builder|Invoice whereId($value)
 * @method static Builder|Invoice whereInvoiceableId($value)
 * @method static Builder|Invoice whereInvoiceableType($value)
 * @method static Builder|Invoice wherePaymentIntent($value)
 * @method static Builder|Invoice wherePdf($value)
 * @method static Builder|Invoice whereStatus($value)
 * @method static Builder|Invoice whereStripeCoupon($value)
 * @method static Builder|Invoice whereStripeId($value)
 * @method static Builder|Invoice whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Invoice extends Model implements Searchable
{
    use HasFactory;

    public function invoiceable()
    {
        return $this->morphTo('invoiceable');
    }

    public function coupon()
    {
        return $this->belongsTo(Coupon::class, 'stripe_coupon', 'stripe_id')
                    ->withoutGlobalScopes();
    }

    public function paid()
    {
        return $this->status === 'paid';
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder;
    }

    public function filterCriteria(): array
    {
        return [];
    }

    public function sortCriteria(): array
    {
        return [];
    }

    public function withRelations(): array
    {
        return ['coupon'];
    }
}
