<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\ProductVariant
 *
 * @property int $id
 * @property int $product_id
 * @property int $variant_id
 * @property-read mixed $name
 * @property-read \App\Models\Variant $lookup
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductVariantOption[] $options
 * @property-read int|null $options_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\VariantOption[] $pivotOptions
 * @property-read int|null $pivot_options_count
 * @property-read \App\Models\Product $product
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariant newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariant newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariant query()
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariant whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariant whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|ProductVariant whereVariantId($value)
 * @mixin \Eloquent
 */
class ProductVariant extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $with = ['lookup'];

    public function lookup()
    {
        return $this->belongsTo(Variant::class, 'variant_id');
    }

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function pivotOptions()
    {
        return $this->belongsToMany(VariantOption::class, 'product_variant_options');
    }

    public function options()
    {
        return $this->hasMany(ProductVariantOption::class)
                    ->with('lookup');
    }

    public function getNameAttribute()
    {
        return $this->lookup->name;
    }
}
