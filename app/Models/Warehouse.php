<?php

namespace App\Models;

use App\Contracts\Datatable\Searchable;
use App\Traits\CanEnable;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\ModelHasAssociate;


/**
 * App\Models\Warehouse
 *
 * @property int $id
 * @property string $country
 * @property string $name
 * @property string $slug
 * @property string $address1
 * @property string|null $address2
 * @property string $city
 * @property string|null $phone
 * @property string|null $fax
 * @property int $default
 * @property bool $enabled
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductStockInventory[] $items
 * @property-read int|null $items_count
 * @method static Builder|Warehouse enabled($state = true)
 * @method static Builder|Warehouse newModelQuery()
 * @method static Builder|Warehouse newQuery()
 * @method static Builder|Warehouse query()
 * @method static Builder|Warehouse whereAddress1($value)
 * @method static Builder|Warehouse whereAddress2($value)
 * @method static Builder|Warehouse whereCity($value)
 * @method static Builder|Warehouse whereCountry($value)
 * @method static Builder|Warehouse whereCreatedAt($value)
 * @method static Builder|Warehouse whereDefault($value)
 * @method static Builder|Warehouse whereEnabled($value)
 * @method static Builder|Warehouse whereFax($value)
 * @method static Builder|Warehouse whereId($value)
 * @method static Builder|Warehouse whereName($value)
 * @method static Builder|Warehouse wherePhone($value)
 * @method static Builder|Warehouse whereSlug($value)
 * @method static Builder|Warehouse whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Warehouse extends Model implements Searchable
{
    use HasFactory, CanEnable;

    protected $fillable = [
        'country',
        'name',
        'slug',
        'address1',
        'address2',
        'city',
        'phone',
        'fax',
        'default'
    ];

    protected $casts = [
        'enabled' => 'boolean',
    ];

    public function items()
    {
        return $this->hasMany(ProductStockInventory::class);
    }

    /**
     * @throws \Exception
     * @throws ModelHasAssociate
     */
    public function safeDelete()
    {
        if($this->items()->count() ) {
            throw new ModelHasAssociate(__('warehouse.delete.failed'));
        }

        return $this->delete();
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder->where('name', 'like', $keyword);
    }

    public function filterCriteria(): array
    {
        return [];
    }

    public function sortCriteria(): array
    {
        return [
            'created_at' => function(Builder $builder, $direction) {
                return $builder->orderBy('created_at', $direction);
            },
        ];
    }

    public function withRelations(): array
    {
        return [];
    }
}
