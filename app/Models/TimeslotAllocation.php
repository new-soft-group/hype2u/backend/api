<?php


namespace App\Models;


use App\Contracts\Datatable\Searchable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\TimeslotAllocation
 *
 * @property int $id
 * @property int $timeslot_id
 * @property int $order_id
 * @property \Illuminate\Support\Carbon|null $start_at
 * @property \Illuminate\Support\Carbon|null $end_at
 * @property-read \App\Models\Order $order
 * @property-read \App\Models\Timeslot $timeslot
 * @method static Builder|TimeslotAllocation day($day)
 * @method static Builder|TimeslotAllocation month($month)
 * @method static Builder|TimeslotAllocation newModelQuery()
 * @method static Builder|TimeslotAllocation newQuery()
 * @method static Builder|TimeslotAllocation query()
 * @method static Builder|TimeslotAllocation week($today)
 * @method static Builder|TimeslotAllocation whereEndAt($value)
 * @method static Builder|TimeslotAllocation whereId($value)
 * @method static Builder|TimeslotAllocation whereOrderId($value)
 * @method static Builder|TimeslotAllocation whereStartAt($value)
 * @method static Builder|TimeslotAllocation whereTimeslotId($value)
 * @mixin \Eloquent
 */
class TimeslotAllocation extends Model implements Searchable
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'start_at',
        'end_at'
    ];

    protected $dates = [
        'start_at',
        'end_at'
    ];

    protected $casts = [
        'timeslot_id' => 'integer',
        'order_id' => 'integer'
    ];

    public function timeslot()
    {
        return $this->belongsTo(Timeslot::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    /**
     * @param mixed $timeslot
     * @param $date
     */
    public function reschedule($timeslot, $date): void
    {
        if(is_int($timeslot)) {
            $timeslot = Timeslot::find($timeslot);
        }

        $this->timeslot()->associate($timeslot)->save();

        $this->update([
            'start_at' => Timeslot::buildTimestamp($date, $timeslot->start_time),
            'end_at' => Timeslot::buildTimestamp($date, $timeslot->end_time)
        ]);
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder;
    }

    public function scopeMonth(Builder $builder, string $month)
    {
        $date = Carbon::parse($month);

        return $builder->whereYear('start_at', $date->year)
                       ->whereMonth('start_at', $date->month);
    }

    public function scopeDay(Builder $builder, string $day)
    {
        return $builder->whereDate('start_at', $day);
    }

    public function scopeWeek(Builder $builder, string $today)
    {
        $today = Carbon::parse($today);

        return $builder->whereBetween('start_at', [
            $today->startOfWeek(Carbon::SUNDAY)->toDate(),
            $today->endOfWeek(Carbon::SATURDAY)->toDate()
        ]);
    }

    public function filterCriteria(): array
    {
        return [
            'day' => 'scopeDay',
            'week' => 'scopeWeek',
            'month' => 'scopeMonth'
        ];
    }

    public function sortCriteria(): array
    {
        return [];
    }

    public function withRelations(): array
    {
        return ['timeslot.type'];
    }
}
