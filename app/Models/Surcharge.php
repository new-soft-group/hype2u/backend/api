<?php

namespace App\Models;

use App\Collections\SurchargeCollection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Surcharge
 *
 * @property int $id
 * @property int|null $plan_id
 * @property float $threshold
 * @property float $charge
 * @property bool $default
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Plan|null $plan
 * @method static SurchargeCollection|static[] all($columns = ['*'])
 * @method static SurchargeCollection|static[] get($columns = ['*'])
 * @method static \Illuminate\Database\Eloquent\Builder|Surcharge newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Surcharge newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Surcharge query()
 * @method static \Illuminate\Database\Eloquent\Builder|Surcharge whereCharge($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Surcharge whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Surcharge whereDefault($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Surcharge whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Surcharge wherePlanId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Surcharge whereThreshold($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Surcharge whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Surcharge extends Model
{
    use HasFactory;

    protected $casts = [
        'plan_id' => 'int',
        'threshold' => 'float',
        'charge' => 'float',
        'default' => 'boolean'
    ];

    public function plan()
    {
        return $this->belongsTo(Plan::class);
    }

    public function setThreshold($amount)
    {
        $this->threshold = $amount;

        return $this;
    }

    public function setCharge($charge)
    {
        $this->charge = $charge;

        return $this;
    }

    public function newCollection(array $models = [])
    {
        return new SurchargeCollection($models);
    }
}
