<?php


namespace App\Models\Pivots;


use Illuminate\Database\Eloquent\Relations\Pivot;

class PolicyRule extends Pivot
{
    protected $casts = [
        'prime' => 'boolean'
    ];

    public function getPrime(): bool
    {
        return $this->getAttribute('prime');
    }
}
