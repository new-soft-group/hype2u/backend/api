<?php


namespace App\Models\Pivots;


use Illuminate\Database\Eloquent\Relations\Pivot;

class Allocation extends Pivot
{
    protected $dates = [
        'start_at',
        'end_at'
    ];
}
