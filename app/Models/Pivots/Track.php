<?php


namespace App\Models\Pivots;


use Illuminate\Database\Eloquent\Relations\Pivot;

class Track extends Pivot
{
    protected $dates = [
        'scanned_at'
    ];
}
