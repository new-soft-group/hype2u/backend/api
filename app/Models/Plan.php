<?php

namespace App\Models;


use App\Collections\CartItemCollection;
use App\Contracts\Datatable\Searchable;
use App\Traits\CanEnable;
use App\Traits\HasPolicy;
use App\Traits\HasSurcharge;
use App\Validators\Plan\PlanValidator;
use App\Validators\Plan\Policy\PolicyValidator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Str;
use Stripe\StripeObject;


/**
 * App\Models\Plan
 *
 * @property int $id
 * @property string $name
 * @property string $stripe_plan
 * @property string $stripe_product
 * @property float $amount
 * @property string $purchase_amount
 * @property string $interval
 * @property string $slug
 * @property string $description
 * @property string $info
 * @property int $exchange_limit
 * @property int|null $max_quantity
 * @property int $sort
 * @property bool $enabled
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Collections\PolicyCollection|\App\Models\PlanPolicy[] $policies
 * @property-read int|null $policies_count
 * @property-read \App\Collections\SurchargeCollection|\App\Models\Surcharge[] $surcharges
 * @property-read int|null $surcharges_count
 * @method static Builder|Plan enabled($state = true)
 * @method static Builder|Plan newModelQuery()
 * @method static Builder|Plan newQuery()
 * @method static Builder|Plan query()
 * @method static Builder|Plan whereAmount($value)
 * @method static Builder|Plan whereCreatedAt($value)
 * @method static Builder|Plan whereDescription($value)
 * @method static Builder|Plan whereEnabled($value)
 * @method static Builder|Plan whereExchangeLimit($value)
 * @method static Builder|Plan whereId($value)
 * @method static Builder|Plan whereInterval($value)
 * @method static Builder|Plan whereMaxQuantity($value)
 * @method static Builder|Plan whereName($value)
 * @method static Builder|Plan wherePurchaseAmount($value)
 * @method static Builder|Plan whereSlug($value)
 * @method static Builder|Plan whereSort($value)
 * @method static Builder|Plan whereStripePlan($value)
 * @method static Builder|Plan whereStripeProduct($value)
 * @method static Builder|Plan whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Plan extends Model implements Searchable
{
    use HasFactory, CanEnable, HasPolicy, HasSurcharge;

    const DAY_INTERVAL = 'day';
    const WEEK_INTERVAL = 'week';
    const MONTH_INTERVAL = 'month';
    const YEAR_INTERVAL = 'year';

    protected $fillable = [
        'name',
        'slug',
        'description',
        'purchase_amount',
        'exchange_limit',
        'sort',
        'sms',
        'featured',
        'info'
    ];

    protected $casts = [
        'enabled' => 'boolean',
        'amount' => 'float',
        'purchase_amount' => 'float',
        'max_quantity' => 'int',
        'exchange_limit' => 'int',
    ];

    protected static function booted()
    {
        static::creating(function (Plan $plan) {
            if (empty($plan->slug)) {
                $plan->slug = Str::slug((string)$plan->name, '-');
            }
        });
    }

    public function setStripe(StripeObject $response)
    {
        $this->stripe_plan = $response->id;
        $this->stripe_product = $response->product;

        return $this;
    }

    public function setAmount(float $amount)
    {
        $this->amount = $amount;

        return $this;
    }

    public function subscriptions()
    {
        return $this->hasMany(Subscription::class, 'stripe_plan', 'stripe_plan');
    }

    public function coupons()
    {
        return $this->belongsToMany(Coupon::class, 'plan_coupons', 'coupon_id', 'plan_id');
    }

    public function setInterval(string $interval)
    {
        $this->interval = $interval;

        return $this;
    }

    public function getStripeOption($currency = 'myr'): array
    {
        return [
            'amount' => amountToCent($this->amount),
            'currency' => $currency,
            'interval' => $this->interval,
            'product' => [
                'name' => $this->name
            ]
        ];
    }

    public function validator()
    {
        return new PlanValidator($this);
    }

    public function policyValidator(CartItemCollection $items)
    {
        return new PolicyValidator($this->policies, $items);
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $this->where('name', 'like', $keyword);
    }

    public function filterCriteria(): array
    {
        return [];
    }

    public function sortCriteria(): array
    {
        return [];
    }

    public function withRelations(): array
    {
        return [];
    }
}
