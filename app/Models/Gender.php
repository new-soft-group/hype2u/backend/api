<?php


namespace App\Models;


use App\Traits\Removable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Gender
 *
 * @property int $id
 * @property string $name
 * @property bool $removable
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Profile[] $profiles
 * @property-read int|null $profiles_count
 * @method static \Illuminate\Database\Eloquent\Builder|Gender newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Gender newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Gender query()
 * @method static \Illuminate\Database\Eloquent\Builder|Gender whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Gender whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Gender whereRemovable($value)
 * @mixin \Eloquent
 */
class Gender extends Model
{
    use HasFactory, Removable;

    protected $fillable = ['name'];

    public $timestamps = false;

    protected $casts = [
        'removable' => 'boolean'
    ];

    public function products()
    {
        return $this->hasMany(Product::class, 'gender_id', 'id')
                    ->where('enabled', true);
    }

    public function profiles()
    {
        return $this->hasMany(Profile::class, 'gender_id', 'id');
    }
}
