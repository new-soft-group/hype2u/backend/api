<?php

namespace App\Models;

use App\Contracts\Datatable\Searchable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Log;

/**
 * App\Models\Pick
 *
 * @property int $id
 * @property int $gender_id
 * @property int $user_id
 * @property string $sizes
 * @property string styles
 * @property string $fits
 * @property string $brands
 * @property string $colors
 * @property string $status
 * @property string $remark
 *
 * @property-read \App\Models\User $user
 *
 */

class Pick extends Model implements Searchable
{
    use HasFactory;

    protected $with = [
        'gender',
        'user'
    ];

    protected $fillable = [
        'user_id',
        'gender_id',
        'sizes',
        'styles',
        'fits',
        'brands',
        'colors',
        'status',
        'remark'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function gender()
    {
        return $this->belongsTo(Gender::class);
    }

    public function setAttended()
    {
        $this->status = PickStatus::ATTENDED;

        return $this->save();
    }

    public function setPending()
    {
        $this->status = PickStatus::PENDING;

        return $this->save();
    }

    public function setClose()
    {
        $this->status = PickStatus::CLOSE;

        return $this->save();
    }

    public function setRejected()
    {
        $this->status = PickStatus::REJECTED;

        return $this->save();
    }

    public function scopeFilterByDate(Builder $builder, array $date)
    {
        $startDate = Carbon::createFromFormat('Y-m-d', min($date))->startOfDay();
        $endDate = Carbon::createFromFormat('Y-m-d', max($date))->endOfDay();

        return $builder->whereBetween('created_at', [$startDate,$endDate]);
    }

    public function scopeFilterByStatus(Builder $builder, string $status)
    {
        return $builder->where('status', $status);
    }

    public function scopeFilterByGender(Builder $builder, array $genderId)
    {
        return $builder->whereHas(
            'gender',
            function (Builder $builder) use ($genderId) {
                $builder->whereIn('id', $genderId);
            });
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder->whereHas(
            'user',
            function (Builder $builder) use ($keyword) {
                $builder->whereHas('profile', function(Builder $builder) use ($keyword){
                    return $builder->where('first_name', 'like', $keyword)
                        ->orWhere('last_name', 'like', $keyword);
                });
            });
    }

    public function filterCriteria(): array
    {
        return [
            'date' => 'scopeFilterByDate',
            'status' => 'scopeFilterByStatus',
            'gender' => 'scopeFilterByGender'
        ];
    }

    public function sortCriteria(): array
    {
        return [];
    }

    public function withRelations(): array
    {
        return $this->with;
    }
}
