<?php


namespace App\Models;


use App\Collections\CartItemCollection;
use App\Collections\Carry\CarryForward;
use App\Collections\Carry\CarryForwardCollection;
use App\Exceptions\DuplicateCartItemException;
use App\Exceptions\EmptyCartException;
use App\Exceptions\InvalidProductBusinessType;
use App\Exceptions\InvalidProductStockException;
use App\Exceptions\ModelHasAssociate;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;


/**
 * App\Models\Cart
 *
 * @property int $id
 * @property int|null $user_id
 * @property string|null $session_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\CartItem[] $items
 * @property-read int|null $items_count
 * @property-read \App\Models\User|null $user
 * @method static \Illuminate\Database\Eloquent\Builder|Cart newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cart newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Cart query()
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereSessionId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Cart whereUserId($value)
 * @mixin \Eloquent
 */
class Cart extends Model
{
    use HasFactory;

    const SESSION_HEADER = 'X-Cart-Session';

    protected $fillable = [
        'business_type_id'
    ];

    /**
     * @var CarryForwardCollection
     */
    protected $carryForward;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(CartItem::class);
    }

    public function businessType()
    {
        return $this->belongsTo(BusinessType::class, 'business_type_id');
    }

    public static function findBySession($session)
    {
        return self::firstWhere(['session_id' => $session]);
    }

    public static function getSessionCart()
    {
        if(request()->hasHeader(self::SESSION_HEADER)) {
            return self::findBySession(request()->header(self::SESSION_HEADER));
        }

        $cart = new static();
        $cart->session_id = session()->getId();
        $cart->save();

        return $cart;
    }

    public function addItem($attribute, $businessType)
    {
        $findCriteria = Arr::only($attribute, ['product_id', 'product_stock_id', 'product_type_id']);

        $this->validateCartProduct($attribute['product_id'], $attribute['product_stock_id']);

        $item = $this->items()->firstOrCreate($findCriteria, $attribute);

        if(!$item->wasRecentlyCreated) {
            if($businessType == BusinessType::RENT){
                throw new DuplicateCartItemException();
            }
            $item->increment('quantity', $attribute['quantity'] ?? 1);
        }

        return $item;
    }

    public function validateCartProduct($productId, $productStockId)
    {
        $product = Product::find($productId)->load('stocks');

        $stock = ProductStock::find($productStockId);

        if(!$product || !$stock) throw new ModelHasAssociate("Stock not found.");

        if(!$product->stocks->firstWhere('id', $stock->id)){
            throw new InvalidProductStockException();
        }
    }

    public function attemptAddItem($attribute, $businessType = BusinessType::RENT)
    {
        /** Check is product's business type is valid to add */
        $this->validateCartItemProductBusinessType($attribute['product_id'], $businessType);

        /** @var CartItem $item */
        $item = $this->items()
                     ->where('product_id', $attribute['product_id'])
                     ->where('product_stock_id', $attribute['product_stock_id'])
                     ->first();

        if(is_null($item)) {
            $this->items->push($this->items()->make($attribute));
        }
        else {
            $this->items->each(function (CartItem $_item) use ($item) {
                if($_item->id === $item->id) {
                    $_item->quantity += 1;
                }
            });
        }
    }

    public function attemptUpdateItem($id, $attribute)
    {
        $item = $this->items()->find($id);

        if(is_null($item)) return false;

        /** @var CartItem $item */
        $item = $item->fill($attribute);

        $this->items->each(function ($_item) use ($item) {
            if($_item->id === $item->id) {
                $_item->fill($item->toArray());
            }
        });

        return true;
    }

    public function validateCartItemProductBusinessType($productId, $businessType)
    {
        $product = Product::find($productId);

        if(!$product->businessType()->where('id',$businessType)->count()) throw new InvalidProductBusinessType();
    }

    public function updateItem($id, $attribute)
    {
        $item = $this->items()->find($id);

        if(is_null($item)) return null;

        $item->fill($attribute);

        $item->save();

        return $item;
    }

    public function removeItem($id)
    {
        $item = $this->items()->find($id);

        try {
            if(!is_null($item)) {
                return $item->delete();
            }
        }
        catch (\Exception $e) {

            Log::warning('Failed to delete cart item', ['message' => $e->getMessage()]);
        }

        return false;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     * @throws EmptyCartException
     */
    public function checkout()
    {
        $items = $this->items()->get();

        if($items->isEmpty()) {
            throw new EmptyCartException(__('cart.empty'), 400);
        }

        $this->items()->delete();

        return $items;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     * @throws EmptyCartException
     */
    public function processCheckout()
    {
        $items = $this->items()->get();

        if($items->isEmpty()) {
            throw new EmptyCartException(__('cart.empty'), 400);
        }

        return $items;
    }

    public function totalAmount(array $exclude = []): float
    {
        $amount = 0;

        $this->loadMissing('items.product', 'items.option');

        if ($this->getCarryForward()->isNotEmpty()) {
            $amount += $this->getCarryForward()->totalAmount($exclude);
        }

        $amount += $this->items->sum(function (CartItem $item) use ($exclude) {
            return $item->getAmount($exclude);
        });

        return $amount;
    }

    public function shopTotalAmount(array $exclude = []): float
    {
        $amount = 0;

        $this->loadMissing('items.product', 'items.option');

        $amount += $this->items->sum(function (CartItem $item) use ($exclude) {
            return $item->getAmount($exclude);
        });

        return $amount;
    }

    public function totalQuantity(): int
    {
        return $this->items->sum('quantity');
    }

    public function setCarryForward(CarryForwardCollection $collection)
    {
        $this->carryForward = $collection;

        $this->setRelation('carryForward', $this->carryForward);

        return $this;
    }

    public function getCarryForward(): CarryForwardCollection
    {
        return $this->carryForward ?: new CarryForwardCollection();
    }

    public function mergeCarryForwardItems()
    {
        $collection = new CartItemCollection();
        $collection->push(...$this->items);

        /** @var CarryForward $carry */
        foreach ($this->getCarryForward() as $carry) {
            $collection->push(new CartItem([
                'product_id' => $carry->getProduct()->id,
                'product_stock_id' => $carry->getStockKey(),
                'product_type_id' => $carry->getProduct()->type->id,
                'quantity' => $carry->getQuantity()
            ]));
        }

        return $collection;
    }
}
