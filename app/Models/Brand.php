<?php


namespace App\Models;


use App\Exceptions\ModelHasAssociate;
use App\Traits\CanEnable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;


/**
 * App\Models\Brand
 *
 * @property int $id
 * @property string $name
 * @property-read Collection|\App\Models\Product[] $products
 * @property-read int|null $products_count
 * @method static \Illuminate\Database\Eloquent\Builder|Brand newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand query()
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Brand whereName($value)
 * @mixin \Eloquent
 */
class Brand extends Model
{
    use HasFactory , CanEnable;

    protected $fillable = ['name' , 'enabled'];

    public $timestamps = false;

    public function products()
    {
        return $this->hasMany(Product::class, 'brand_id', 'id')
                    ->where('enabled', true);
    }

    /**
     * @return bool|null
     * @throws ModelHasAssociate
     * @throws \Exception
     */
    public function safeDelete()
    {
        if($this->products()->count()) {
            throw new ModelHasAssociate(__('brand.delete.failed'));
        }

        return $this->delete();
    }

    public static function massUpdate(array $attributes): Collection
    {
        $ids = Arr::pluck($attributes, 'id');

        $brands = self::whereKey($ids)->get();

        foreach ($brands as $index => $brand) {
            /** @var Brand $brand */
            $brand->fill($attributes[$index]);
            $brand->save();
        }

        return $brands;
    }
}
