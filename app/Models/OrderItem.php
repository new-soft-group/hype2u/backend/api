<?php

namespace App\Models;

use App\Exceptions\InsufficientStockException;
use App\Exceptions\OutOfStockException;
use App\Traits\HasPricing;
use Exception;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\OrderItem
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property int|null $product_stock_id
 * @property float $unit_price
 * @property int $quantity
 * @property string|null $variant
 * @property float $variant_price
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $amount
 * @property-read mixed $pricing
 * @property-read \App\Models\Order $order
 * @property-read \App\Models\Product $product
 * @property-read \App\Models\ProductStock|null $stock
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem query()
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereProductId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereProductStockId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereQuantity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereUnitPrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereVariant($value)
 * @method static \Illuminate\Database\Eloquent\Builder|OrderItem whereVariantPrice($value)
 * @mixin \Eloquent
 */
class OrderItem extends Model
{
    use HasFactory, HasPricing;

    protected $casts = [
        'unit_price' => 'float',
        'variant_price' => 'float',
    ];

    protected $priceField = 'unit_price';

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }

    public function stock()
    {
        return $this->belongsTo(ProductStock::class, 'product_stock_id');
    }

    public function getAmountAttribute()
    {
//        $unitPrice = $this->unit_price + $this->variant_price;
        $unitPrice = $this->unit_price;

        return round($unitPrice * $this->quantity);
    }

    public function setVariant(ProductStock $stock)
    {
        $this->variant = $stock->combination;
        $this->variant_price = $stock->price;

        return $this;
    }

    /**
     * @param ProductStock $stock
     * @param int $quantity
     * @throws OutOfStockException
     * @throws InsufficientStockException
     */
    public function allocateStock(ProductStock $stock, int $quantity)
    {
        /**
         * Acquire atomic lock
         */
        $lock = $stock->getLock();

        try {
            /**
             * Lock acquire for 5 seconds
             */
            $lock->block(5);
            /**
             * Total available inventory in this stock
             */
            $inventoryCount = $stock->availableInventoryCount();

            if(empty($inventoryCount)) {
                throw new OutOfStockException(__('product.out_of_stock', [
                    'name' => $stock->product->name,
                    'variant' => $stock->combination
                ]), 400);
            }

            if($quantity > $inventoryCount) {
                throw new InsufficientStockException(__('product.insufficient_stock', [
                    'name' => $stock->product->name,
                    'variant' => $stock->combination,
                    'quantity' => $inventoryCount
                ]), 400);
            }
            /**
             * Set order item quantity
             */
            $this->quantity = $quantity;
            /**
             * Reserve stock inventory
             */
            $stock->reserved($quantity);
            /**
             * Associate product stock for stock recovery when exception happen
             */
            $this->stock()->associate($stock);
        }
        catch (Exception $exception) {
            throw $exception;
        }
        finally {
            optional($lock)->release();
        }
    }
}
