<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BusinessType extends Model
{
    use HasFactory;

    protected $fillable = ['name'];

    const RENT = 1;
    const SHOP = 2;

    public function products()
    {
        return $this->hasMany(Product::class);
    }

}
