<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use App\Contracts\Datatable\Searchable;


/**
 * App\Models\WishlistItem
 *
 * @property int $id
 * @property int $wishlist_id
 * @property int $product_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\Product $product
 * @method static Builder|WishlistItem filterByProductType($typeId)
 * @method static Builder|WishlistItem newModelQuery()
 * @method static Builder|WishlistItem newQuery()
 * @method static Builder|WishlistItem query()
 * @method static Builder|WishlistItem searchByProductName($name)
 * @method static Builder|WishlistItem whereCreatedAt($value)
 * @method static Builder|WishlistItem whereId($value)
 * @method static Builder|WishlistItem whereProductId($value)
 * @method static Builder|WishlistItem whereUpdatedAt($value)
 * @method static Builder|WishlistItem whereWishlistId($value)
 * @mixin \Eloquent
 */
class WishlistItem extends Model implements Searchable
{
    use HasFactory;

    protected $fillable = ['product_id'];

    protected $with = ['product'];


    public function product()
    {
        return $this->belongsTo(Product::class)
                    ->with('images');
    }

    public function scopeSearchByProductName(Builder $builder, string $name)
    {
        return $builder->whereHas(
            'product',
            function (Builder $builder) use ($name) {
                $builder->where('name','like', $name);
            });
    }

    public function scopeFilterByProductType(Builder $builder, array $typeId)
    {
        return $builder->whereHas(
            'product',
            function (Builder $builder) use ($typeId) {
                $builder->whereHas('type',function(Builder $builder) use($typeId){
                    $builder->whereIn('id',$typeId);
                });
            });
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $this->scopeSearchByProductName($builder, $keyword);
    }

    public function filterCriteria(): array
    {
        return ['types' => 'scopeFilterByProductType'];
    }

    public function sortCriteria(): array
    {
        return [];
    }


    public function withRelations(): array
    {
        return $this->with;
    }
}
