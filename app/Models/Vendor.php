<?php

namespace App\Models;

use App\Contracts\Datatable\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Builder;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Vendor
 *
 * @property int $id
 * @property string $company_name
 * @property string|null $contact_name
 * @property string|null $contact_title
 * @property string $address1
 * @property string|null $address2
 * @property string $city
 * @property string $country
 * @property string|null $phone
 * @property string|null $fax
 * @property string|null $email
 * @property string|null $site_url
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\ProductStock[] $stocks
 * @property-read int|null $stocks_count
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor query()
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereAddress1($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereAddress2($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereCity($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereCompanyName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereContactName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereContactTitle($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereCountry($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereFax($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereSiteUrl($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Vendor whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Vendor extends Model implements Searchable
{
    use HasFactory;

    protected $fillable = [
        'company_name',
        'contact_name',
        'contact_title',
        'address1',
        'address2',
        'city',
        'country',
        'phone',
        'fax',
        'email',
        'site_url'
    ];

    public function stocks()
    {
        return $this->hasMany(ProductStock::class);
    }

    /**
     * @throws \Exception
     */
    public function safeDelete()
    {
        return $this->delete();
    }


    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder->where('contact_name', 'like', $keyword);
    }

    public function filterCriteria(): array
    {
        return [];
    }

    public function sortCriteria(): array
    {
        return [
            'created_at' => function(Builder $builder, $direction) {
                return $builder->orderBy('created_at', $direction);
            },
        ];
    }

    public function withRelations(): array
    {
        return [];
    }

}
