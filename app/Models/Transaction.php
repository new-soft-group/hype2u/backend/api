<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Transaction
 *
 * @property string $id
 * @property int $order_id
 * @property int $transaction_status_id
 * @property float $amount
 * @property string $currency
 * @property float $currency_rate
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read mixed $converted_amount
 * @property-read \App\Models\Order $order
 * @property-read \App\Models\TransactionStatus $status
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereAmount($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCurrency($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereCurrencyRate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereOrderId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereTransactionStatusId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Transaction whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Transaction extends Model
{
    public $incrementing = false;
    
    protected $casts = [
        'id' => 'string'
    ];
    
    protected $keyType = 'string';
    
    public function order()
    {
        return $this->belongsTo(Order::class);
    }
    
    public function status()
    {
        return $this->belongsTo(TransactionStatus::class, 'transaction_status_id');
    }
    
    public function getConvertedAmountAttribute()
    {
        return round($this->amount * $this->currency_rate);
    }
    
    public static function chart(string $from, string $to)
    {
        return self::selectRaw("transaction_status_id, SUM(amount) as data, DATE_FORMAT(created_at, '%Y-%m') as date")
                   ->whereDate('created_at', '>=', $from)
                   ->whereDate('created_at', '<=', $to)
                   ->where('transaction_status_id', TransactionStatus::SUCCEED)
                   ->orderBy('date')
                   ->groupBy('date', 'transaction_status_id')
                   ->get();
    }
}
