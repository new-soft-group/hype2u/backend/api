<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TransactionStatus
 *
 * @property int $id
 * @property string $name
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionStatus newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionStatus newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionStatus query()
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionStatus whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionStatus whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionStatus whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\TransactionStatus whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class TransactionStatus extends Model
{
    const SUCCEED = 1;
    const FAILED = 2;
}
