<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\TimeslotType
 *
 * @property int $id
 * @property string $name
 * @method static \Illuminate\Database\Eloquent\Builder|TimeslotType newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TimeslotType newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|TimeslotType query()
 * @method static \Illuminate\Database\Eloquent\Builder|TimeslotType whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|TimeslotType whereName($value)
 * @mixin \Eloquent
 */
class TimeslotType extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = ['name'];

    const DELIVER = 1;
    const RETURN_BACK = 2;
}
