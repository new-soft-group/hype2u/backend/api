<?php

namespace App\Models;

use App\Contracts\Datatable\Searchable;
use App\Exceptions\DuplicateTimeslotException;
use App\Traits\CanEnable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


/**
 * App\Models\Timeslot
 *
 * @property int $id
 * @property int $type_id
 * @property string $start_time
 * @property string $end_time
 * @property int $limit
 * @property int $enabled
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Order[] $allocations
 * @property-read int|null $allocations_count
 * @property-read \App\Models\TimeslotType $type
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot newModelQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot newQuery()
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot query()
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot whereEnabled($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot whereEndTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot whereLimit($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot whereStartTime($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot whereTypeId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|Timeslot whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Timeslot extends Model implements Searchable
{
    use HasFactory, CanEnable;

    protected $fillable = [
        'type_id',
        'start_time',
        'end_time',
        'limit',
        'enabled'
    ];

    protected $casts = [
        'limit' => 'integer',
        'enabled' => 'boolean'
    ];

    public function allocations()
    {
        return $this->hasMany(TimeslotAllocation::class);
    }

    public function type()
    {
        return $this->belongsTo(TimeslotType::class, 'type_id');
    }

    public function fullyBooked($date): bool
    {
        $inputDate = Carbon::parse($date);
        return $this->allocations()->whereDate('start_at',$inputDate)->whereDate('end_at',$inputDate)->count() >= $this->limit;
    }

    /**
     * @param $orders
     * @return bool
     */
    public function hasAllocation($orders): bool
    {
        if (is_int($orders)) {
            return $this->allocations->contains('order_id', $orders);
        }

        if ($orders instanceof Order) {
            return $this->allocations->contains('order_id', $orders->id);
        }

        if (is_array($orders)) {
            foreach ($orders as $order) {
                if ($this->hasAllocation($order)) {
                    return true;
                }
            }
        }

        return false;
    }

    public static function buildTimestamp($date, $time): string
    {
        $timestamp = strtotime("$date $time");

        return Carbon::parse($timestamp)->format('Y-m-d H:i:s');
    }

    /**
     * @param $order
     * @param string $date
     * @return mixed|null
     * @throws DuplicateTimeslotException
     */
    public function allocate($order, string $date): TimeslotAllocation
    {
        if ($this->hasAllocation($order)) {
            throw new DuplicateTimeslotException();
        }

        /** @var TimeslotAllocation $instance */
        $instance = $this->allocations()
                         ->make([
                             'start_at' => self::buildTimestamp($date, $this->start_time),
                             'end_at' => self::buildTimestamp($date, $this->end_time)
                         ]);

        $instance->order()->associate($order)->save();

        if($this->type->id == TimeslotType::RETURN_BACK){
            $instance->order->pick_up_requested_at = Carbon::now();
            $instance->order->save();
        }

        return $instance;
    }

    public function scopeFilterByType(Builder $builder, array $typeId)
    {
        return $builder->whereHas('type',
            function (Builder $builder) use ($typeId) {
            $builder->whereIn('id',$typeId);
        });
    }

    public function searchCriterion(Builder $builder, string $keyword): Builder
    {
        return $builder;
    }

    public function filterCriteria(): array
    {
        return [
            'type' => 'scopeFilterByType'
        ];
    }

    public function sortCriteria(): array
    {
        return [];
    }

    public function withRelations(): array
    {
        return ['type'];
    }
}
