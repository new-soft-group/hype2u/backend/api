<?php

namespace App\Events;

use App\Models\ProductStock;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class StockArrival
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var ProductStock
     */
    public $stock;

    /**
     * Create a new event instance.
     *
     * @param ProductStock $stock
     */
    public function __construct(ProductStock $stock)
    {
        $this->stock = $stock;
    }
}
