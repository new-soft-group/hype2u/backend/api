<?php

namespace App\Events;

use App\Models\Pick;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class UserPickSubmittedEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    public $pick;

    /**
     * Create a new event instance.
     *
     * @param Pick $pick
     */
    public function __construct(Pick $pick)
    {
        $this->pick = $pick;
    }
}
