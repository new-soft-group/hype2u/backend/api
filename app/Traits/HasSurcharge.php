<?php


namespace App\Traits;


use App\Models\Surcharge;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Validation\ValidationException;

/**
 * @mixin \Eloquent
 */
trait HasSurcharge
{
    public function surcharges()
    {
        return $this->hasMany(Surcharge::class);
    }

    /**
     * @param array $attributes
     * @throws ValidationException
     */
    public function validateThreshold(array $attributes)
    {
        foreach ($attributes as $index => $attribute) {
            if($this->amount >= (float)$attribute['threshold']) {
                throw ValidationException::withMessages([
                    "surcharges.${index}.threshold" => [__('plan.threshold.invalid')]
                ]);
            }
        }
    }

    public function newSurcharges(array $attributes): iterable
    {
        $surcharges = new Collection();

        foreach ($attributes as $attribute) {
            /** @var Surcharge $surcharge */
            $surcharge = $this->surcharges()->make();
            $surcharge->setThreshold($attribute['threshold'])
                      ->setCharge($attribute['charge']);

            $surcharges->push($surcharge);
        }

        return $this->surcharges()->saveMany($surcharges);
    }

    public function hasThreshold()
    {
        return $this->surcharges->isNotEmpty();
    }

    public function getThreshold(): float
    {
        if ($this->hasThreshold())
        {
            return $this->surcharges->sortByDesc('threshold')->first()->threshold;
        }

        return $this->purchase_amount;
    }

    public function getSurchargeAmount(float $amount): float
    {
        $charge = 0;

        foreach($this->surcharges as $surcharge) {

            if($amount <= $surcharge->threshold) {
                return $surcharge->charge;
            }
        }

        return $charge;
    }
}
