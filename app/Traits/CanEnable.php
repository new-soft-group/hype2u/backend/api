<?php


namespace App\Traits;


use Illuminate\Database\Eloquent\Builder;

/**
 * Trait CanEnable
 * @package App\Traits
 * @mixin \Eloquent
 */
trait CanEnable
{
    public function setEnabled(bool $state = true)
    {
        $this->setAttribute('enabled', $state);

        return $this;
    }

    public function scopeEnabled(Builder $query, bool $state = true)
    {
        return $query->where('enabled', $state);
    }
}
