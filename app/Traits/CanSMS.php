<?php


namespace App\Traits;


trait CanSMS
{
    public function getPhoneNumberForSms(): string
    {
        return str_replace("+", "", $this->profile->mobile);
    }
}
