<?php


namespace App\Traits;


use App\Exceptions\DuplicateQueueEntryException;
use App\Models\NotifyQueue;
use App\Models\ProductStock;
use App\Models\Queuer;
use Exception;
use Illuminate\Database\Eloquent\Collection;

/**
 * @mixin \Eloquent
 */
trait QueueForStock
{
    public function queuer()
    {
        return $this->hasMany(Queuer::class);
    }

    public function queues()
    {
        return $this->hasManyThrough(
            NotifyQueue::class,
            Queuer::class,
            'user_id',
            'id',
            'id',
            'queue_id'
        );
    }

    /**
     * @param ProductStock $stock
     * @return Queuer
     * @throws DuplicateQueueEntryException
     */
    public function queue(ProductStock $stock): Queuer
    {
        if($this->isQueueing($stock)) {
            throw new DuplicateQueueEntryException();
        }

        /** @var NotifyQueue $queue */
        $queue = $stock->notifyQueue()
                       ->firstOrCreate([
                           'stock_id' => $stock->getKey()
                       ]);

        /** @var Queuer $queuer */
        $queuer = $this->queuer()->make();

        $queuer->queue()
               ->associate($queue)
               ->save();

        return $queuer;
    }

    public function dequeue(ProductStock $stock): bool
    {
        return $stock->queuers()
                     ->where($this->getForeignKey(), $this->getKey())
                     ->delete();
    }

    public function isQueueing(ProductStock $stock): bool
    {
        return $this->queues()
                    ->where('stock_id', $stock->id)
                    ->exists();
    }
}
