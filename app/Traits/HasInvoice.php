<?php


namespace App\Traits;


use App\Models\Invoice;
use Stripe\Invoice as StripeInvoice;

/**
 * @mixin \Eloquent
 */
trait HasInvoice
{
    /**
     * @param StripeInvoice $stripe
     * @return Invoice
     */
    public function newInvoice(StripeInvoice $stripe)
    {
        $invoice = new Invoice();
        $invoice->stripe_id = $stripe->id;
        $invoice->currency = $stripe->currency;
        $invoice->payment_intent = $stripe->payment_intent;
        $invoice->amount = centToAmount($stripe->amount_paid);
        $invoice->status = $stripe->status;
        $invoice->pdf = $stripe->invoice_pdf;

        if(!is_null($stripe->discount)) {
            $invoice->stripe_coupon = $stripe->discount->coupon->id;
        }

        return $invoice;
    }
}
