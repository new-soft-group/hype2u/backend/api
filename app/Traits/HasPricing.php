<?php


namespace App\Traits;

use App\Models\Setting;
use Illuminate\Support\Collection;

trait HasPricing
{
    public function getPricingAttribute()
    {
        $collection = new Collection();

        $currencies = config('currencies.rate', []);

        foreach ($currencies as $code => $rate) {
            $collection->push([
                'currency_code' => strtoupper($code),
                'price' => round($this->price * (float)$rate),
                'cost_price' => round($this->cost_price * (float)$rate)
            ]);
        }

        return $collection;
    }
}
