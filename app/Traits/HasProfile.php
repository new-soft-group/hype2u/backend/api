<?php


namespace App\Traits;


use App\Models\Profile;

/**
 * @mixin \Eloquent
 */
trait HasProfile
{
    use HasAddress;

    public function profile()
    {
        return $this->morphOne(Profile::class, 'profileable');
    }
}
