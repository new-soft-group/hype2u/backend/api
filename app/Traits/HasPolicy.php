<?php


namespace App\Traits;


use App\Collections\PolicyCollection;
use App\Models\PlanPolicy;
use App\Models\TargetType;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

/**
 * @mixin \Eloquent
 */
trait HasPolicy
{
    public function policies()
    {
        return $this->hasMany(PlanPolicy::class);
    }

    protected function mapRuleKeys($rules): array
    {
        $collection = collect($rules);
        return $collection->mapWithKeys(function ($item) {
            return [$item['id'] => ['prime' => $item['prime']]];
        })->toArray();
    }

    public function createPolicy(array $attribute)
    {
        /** @var PlanPolicy $policy */
        $policy = $this->policies()->make($attribute);
        $policy->save();

        if(array_key_exists('rules', $attribute)) {
            $keys = $this->mapRuleKeys($attribute['rules']);
            $policy->rules()->sync($keys);
        }

        return $policy;
    }

    public function createPolicies(array $attributes)
    {
        $policies = new PolicyCollection();

        foreach ($attributes as $attribute) {
            $policies->push($this->createPolicy($attribute));
        }

        return $policies;
    }

    public function syncPolicies(array $attributes)
    {
        foreach ($attributes as $attribute) {
            if(array_key_exists('id', $attribute)) {
                /** @var PlanPolicy $policy */
                $policy = $this->policies()->find($attribute['id']);

                if(!is_null($policy)) {
                    $policy->fill($attribute)->save();

                    if(array_key_exists('rules', $attribute)) {
                        $keys = $this->mapRuleKeys($attribute['rules']);
                        $policy->rules()->sync($keys);
                    }
                }
            }
            else {
                $this->createPolicy($attribute);
            }
        }
    }

    public function maxQuantityPerOrder(): int
    {
        $total = $this->policies()->sum('quantity');

        return $this->max_quantity + $total;
    }
}
