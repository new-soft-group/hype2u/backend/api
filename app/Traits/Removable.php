<?php


namespace App\Traits;


trait Removable
{
    public function setRemovable(bool $removable)
    {
        $this->removable = $removable;

        return $this;
    }

    public function isRemovable(): bool
    {
        return $this->removable;
    }
}
