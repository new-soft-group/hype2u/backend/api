<?php


namespace App\Traits;


use Illuminate\Support\Collection;

trait HasAny
{
    /**
     * @param Collection $collection
     * @param string|int|array|Collection $any
     * @return bool
     */
    public function hasAny($collection, $any): bool
    {
        if (is_string($any)) {
            return $collection->contains('name', $any);
        }
        
        if (is_int($any)) {
            return $collection->contains('id', $any);
        }
        
        if (is_array($any)) {
            foreach ($any as $data) {
                if ($this->hasAny($collection, $data)) {
                    return true;
                }
            }
            
            return false;
        }
        
        return $any->intersect($collection)->isNotEmpty();
    }
}
