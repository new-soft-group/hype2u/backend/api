<?php


namespace App\Traits;


use App\Models\BusinessType;
use App\Models\Cart;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
trait HasCart
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function cart()
    {
        return $this->hasOne(Cart::class)->where('business_type_id', BusinessType::RENT);
    }

    public function shopCart()
    {
        return $this->hasOne(Cart::class)->where('business_type_id', BusinessType::SHOP);
    }

    /**
     * @return Cart|Model
     */
    public function getCart()
    {
        return $this->cart()->firstOrCreate(['business_type_id' => BusinessType::RENT]);
    }

    /**
     * @return Cart|Model
     */
    public function getShopCart()
    {
        return $this->shopCart()->firstOrCreate(['business_type_id' => BusinessType::SHOP]);
    }

    public function mergeWithGuestCart(Cart $guestCart): Cart
    {
        /** @var Cart $cart */
        $cart = $this->cart()->firstOrCreate([]);

        try {
            if($cart->wasRecentlyCreated || $cart->items()->count() === 0) {

                $cart->items()->saveMany($guestCart->items()->get());
            }

            $guestCart->items()->delete();

            $guestCart->delete();
        }
        catch (\Exception $e) {

            \Log::warning('Failed to merge guest cart to user', ['message' => $e->getMessage()]);
        }

        return $cart;
    }
}
