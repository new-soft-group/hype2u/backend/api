<?php


namespace App\Traits;


use App\Collections\Carry\CarryForward;
use App\Collections\Carry\CarryForwardCollection;
use App\Models\BusinessType;
use App\Models\CartItem;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStatus;
use App\Models\Product;
use App\Models\ProductStockInventory;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Log;

/**
 * @mixin \Eloquent
 */
trait HasOrder
{
    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function completedOrders()
    {
        return $this->orders()
                    ->where('order_status_id', OrderStatus::COMPLETED);
    }

    public function totalSales()
    {
        $builder = $this->completedOrders();

        if (empty($builder->count())) return 0;

        return $builder->with(['items', 'invoice'])
                       ->get()
                       ->sum(function (Order $order) {
                           $invoice = $order->invoice ? $order->invoice->amount : 0;
                           return $order->amount + $invoice;
                       });
    }

    public function ongoingOrders()
    {
        return $this->orders()
                    ->whereIn('order_status_id', [
                        OrderStatus::PENDING,
                        OrderStatus::DELIVERED
                    ])
                    ->where('business_type_id', BusinessType::RENT)
                    ->whereNull('returned_at');
    }

    public function partialReturnedOrders()
    {
        return $this->orders()
                    ->whereHas('tracks', function (Builder $query) {
                        $query->whereNotNull('scanned_at');
                    })
                    ->where('order_status_id', OrderStatus::PARTIAL_RETURNED)
                    ->whereNull('returned_at');
    }

    public function carryForwardOrders()
    {
        return $this->orders()
                    ->whereHas('tracks', function (Builder $query) {
                        $query->whereNull('scanned_at');
                    })->whereHas('businessType', function (Builder $query){
                        $query->where('id', BusinessType::RENT);
                    });
    }

    public function validOrderStatuses(): array
    {
        return [
            OrderStatus::PENDING,
            OrderStatus::DELIVERED,
            OrderStatus::COMPLETED,
            OrderStatus::PARTIAL_RETURNED
        ];
    }

    public function validOrders()
    {
        return $this->orders()->whereIn('order_status_id', $this->validOrderStatuses());
    }

    public function ongoingMonthlyOrders()
    {
        return $this->validOrders()
                    ->whereMonth('created_at', now()->month)
                    ->whereYear('created_at', now()->year)
                    ->union($this->partialReturnedOrders()->getBaseQuery());
    }

    public function carryForwardItems(): CarryForwardCollection
    {
        $result = new CarryForwardCollection();

        $orders = $this->carryForwardOrders()
                       ->with(['tracks', 'items.product.images'])
                       ->get();

        if ($orders->isEmpty()) return $result;

        $notYetReturn = $orders->pluck('tracks')
                               ->flatten()
                               ->whereNull('pivot.scanned_at')
                               ->groupBy('product_stock_id');

        /** Get returned order */
        $returned = $orders->pluck('tracks')
            ->flatten()
            ->whereNotNull('pivot.scanned_at')
            ->groupBy('product_stock_id')
            ->flatten();

        $items = $orders->pluck('items')
                    ->flatten()
                    ->whereIn('product_stock_id', $notYetReturn->unique()->keys());

        /** Filtered Items that remove duplicated data */
        $filteredItems = [];

        /** @var OrderItem $item */
        foreach ($items as $item){
            $delivered = new CarryForward();
            $quantity = $notYetReturn->get($item->product_stock_id)->count();

            $delivered->setOrderKey($item->order_id)
                ->setOrderStatusId($item->order_id)
                ->setProduct($item->product)
                ->setStockKey($item->product_stock_id)
                ->setVariant($item->variant)
                ->setVariantPrice($item->variant_price)
                ->setUnitPrice($item->unit_price)
                ->setQuantity($quantity);

            if($returned->where('product_stock_id', $item->product_stock_id)->where('pivot.order_id',$item->order_id)->count() == 0 ) array_push($filteredItems , $delivered);
        }


//        $items = $orders->pluck('items')
//                        ->unique('product_stock_id')
//                        ->flatten()
//                        ->whereIn('product_stock_id', $notYetReturn->unique()->keys())
//                        ->map(function (OrderItem $item) use ($notYetReturn, $orders) {
//                            $delivered = new CarryForward();
//                            $quantity = $notYetReturn->get($item->product_stock_id)->count();
//
//                            return $delivered->setOrderKey($item->order_id)
//                                ->setOrderStatusId($item->order_id)
//                                ->setProduct($item->product)
//                                ->setStockKey($item->product_stock_id)
//                                ->setVariant($item->variant)
//                                ->setVariantPrice($item->variant_price)
//                                ->setUnitPrice($item->unit_price)
//                                ->setQuantity($quantity);
//                        });

        return $result->push(...$filteredItems);
    }
}
