<?php


namespace App\Traits;


use App\Models\Wishlist;
use App\Models\WishlistItem;
use Illuminate\Database\Eloquent\Model;

trait HasWishlist
{
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function wishlist()
    {
        return $this->hasOne(Wishlist::class);
    }

    /**
     * @return Wishlist|Model
     */
    public function getWishlist(): Wishlist
    {
        return $this->wishlist()->firstOrCreate([]);
    }
}
