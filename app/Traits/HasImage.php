<?php


namespace App\Traits;


use App\Models\Image;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Support\Arr;
use Storage;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @mixin \Eloquent
 */
trait HasImage
{
    /**
     * @return MorphMany
     */
    public function images()
    {
        return $this->morphMany(Image::class, 'imageable')->orderBy('sort');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    /**
     * Upload single image to file system
     *
     * @param UploadedFile $uploaded
     * @param int $sort
     * @return \Illuminate\Database\Eloquent\Model|void
     */
    public function uploadImage(UploadedFile $uploaded, $sort = 0)
    {
        if(is_null($uploaded)) return;

        $imageSize = getimagesize($uploaded);

        $model = $this->image()->create([
            'disk' => $this->disk,
            'path' => Storage::disk($this->disk)->put('/', $uploaded),
            'width' => $imageSize[0],
            'height' => $imageSize[1],
            'sort' => $sort
        ]);

        $this->setRelation('image', $model);

        return $model;
    }

    /**
     * Upload multiple images to file system
     *
     * @param array $uploaded
     * @return iterable
     */
    public function uploadImages(array $uploaded): iterable
    {
        $images = new Collection();
        $fileSystem = config("filesystems.default");

        foreach ($uploaded as $image) {
            $file = $image['file'];
            $sort = $image['sort'] ?? 0;

            if($file instanceof UploadedFile) {
                $fileSize = getimagesize($file);
                if($fileSystem == "s3"){
                    $model = $this->processUploadImageToS3($file, $fileSize, $sort);
                }
                else{
                    $model = $this->processUploadImageToLocal($file, $fileSize, $sort);
                }

                $images->push($model);
            }
        }

        $relation = $this->images()->saveMany($images);

        $this->setRelation('images', $relation);



        return $relation;
    }

    /**
     * @param int $id
     * @return bool
     * @throws \Exception
     */
    public function deleteImage(): bool
    {
        Storage::disk($this->disk)->delete($this->image->path);

        return $this->image->delete();
    }

    /**
     * @param int $id
     * @return bool
     */
    public function deleteImages($id = null): bool
    {
        $builder = isset($id)
            ? $this->images()->whereKey($id)
            : $this->images();

        $paths = $builder->pluck('path')->toArray();

        Storage::disk($this->disk)->delete($paths);

        return $builder->delete();
    }

    public function sortImage(array $attributes = []): bool
    {
        foreach ($attributes as $attribute) {
            if(array_key_exists('sort', $attribute)) {
                /** @var Image $image */
                $image = $this->images()->find($attribute['id']);
                if($image->sort === (int)$attribute['sort']) continue;

                $image->update(['sort' => (int)$attribute['sort']]);
            }
        }

        return true;
    }

    public function processUploadImageToS3($file, $fileSize, $sort)
    {
        /** Resize image before upload*/
        $streamImage = $this->resizeImage($file);

        $imageName = microtime(true). '.png';

        $s3UploadPath = 'uploads/product/original/'.$imageName;

        /** Save image to s3 */
        Storage::disk('s3')->put($s3UploadPath, $streamImage->stream('png'));

        return $this->images()->make([
            'disk' => 's3',
            'path' => $s3UploadPath,
            'width' => $streamImage->width(),
            'height' => $streamImage->height(),
            'sort' => $sort
        ]);
    }

    public function processUploadImageToLocal($file, $fileSize, $sort)
    {
        $imageName = microtime(true). '.png';

        $imagePathName = Storage::disk($this->disk)->putFileAs('/', $file, $imageName);

        $model = $this->images()->make([
            'disk' => $this->disk,
            'path' => $imagePathName,
            'width' => $fileSize[0],
            'height' => $fileSize[1],
            'sort' => $sort
        ]);

        /** Image file path */
        $path = 'storage/'.$this->disk.'/'.$imagePathName;

        /** Resize image after upload*/
        $this->resizeImage($file, $path, $model);

        return $model;
    }

    public function resizeImage($file, $path = null, $model = null)
    {
        $fileSystem = config("filesystems.default");

        /** Check if file is from local */
        $file = $fileSystem == 's3' ? $file : $path;

        /** Resize uploaded images */
        $resizedImage = \Intervention\Image\Facades\Image::make($file)->resize(410, null, function ($constraint) {
            $constraint->aspectRatio();
        });

        /** Standardize image with a background*/
        $imageBackground = \Intervention\Image\Facades\Image::canvas(410,560, '#ffffff');
        $imageBackground->insert($resizedImage,'center');

        /** Retrieve image record and update resized width and height for local*/
        if($model !== null){
            $model->width = $imageBackground->width();
            $model->height = $imageBackground->height();
            $model->save();
        }

        /** Return stream image for s3 or save and overwrite resized image with background to original image file for local */
        return $fileSystem == "s3" ? $imageBackground : $imageBackground->save($file);
    }
}
