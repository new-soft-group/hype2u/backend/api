<?php


namespace App\Traits\Product;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @mixin \Eloquent
 */
trait HasCategory
{
    /**
     * Product categories relationship
     *
     * @return BelongsToMany
     */
    public function categories()
    {
        return $this->belongsToMany(Category::class, 'product_categories');
    }

    public function assignCategories($ids)
    {
        $categories = Category::whereKey($ids)->get();

        $this->categories()->sync($categories->pluck('id')->toArray());

        $this->setRelation('categories', $categories);
    }

    public function hasCategories($categories): bool
    {
        return $this->hasAny($this->categories, $categories);
    }

    public function scopeByCategory(Builder $query, string $category)
    {
        return $query->with('categories')->whereHas('categories',
                function (Builder $query) use ($category) {
                    $query->where('name', $category);
                });
    }

    public function deleteCategories()
    {
        return $this->categories()->detach();
    }
}
