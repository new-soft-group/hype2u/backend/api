<?php


namespace App\Traits\Product;

use App\Models\NotifyQueue;
use App\Models\Queuer;

/**
 * @mixin \Eloquent
 */
trait StockQueue
{
    public function notifyQueue()
    {
        return $this->hasOne(NotifyQueue::class, 'stock_id');
    }

    public function queuers()
    {
        return $this->hasManyThrough(
            Queuer::class,
            NotifyQueue::class,
            'stock_id',
            'queue_id'
        );
    }
}
