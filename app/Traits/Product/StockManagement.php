<?php


namespace App\Traits\Product;

use App\Models\ProductStock;
use App\Models\ProductVariant;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Log;

/**
 * @mixin \Eloquent
 */
trait StockManagement
{
    public function stocks()
    {
        return $this->hasMany(ProductStock::class);
    }

    /**
     * Initialize stock with all the possible variants combinations
     * during product creation
     *
     * @param Collection $variants
     * @param null $vendor
     * @return Collection
     */
    public function initializeStocks(Collection $variants, $vendor = null): Collection
    {
        $instances = new Collection();
        $arrays = $this->mapVariantKey($variants);

        $combinations = $this->getVariantCombinations($arrays);

        foreach ($combinations as $combination) {
            /** @var ProductStock $stock */
            $stock = $this->stocks()->make();

            if(!is_null($vendor)) {
                $stock->vendor()->associate($vendor);
            }

            $stock->save();
            $stock->combinations()->sync($combination);
            $instances->push($stock);
        }

        return $instances;
    }

    public function mapVariantKey(Collection $variants)
    {
        $result = $variants->mapWithKeys(function (ProductVariant $variant) {
            $options = $variant->options->pluck('id')->toArray();

            return [$variant->lookup->name => $options];
        });

        return $result->toArray();
    }

    public function getVariantCombinations($arrays)
    {
        $result = [[]];

        foreach ($arrays as $property => $values) {
            $tmp = [];
            foreach ($result as $item) {
                foreach ($values as $value) {
                    $tmp[] = array_replace($item, [$property => $value]);
                }
            }
            $result = $tmp;
        }
        return $result;
    }
}
