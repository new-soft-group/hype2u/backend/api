<?php


namespace App\Traits\Product;


use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Traits\CanEnable;
use App\Traits\HasAny;
use App\Traits\HasImage;
use App\Traits\HasPricing;
use App\Traits\Product\HasVariant;

trait ExtraFeature
{
    use CanEnable,
        HasAny,
        HasImage,
        HasPricing,
        HasCategory,
        HasVariant;

    public function stocks()
    {
        return $this->hasMany(ProductStock::class)
                    ->withCount('inventoryOnHand as inventories_count');
    }

    public function inventories()
    {
        return $this->hasManyThrough(ProductStockInventory::class, ProductStock::class);
    }
}
