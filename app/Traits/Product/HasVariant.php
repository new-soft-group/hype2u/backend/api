<?php


namespace App\Traits\Product;

use App\Collections\StockCollection;
use App\Exceptions\ModelHasAssociate;
use App\Models\ProductVariant;
use App\Models\ProductVariantOption;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Arr;

/**
 * @mixin \Eloquent
 */
trait HasVariant
{
    use StockManagement;

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function variants()
    {
        return $this->hasMany(ProductVariant::class)
                    ->with('options');
    }

    public function options()
    {
        return $this->hasManyThrough(
            ProductVariantOption::class,
            ProductVariant::class,
            'product_id',
            'product_variant_id'
        )->with('lookup');
    }

    public function getOptionNames()
    {
        return $this->options()
                    ->get()
                    ->pluck('lookup.name')
                    ->toArray();
    }

    public function availableVariantCombination()
    {
        $variants = $this->variants()->get();

        $options = $variants->pluck('options')
                            ->flatten(1);

        $arrays = $this->mapVariantKey($variants);

        $combinations = $this->getVariantCombinations($arrays);

        $result = [];

        foreach ($combinations as $combination) {
            $combinationIds = array_values($combination);

            $combinationOptions = $options->whereIn('id', $combinationIds);

            $result[] = [
                'ids' => $combinationOptions->pluck('id')->toArray(),
                'name' => $combinationOptions->implode('lookup.name', '/')
            ];
        }

        return $result;
    }

    public function assignVariants(array $attributes)
    {
        $variants = new Collection();

        foreach ($attributes as $attribute) {

            if (array_key_exists('variant_id', $attribute)) {
                $variant = $this->assignVariant($attribute['variant_id']);

                if (array_key_exists('options', $attribute)) {
                    $this->syncOptions($variant, $attribute['options']);
                }

                $variants->push($variant);
            }
        }

        $variants->load('options');

        return $this->setRelation('variants', $variants);
    }

    /**
     * @param array $variants
     * @return bool
     * @throws ModelHasAssociate
     */
    public function syncVariants($variants = [])
    {
        /** @var StockCollection $stocks */
        $stocks = $this->stocks->load('combinations.variant');
        /**
         * Loop through each attached variant
         */
        foreach (collect($variants) as $newVariant) {
            /**
             * Look for new variant in current stocks combination
             */
            $found = $stocks->findByVariant($newVariant['variant_id']);
            /**
             * Given variant has already attached to existing stock
             */
            if (!is_null($found) && $stocks->missingOptions($found->lookup->variant_id, $newVariant['options'])) {
                throw new ModelHasAssociate(__('product.variant.update.failed'));
            }
            /**
             * Sync existing variant's options
             */
            elseif (isset($newVariant['id'])) {
                /** @var ProductVariant $existed */
                $existed = $this->variants()->find($newVariant['id']);
                /**
                 * Sync new variant's option
                 */
                $this->syncOptions($existed, $newVariant['options']);
            }
            /**
             * Create new variant with associate options
             */
            else {
                /** Create new variant */
                $new = $this->assignVariant($newVariant['variant_id']);
                /**
                 * Sync variant's option
                 */
                $this->syncOptions($new, $newVariant['options']);
            }
        }

        return true;
    }

    /**
     * Assign Variant
     *
     * @param int $id
     * @return ProductVariant
     */
    public function assignVariant(int $id)
    {
        /** @var ProductVariant $variant */
        $variant = $this->variants()->make();

        $variant->lookup()
                ->associate($id)
                ->save();

        return $variant;
    }

    public function syncOptions(ProductVariant $variant, array $ids)
    {
        $ids = Arr::pluck($ids, 'variant_option_id');

        $variant->pivotOptions()->sync($ids);
    }
}
