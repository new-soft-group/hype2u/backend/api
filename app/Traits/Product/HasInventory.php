<?php


namespace App\Traits\Product;

use App\Events\StockArrival;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\StockStatus;
use App\Models\Warehouse;
use Exception;
use Illuminate\Database\Eloquent\Collection;
use Log;

/**
 * @mixin \Eloquent
 */
trait HasInventory
{
    public function reserved(int $quantity)
    {
        $quantity = min($quantity, $this->availableInventoryCount());

        return $this->increment('reserved', $quantity);
    }

    public function unreserved(int $quantity)
    {
        $quantity = min($quantity, $this->reserved);

        return $this->decrement('reserved', $quantity);
    }

    public function inventories()
    {
        return $this->hasMany(ProductStockInventory::class);
    }

    public function inventoryOnHand()
    {
        return $this->inventories()
                    ->where('stock_status_id', StockStatus::IN_STOCK);
    }

    public function availableInventoryCount(): int
    {
        $onHand = $this->inventoryOnHand()->count();

        return $onHand - $this->reserved;
    }

    /**
     * Create product tag
     * @param string $tag
     * @param int|Warehouse $warehouse
     * @param int|StockStatus $status
     * @return ProductStockInventory
     */
    public function restock(string $tag, $warehouse, $status): ProductStockInventory
    {
        /** @var ProductStockInventory $item */
        $item = $this->inventories()->make();
        $item->warehouse()->associate($warehouse);
        $item->status()->associate($status);
        $item->setTag($tag)->save();

        if ($item->isInStock()) {
            /** @var ProductStock $this */
            event(new StockArrival($this));
        }

        return $item;
    }

    /**
     * Create multiple product tag
     * @param array $tags
     * @param $warehouse
     * @param $status
     * @return Collection
     */
    public function restocks(array $tags, $warehouse, $status): Collection
    {
        $inventories = new Collection();

        foreach ($tags as $tag) {
            /** @var ProductStockInventory $item */
            $item = $this->inventories()->make();
            $item->warehouse()->associate($warehouse);
            $item->status()->associate($status);
            $item->setTag($tag)->save();

            $inventories->push($item);
        }

        $trigger = $inventories->groupBy('stock_status_id')
                               ->has(StockStatus::IN_STOCK);

        if ($trigger) {
            /** @var ProductStock $this */
            event(new StockArrival($this));
        }

        return $inventories;
    }

    /**
     * @param $id
     * @return bool
     */
    public function deleteTags($id): bool
    {
        try {
            if (is_int($id)) {
                return $this->inventories()->find($id)->delete();
            }
            elseif (is_array($id)) {
                return $this->inventories()->whereKey($id)->delete();
            }
        }
        catch (Exception $exception) {
            Log::error('Failed to delete product tag', ['message' => $exception->getMessage()]);
        }

        return false;
    }
}
