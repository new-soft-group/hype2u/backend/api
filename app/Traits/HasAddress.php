<?php


namespace App\Traits;


use App\Models\Address;
use App\Models\Profile;
use Illuminate\Database\Eloquent\Model;

/**
 * @mixin \Eloquent
 */
trait HasAddress
{
    public function addresses()
    {
        return $this->hasManyThrough(
            Address::class,
            Profile::class,
            'id',
            'addressable_id'
        );
    }

    /**
     * @param array $address
     * @return Model|Address
     */
    public function newAddress(array $address)
    {
        return $this->profile->addresses()->create($address);
    }

    public function updateAddress(Address $address, array $attributes): bool
    {
        return $address->fill($attributes)
                       ->save();
    }

    /**
     * @param Address $address
     * @return bool|null
     * @throws \Exception
     */
    public function deleteAddress(Address $address): bool
    {
        if($this->profile->isDefaultShippingAddress($address)) {
            $this->profile->unsetShippingAddress();
        }

        if($this->profile->isDefaultBillingAddress($address)) {
            $this->profile->unsetBillingAddress();
        }

        $this->profile->update();

        return $address->delete();
    }

    public function updateShippingAddress(Address $address)
    {
        return $this->profile->defaultShippingAddress()
                             ->associate($address)
                             ->save();
    }

    public function updateBillingAddress(Address $address)
    {
        return $this->profile->defaultBillingAddress()
                             ->associate($address)
                             ->save();
    }
}
