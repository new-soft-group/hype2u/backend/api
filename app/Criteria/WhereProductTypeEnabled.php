<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class WhereProductTypeEnabled implements CriteriaContract
{
    public function apply(Builder $builder)
    {
        return $builder->with('type')
                       ->whereHas('type', function (Builder $subQuery) {
                           $subQuery->where('enabled', true);
                       });
    }
}
