<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class WhereExcept implements CriteriaContract
{
    /**
     * @var Model
     */
    private $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function apply(Builder $builder)
    {
        $builder->whereKeyNot($this->model->getKey());
    }
}
