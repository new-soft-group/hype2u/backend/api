<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class GroupStockInventories implements CriteriaContract
{
    public function apply(Builder $builder)
    {
        return $builder->with(['warehouse', 'stock.product'])
                       ->select([
                           DB::raw('count(*) as quantity'),
                           'warehouse_id',
                           'product_stock_id'
                       ])
                       ->groupBy('warehouse_id', 'product_stock_id');
    }
}
