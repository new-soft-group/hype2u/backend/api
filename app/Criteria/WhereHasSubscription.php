<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use DB;
use Illuminate\Database\Eloquent\Builder;

class WhereHasSubscription implements CriteriaContract
{
    public function apply(Builder $builder)
    {
        return $builder->whereHas('subscriptions');
    }
}
