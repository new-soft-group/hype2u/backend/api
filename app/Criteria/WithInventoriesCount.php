<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class WithInventoriesCount implements CriteriaContract
{
    public function apply(Builder $builder)
    {
        return $builder->withCount('inventories')
                       ->with('stocks')
                       ->whereHas('stocks', function (Builder $subQuery) {
                           $subQuery->where('enabled', true);
                       });
    }
}
