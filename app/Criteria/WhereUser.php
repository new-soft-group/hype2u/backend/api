<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class WhereUser implements CriteriaContract
{
    /**
     * @var Model
     */
    private $model;

    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function apply(Builder $builder)
    {
        $builder->where($this->model->getForeignKey(), $this->model->getKey());
    }
}
