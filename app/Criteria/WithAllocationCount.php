<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use Illuminate\Database\Eloquent\Builder;

class WithAllocationCount implements CriteriaContract
{
    public function apply(Builder $builder)
    {
        return $builder->withCount('allocations');
    }
}
