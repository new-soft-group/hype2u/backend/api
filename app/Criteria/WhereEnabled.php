<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class WhereEnabled implements CriteriaContract
{
    /**
     * @var bool
     */
    private $enabled;

    /**
     * WhereEnabled constructor.
     * @param bool $enabled
     */
    public function __construct(bool $enabled)
    {

        $this->enabled = $enabled;
    }

    public function apply(Builder $builder)
    {
        return $builder->where('enabled', $this->enabled);
    }
}
