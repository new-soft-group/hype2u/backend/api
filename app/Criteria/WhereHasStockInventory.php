<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use App\Models\BusinessType;
use DB;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class WhereHasStockInventory implements CriteriaContract
{
    public $request;
    public $filter;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getFilter(): array
    {
        $filter = $this->request->get('filter', null);
        return $filter ? json_decode($filter, true) : [];
    }

    public function getFilterBusinessType()
    {
        if(array_key_exists('business_type', $this->getFilter())) return $this->getFilter()['business_type'];

        return BusinessType::RENT;
    }

    public function apply(Builder $builder)
    {
        return $builder->with('stocks')
                       ->whereHas('stocks', function (Builder $subQuery) {
                           $subQuery->where('enabled', true);
                       })
                       ->orWhereDoesntHave('stocks')
                       ->whereHas('businessType', function (Builder $subQuery) {
                           $subQuery->where('business_type_id', $this->getFilterBusinessType());
                       });
    }
}
