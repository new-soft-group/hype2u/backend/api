<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use Illuminate\Database\Eloquent\Builder;

class WhereBrandEnabled implements CriteriaContract
{
    public function apply(Builder $builder)
    {
        return $builder->with('brand')
                       ->whereHas('brand', function (Builder $subQuery) {
                           $subQuery->where('enabled', true);
                       });
    }
}
