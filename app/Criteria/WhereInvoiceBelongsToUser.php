<?php


namespace App\Criteria;


use App\Contracts\Criteria\CriteriaContract;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

class WhereInvoiceBelongsToUser implements CriteriaContract
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function apply(Builder $builder)
    {
        return $builder->whereHasMorph('invoiceable', '*' ,function( Builder $builder){
            $builder->where($this->user->getForeignKey(), $this->user->getKey());
        });
//        return $builder->with(['invoiceable' => function($builder) {
//            $builder->where($this->user->getForeignKey(), $this->user->getKey());
//        }]);
    }
}
