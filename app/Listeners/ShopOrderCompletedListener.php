<?php

namespace App\Listeners;

use App\Events\ShopOrderCompletedEvent;
use App\Mail\ShopOrderCompletedMail;
use App\Notifications\ShopOrderCompletedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ShopOrderCompletedListener
{
    /**
     * Handle the event.
     *
     * @param ShopOrderCompletedEvent $event
     * @return void
     */
    public function handle(ShopOrderCompletedEvent $event)
    {
        try{
            $event->order->user->notify(new ShopOrderCompletedNotification($event->order));
            Mail::to($event->order->user->email)->send(new ShopOrderCompletedMail($event->order));
        }catch (\Exception $exception){
            Log::error("Failed to send shop order completed notification", [
                'message' => $exception->getMessage()
            ]);
        }
    }
}
