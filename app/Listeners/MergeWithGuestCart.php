<?php

namespace App\Listeners;

use App\Events\UserLoggedIn;
use App\Models\Cart;

class MergeWithGuestCart
{
    /**
     * Create the event listener.
     *
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserLoggedIn  $event
     * @return void
     */
    public function handle(UserLoggedIn $event)
    {
        if($event->request->hasHeader(Cart::SESSION_HEADER)) {
            $session = $event->request->header(Cart::SESSION_HEADER);
            
            $guestCart = Cart::findBySession($session);
            
            if($guestCart) {
                $event->user->mergeWithGuestCart($guestCart);
            }
        }
    }
}
