<?php

namespace App\Listeners;

use App\Events\UpdateSubscriptionEvent;
use App\Notifications\UpdateSubscriptionNotification;
use Illuminate\Support\Facades\Log;

class UpdateSubscriptionNotificationListener
{
    /**
     * Handle the event.
     *
     * @param  UpdateSubscriptionEvent  $event
     * @return void
     */
    public function handle(UpdateSubscriptionEvent $event)
    {
        try{
            $event->subscription->user->notify(new UpdateSubscriptionNotification($event->subscription));
        }catch (\Exception $exception){
            Log::error("Failed to send update subscription notification", [
                'message' => $exception->getMessage()
            ]);
        }

    }
}
