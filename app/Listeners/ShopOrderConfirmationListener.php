<?php

namespace App\Listeners;

use App\Events\ShopOrderConfirmationEvent;
use App\Mail\ShopOrderConfirmationMail;
use App\Notifications\OrderConfirmationNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ShopOrderConfirmationListener
{
    /**
     * Handle the event.
     *
     * @param ShopOrderConfirmationEvent $event
     * @return void
     */
    public function handle(ShopOrderConfirmationEvent $event)
    {
        try{
            $event->order->user->notify(new OrderConfirmationNotification($event->order));
            Mail::to($event->order->user->email)->send(new ShopOrderConfirmationMail($event->order));
        }catch (\Exception $exception){
            Log::error("Failed to send shop order confirmation notification", [
                'message' => $exception->getMessage()
            ]);
        }
    }
}
