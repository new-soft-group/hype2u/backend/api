<?php

namespace App\Listeners;

use App\Events\NewRegistrationEvent;
use App\Mail\NewUserRegistrationMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class NewRegistrationListener
{
    /**
     * Handle the event.
     *
     * @param NewRegistrationEvent $event
     * @return void
     */
    public function handle(NewRegistrationEvent $event)
    {
        try {
            Mail::to($event->user->email)->send(new NewUserRegistrationMail($event->user));
        }catch (\Exception $exception){
            Log::error("Failed to send new registration notification", [
                'message' => $exception->getMessage()
            ]);
        }
    }
}
