<?php

namespace App\Listeners;

use App\Events\SubscriptionRenewEvent;
use App\Mail\SubscriptionRenewMail;
use App\Notifications\SubscriptionRenewNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class RenewSubscriptionNotificationListener
{
    /**
     * Handle the event.
     *
     * @param  SubscriptionRenewEvent $event
     * @return void
     */
    public function handle(SubscriptionRenewEvent $event)
    {
        try {
            $event->subscription->user->notify(new SubscriptionRenewNotification($event->subscription));
            Mail::to($event->subscription->user->email)->send(new SubscriptionRenewMail($event->subscription));
        }catch (\Exception $exception){
            Log::error("Failed to send subscription renew notification", [
                'message' => $exception->getMessage()
            ]);
        }

    }
}
