<?php

namespace App\Listeners;

use App\Events\OrderConfirmationEvent;
use App\Mail\OrderConfirmationMail;
use App\Notifications\OrderConfirmationNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderConfirmationListener
{
    /**
     * Handle the event.
     *
     * @param  OrderConfirmationEvent  $event
     * @return void
     */
    public function handle(OrderConfirmationEvent $event)
    {
        try{
            $event->order->user->notify(new OrderConfirmationNotification($event->order));
            Mail::to($event->order->user->email)->send(new OrderConfirmationMail($event->order));
        }catch (\Exception $exception){
            Log::error("Failed to send order confirmation notification", [
                'message' => $exception->getMessage()
            ]);
        }

    }
}
