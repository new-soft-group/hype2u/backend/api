<?php

namespace App\Listeners;

use App\Events\NewSubscriptionEvent;
use App\Mail\NewSubscriptionMail;
use App\Notifications\NewSubscriptionNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class NewSubscriptionNotificationListener
{
    /**
     * Handle the event.
     *
     * @param  NewSubscriptionEvent $event
     * @return void
     */
    public function handle(NewSubscriptionEvent $event)
    {
        try{
            $event->subscription->user->notify(new NewSubscriptionNotification($event->subscription));
            Mail::to($event->subscription->user->email)->send(new NewSubscriptionMail($event->subscription));
        }catch (\Exception $exception){
            Log::error("Failed to send new subscription notification", [
                'message' => $exception->getMessage()
            ]);
        }

    }
}
