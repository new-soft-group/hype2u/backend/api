<?php

namespace App\Listeners;

use App\Events\OrderPickUpReminderEvent;
use App\Mail\OrderPickUpReminderMail;
use App\Notifications\OrderPickUpReminderNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderPickUpReminderListener
{
    /**
     * Create the event listener.
     *
     * @param OrderPickUpReminderEvent $event
     */
    public function handle(OrderPickUpReminderEvent $event)
    {
        try{
            $event->order->user->notify(new OrderPickUpReminderNotification($event->order));
            Mail::to($event->order->user->email)->send(new OrderPickUpReminderMail($event->order));
        }catch(\Exception $exception){
            Log::error("Failed to send order deliver pickup reminder notification", [
                'message' => $exception->getMessage()
            ]);
        }
    }
}
