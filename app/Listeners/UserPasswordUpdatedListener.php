<?php

namespace App\Listeners;

use App\Events\UserPasswordUpdatedEvent;
use App\Mail\UserPasswordUpdatedMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserPasswordUpdatedListener
{
    /**
     * Handle the event.
     *
     * @param UserPasswordUpdatedEvent $event
     * @return void
     */
    public function handle(UserPasswordUpdatedEvent $event)
    {
        try{
            Mail::to($event->user->email)->send(new UserPasswordUpdatedMail($event->user));
        }catch (\Exception $exception){
            Log::error("Failed to send user password updated notification notification", [
                'message' => $exception->getMessage()
            ]);
        }

    }
}
