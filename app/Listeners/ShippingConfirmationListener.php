<?php

namespace App\Listeners;


use App\Events\ShippingConfirmationEvent;
use App\Mail\ShippingConfirmationMail;
use App\Notifications\ShippingConfirmationNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ShippingConfirmationListener
{
    /**
     * Handle the event.
     *
     * @param  ShippingConfirmationEvent  $event
     * @return void
     */
    public function handle(ShippingConfirmationEvent $event)
    {
        try{
            $event->order->user->notify(new ShippingConfirmationNotification($event->order));
            Mail::to($event->order->user->email)->send(new ShippingConfirmationMail($event->order));
        }catch(\Exception $exception){
            Log::error("Failed to send shipping confirmation notification", [
                'message' => $exception->getMessage()
            ]);
        }

    }
}
