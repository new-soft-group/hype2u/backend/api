<?php

namespace App\Listeners;

use App\Events\UserPickSubmittedEvent;
use App\Mail\UserPickSubmittedMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class UserPickSubmittedListener
{
    /**
     * Handle the event.
     *
     * @param UserPickSubmittedEvent $event
     * @return void
     */
    public function handle(UserPickSubmittedEvent $event)
    {
        try{
            Mail::to($event->pick->user->email)->send(new UserPickSubmittedMail($event->pick));
        }catch (\Exception $exception){
            Log::error("Failed to send user pick submitted email", [
                'message' => $exception->getMessage()
            ]);
        }
    }
}
