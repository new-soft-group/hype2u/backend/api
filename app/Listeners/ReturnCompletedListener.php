<?php

namespace App\Listeners;

use App\Events\ReturnCompletedEvent;
use App\Mail\ReturnCompletedMail;
use App\Notifications\ReturnCompletedNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ReturnCompletedListener
{
    /**
     * Handle the event.
     *
     * @param  ReturnCompletedEvent  $event
     * @return void
     */
    public function handle(ReturnCompletedEvent $event)
    {
        try{
            $event->order->user->notify(new ReturnCompletedNotification($event->order));
            Mail::to($event->order->user->email)->send(new ReturnCompletedMail($event->order));
        }catch (\Exception $exception){
            Log::error("Failed to send return completed notification", [
                'message' => $exception->getMessage()
            ]);
        }

    }
}
