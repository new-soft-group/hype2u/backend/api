<?php


namespace App\Listeners;


use App\Events\StockArrival;
use App\Notifications\StockArrivalNotification;

class SendStockArrivalNotification
{
    /**
     * @param StockArrival $arrival
     * @throws \Exception
     */
    public function handle(StockArrival $arrival)
    {
        foreach ($arrival->stock->queuers as $queuer) {
            $queuer->user->notify(new StockArrivalNotification($arrival->stock));
            $queuer->delete();
        }
    }
}
