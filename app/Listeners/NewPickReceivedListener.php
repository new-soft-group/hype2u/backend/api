<?php

namespace App\Listeners;


use App\Events\NewPickReceivedEvent;
use App\Mail\NewPickReceivedMail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class NewPickReceivedListener
{
    /**
     * Handle the event.
     *
     * @param NewPickReceivedEvent $event
     * @return void
     */
    public function handle(NewPickReceivedEvent $event)
    {
        try{
            Mail::to(config('constants.admin.email'))->send(new NewPickReceivedMail($event->pick));
        }catch (\Exception $exception){
            Log::error("Failed to send new pick received email", [
                'message' => $exception->getMessage()
            ]);
        }
    }
}
