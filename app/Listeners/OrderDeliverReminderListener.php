<?php

namespace App\Listeners;

use App\Events\OrderDeliverReminderEvent;
use App\Mail\OrderDeliverReminderMail;
use App\Notifications\OrderDeliverReminderNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class OrderDeliverReminderListener
{
    /**
     * Create the event listener.
     *
     * @param OrderDeliverReminderEvent $event
     */
    public function handle(OrderDeliverReminderEvent $event)
    {
        try{
            $event->order->user->notify(new OrderDeliverReminderNotification($event->order));
            Mail::to($event->order->user->email)->send(new OrderDeliverReminderMail($event->order));
        }catch(\Exception $exception){
            Log::error("Failed to send order deliver pickup reminder notification", [
                'message' => $exception->getMessage()
            ]);
        }

    }
}
