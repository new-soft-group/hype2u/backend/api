<?php

namespace App\Listeners;

use App\Events\ShopOrderRejectedEvent;
use App\Mail\ShopOrderRejectedMail;
use App\Notifications\ShopOrderRejectedNotification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class ShopOrderRejectedListener
{
    /**
     * Handle the event.
     *
     * @param  object  $event
     * @return void
     */
    public function handle(ShopOrderRejectedEvent $event)
    {
        try{
            $event->order->user->notify(new ShopOrderRejectedNotification($event->order));
            Mail::to($event->order->user->email)->send(new ShopOrderRejectedMail($event->order));
        }catch (\Exception $exception){
            Log::error("Failed to send shop order confirmation notification", [
                'message' => $exception->getMessage()
            ]);
        }
    }
}
