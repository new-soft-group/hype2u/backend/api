<?php

namespace App\Listeners;

use App\Events\NextBillingCycleReminderEvent;
use App\Mail\NextBillingCycleReminderMail;
use App\Notifications\NextBillingCycleReminderNotification;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Mail;

class NextBillingCycleReminderListener
{
    /**
     * Handle the event.
     *
     * @param  NextBillingCycleReminderEvent $event
     * @return void
     */
    public function handle(NextBillingCycleReminderEvent $event)
    {
        try {
            $event->subscription->user->notify(new NextBillingCycleReminderNotification($event->subscription));
            Mail::to($event->subscription->user->email)->send(new NextBillingCycleReminderMail($event->subscription));
        }catch (\Exception $exception){
            Log::error("Failed to send next billing cycle reminder notification", [
                'message' => $exception->getMessage()
            ]);
        }
    }
}
