<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\TimeslotType;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReturnCompletedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Order
     */
    private $order;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Order Return')
                    ->markdown('emails.return-completed', [
                    'order' => $this->order,
                    'user' => $this->order->user,
                    'items' => $this->order->items,
                    'url' => config('constants.mail.url'),
                    'button' => 'Explore styles'
                ]);
    }
}
