<?php

namespace App\Mail;

use App\Models\Pick;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewPickReceivedMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Pick
     */
    private $pick;

    /**
     * Create a new message instance.
     *
     * @param Pick $pick
     */
    public function __construct(Pick $pick)
    {
        $this->pick = $pick;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('New Pick For Me Request Received!')
            ->markdown('emails.new-pick-received', [
                'user' => $this->pick->user,
            ]);
    }
}
