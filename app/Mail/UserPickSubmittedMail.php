<?php

namespace App\Mail;

use App\Models\Pick;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class UserPickSubmittedMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Pick
     */
    private $pick;

    /**
     * Create a new message instance.
     *
     * @param Pick $pick
     */
    public function __construct(Pick $pick)
    {
        $this->pick = $pick;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Your Pick For Me Request has been submitted!')
            ->markdown('emails.user-pick-submitted', [
                'user' => $this->pick->user,
            ]);
    }
}
