<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactUs extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var array
     */
    private $attribute;

    /**
     * Create a new message instance.
     *
     * @param array $attribute
     */
    public function __construct(array $attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Contact Us')
                    ->markdown('emails.contact', [
                        'firstName' => $this->attribute['first_name'],
                        'lastName' => $this->attribute['last_name'],
                        'email' => $this->attribute['email'],
                        'question' => $this->attribute['question']
                    ]);
    }
}
