<?php

namespace App\Mail;

use App\Models\Order;
use App\Models\TimeslotType;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class OrderPickUpReminderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Order
     */
    private $order;

    /**
     * Create a new message instance.
     *
     * @param Order $order
     */
    public function __construct(Order $order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Order Pickup Reminder')
            ->markdown('emails.order-pickup-reminder', [
                'order' => $this->order,
                'user' => $this->order->user,
                'items' => $this->order->items,
                'timeslot' => $this->order->timeslots->where('timeslot_id', TimeslotType::RETURN_BACK)->first()->start_at->format('g:i A'),
                'url' => config('constants.mail.url'),
                'button' => 'Explore styles'
            ]);
    }
}
