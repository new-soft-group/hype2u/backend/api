<?php

namespace App\Mail;

use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewSubscriptionMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Subscription
     */
    private $subscription;

    /**
     * Create a new message instance.
     *
     * @param Subscription $subscription
     */
    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Plan Subscribed Successfully')
                    ->markdown('emails.new-subscription', [
                    'subscription' => $this->subscription,
                    'user' => $this->subscription->user,
                    'url' => config('constants.mail.url'),
                    'button' => 'Explore styles'
                ]);
    }
}
