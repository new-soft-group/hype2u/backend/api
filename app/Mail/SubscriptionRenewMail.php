<?php

namespace App\Mail;

use App\Models\Subscription;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubscriptionRenewMail extends Mailable
{
    use Queueable, SerializesModels;
    /**
     * @var Subscription
     */
    private $subscription;

    /**
     * Create a new message instance.
     *
     * @param Subscription $subscription
     */
    public function __construct(Subscription $subscription)
    {
        $this->subscription = $subscription;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject('Renewed Subscription Successfully')
                    ->markdown('emails.subscription-renew', [
                    'subscription' => $this->subscription,
                    'user' => $this->subscription->user,
                    'url' => config('constants.mail.url'),
                    'button' => 'Explore styles'
                ]);
    }
}
