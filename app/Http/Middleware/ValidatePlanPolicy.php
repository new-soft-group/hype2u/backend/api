<?php

namespace App\Http\Middleware;

use App\Exceptions\JsonException;
use App\Exceptions\ViolatePolicyException;
use App\Models\Cart;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class ValidatePlanPolicy
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws ViolatePolicyException
     * @throws JsonException
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
        $user = $request->user();
        /**
         * Get default subscription
         */
        $subscription = $user->defaultSubscription();
        /**
         * Subscription doesn't exist
         */
        if(is_null($subscription)) abort(403);
        /**
         * Subscribed plan
         */
        $plan = $subscription->plan;
        /**
         * Current checkout cart
         */
        $cart = $user->getCart();
        /**
         * Put items that are not return by current user
         * into cart for validation
         */
        $itemsNotReturn = $user->carryForwardItems();

        $itemsNotReturn->moveToCart($cart);
        /**
         * Based on route to decide whether add/update cart item
         */
        if($request->routeIs('add.cart.items')) {
            /**
             * Attempt to add item to existing cart
             * this action will not create record in database
             */
            $cart->attemptAddItem($request->all());
        }
        else {
            /**
             * Attempt to update item for validation purpose
             * this action will not create record in database
             */
            $updated = $cart->attemptUpdateItem($request->route('id'), $request->all());

            if($updated === false) {
                return $next($request);
            }
        }
        /**
         * Validate current cart's items against the subscribed plan's policy
         */
        $validator = $plan->policyValidator($cart->mergeCarryForwardItems());

        if(!$validator->validate()) {
            throw new ViolatePolicyException($validator->getMessage());
        }

        $validator = $plan->validator()
                          ->setPurchaseAmount($cart->totalAmount($plan->policies->primeTargetIds()))
                          ->setExchange($user->ongoingMonthlyOrders()->count());

        if(!$validator->validate()) {
            throw new JsonException($validator->getMessage(), 400);
        }

        return $next($request);
    }
}
