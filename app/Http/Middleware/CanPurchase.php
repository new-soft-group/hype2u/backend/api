<?php

namespace App\Http\Middleware;

use App\Exceptions\OngoingOrderException;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class CanPurchase
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param \Closure $next
     * @return mixed
     * @throws OngoingOrderException
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
        $user = $request->user();

        $orders = $user->ongoingOrders()->get();

        if($orders->isNotEmpty()) {
            throw new OngoingOrderException($orders->count());
        }

        return $next($request);
    }
}
