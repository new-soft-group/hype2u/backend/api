<?php

namespace App\Http\Middleware;

use App\Models\BusinessType;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class CanAddShopCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
        $user = $request->user();

        /**
         * Current checkout cart
         */
        $cart = $user->getCart();

        /**
         * Based on route to decide whether add/update cart item
         */
        if($request->routeIs('add.shop.cart.items')) {
            /**
             * Attempt to add item to existing cart
             * this action will not create record in database
             */
            $cart->attemptAddItem($request->all(), BusinessType::SHOP);
        }
        else {
            /**
             * Attempt to update item for validation purpose
             * this action will not create record in database
             */
            $updated = $cart->attemptUpdateItem($request->route('id'), $request->all());

            if($updated === false) {
                return $next($request);
            }
        }

        return $next($request);
    }
}
