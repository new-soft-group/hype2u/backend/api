<?php

namespace App\Http\Middleware;

use App\Models\Cart;
use Closure;

class CheckSessionCart
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $guest = is_null($request->user('api'));
        
        if($guest && !$request->hasHeader(Cart::SESSION_HEADER)) {
            
            abort(400, trans('cart.failed.session'));
        }
        
        return $next($request);
    }
}
