<?php

namespace App\Http\Middleware;

use App\Exceptions\TimeslotFullyBookedException;
use App\Exceptions\UnavailableDateException;
use App\Models\Holiday;
use App\Models\Timeslot;
use Carbon\Carbon;
use Closure;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class TimeslotReservation
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws TimeslotFullyBookedException
     * @throws UnavailableDateException
     */
    public function handle(Request $request, Closure $next)
    {
        $timeslot = $this->getTimeslotModel($request);
        $date = $this->getDate($request);

        if (!is_null($timeslot) && $timeslot->fullyBooked($date)) {
            throw new TimeslotFullyBookedException();
        }

        if (!is_null($date) && (bool)strtotime($date)) {
            $carbon = Carbon::parse($date);

            if (!config('timeslot.ignore.weekend') && $carbon->isWeekend()) {
                throw new UnavailableDateException(__('timeslot.weekend'));
            }

            if (!config('timeslot.ignore.holiday') && Holiday::crashOn($date)) {
                throw new UnavailableDateException(__('timeslot.holiday'));
            }
        }

        return $next($request);
    }

    public function getTimeslotModel(Request $request)
    {
        $model = $this->getTimeslot($request);

        if ($model instanceof Model) {
            return $model;
        }
        elseif (is_int($model) || is_string($model)) {
            return Timeslot::find($model);
        }

        return null;
    }

    public function getTimeslot(Request $request)
    {
        $default = $request->route()->parameter('timeslot');

        return $request->get('timeslot', $default);
    }

    public function getDate(Request $request)
    {
        $default = $request->get('timeslot_date');

        return $request->get('date', $default);
    }

    public function timeslotExist(Request $request)
    {
        $param = $request->route()->hasParameter('timeslot');

        return $request->exists('timeslot') || $param;
    }

    public function dateExist(Request $request)
    {
        return $request->exists('date')
            || $request->exists('timeslot_date');
    }
}
