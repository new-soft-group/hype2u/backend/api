<?php

namespace App\Http\Middleware;

use App\Exceptions\InvalidSubscriptionException;
use App\Exceptions\RequireSubscriptionException;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class Subscribed
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     * @throws RequireSubscriptionException|InvalidSubscriptionException
     */
    public function handle(Request $request, Closure $next)
    {
        /** @var User $user */
        $user = $request->user();

        if(is_null($user)) {
            abort(401);
        }

        $subscription = $user->defaultSubscription();

        if(is_null($subscription)) {
            throw new RequireSubscriptionException();
        }

        else if(!$subscription->valid()) {
            $status = str_replace('_', ' ', $subscription->stripe_status);
            throw new InvalidSubscriptionException($status);
        }

        return $next($request);
    }
}
