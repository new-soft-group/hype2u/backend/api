<?php

namespace App\Http\Resources;

use App\Models\DiscountCode;
use App\Models\User;
use Illuminate\Http\Resources\Json\JsonResource;

class AffiliateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource($this->whenLoaded('user')),
            'level' => $this->level,
            'commission' => $this->commission,
            'parent_id' => $this->parent_id,
            'discount_code' => new DiscountCodeResource($this->whenLoaded('discountCode'))
        ];
    }
}
