<?php

namespace App\Http\Resources;

use App\Models\DiscountType;
use Illuminate\Http\Resources\Json\JsonResource;

class DiscountCodeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'code' => $this->code,
            'discount' => $this->discount,
            'discount_pricing' => $this->discount_pricing,
            'discount_type' => new DiscountTypeResource($this->whenLoaded('discountType')),
            'discount_method' => new DiscountMethodResource($this->whenLoaded('discountMethod')),
            'expired_at' => $this->expired_at,
            'updated_at' => $this->updated_at
        ];
    }
}
