<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class UserResource extends JsonResource
{
    /**
     * @var null
     */
    private $token;
    
    public function __construct($resource, $token = null)
    {
        parent::__construct($resource);
        $this->token = $token;
    }
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'token' => $this->when(isset($this->token), $this->token),
            'roles' => RoleResource::collection($this->whenLoaded('roles')),
            'profile' => new ProfileResource($this->whenLoaded('profile')),
            'affiliate' => new AffiliateResource($this->whenLoaded('affiliate')),
            'created_at' => $this->created_at,
        ];
    }
}
