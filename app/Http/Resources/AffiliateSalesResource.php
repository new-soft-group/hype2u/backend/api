<?php

namespace App\Http\Resources;

use App\Models\OrderItem;
use Illuminate\Http\Resources\Json\JsonResource;

class AffiliateSalesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'by' => new SalesByResource($this->whenLoaded('by')),
            'commission' => $this->commission,
            'created_at' => $this->created_at,
            'amount' => $this->order->converted_amount,
            'currency' => $this->order->currency,
            'currency_rate' => $this->order->currency_rate,
            'order_discount' => $this->order->discount,
            'order_discount_type' => $this->order->discount_type,
            'order_created_at' => $this->order->created_at,
            'customer_name' => $this->order->customer->name,
            'order_items' => OrderItemResource::collection($this->order->items)
        ];
    }
}
