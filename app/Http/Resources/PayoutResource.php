<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PayoutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'affiliate_id' => $this->affiliate_id,
            'commission_sum' => $this->commission_sum,
            'reference_no' => $this->reference_no,
            'paid' => $this->paid,
            'user_id' => $this->affiliate->user->id,
            'name' => $this->affiliate->user->name,
            'email' => $this->affiliate->user->email,
            'mobile' => $this->affiliate->user->mobile,
            'date' => $this->date,
        ];
    }
}
