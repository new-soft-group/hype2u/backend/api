<?php


namespace App\Http\Resources\Coupon;


use App\Http\Resources\Plan\PlanResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'stripe_id' => $this->stripe_id,
            'code' => $this->code,
            'amount_off' => $this->amount_off ?? null,
            'currency' => $this->currency ?? null,
            'percent_off' => $this->percent_off ?? null,
            'duration' => $this->duration,
            'duration_in_months' => $this->duration_in_months ?? null,
            'max_redemptions' => $this->max_redemptions ?? null,
            'times_redeemed' => $this->times_redeemed ?? null,
            'redeem_by' => $this->redeem_by ?? null,
            'enabled' => $this->enabled,
            'deleted_at' => $this->deleted_at,
            'created_at' => $this->created_at,
            'plan' => PlanResource::collection($this->whenLoaded('plans'))
        ];
    }
}
