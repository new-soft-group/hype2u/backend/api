<?php


namespace App\Http\Resources\Coupon;


use Illuminate\Http\Resources\Json\JsonResource;

class SimpleCouponResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'code' => $this->code,
            'amount_off' => $this->amount_off ?? null,
            'currency' => $this->currency ?? null,
            'percent_off' => $this->percent_off ?? null
        ];
    }
}
