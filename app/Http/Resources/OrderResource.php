<?php

namespace App\Http\Resources;


use App\Http\Resources\Invoice\InvoiceResource;
use App\Http\Resources\Product\ProductStockInventoryResource;
use App\Http\Resources\Timeslot\TimeslotAllocationResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'status' => new OrderStatusResource($this->whenLoaded('status')),
            'amount' => $this->amount,
            'currency' => $this->currency,
            'user' => new UserResource($this->whenLoaded('user')),
            'items' => OrderItemResource::collection($this->whenLoaded('items')),
            'shipping_address' => new AddressResource($this->whenLoaded('shippingAddress')),
            'tracks' => ProductStockInventoryResource::collection($this->whenLoaded('tracks')),
            'invoice' => new InvoiceResource($this->whenLoaded('invoice')),
            'timeslots' => TimeslotAllocationResource::collection($this->whenLoaded('timeslots')),
            'order_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'shipped_at' => $this->shipped_at,
            'approved_at' => $this->approved_at,
            'shipping_at' => $this->shipping_at,
            'return_at' => $this->returned_at,
            'pick_up_requested_at' => $this->pick_up_requested_at,
            'reason' => $this->reason,
            'business_type_id' => $this->business_type_id,
            'business_type' => new BusinessTypeResource($this->whenLoaded('businessType'))
        ];
    }
}
