<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\OrderCollection;
use App\Http\Resources\StockStatusResource;
use App\Http\Resources\WarehouseResource;
use App\Models\ProductStock;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductStockInventoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'product_stock_id' => $this->product_stock_id,
            'stock' => new ProductStockResource(ProductStock::find($this->product_stock_id)->load('product')),
            'warehouse' => new WarehouseResource($this->whenLoaded('warehouse')),
            'tag' => $this->tag,
            'status' => new StockStatusResource($this->whenLoaded('status')),
            'shipped_out_tracks' => new OrderCollection($this->whenLoaded('shippedOutTracks')),
            'scanned_at' => $this->whenPivotLoaded('inventory_order_tracks', function () {
                return $this->pivot->scanned_at ?? null;
            })
        ];
    }
}


