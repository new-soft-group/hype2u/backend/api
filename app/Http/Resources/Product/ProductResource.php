<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\BrandResource;
use App\Http\Resources\BusinessTypeResource;
use App\Http\Resources\CategoryResource;
use App\Http\Resources\GenderResource;
use App\Http\Resources\ImageResource;
use App\Models\ProductType;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'description' => $this->description,
            'sku' => $this->sku,
            'slug' => $this->slug,
            'images' => ImageResource::collection($this->whenLoaded('images')),
            'pricing' => $this->pricing,
            'categories' => CategoryResource::collection($this->whenLoaded('categories')),
            'brand' => new BrandResource($this->whenLoaded('brand')),
            'gender' => new GenderResource($this->whenLoaded('gender')),
            'type' => new ProductTypeResource($this->whenLoaded('type')),
            'variants' => ProductVariantResource::collection($this->whenLoaded('variants')),
            'stocks' => ProductStockResource::collection($this->whenLoaded('stocks')),
            'featured' => $this->featured,
            'enabled' => $this->enabled,
            'new_arrival' => $this->new_arrival,
            'most_popular' => $this->most_popular,
            'total_inventories_count' => $this->total_inventories_count,
            'business_type' => new BusinessTypeResource($this->whenLoaded('businessType'))
        ];
    }
}


