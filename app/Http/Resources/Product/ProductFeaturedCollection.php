<?php

namespace App\Http\Resources\Product;


use App\Models\Product;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class ProductFeaturedCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $grouped = $this->collection
            ->groupBy(function (Product $product) {
                return $product->categories->first()->name;
            })
            ->map(function (Collection $products) {
                return ProductResource::collection($products);
            });

        return $grouped->toArray();
    }
}


