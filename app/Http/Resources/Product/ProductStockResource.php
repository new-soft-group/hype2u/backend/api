<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductStockResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'sku' => $this->sku,
            'pricing' => $this->pricing,
            'price' => $this->price,
            'cost_price' => $this->cost_price,
            'vendor_id' => $this->vendor_id,
            'product' => new ProductResource($this->whenLoaded('product')),
            $this->mergeWhen($this->whenLoaded('options'), [
                'product_variant_option_ids' => $this->combination_ids,
                'option_combination' => $this->combination
            ]),
            'reserved' => $this->reserved,
            'enabled' => $this->enabled,
            'inventories_count' => isset($this->inventories_count) ? $this->inventories_count - $this->reserved : 0
        ];
    }
}


