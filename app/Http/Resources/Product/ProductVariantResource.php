<?php

namespace App\Http\Resources\Product;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductVariantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'variant_id' => $this->lookup->id,
            'name' => $this->lookup->name,
            'options' => ProductVariantOptionResource::collection($this->whenLoaded('options'))
        ];
    }
}


