<?php

namespace App\Http\Resources\Product;

use App\Http\Resources\BusinessTypeResource;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'enabled' => $this->enabled,
            'business_type_id' => $this->business_type_id,
            'business_type' => new BusinessTypeResource($this->whenLoaded('businessType'))
        ];
    }
}
