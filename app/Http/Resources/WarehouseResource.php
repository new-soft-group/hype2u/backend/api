<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class WarehouseResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'country' => $this->country,
            'name' => $this->name,
            'slug' => $this->slug,
            'address1' => $this->address1,
            'address2' => $this->address2,
            'city' => $this->city,
            'phone' => $this->phone,
            'fax' => $this->fax,
            'default' => $this->default,
            'enabled' => $this->enabled,
        ];
    }
}


