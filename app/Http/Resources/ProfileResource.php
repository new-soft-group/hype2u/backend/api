<?php

namespace App\Http\Resources;

use App\Models\Address;
use App\Models\Gender;
use App\Models\VariantOption;
use Illuminate\Http\Resources\Json\JsonResource;

class ProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'first_name' => $this->first_name,
            'last_name' => $this->last_name,
            'name' => $this->name,
            'mobile' => $this->mobile,
            'dob' => $this->dob !== null ?$this->dob->format('Y-m-d') : null,
            'gender' => new GenderResource(Gender::find($this->gender_id)),
            'size' => new VariantOptionResource(VariantOption::find($this->size_id)),
            'shipping_address' => new AddressResource(Address::find($this->shipping_address_id)),
            'billing_address' => new AddressResource(Address::find($this->billing_address_id))
        ];
    }
}
