<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CheckoutResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'currency' => $this->converted_currency,
            'amount' => $this->converted_amount,
            'discount' => $this->order->discount,
            'order_id' => $this->order_id,
            'order_status' => $this->order->status->name,
            'order_items' => CheckoutItemResource::collection($this->order->items),
            'status' => $this->status->name,
            'created_at' => $this->created_at
        ];
    }
}
