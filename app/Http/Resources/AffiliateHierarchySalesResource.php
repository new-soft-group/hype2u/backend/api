<?php

namespace App\Http\Resources;

use App\Models\Affiliate;
use Illuminate\Http\Resources\Json\JsonResource;

class AffiliateHierarchySalesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $sales = [
            'affiliate_id' => $this->id,
            'commission_sum' => 0
        ];
        
        if(count($this->sumOfSales)) {
            $sales['commission_sum'] = $this->sumOfSales->first()->commission_sum;
        }
    
        $children = $this->children->map(function (Affiliate $affiliate) {
            return new AffiliateHierarchySalesResource($affiliate);
        });

        return [
            'level' => $this->level,
            'total_member' => $this->descendants_count,
            'user' => new UserResource($this->whenLoaded('user')),
            'commission' => $this->commission,
            'children' => $children ?? [],
            $this->merge($sales)
        ];
    }
}
