<?php

namespace App\Http\Resources\Plan;

use Illuminate\Http\Resources\Json\JsonResource;

class PlanPolicyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'logical' => $this->logical,
            'quantity' => $this->quantity,
            'rules' => PolicyPivotResource::collection($this->whenLoaded('rules'))
        ];
    }
}
