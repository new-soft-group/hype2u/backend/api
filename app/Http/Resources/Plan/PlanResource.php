<?php

namespace App\Http\Resources\Plan;

use Illuminate\Http\Resources\Json\JsonResource;

class PlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'stripe_plan' => $this->stripe_plan,
            'amount' => $this->amount,
            'purchase_amount' => $this->purchase_amount,
            'interval' => $this->interval,
            'slug' => $this->slug,
            'description' => $this->description,
            'info' => $this->info,
            'exchange_limit' => $this->exchange_limit,
            'sort' => $this->sort,
            'enabled' => $this->enabled,
            'featured' => $this->featured,
            'sms' => $this->sms,
            'policies' => PlanPolicyResource::collection($this->whenLoaded('policies')),
            'surcharges' => SurchargeResource::collection($this->whenLoaded('surcharges'))
        ];
    }
}
