<?php

namespace App\Http\Resources;

use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Product\ProductStockInventoryResource;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItemResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'unit_price' => $this->unit_price,
            'quantity' => $this->quantity,
            'product' => new ProductResource($this->whenLoaded('product')),
            'variant' => [
                'name' => $this->variant,
                'price' => $this->variant_price,
            ]
        ];
    }
}
