<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\Plan\PlanResource;

class SubscriptionResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'plan' => new PlanResource($this->whenLoaded('plan')),
            'user' => new UserResource($this->whenLoaded('user')),
            'stripe_id' => $this->stripe_id,
            'stripe_plan' => $this->stripe_plan,
            'stripe_status' => $this->stripe_status,
            'quantity' => $this->quantity,
            'trial_ends_at' => $this->trial_ends_at,
            'created_at' => $this->created_at,
            'next_cycle_date' => $this->next_cycle_date,
        ];
    }
}
