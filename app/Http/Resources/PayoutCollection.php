<?php

namespace App\Http\Resources;

use App\Models\Affiliate;
use Illuminate\Http\Resources\Json\ResourceCollection;

class PayoutCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => PayoutResource::collection($this->collection)
        ];
    }
}
