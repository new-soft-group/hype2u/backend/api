<?php

namespace App\Http\Resources\Timeslot;

use App\Http\Resources\OrderResource;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TimeslotEventResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'timeslot_id' => $this->timeslot_id,
            'order_id' => $this->order_id,
            'order' => new OrderResource(Order::find($this->order_id)),
            'name' => $this->whenLoaded('timeslot', function () {
                return $this->timeslot->type->name;
            }),
            'date' => $this->start_at->toDateString(),
            'start' => $this->start_at,
            'end' => $this->end_at
        ];
    }
}
