<?php

namespace App\Http\Resources\Timeslot;

use App\Http\Resources\OrderResource;
use App\Http\Resources\OrderStatusResource;
use App\Models\Order;
use Illuminate\Http\Resources\Json\JsonResource;

class TimeslotAllocationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'timeslot_id' => $this->timeslot_id,
            'order_id' => $this->order_id,
            'timeslot' => new TimeslotResource($this->whenLoaded('timeslot')),
            'order' => new OrderResource($this->whenLoaded('order')),
            'start_at' => $this->start_at,
            'end_at' => $this->end_at
        ];
    }
}
