<?php

namespace App\Http\Resources\Timeslot;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TimeslotResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'start_time' => Carbon::parse($this->start_time)->format('H:i'),
            'end_time' => Carbon::parse($this->end_time)->format('H:i'),
            'type_id' => $this->type_id,
            'limit' => $this->limit,
            'enabled' => $this->enabled,
            'type' => new TimeslotTypeResource($this->whenLoaded('type')),
            'allocations' => TimeslotAllocationResource::collection($this->whenLoaded('allocations')),
            $this->mergeWhen(isset($this->allocations_count), function () {
                return ['full' => $this->allocations_count >= $this->limit];
            })
        ];
    }
}
