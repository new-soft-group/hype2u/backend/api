<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PaymentMethodResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'type' => $this->type,
            'brand' => $this->card->brand,
            'country' => $this->card->country,
            'exp_month' => $this->card->exp_month,
            'exp_year' => $this->card->exp_year,
            'last4' => $this->card->last4
        ];
    }
}
