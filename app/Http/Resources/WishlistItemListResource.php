<?php

namespace App\Http\Resources;

use App\Http\Resources\Product\ProductResource;
use App\Models\User;
use App\Models\Wishlist;
use Illuminate\Http\Resources\Json\JsonResource;

class WishlistItemListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user' => new UserResource(User::find(Wishlist::find($this->wishlist_id)->user_id)),
            'product'=> new ProductResource($this->whenLoaded('product')),
            'created_at'=> $this->created_at != null ? $this->created_at->format('d-m-Y') : null,
            'updated_at'=> $this->updated_at != null ? $this->updated_at->format('d-m-Y') : null,
        ];
    }
}
