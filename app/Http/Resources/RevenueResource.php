<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RevenueResource extends JsonResource
{
    /**
     * @var null
     */
    private $total_success_amount = 0;
    private $total_amount_refunded = 0;
    private $total_revenue;
    private $currency;

    public function __construct($resource)
    {
        parent::__construct($resource);

        /** Get payment summary relation from user */
        $payment_summary = $this->payments;

        /** Sum all refunded amount from user */
        foreach($payment_summary as $payment){
            if($payment->type == 'charge.refunded'){
                $this->total_amount_refunded += $payment->amount_refunded;
            }
        }

        /** Get succeeded invoice from user*/
        $invoices = $this->invoices()->map(function($invoice) {
            return [
                'total' => $invoice->total,
                'currency' => $invoice->currency
            ];
        });

        /** Sum all invoice amount and set currency*/
        foreach($invoices as $invoice){
            $this->total_success_amount += ($invoice['total'] / 100);
            $this->currency = $invoice['currency'];
        }

        /** Get total_revenue */
        $this->total_revenue = $this->total_success_amount - $this->total_amount_refunded;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'email' => $this->email,
            'stripe_id' => $this->stripe_id,
            'total_success_amount' => $this->total_success_amount,
            'total_amount_refunded' => $this->total_amount_refunded,
            'total_revenue' => $this->total_revenue,
            'currency' => $this->currency,
            'profile' => new ProfileResource($this->whenLoaded('profile')),
        ];
    }
}
