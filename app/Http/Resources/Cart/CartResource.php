<?php

namespace App\Http\Resources\Cart;

use App\Http\Resources\BusinessTypeResource;
use App\Http\Resources\Cart\CartItemResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CartResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'business_type' => new BusinessTypeResource($this->whenLoaded('business_type')),
            'items' => CartItemResource::collection($this->whenLoaded('items')),
            'carry_forward' => CarryForwardItemResource::collection($this->whenLoaded('carryForward'))
        ];
    }
}
