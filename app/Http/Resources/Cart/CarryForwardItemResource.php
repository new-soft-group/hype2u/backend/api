<?php

namespace App\Http\Resources\Cart;

use App\Collections\Carry\CarryForward;
use App\Http\Resources\Product\ProductResource;
use Illuminate\Http\Resources\Json\JsonResource;

class CarryForwardItemResource extends JsonResource
{
    /**
     * @var CarryForward
     */
    public $resource;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'order_id' => $this->resource->getOrderKey(),
            'order_status_id' => $this->resource->getOrderStatusId(),
            'product' => new ProductResource($this->resource->getProduct()),
            'variant' => $this->resource->getVariant(),
            'variant_price' => $this->resource->getVariantPrice(),
            'unit_price' => $this->resource->getUnitPrice(),
            'quantity' => $this->resource->getQuantity(),
            'amount' => $this->resource->getAmount()
        ];
    }
}
