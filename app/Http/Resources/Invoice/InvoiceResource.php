<?php

namespace App\Http\Resources\Invoice;

use App\Http\Resources\Coupon\CouponResource;
use Illuminate\Http\Resources\Json\JsonResource;

class InvoiceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'stripe_id' => $this->stripe_id,
            'payment_intent' => $this->payment_intent,
            'currency' => $this->currency,
            'amount' => $this->amount,
            'status' => $this->status,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
            'pdf' => $this->pdf,
            'coupon' => new CouponResource($this->whenLoaded('coupon'))
        ];
    }
}
