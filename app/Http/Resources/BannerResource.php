<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class BannerResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'viewport' => $this->viewport,
            'sort' => $this->sort ?? 0,
            'image' => new ImageResource($this->whenLoaded('image')),
            'position' => new BannerPositionResource($this->whenLoaded('position'))
        ];
    }
}
