<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class WishlistItemListCollection extends ResourceCollection
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }
    
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => WishlistItemListResource::collection($this->collection),
        ];
    }
}
