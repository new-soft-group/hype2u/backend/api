<?php

namespace App\Http\Resources;

use App\Models\Gender;
use Illuminate\Http\Resources\Json\JsonResource;

class PickResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'user'=> new UserResource($this->whenLoaded('user')),
            'gender' => new GenderResource($this->whenLoaded('gender')),
            'sizes' => $this->sizes,
            'styles' => $this->styles,
            'fits' => $this->fits,
            'brands' => $this->brands,
            'colors' => $this->colors,
            'status' => $this->status,
            'remark' => $this->remark,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at,
        ];
    }
}
