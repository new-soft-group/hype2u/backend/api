<?php

namespace App\Http\Requests\Product;

use App\Models\Product;
use App\Rules\AllowedLanguage;
use App\Rules\Decimal;
use App\Rules\Discount;
use App\Rules\DiscountType;
use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'slug' => 'required|unique:products,slug',
            'sku' => 'nullable|string|max:255',
            'price' => ['required', new Decimal()],
            'cost_price' => ['required', new Decimal()],
            'images' => 'required|array|min:1',
            'images.*.file' => 'required|file|image',
            'images.*.sort' => 'nullable|integer|min:0',
            'available_date' => 'nullable|date',
            'category_ids' => 'required|array|min:1',
            'category_ids.*' => 'required|integer|distinct|exists:categories,id',
            'gender_id' => 'required|integer|exists:genders,id',
            'brand_id' => 'required|integer|exists:brands,id',
            'business_type_id' => 'required|integer|exists:business_types,id',
            'product_type_id' => 'required|integer|exists:product_types,id',
            'variants' => 'nullable|array|min:1',
            'variants.*.variant_id' => 'required|distinct|exists:variants,id',
            'variants.*.options' => 'required|array|min:1',
            'variants.*.options.*.variant_option_id' => 'required|distinct|exists:variant_options,id',
            'enabled' => 'nullable|boolean',
            'featured' => 'nullable|boolean',
            'new_arrival' => 'nullable|boolean',
            'most_popular' => 'nullable|boolean',
        ];
    }
}
