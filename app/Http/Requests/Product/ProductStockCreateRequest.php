<?php

namespace App\Http\Requests\Product;

use App\Rules\Decimal;
use Illuminate\Foundation\Http\FormRequest;

class ProductStockCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sku' => 'nullable|string|max:255',
            'price' => ['required', new Decimal()],
            'product_variant_option_ids' => 'required|array|min:1',
            // 'product_variant_option_ids.*' => 'required|distinct|exists:variant_options,id',
            'product_variant_option_ids.*' => 'required|distinct',
        ];
    }
}
