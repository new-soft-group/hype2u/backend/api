<?php

namespace App\Http\Requests\Product;

use App\Rules\Decimal;
use Illuminate\Foundation\Http\FormRequest;

class ProductStockUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vendor' => 'nullable|exists:vendors,id',
            'price' => ['nullable', new Decimal()],
        ];
    }
}
