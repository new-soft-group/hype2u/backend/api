<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductTypeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'types' => 'required|array|min:1',
            'types.*.id' => 'required|integer|exists:product_types,id',
            'types.*.name' => 'required|string|max:255',
            'types.*.business_type_id' => 'required|integer|exists:business_types,id',
            'enabled' => 'nullable|boolean',
        ];
    }
}
