<?php

namespace App\Http\Requests\Product;

use App\Rules\AllowedLanguage;
use App\Rules\Decimal;
use App\Rules\Discount;
use App\Rules\DiscountType;
use Illuminate\Foundation\Http\FormRequest;

class ProductUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'description' => 'nullable|string',
            'slug' => 'nullable|unique:products,slug,'.$this->request->get('id'),
            'discount' => [
                'bail',
                'required_with:discount_type_id',
                'numeric',
                'min:0',
                new Decimal(),
                new Discount($this->request->get('discount_type_id', 0))
            ],
            'discount_type_id' => [
                'required_with:discount',
                new DiscountType()
            ],
            'price' => ['nullable', new Decimal()],
            'cost_price' => ['nullable', new Decimal()],
            'images' => 'nullable|array|min:1',
            'images.*.file' => 'required|file|image',
            'images.*.sort' => 'required|integer|min:0',
            'deleted_images' => 'nullable|array|min:1',
            'deleted_images.*' => 'required|exists:images,id',
            'uploaded_images' => 'nullable|array|min:1',
            'uploaded_images.*.id' => 'required|exists:images,id',
            'available_date' => 'nullable|date',
            'categories_id' => 'nullable|array|min:1',
            'categories_id.*' => 'required|integer|distinct|exists:categories,id',
            'gender_id' => 'nullable|integer|exists:genders,id',
            'brand_id' => 'nullable|integer|exists:brands,id',
            'business_type_id' => 'nullable|integer|exists:business_types,id',
            'product_type_id' => 'nullable|integer|exists:product_types,id',
            'variants' => 'nullable|array|min:1',
            'variants.*.variant_id' => 'required|distinct|exists:variants,id',
            'variants.*.options' => 'required|array|min:1',
            'variants.*.options.*.variant_option_id' => 'required|distinct|exists:variant_options,id',
            'enabled' => 'nullable|boolean',
            'featured' => 'nullable|boolean',
            'new_arrival' => 'nullable|boolean',
            'most_popular' => 'nullable|boolean'
        ];
    }
}
