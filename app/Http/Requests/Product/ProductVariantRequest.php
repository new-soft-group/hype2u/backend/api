<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductVariantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'variants' => 'required|array|min:1',
            'variants.*.id' => 'nullable|distinct|exists:product_variants,id',
            'variants.*.variant_id' => 'nullable|distinct|exists:variants,id',
            'variants.*.options' => 'required|array|min:1',
            'variants.*.options.*.id' => 'nullable|distinct|exists:product_variant_options,id',
            'variants.*.options.*.variant_option_id' => 'nullable|distinct|exists:variant_options,id',
        ];
    }
}
