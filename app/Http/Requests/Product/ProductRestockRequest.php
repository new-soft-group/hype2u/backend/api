<?php

namespace App\Http\Requests\Product;

use Illuminate\Foundation\Http\FormRequest;

class ProductRestockRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tags' => 'required|array|min:1',
            'tags.*' => 'required|distinct|unique:product_stock_inventories,tag',
            'warehouse_id' => 'required|exists:warehouses,id',
            'stock_status_id' => 'required|integer|exists:stock_statuses,id'
        ];
    }
}
