<?php

namespace App\Http\Requests;

use App\Rules\OldPasswordCheck;
use App\Rules\UniqueNewPassword;
use Illuminate\Foundation\Http\FormRequest;
use Mockery\Generator\StringManipulation\Pass\Pass;

class ChangePasswordRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'old_password' => [
                'required',
                'string',
                resolve(OldPasswordCheck::class)
            ],
            'password' => [
                'required',
                'string',
                'min:8',
                'confirmed',
                resolve(UniqueNewPassword::class)
            ],
        ];
    }
}
