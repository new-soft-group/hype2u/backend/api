<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RolePermissionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'role_permissions' => 'required|array|min:1',
            'role_permissions.*.id' => 'required|exists:roles,id',
            'role_permissions.*.permissions' => 'required|array|min:1',
            'role_permissions.*.permissions.*.id' => 'required|exists:permissions,id',
        ];
    }
}
