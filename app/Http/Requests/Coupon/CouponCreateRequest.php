<?php


namespace App\Http\Requests\Coupon;


use App\Models\Coupon;
use App\Rules\AllowedCurrencies;
use App\Rules\Decimal;
use Illuminate\Foundation\Http\FormRequest;

class CouponCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'code' => 'required|string|max:255',
            'amount_off' => ['required_without:percent_off', new Decimal()],
            'currency' => ['required_with:amount_off', new AllowedCurrencies()],
            'percent_off' => ['required_without:amount_off,currency', new Decimal()],
            'duration' => 'required|in:'.Coupon::DURATION_ONCE.','.Coupon::DURATION_REPEATING.','.Coupon::DURATION_FOREVER,
            'duration_in_months' => 'required_if:duration,'.Coupon::DURATION_REPEATING.'|integer|min:1',
            'max_redemptions' => 'nullable|integer|min:1',
            'redeem_by' => 'nullable|date_format:Y-m-d H:i:s|after:now',
            'enabled' => 'nullable|boolean',
            'plan_ids' => 'required|array'
        ];
    }
}
