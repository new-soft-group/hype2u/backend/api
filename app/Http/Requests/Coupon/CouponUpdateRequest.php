<?php


namespace App\Http\Requests\Coupon;


use App\Models\Coupon;
use App\Rules\AllowedCurrencies;
use App\Rules\Decimal;
use Illuminate\Foundation\Http\FormRequest;

class CouponUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'code' => 'nullable|string|max:255',
            'enabled' => 'nullable|boolean',
            'plan_ids' => 'nullable|array'
        ];
    }
}
