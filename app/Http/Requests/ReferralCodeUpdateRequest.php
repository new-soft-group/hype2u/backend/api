<?php

namespace App\Http\Requests;

use App\Rules\Decimal;
use App\Rules\UniqueDiscountCode;
use Illuminate\Foundation\Http\FormRequest;

class ReferralCodeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'ref_code' => ['nullable', new UniqueDiscountCode()],
        ];
    }
}
