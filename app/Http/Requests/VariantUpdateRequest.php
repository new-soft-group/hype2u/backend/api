<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VariantUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
            'options' => 'nullable|array|min:1',
            'options.*.id' => 'nullable|integer|exists:variant_options,id',
            'options.*.name' => 'required|string|max:255',
            'removed_options' => 'nullable|array|min:1',
            'removed_options.*' => 'required|integer|exists:variant_options,id'
        ];
    }
}
