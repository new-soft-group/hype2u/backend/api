<?php

namespace App\Http\Requests;

use App\Rules\AllowedLanguage;
use App\Rules\Decimal;
use Illuminate\Foundation\Http\FormRequest;

class VendorCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'company_name' => 'required|string',
            'contact_name' => 'required|string',
            'contact_title' => 'required|string',
            'address1' => 'required',
            'address2' => 'nullable',
            'city' => 'required|string',
            'country' => 'required|string',
            'phone' => 'required',
            'fax' => 'required',
            'email' => 'required|email|string',
            'site_url' => 'nullable'
        ];
    }
}
