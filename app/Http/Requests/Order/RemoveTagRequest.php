<?php


namespace App\Http\Requests\Order;


use Illuminate\Foundation\Http\FormRequest;

class RemoveTagRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tag' => 'required|array|min:1',
            'tag.*' => 'required|distinct|exists:product_stock_inventories,tag'
        ];
    }
}
