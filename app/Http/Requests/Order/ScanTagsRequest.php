<?php


namespace App\Http\Requests\Order;


use Illuminate\Foundation\Http\FormRequest;

class ScanTagsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tags' => 'required|array|min:1',
            'tags.*' => 'required|distinct|exists:product_stock_inventories,tag'
        ];
    }
}
