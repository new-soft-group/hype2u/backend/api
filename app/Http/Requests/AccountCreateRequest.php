<?php

namespace App\Http\Requests;

use App\Rules\Decimal;
use App\Rules\Discount;
use App\Rules\DiscountMethod;
use App\Rules\DiscountType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Arr;

class AccountCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $discountCode = $this->request->get('affiliate', []);
    
        $discountType = Arr::get($discountCode, 'discount_code.discount_type_id', 0);
        
        return [
            'first_name' => 'required|string|max:255',
            'last_name' => 'nullable|string|max:255',
            'password' => 'required|string|min:8|confirmed',
            'mobile' => 'required|string|max:20',
            'role_id' => 'required|integer|exists:roles,id',
            'affiliate.parent_id' => 'nullable|integer|exists:affiliates,id',
            'affiliate.commission' => ['nullable', 'bail', 'numeric', 'min:0', new Decimal()],
            'affiliate.discount_code.discount_type_id' => [
                'bail',
                'required_with:affiliate.discount_code.discount',
                new DiscountType()
            ],
            'affiliate.discount_code.discount' => [
                'bail',
                'required_with:affiliate.discount_code.discount_type_id',
                'numeric',
                'min:0',
                new Decimal(),
                new Discount($discountType)
            ],
            'affiliate.discount_code.discount_method_id' => [
                'bail',
                'required_with:affiliate.discount_code.discount',
                new DiscountMethod()
            ]
        ];
    }
}
