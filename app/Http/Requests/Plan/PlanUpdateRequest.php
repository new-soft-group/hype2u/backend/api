<?php

namespace App\Http\Requests\Plan;

use App\Models\PlanPolicy;
use App\Rules\Decimal;
use Illuminate\Foundation\Http\FormRequest;

class PlanUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'description' => 'nullable|string',
            'info' => 'nullable|string',
            'purchase_amount' => ['nullable', new Decimal()],
            'amount' => ['nullable', new Decimal()],
            'exchange_limit' => 'nullable|integer',
            'sort' => 'nullable|integer|min:0',
            'enabled' => 'nullable|boolean',
            'featured' => 'nullable|boolean',
            'sms' => 'nullable|boolean',
            'policies.*.quantity' => 'required|integer|max:100',
            'policies.*.logical' => 'required|string|in:'.PlanPolicy::LOGICAL_OR.','.PlanPolicy::LOGICAL_AND.','.PlanPolicy::LOGICAL_NONE,
            'policies.*.rules' => 'required|array|min:1',
            'policies.*.rules.*.id' => 'required|exists:product_types',
            'policies.*.rules.*.prime' => 'required|boolean',
            'surcharges' => 'nullable|array',
            'surcharges.*.threshold' => 'required_with:surcharges|min:0',
            'surcharges.*.charge' => 'required_with:surcharges|min:0'
        ];
    }
}
