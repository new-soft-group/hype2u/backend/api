<?php

namespace App\Http\Requests\Plan;

use App\Models\Plan;
use App\Models\PlanPolicy;
use App\Rules\Decimal;
use Illuminate\Foundation\Http\FormRequest;

class PlanCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $amount = $this->request->get('amount');

        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string',
            'info' => 'required|string',
            'purchase_amount' => ['required', new Decimal()],
            'amount' => ['required', new Decimal()],
            'interval' => 'required|in:'.Plan::DAY_INTERVAL.','.Plan::WEEK_INTERVAL.','.Plan::MONTH_INTERVAL.','.Plan::YEAR_INTERVAL,
            'exchange_limit' => 'required|integer',
            'sort' => 'nullable|integer|min:0',
            'enabled' => 'nullable|boolean',
            'featured' => 'nullable|boolean',
            'sms' => 'nullable|boolean',
            'policies' => 'required|array',
            'policies.*.quantity' => 'required|integer|max:100',
            'policies.*.logical' => 'required|string|in:'.PlanPolicy::LOGICAL_OR.','.PlanPolicy::LOGICAL_AND.','.PlanPolicy::LOGICAL_NONE,
            'policies.*.rules' => 'required|array|min:1',
            'policies.*.rules.*.id' => 'required|exists:product_types',
            'policies.*.rules.*.prime' => 'required|boolean',
            'surcharges' => 'nullable|array',
            'surcharges.*.threshold' => [
                'required',
                function ($attribute, $value, $fail) use ($amount) {
                    if ($amount >= $value) {
                        $fail(__('plan.threshold.invalid'));
                    }
                },
            ],
            'surcharges.*.charge' => 'required|min:0'
        ];
    }
}
