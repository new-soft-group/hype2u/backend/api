<?php

namespace App\Http\Requests;

use App\Rules\AllowedCurrencies;
use Illuminate\Foundation\Http\FormRequest;

class ShopCheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'shipping_address' => 'nullable',
            'shipping_address.first_name' => 'required_with:shipping_address|string|max:255',
            'shipping_address.last_name' => 'required_with:shipping_address|string|max:255',
            'shipping_address.mobile' => 'required_with:shipping_address|string|max:20',
            'shipping_address.company' => 'nullable|string|max:255',
            'shipping_address.address_1' => 'required_with:shipping_address|string|max:255',
            'shipping_address.address_2' => 'nullable|string|max:255',
            'shipping_address.city' => 'required_with:shipping_address|string|max:20',
            'shipping_address.state' => 'required_with:shipping_address|string|max:20',
            'shipping_address.country' => 'required_with:shipping_address|string|max:20',
            'shipping_address.postcode' => 'required_with:shipping_address|string|max:10',
            'currency' => ['required', new AllowedCurrencies()],
            'payment_method' => 'nullable|array'
        ];
    }
}
