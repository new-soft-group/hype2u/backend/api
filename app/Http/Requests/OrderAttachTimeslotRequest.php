<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderAttachTimeslotRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'timeslot' => 'required_with:timeslot_date|exists:timeslots,id',
            'timeslot_date' => 'required_with:timeslot|date_format:Y-m-d|after:today'
        ];
    }
}
