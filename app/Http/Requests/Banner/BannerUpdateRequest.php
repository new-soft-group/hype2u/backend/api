<?php

namespace App\Http\Requests\Banner;

use App\Models\Banner;
use Illuminate\Foundation\Http\FormRequest;

class BannerUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'banners' => 'required|array|min:1',
            'banners.*.id' => 'required|exists:banners,id',
            'banners.*.viewport' => 'nullable|in:'.Banner::DESKTOP_VIEWPORT.','.Banner::MOBILE_VIEWPORT,
            'banners.*.banner_position_id' => 'nullable|integer|exists:banner_positions,id',
            'banners.*.sort' => 'nullable|integer',
        ];
    }
}
