<?php

namespace App\Http\Requests\Banner;

use App\Models\Banner;
use Illuminate\Foundation\Http\FormRequest;

class BannerCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'banners' => 'required|array|min:1',
            'banners.*.viewport' => 'required|in:'.Banner::DESKTOP_VIEWPORT.','.Banner::MOBILE_VIEWPORT,
            'banners.*.banner_position_id' => 'required|integer|exists:banner_positions,id',
            'banners.*.image.file' => 'required|file|image',
            'banners.*.image.sort' => 'required|integer|min:0',
        ];
    }
}
