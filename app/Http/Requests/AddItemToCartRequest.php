<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddItemToCartRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'product_id' => 'required|integer|exists:products,id',
            'product_stock_id' => 'nullable|integer|exists:product_stocks,id',
            'product_type_id' => 'required|integer|exists:product_types,id',
            'quantity' => 'required|integer|min:1',
        ];
    }
}
