<?php

namespace App\Http\Requests;

use App\Rules\AllowedLanguage;
use App\Rules\Decimal;
use Illuminate\Foundation\Http\FormRequest;

class WarehouseCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'country' => 'required|string',
            'name' => 'required|string',
            'slug' => 'required',
            'address1' => 'required|string',
            'address2' => 'nullable|string',
            'city' => 'required',
            'phone' => 'required',
            'fax' => 'required',
            'default' => 'nullable',
            'enabled' => 'nullable|boolean'
        ];
    }
}
