<?php

namespace App\Http\Requests;

use App\Rules\Decimal;
use App\Rules\Discount;
use App\Rules\DiscountMethod;
use App\Rules\DiscountType;
use App\Rules\UniqueDiscountCode;
use Illuminate\Foundation\Http\FormRequest;

class PromotionCodeUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable|string|max:255',
            'code' => ['nullable', 'string', new UniqueDiscountCode()],
            'discount' => [
                'bail',
                'required_with:discount_type_id',
                'numeric',
                'min:0',
                new Decimal(),
                new Discount($this->request->get('discount_type_id', 0))
            ],
            'discount_type_id' => [
                'required_with:discount',
                new DiscountType()
            ],
            'discount_method_id' => [
                'required_with:discount',
                new DiscountMethod()
            ],
            'expired_at' => 'nullable|date'
        ];
    }
}
