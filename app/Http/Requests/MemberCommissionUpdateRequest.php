<?php

namespace App\Http\Requests;

use App\Rules\Decimal;
use App\Rules\MinMaxCommission;
use Illuminate\Foundation\Http\FormRequest;

class MemberCommissionUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    public function rules()
    {
        return [
            'commission' => ['nullable', 'bail', 'numeric', 'min:0', new Decimal(), app()->make(MinMaxCommission::class)],
        ];
    }
}
