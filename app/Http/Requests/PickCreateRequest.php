<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class

PickCreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'gender_id' => 'required|exists:genders,id',
            'sizes' => 'required|array|min:1|max:2',
            'styles' => 'required|array|min:1',
            'fits' => 'required|array|min:1',
            'brands' => 'required|array|min:1|max:5',
            'colors' => 'required|array|min:1',
        ];
    }
}
