<?php

namespace App\Http\Requests;

use App\Models\Product;
use App\Rules\AllowedCurrencies;
use App\Rules\AllowedPaymentMethod;
use Illuminate\Foundation\Http\FormRequest;

class GuestCheckoutRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'guest' => 'required',
            'guest.email' => 'required|string|email|max:255',
            'guest.profile' => 'required',
            'guest.profile.first_name' => 'required|string|max:255',
            'guest.profile.last_name' => 'required|string|max:255',
            'guest.profile.mobile' => 'required|string|max:20',
            'shipping_address' => 'required',
            'shipping_address.first_name' => 'required|string|max:255',
            'shipping_address.last_name' => 'required|string|max:255',
            'shipping_address.mobile' => 'required|string|max:20',
            'shipping_address.company' => 'nullable|string|max:255',
            'shipping_address.address_1' => 'required|string|max:255',
            'shipping_address.address_2' => 'required|string|max:255',
            'shipping_address.city' => 'required|string|max:20',
            'shipping_address.state' => 'required|string|max:20',
            'shipping_address.country' => 'required|string|max:20',
            'shipping_address.postcode' => 'required|string|max:10',
            'currency' => ['required', new AllowedCurrencies()],
            'method' => ['required_with:currency', new AllowedPaymentMethod($this->request->get('currency'))],
            'code' => 'nullable|string'
        ];
    }
}
