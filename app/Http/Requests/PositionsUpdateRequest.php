<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PositionsUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'positions' => 'required|array|min:1',
            'positions.*.id' => 'required|integer|exists:banner_positions,id',
            'positions.*.name' => 'required|string|max:255',
        ];
    }
}
