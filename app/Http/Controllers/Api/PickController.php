<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\PickCreateRequest;
use App\Http\Requests\PickUpdateRequest;
use App\Http\Resources\PickCollection;
use App\Http\Resources\PickResource;
use App\Http\Resources\PickStatusResource;
use App\Models\Pick;
use App\Models\PickStatus;
use App\Models\User;
use Datatable;
use App\Services\PickService;
use Illuminate\Http\Request;

class PickController extends Controller
{
    /**
     * @var PickService
     */
    private $pickService;

    public function __construct(PickService $pickService)
    {
        $this->pickService = $pickService;
    }

    public function index(Request $request)
    {
        $picks = Datatable::setRequest($request)
                            ->setModel(new Pick())
                            ->setDefaultSort()
                            ->get();

        return new PickCollection($picks);
    }

    public function create(PickCreateRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $pick = $this->pickService->create($user, $request->all());

        return response()->json(new PickResource($pick));
    }

    public function update(Pick $pick, PickUpdateRequest $request)
    {
        $this->pickService->update($pick, $request->all());

        $pick->load(['user', 'gender']);

        return response()->json(new PickResource($pick));
    }

    public function show(Pick $pick)
    {
        $pick->load('gender', 'user');

        return response()->json(new PickResource($pick));
    }

    public function attended(Pick $pick)
    {
        $pick->setAttended();

        return response()->json(new PickResource($pick));
    }

    public function pending(Pick $pick)
    {
        $pick->setPending();

        return response()->json(new PickResource($pick));
    }

    public function reject(Pick $pick)
    {
        $pick->setRejected();

        return response()->json(new PickResource($pick));
    }

    public function close(Pick $pick)
    {
        $pick->setClose();

        return response()->json(new PickResource($pick));
    }

    public function status()
    {
        /** @var User $user */
        $user = auth()->user();

        $status = $this->pickService->validatePickRequest($user);

        return response()->json(["status"=> $status]);
    }

    public function pendingPickCount()
    {
        $picks = Pick::where('status', PickStatus::PENDING)->count();

        return response()->json(['pending_pick_count' => $picks]);
    }
}
