<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\OrderResource;
use App\Models\Order;
use App\Models\User;

class GuestController extends Controller
{
    /**
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function showOrder(Order $order)
    {
        $this->authorizeForUser(auth()->user() ?? new User(),'guest', $order);
    
        $order->loadMissing('shippingAddress', 'items.product.images', 'payment');
        
        return response()->json(new OrderResource($order));
    }
}
