<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\RolePermissionUpdateRequest;
use App\Http\Resources\RoleResource;
use App\Services\RolePermissionService;

class RoleController extends Controller
{
    /**
     * @var RolePermissionService
     */
    private $rolePermissionService;

    /**
     * RoleController constructor.
     * @param RolePermissionService $rolePermissionService
     */
    public function __construct(RolePermissionService $rolePermissionService)
    {
        $this->rolePermissionService = $rolePermissionService;
    }

    public function roles()
    {
        return response()->json(RoleResource::collection($this->rolePermissionService->getRoles()));
    }

    public function permissions()
    {
        $all = $this->rolePermissionService->getAllRoleWithPermissions();

        return response()->json(RoleResource::collection($all));
    }

    public function update(RolePermissionUpdateRequest $request)
    {
        $this->rolePermissionService->updateRolePermissions($request->get('role_permissions'));

        $all = $this->rolePermissionService->getAllRoleWithPermissions();

        return response()->json(RoleResource::collection($all));
    }
}
