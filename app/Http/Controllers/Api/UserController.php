<?php

namespace App\Http\Controllers\Api;

use App\Criteria\WhereInvoiceBelongsToUser;
use App\Criteria\WhereUser;
use App\Events\UserPasswordUpdatedEvent;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddressCreateRequest;
use App\Http\Requests\AddressUpdateRequest;
use App\Http\Requests\UserProfileUpdateRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Http\Resources\AddressResource;
use App\Http\Resources\Invoice\InvoiceResource;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderResource;
use App\Http\Resources\SubscriptionResource;
use App\Http\Resources\UserOrderCollection;
use App\Http\Resources\UserResource;
use App\Models\Address;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\User;
use App\Services\UserService;
use Datatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * RoleController constructor.
     * @param UserService $userService
     */
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function getProfile()
    {
        /** @var User $user */
        $user = auth()->user();

        $user->load('profile');

        return response()->json(new UserResource($user));
    }

    public function updateProfile(UserProfileUpdateRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $result = $user->profile->update($request->all());

        return response()->json(['result' => $result]);
    }

    public function getAddresses()
    {
        /** @var User $user */
        $user = auth()->user();

        return AddressResource::collection($user->profile->addresses)
                              ->additional(['meta' => [
                                  'default_shipping' => $user->profile->shipping_address_id,
                                  'default_billing' => $user->profile->billing_address_id
                              ]]);
    }

    public function createAddress(AddressCreateRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $address = $user->newAddress($request->all());

        return response()->json(new AddressResource($address));
    }

    /**
     * @param AddressUpdateRequest $request
     * @param Address $address
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function updateAddress(AddressUpdateRequest $request, Address $address)
    {
        $this->authorize('update', $address);

        /** @var User $user */
        $user = auth()->user();

        $user->updateAddress($address, $request->all());

        return response()->json(new AddressResource($address));
    }

    /**
     * @param Address $address
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function deleteAddress(Address $address)
    {
        $this->authorize('delete', $address);

        /** @var User $user */
        $user = auth()->user();

        $result = $user->deleteAddress($address);

        return response()->json(['result' => $result]);
    }

    /**
     * @param Address $address
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function setShippingAddress(Address $address)
    {
        $this->authorize('update', $address);

        /** @var User $user */
        $user = auth()->user();

        $result = $user->updateShippingAddress($address);

        return response()->json(['result' => $result]);
    }

    /**
     * @param Address $address
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function setBillingAddress(Address $address)
    {
        $this->authorize('update', $address);

        /** @var User $user */
        $user = auth()->user();

        $result = $user->updateBillingAddress($address);

        return response()->json(['result' => $result]);
    }

    public function updatePassword(ChangePasswordRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $result = $user->changePassword($request->get('password'));

        event(new UserPasswordUpdatedEvent($user));

        return response()->json(['result' => $result]);
    }

    public function orders(Request $request)
    {
        /** @var User $user */
        $user = $request->user();

        $orders = Datatable::setRequest($request)
                           ->setModel(new Order())
                           ->setCriteria(new WhereUser($user))
                           ->setDefaultSort()
                           ->get();

        return new OrderCollection($orders);
    }

    /**
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function showOrder(Order $order)
    {
        $this->authorize('view', $order);

        $order->loadMissing('shippingAddress', 'items.product.images', 'invoice');

        return response()->json(new OrderResource($order));
    }

    public function subscriptions()
    {
        /** @var User $user */
        $user = auth()->user();

        $subscribed = $user->defaultSubscription();

        return response()->json($subscribed ? new SubscriptionResource($subscribed) : null);
    }

    public function cardSetup()
    {
        /** @var User $user */
        $user = auth()->user();

        return response()->json(['secret' => $user->createSetupIntent()]);
    }

    public function invoices(Request $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $invoices = Datatable::setRequest($request)
                             ->setModel(new Invoice())
                             ->setCriteria(new WhereInvoiceBelongsToUser($user))
                             ->setDefaultSort()
                             ->get();

        return InvoiceResource::collection($invoices);
    }
}
