<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductTypeCreateRequest;
use App\Http\Requests\Product\ProductTypeUpdateRequest;
use App\Models\ProductType;
use App\Http\Resources\Product\ProductTypeResource;
use Illuminate\Http\Request;

class ProductTypeController extends Controller
{
    public function index(Request $request)
    {
        $businessType = $request->query('business_type_id');

        $types = ProductType::withCount('products')->where('enabled',true)->get();

        if($businessType) $types = ProductType::withCount('products')->where('enabled',true)->where('business_type_id', $businessType)->get();

        return response()->json(ProductTypeResource::collection($types));
    }

    public function all()
    {
        $types = ProductType::withCount('products')->get();

        $types->load('businessType');

        return response()->json(ProductTypeResource::collection($types));
    }

    public function create(ProductTypeCreateRequest $request)
    {
        $type = ProductType::create($request->all());

        $type->load('businessType');

        return response()->json(new ProductTypeResource($type));
    }

    public function update(ProductTypeUpdateRequest $request)
    {
        $types = ProductType::massUpdate($request->get('types'));

        $types->loadCount('products');

        $types->load('businessType');

        return response()->json(ProductTypeResource::collection($types));
    }

    /**
     * @param ProductType $type
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(ProductType $type)
    {
        $result = $type->safeDelete();

        return response()->json(['result' => $result]);
    }
}
