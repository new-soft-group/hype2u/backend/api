<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\WarehouseCreateRequest;
use App\Http\Requests\WarehouseUpdateRequest;
use App\Http\Resources\WarehouseInventoryResource;
use App\Models\ProductStockInventory;
use App\Services\WarehouseService;
use Illuminate\Http\Request;
use App\Http\Resources\WarehouseCollection;
use App\Http\Resources\WarehouseResource;
use App\Models\Warehouse;
use Datatable;

class WarehouseController extends Controller
{
    /**
     * @var WarehouseService
     */
    private $warehouseService;

    public function __construct(WarehouseService $warehouseService)
    {
        $this->warehouseService = $warehouseService;
    }

    public function show(Warehouse $warehouse)
    {
        return response()->json(new WarehouseResource($warehouse));
    }

    public function index(Request $request)
    {
        $warehouse = Datatable::setRequest($request)
                              ->setModel(new Warehouse())
                              ->setDefaultSort()
                              ->get();

        return new WarehouseCollection($warehouse);
    }

    /**
     * @param WarehouseCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(WarehouseCreateRequest $request)
    {
        $warehouse = $this->warehouseService->create($request->all());

        return response()->json(new WarehouseResource($warehouse));
    }

    /**
     * @param WarehouseUpdateRequest $request
     * @param Warehouse $warehouse
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(WarehouseUpdateRequest $request, Warehouse $warehouse)
    {
        $this->warehouseService->update($warehouse, $request->all());

        return response()->json(new WarehouseResource($warehouse));
    }

    /**
     * @param Warehouse $warehouse
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Warehouse $warehouse)
    {
        $result = $warehouse->safeDelete();

        return response()->json(['result' => $result]);
    }
}
