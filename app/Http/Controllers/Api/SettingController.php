<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\SettingUpdateRequest;
use App\Http\Resources\SettingResource;
use App\Models\Setting;
use App\Services\SettingService;

class SettingController extends Controller
{
    /**
     * @var SettingService
     */
    private $settingService;

    public function __construct(SettingService $settingService)
    {
        $this->settingService = $settingService;
    }

    public function index()
    {
        $settings = Setting::all();

        return response()->json(SettingResource::collection($settings));
    }

    public function update(SettingUpdateRequest $request)
    {
        $result = $this->settingService->update($request->get('settings'));

        return response()->json($result);
    }
}
