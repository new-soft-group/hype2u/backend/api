<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ChartRequest;
use App\Http\Resources\TimeSeriesChartResource;
use App\Http\Resources\MemberSalesChartResource;
use App\Models\Order;
use App\Models\Transaction;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class ChartController extends Controller
{
    public function overall()
    {
        return response()->json([
            'sales' => Transaction::sum('amount'),
            'orders' => Order::count(),
            'transactions' => Transaction::count(),
        ]);
    }
    
    public function affiliateOverall(Request $request)
    {
        /** @var User $user */
        $user = $request->user()->load('affiliate.discountCode');
        
        return response()->json([
            'referral_code' => $user->affiliate->referral_code,
            'discount_code' => [
                'discount' => $user->affiliate->discountCode->discount,
                'discount_type' => $user->affiliate->discountCode->discount_type
            ],
            'commission' => $user->affiliate->commission,
            'sales' => $user->affiliate->sales()->sum('commission'),
            'total_member' => $user->affiliate->children()->count(),
        ]);
    }
    
    public function totalOrders(ChartRequest $request)
    {
        $orders = Order::chart($request->get('from'), $request->get('to'));
        
        return TimeSeriesChartResource::collection($orders);
    }
    
    public function totalSales(ChartRequest $request)
    {
        $sales = Transaction::chart($request->get('from'), $request->get('to'));
        
        return TimeSeriesChartResource::collection($sales);
    }
    
    public function affiliateSales(Request $request)
    {
        /** @var User $user */
        $user = $request->user()->load('affiliate');
        
        $date = $request->get('date');
        
        $filterDate = $date ? Carbon::parse($date) : Carbon::now();
        
        return TimeSeriesChartResource::collection($user->affiliate->totalSales($filterDate));
    }
    
    public function affiliateMemberSales(Request $request)
    {
        /** @var User $user */
        $user = $request->user()->load('affiliate');
        
        return MemberSalesChartResource::collection($user->affiliate->totalMemberSales());
    }
}
