<?php

namespace App\Http\Controllers\Api;

use App\Events\ReturnCompletedEvent;
use App\Http\Requests\Order\OrderCreateRequest;
use App\Http\Requests\Order\RemoveTagRequest;
use App\Http\Requests\Order\ReturnTagsRequest;
use App\Http\Requests\Order\ScanTagsRequest;
use App\Http\Requests\OrderAttachTimeslotRequest;
use App\Http\Requests\OrderRejectRequest;
use App\Http\Resources\OrderCountResource;
use App\Http\Resources\OrderResource;
use App\Http\Resources\Product\ProductStockInventoryResource;
use App\Models\BusinessType;
use App\Models\CartItem;
use App\Models\User;
use App\Services\OrderService;
use Datatable;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\OrderStatusResource;
use App\Models\Order;
use App\Models\OrderStatus;
use App\Options\OrderOptions as Options;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    /**
     * @var OrderService
     */
    private $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function index(Request $request)
    {
        $orders = Datatable::setRequest($request)
                           ->setModel(new Order())
                           ->setDefaultSort()
                           ->get();

        return new OrderCollection($orders);
    }

    public function status()
    {
        return response()->json(OrderStatusResource::collection(OrderStatus::all()));
    }

    public function show(Order $order)
    {
        $order->load('shippingAddress', 'invoice', 'user.profile', 'tracks', 'timeslots','businessType');
        $order->loadMissing('items.product.image');

        return response()->json(new OrderResource($order));
    }

    /**
     * @param OrderCreateRequest $request
     * @param User $user
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\OutOfStockException
     * @throws \App\Exceptions\InsufficientStockException
     * @throws \App\Exceptions\ShippingAddressNotFound
     * @throws \App\Exceptions\DuplicateTimeslotException
     */
    public function create(OrderCreateRequest $request, User $user)
    {
        $items = collect($request->get('items'))->mapInto(CartItem::class);

        $option = new Options($request->only(['currency', 'shipping_address']));

        $order = $this->orderService->createOnBehalf($user, $items, $option, Order::ORDER_CUSTOM);

        return response()->json(new OrderResource($order));
    }

    /**
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function delete(Order $order)
    {
        $this->authorize('delete', $order);

        $order->safeDelete();

        return response()->json(['result' => true]);
    }

    /**
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\InsufficientStockException|\App\Exceptions\StockNotFoundException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function approve(Order $order)
    {
        $this->authorize('approve', $order);

        $order->approve();

        return response()->json(new OrderResource($order));
    }

    /**
     * @param ScanTagsRequest $request
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\InsufficientStockException|\App\Exceptions\StockNotFoundException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function ship(Order $order)
    {
        $this->authorize('ship', $order);

        $order->ship();

        return response()->json(new OrderResource($order));
    }

    /**
     * @param ScanTagsRequest $request
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\InsufficientStockException|\App\Exceptions\StockNotFoundException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function scanReservedTags(ScanTagsRequest $request, Order $order)
    {
        $this->authorize('reserveTags', $order);

        $inventories = $order->scanReservedTags($request->get('tags'));

        return response()->json(ProductStockInventoryResource::collection($inventories));
    }

    /**
     * @param RemoveTagRequest $request
     * @param Order $order
     * @throws \App\Exceptions\InsufficientStockException|\App\Exceptions\StockNotFoundException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function removeScannedReservedTag(RemoveTagRequest $request, Order $order)
    {
        $this->authorize('removeReserveTag', $order);

        $removed = $order->removeScannedReservedTag($request->get('tag'));

        return $removed ? true : false;
    }

    /**
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\InsufficientStockException|\App\Exceptions\StockNotFoundException
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function deliver(Order $order)
    {
        $this->authorize('deliver', $order);

        $order->deliver();

        return response()->json(new OrderResource($order));
    }

    /**
     * @param OrderRejectRequest $request
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function reject(Order $order)
    {
        $this->authorize('reject', $order);

        $order->reject();

        if($order->business_type_id == BusinessType::SHOP) $order->refund();

        $order->load('shippingAddress', 'invoice', 'user.profile','status','tracks','businessType');
        $order->loadMissing('items.product.image');

        return response()->json(new OrderResource($order));
    }

    /**
     * @param ScanTagsRequest $request
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \App\Exceptions\TagNotMatchException
     * @throws \App\Exceptions\RequiredNumberOfTagException
     */
    public function returnBack(ReturnTagsRequest $request, Order $order)
    {
        $this->authorize('returnBack', $order);

        $order->returnBack($request->get('tags'));

        $order->load('shippingAddress', 'invoice', 'user.profile', 'tracks');
        $order->loadMissing('items.product.image');

        event(new ReturnCompletedEvent($order));

        // Close pick for me request if any
        $this->orderService->closePickRequest($order);

        return response()->json(new OrderResource($order));
    }

    /**
     * @param ScanTagsRequest $request
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \App\Exceptions\TagNotMatchException
     * @throws \App\Exceptions\RequiredNumberOfTagException
     */
    public function returnBackPartial(ReturnTagsRequest $request, Order $order)
    {
        $this->authorize('returnBack', $order);

        $order->returnBackPartial($request->get('tags'));

        $order->load('shippingAddress', 'invoice', 'user.profile', 'tracks');
        $order->loadMissing('items.product.image');

        event(new ReturnCompletedEvent($order));

        // Close pick for me request if any
        $this->orderService->closePickRequest($order);

        return response()->json(new OrderResource($order));
    }

    public function pendingOrderCount()
    {
        $orders = Order::where('order_status_id', OrderStatus::PENDING)->count();

        return response()->json(['pending_order_count' => $orders]);
    }

    /**
     * @param OrderAttachTimeslotRequest $request
     * @param Order $order
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\DuplicateTimeslotException
     */
    public function attachTimeslot(OrderAttachTimeslotRequest $request, Order $order)
    {
        $option = new Options($request->only([
            'timeslot',
            'timeslot_date'
        ]));

        $this->orderService->attachTimeslot($option, $order);

        return response()->json(new OrderResource($order));
    }
}
