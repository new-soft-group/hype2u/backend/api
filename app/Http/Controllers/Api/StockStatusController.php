<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Resources\StockStatusResource;
use App\Models\StockStatus;

class StockStatusController extends Controller
{
    public function index()
    {
        $statuses = StockStatus::all();

        return response()->json(StockStatusResource::collection($statuses));
    }
}
