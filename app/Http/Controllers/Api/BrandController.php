<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\BrandCreateRequest;
use App\Http\Requests\BrandUpdateRequest;
use App\Http\Resources\BrandResource;
use App\Models\Brand;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::withCount('products')->where('enabled', true)->get();

        return response()->json(BrandResource::collection($brands));
    }

    public function all()
    {
        $brands = Brand::withCount('products')->get();

        return response()->json(BrandResource::collection($brands));
    }

    public function create(BrandCreateRequest $request)
    {
        $brand = Brand::create($request->all());

        return response()->json(new BrandResource($brand));
    }

    public function update(BrandUpdateRequest $request)
    {
        $brands = Brand::massUpdate($request->get('brands'));

        $brands->loadCount('products');

        return response()->json(BrandResource::collection($brands));
    }

    /**
     * @param Brand $brand
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Brand $brand)
    {
        $result = $brand->safeDelete();

        return response()->json(['result' => $result]);
    }
}
