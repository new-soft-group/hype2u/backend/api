<?php


namespace App\Http\Controllers\Api\Auth;

use App\Events\NewRegistrationEvent;
use App\Models\Role;
use App\Events\UserLoggedIn;
use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\Http\Resources\UserResource;
use App\Services\AffiliateService;
use App\Services\UserService;

class RegisterController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var AffiliateService
     */
    private $affiliateService;

    public function __construct(UserService $userService, AffiliateService $affiliateService)
    {
        $this->userService = $userService;
        $this->affiliateService = $affiliateService;
    }

    public function register(RegisterRequest $request)
    {
        $user = $this->userService->create($request->all(), Role::USER);

        $user->load('profile', 'roles');

        $token = $user->createToken('Web')->accessToken;

        event(new UserLoggedIn($request, $user));

        event(new NewRegistrationEvent($user));

        return response()->json(new UserResource($user, $token), 201);
    }
}
