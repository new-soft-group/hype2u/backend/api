<?php


namespace App\Http\Controllers\Api\Auth;


use App\Http\Requests\ForgotPasswordRequest;
use Illuminate\Http\Request;
use Password;

class ForgotPasswordController
{
    protected function broker()
    {
        return Password::broker();
    }

    protected function credentials(Request $request)
    {
        return $request->only('email');
    }

    public function sendResetLink(ForgotPasswordRequest $request)
    {
        $response = $this->broker()->sendResetLink($this->credentials($request));
        if($response == "passwords.user"){
            return response()->json(['message' => trans($response)],422);
        }
        else{
            return response()->json(['message' => trans($response)]);
        }
    }
}
