<?php


namespace App\Http\Controllers\Api\Auth;

use App\Events\UserLoggedIn;
use App\Exceptions\AuthMismatchException;
use App\Http\Controllers\Controller;
use App\Http\Requests\SocialAuthRequest;
use App\Http\Resources\UserResource;
use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;

class LoginController extends Controller
{
    /**
     * @var UserService
     */
    private $service;

    public function __construct(UserService $userService)
    {
        $this->service = $userService;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function login(Request $request)
    {
        if(auth()->attempt(['email' => $request->get('email'), 'password' => $request->get('password')])) {

            /** @var User $user */
            $user = auth()->user();

            $user->load('profile', 'roles');

            $user->tokens()->delete();

            $token = $user->createToken('Web')->accessToken;

            event(new UserLoggedIn($request, $user));

            return response()->json(new UserResource($user, $token));
        }
        else {
            return response()->json(['message' => trans('auth.failed')], 401);
        }
    }

    public function logout() {
        /** @var User $user */
        $user = auth()->user();

        $user->tokens()->delete();

        return response()->json(['message' => trans('auth.logout')]);
    }

    public function socialLogin(SocialAuthRequest $request)
    {
        try {
            $provider = $request->get('provider');

            $socialUser = Socialite::driver($provider)
                                   ->fields([
                                       'email',
                                       'name',
                                       'first_name',
                                       'last_name'
                                   ])
                                   ->userFromToken($request->get('token'));

            $user = $this->service->findOrRegister($socialUser, $provider);

            $user->load('profile', 'roles');

            $token = $user->createToken('Web')->accessToken;

            event(new UserLoggedIn($request, $user));

            return response()->json(new UserResource($user, $token));
        }
        catch (AuthMismatchException $exception) {

            return response()->json(['message' => $exception->getMessage()], 400);
        }
        catch (\Exception $exception) {

            return response()->json(['message' => trans('auth.provider.failed')], 400);
        }
    }
}
