<?php


namespace App\Http\Controllers\Api\Auth;


use App\Http\Requests\ResetPasswordRequest;
use App\Models\User;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Password;

class ResetPasswordController
{
    protected function broker()
    {
        return Password::broker();
    }
    
    protected function credentials(Request $request)
    {
        return $request->only(
            'email', 'password', 'password_confirmation', 'token'
        );
    }
    
    public function resetPassword(ResetPasswordRequest $request)
    {
        $response = $this->broker()->reset(
            $this->credentials($request),
            function (User $user, $password) {
                $user->resetPassword($password);
            });
        
        return response()->json(['message' => trans($response)]);
    }
}
