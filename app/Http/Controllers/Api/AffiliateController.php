<?php

namespace App\Http\Controllers\Api;

use App\Facades\Datatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\DiscountCodeUpdateRequest;
use App\Http\Requests\MarkPayoutRequest;
use App\Http\Requests\MemberCommissionUpdateRequest;
use App\Http\Requests\ReferralCodeUpdateRequest;
use App\Http\Resources\AffiliateCollection;
use App\Http\Resources\AffiliateHierarchySalesResource;
use App\Http\Resources\AffiliateSalesCollection;
use App\Http\Resources\AffiliateResource;
use App\Http\Resources\PayoutCollection;
use App\Models\Affiliate;
use App\Models\AffiliateSale;
use App\Models\User;
use App\Modules\DatatableModule;
use App\Services\AffiliateService;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

class AffiliateController extends Controller
{
    /**
     * @var AffiliateService
     */
    private $affiliateService;
    
    /**
     * RoleController constructor.
     * @param AffiliateService $affiliateService
     */
    public function __construct(AffiliateService $affiliateService)
    {
        $this->affiliateService = $affiliateService;
    }
    
    public function allMembers(Request $request)
    {
        $date = $request->get('date');
        
        $filterDate = $date ? Carbon::parse($date) : Carbon::now();
        
        /** @var \Kalnoy\Nestedset\Collection $collection */
        $collection = Affiliate::with([
            'user.profile',
            'sumOfSales' => function (HasMany $hasMany) use ($filterDate) {
                $hasMany->whereMonth('created_at', $filterDate->month)->whereYear('created_at', $filterDate->year);
            }
        ])->withCount('descendants')->get();
        
        return AffiliateHierarchySalesResource::collection($collection->toTree());
    }
    
    public function index(Request $request)
    {
        $affiliates = Datatable::setRequest($request)
                               ->setModel(new Affiliate())
                               ->setRelation(['user.profile', 'discountCode'])
                               ->setSearchable([
                                   'user.profile.first_name',
                                   'user.profile.last_name',
                                   'user.profile.email',
                                   'discountCode.code'
                               ])
                               ->result();
        
        return new AffiliateCollection($affiliates);
    }
    
    public function members(Request $request)
    {
        $user = $request->user()->load('affiliate');
        
        $date = $request->get('date');
        
        $filterDate = $date ? Carbon::parse($date) : Carbon::now();
        
        /** @var \Kalnoy\Nestedset\Collection $collection */
        $collection = $user->affiliate->getDescendants()->load([
            'user',
            'sumOfSales' => function (HasMany $hasMany) use ($filterDate) {
                $hasMany->whereMonth('created_at', $filterDate->month)->whereYear('created_at', $filterDate->year);
            }
        ])->loadCount('descendants');
        
        return AffiliateHierarchySalesResource::collection($collection->toTree());
    }
    
    public function superiors(Request $request)
    {
        
        $superiors = Datatable::setRequest($request)
                              ->setModel(new Affiliate())
                              ->setRelation(['user'])
                              ->setSearchable(['user.first_name|last_name', 'user.email'])
                              ->result();
        
        return AffiliateResource::collection($superiors);
    }
    
    public function sales(Request $request)
    {
        /** @var User $user */
        $user = $request->user()->load('affiliate');
        
        $model = $user->affiliate;
        
        $date = $request->get('date');
        
        $filterDate = $date ? Carbon::parse($date) : Carbon::now();
        
        $sales = Datatable::setRequest($request)->setModel(new AffiliateSale())->setRelation([
                'by',
                'order.customer',
                'order.items.product.locales'
            ])->setSortable(['created_at'])->result(function (Builder $builder) use ($model, $filterDate) {
                return $builder->where('affiliate_id', $model->id)
                               ->whereYear('created_at', $filterDate->year)
                               ->whereMonth('created_at', $filterDate->month)
                               ->orderByDesc('created_at');
            });
        
        return new AffiliateSalesCollection($sales);
    }
    
    public function payoutIndex(Request $request)
    {
        
        $model = new AffiliateSale();
        
        $payout = Datatable::setRequest($request)
                           ->setModel($model)
                           ->setRelation(['affiliate.user'])
                           ->setFilterDate(['created_at' => DatatableModule::FILTER_YEAR_MONTH])
                           ->setFilterable(['created_at', 'paid'])
                           ->result(function (Builder $builder) use ($model) {
                               return $model->payoutQuery($builder);
                           });
        
        return new PayoutCollection($payout);
    }
    
    public function markPayout(MarkPayoutRequest $request, Affiliate $affiliate)
    {
        
        $carbon = Carbon::parse($request->get('date'));
        
        $result = $this->affiliateService->markSalesAsPaid($affiliate, $carbon, $request->get('reference_no'));
        
        return response()->json($result);
    }
    
    public function updateReferralCode(ReferralCodeUpdateRequest $request)
    {
        $user = $request->user()->load('affiliate.discountCode');
        
        if (is_null($user->affiliate)) {
            new \Exception('User doesnt have affiliate account');
        }
        
        $result = $this->affiliateService->updateReferralCode($user->affiliate, $request->get('ref_code', ''));
        
        return response()->json($result);
    }
    
    public function updateCommission(MemberCommissionUpdateRequest $request, Affiliate $member)
    {
        $result = $this->affiliateService->updateCommission($member, (float)$request->get('commission'));
        return response()->json($result);
    }
    
    public function updateDiscountCode(DiscountCodeUpdateRequest $request, Affiliate $affiliate)
    {
        $result = $this->affiliateService->updateDiscountCode($affiliate, $request->all());
        return response()->json($result);
    }
}
