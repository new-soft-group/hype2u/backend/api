<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\CouponNotBelongToPlanException;
use App\Exceptions\CouponNotValidException;
use App\Gateways\PaymentGateway;
use App\Http\Controllers\Controller;
use App\Http\Requests\Subscription\SubscriptionChangeRequest;
use App\Http\Requests\Subscription\SubscriptionRequest;
use App\Http\Resources\RevenueCollection;
use App\Http\Resources\SubscriptionCollection;
use App\Models\Coupon;
use App\Models\Subscription;
use App\Models\User;
use Datatable;
use Illuminate\Http\Request;
use Laravel\Cashier\Cashier;


class SubscriptionController extends Controller
{
    public function index(Request $request)
    {
        $subscriptions = Datatable::setRequest($request)
            ->setModel(new Subscription())
            ->setDefaultSort()
            ->get();

        $subscriptions->load('plan','user');
        return new SubscriptionCollection($subscriptions);
    }

    public function subscribe(SubscriptionRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $subscription = $user->newSubscription('default', $request->get('stripe_plan'));

        $couponCode = $request->get('coupon', null);

        if(!is_null($couponCode)) {
            $coupon = Coupon::findByCode($couponCode);

            $canApplyCoupon = $coupon->isCodeBelongsPlan($request->get('stripe_plan'));

            if(!$canApplyCoupon) throw new CouponNotBelongToPlanException();

            $subscription->withCoupon($coupon->stripe_id);
        }

        $subscription->create($request->get('payment'));

        return response()->json(['result' => true]);
    }

    public function upgrade(SubscriptionChangeRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $user->subscription('default')
             ->anchorBillingCycleOn()
             ->swap($request->get('stripe_plan'));

        return response()->json(['result' => true]);
    }

    public function downgrade(SubscriptionChangeRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $user->subscription('default')
             ->anchorBillingCycleOn()
             ->swap($request->get('stripe_plan'));

        return response()->json(['result' => true]);
    }

    public function cancel(User $user)
    {
        $subscription = $user->subscription('default');

        if(!is_null($subscription)) {
            $subscription->cancelNow();
            return response()->json(['result' => true]);
        }

        return response()->json(['result' => false]);
    }
}
