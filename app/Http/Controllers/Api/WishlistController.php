<?php

namespace App\Http\Controllers\Api;

use Datatable;
use App\Http\Controllers\Controller;
use App\Http\Resources\WishlistItemListCollection;
use App\Http\Resources\WishlistItemResource;
use App\Http\Resources\WishlistResource;
use App\Models\Product;
use App\Models\User;
use App\Models\Wishlist;
use App\Models\WishlistItem;
use Illuminate\Http\Request;


class WishlistController extends Controller
{
    public function index()
    {
        /** @var User $user */
        $user = auth()->user();

        $wishlist = $user->getWishlist();

        $wishlist->load('items.product');

        return response()->json(new WishlistResource($wishlist));
    }

    public function addItem(Product $product)
    {
        /** @var User $user */
        $user = auth()->user();

        $item = $user->getWishlist()
                     ->addItem($product)
                     ->load('product');

        return response()->json(new WishlistItemResource($item));
    }

    public function deleteItem(Product $product)
    {
        /** @var User $user */
        $user = auth()->user();

        $result = $user->getWishlist()
                       ->removeItem($product);

        return response()->json(['result' => $result]);
    }

    public function all(Request $request)
    {
        $wishlists = Datatable::setRequest($request)
                             ->setModel(new WishlistItem())
                             ->setDefaultSort()
                             ->get();

        return new WishlistItemListCollection($wishlists);
    }
}
