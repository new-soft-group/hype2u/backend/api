<?php


namespace App\Http\Controllers\Api;


use App\Criteria\WithoutGlobalScopes;
use App\Exceptions\CouponNotValidException;
use App\Facades\Datatable\Datatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Coupon\CouponCreateRequest;
use App\Http\Requests\Coupon\CouponUpdateRequest;
use App\Http\Requests\Coupon\CouponValidateRequest;
use App\Http\Resources\Coupon\CouponResource;
use App\Models\Coupon;
use App\Services\CouponService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CouponController extends Controller
{
    /**
     * @var CouponService
     */
    private $service;

    public function __construct(CouponService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        $coupons = Datatable::setRequest($request)
                            ->setModel(new Coupon())
                            ->setCriteria(new WithoutGlobalScopes())
                            ->setDefaultSort()
                            ->get();

        return CouponResource::collection($coupons);
    }

    /**
     * @param Coupon $coupon
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Coupon $coupon)
    {
        return response()->json(new CouponResource($coupon));
    }

    public function validateCoupon(CouponValidateRequest $request)
    {
        $coupon = Coupon::findByCode($request->all()['code']);

        if(!$coupon->count()) throw new CouponNotValidException();

        return response()->json(new CouponResource($coupon));
    }

    /**
     * @param CouponCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function create(CouponCreateRequest $request)
    {
        $options = $request->except([
            'name',
            'code',
            'duration',
            'amount_off',
            'percent_off',
            'currency'
        ]);

        $discount = $request->get('amount_off') ?? $request->get('percent_off');

        $coupon = $this->service->create(
            $request->get('name'),
            $request->get('code'),
            $request->get('duration'),
            $request->get('plan_ids'),
            $discount,
            $request->get('currency', ''),
            $options
        );

        return response()->json(new CouponResource($coupon));
    }

    /**
     * @param CouponUpdateRequest $request
     * @param Coupon $coupon
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(CouponUpdateRequest $request, Coupon $coupon)
    {
        $this->authorize('update', $coupon);

        $updated = $this->service->update($coupon, $request->all());

        $updated->load('plans');

        return response()->json(new CouponResource($updated));
    }

    /**
     * @param Coupon $coupon
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Coupon $coupon)
    {
        $result = $this->service->delete($coupon);

        return response()->json(['result' => $result]);
    }
}
