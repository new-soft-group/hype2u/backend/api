<?php

namespace App\Http\Controllers\Api;

use App\Exceptions\CartItemNotBelongsToUserCartException;
use App\Http\Controllers\Controller;
use App\Http\Requests\AddItemToCartRequest;
use App\Http\Requests\UpdateCartItemRequest;
use App\Http\Resources\Cart\CartItemResource;
use App\Http\Resources\Cart\CartResource;
use App\Models\BusinessType;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\User;

class CartController extends Controller
{
    public function index()
    {
        /** @var User $user */
        $user = auth()->user();

        $cart = $user->getCart();

        $cart->load('items.product.images', 'items.option')
             ->loadCount('items');

        $carryForwardItems = $user->carryForwardItems();

        if($carryForwardItems->isNotEmpty()) {
            $carryForwardItems->moveToCart($cart);
        }

        return response()->json(new CartResource($cart));
    }

    public function shopIndex()
    {
        /** @var User $user */
        $user = auth()->user();

        $cart = $user->getShopCart();

        $cart->load('items.product.images', 'items.option')
            ->loadCount('items');

        return response()->json(new CartResource($cart));
    }

    /**
     * @param AddItemToCartRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function add(AddItemToCartRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $cart = $user->getCart();

        $item = $cart->addItem($request->all(), BusinessType::RENT);

        $item->load('product.images', 'option');

        return response()->json(new CartItemResource($item));
    }

    /**
     * @param AddItemToCartRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function addShopCart(AddItemToCartRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();

        $cart = $user->getShopCart();

        $item = $cart->addItem($request->all(), BusinessType::SHOP);

        $item->load('product.images', 'option');

        return response()->json(new CartItemResource($item));
    }

    public function update(UpdateCartItemRequest $request, $id)
    {
        /** @var User $user */
        $user = auth()->user();

        $cart = $user->getCart();

        $item = $cart->updateItem($id, $request->all());

        $item->load('product.images', 'option');

        if(is_null($item)) {
            return response()->json(['message' => __('cart.item.not_exist')], 400);
        }

        return response()->json(new CartItemResource($item));
    }

    public function updateShopCart(UpdateCartItemRequest $request, $id)
    {
        /** @var User $user */
        $user = auth()->user();

        $cart = $user->getShopCart();

        $item = $cart->updateItem($id, $request->all());

        $item->load('product.images', 'option');

        if(is_null($item)) {
            return response()->json(['message' => __('cart.item.not_exist')], 400);
        }

        return response()->json(new CartItemResource($item));
    }

    public function remove(int $id)
    {
        /** @var User $user */
        $user = auth()->user();

        $cart = $user->getCart();

        $result = $cart->removeItem($id);

        return response()->json(['result' => $result]);
    }

    public function removeShopCart(int $id)
    {
        /** @var User $user */
        $user = auth()->user();

        $cart = $user->getShopCart();

        $result = $cart->removeItem($id);

        return response()->json(['result' => $result]);
    }

    public function incrementShopCartItem(CartItem $item)
    {
        /** @var User $user */
        $user = auth()->user();

        $cart = $user->getShopCart();

        if(!$cart->items()->where('id', $item->id)->count()) throw new CartItemNotBelongsToUserCartException();

        $item->incrementQuantity();

        $item->save();

        return response()->json(new CartItemResource($item));
    }

    public function decrementShopCartItem(CartItem $item)
    {
        /** @var User $user */
        $user = auth()->user();

        $cart = $user->getShopCart();

        if(!$cart->items()->where('id', $item->id)->count()) throw new CartItemNotBelongsToUserCartException();

        $item->decrementQuantity();

        $item->save();

        return response()->json(new CartItemResource($item));
    }

    public function surcharges()
    {
        /** @var User $user */
        $user = auth()->user();

        $subscription = $user->defaultSubscription();

        $totalAmount = $user->cart->totalAmount();

        if($subscription) {
            $amount = $subscription->plan->getSurchargeAmount($totalAmount);
        }

        return response()->json(['surcharges' => $amount ?? 0]);
    }
}
