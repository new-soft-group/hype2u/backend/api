<?php

namespace App\Http\Controllers\Api;

use App\Criteria\WhereBrandEnabled;
use App\Criteria\WhereEnabled;
use App\Criteria\WhereHasStockInventory;
use App\Criteria\WhereProductTypeEnabled;
use App\Models\User;
use Datatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Product\ProductCreateRequest;
use App\Http\Requests\Product\ProductDeleteStockRequest;
use App\Http\Requests\Product\ProductStockCreateRequest;
use App\Http\Requests\Product\ProductStockUpdateRequest;
use App\Http\Requests\Product\ProductRestockRequest;
use App\Http\Requests\Product\ProductTagUpdateRequest;
use App\Http\Requests\Product\ProductTagDeleteRequest;
use App\Http\Requests\Product\ProductUpdateRequest;
use App\Http\Requests\VariantCreateRequest;
use App\Http\Requests\VariantUpdateRequest;
use App\Http\Resources\Product\ProductCollection;
use App\Http\Resources\Product\ProductFeaturedCollection;
use App\Http\Resources\Product\ProductResource;
use App\Http\Resources\Product\ProductStockResource;
use App\Http\Resources\Product\ProductStockInventoryResource;
use App\Http\Resources\VariantOptionResource;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\Variant;
use App\Models\VariantOption;
use App\Services\ProductService;
use App\Services\WarehouseItemsService;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use App\Exceptions\ModelHasAssociate;
use Intervention\Image\Facades\Image;


class ProductController extends Controller
{
    /**
     * @var ProductService
     */
    private $productService;

    /**
     * @var WarehouseItemsService
     */
    private $warehouseItemsService;

    public function __construct(ProductService $productService, WarehouseItemsService $warehouseItemsService)
    {
        $this->productService = $productService;
        $this->warehouseItemsService = $warehouseItemsService;
    }

    public function show(Product $product)
    {
        $product->loadCount('inventories')
                ->load('images', 'categories', 'variants', 'stocks');

        return response()->json(new ProductResource($product));
    }

    public function index(Request $request)
    {
        $products = Datatable::setRequest($request)
                             ->setModel(new Product())
                             ->setCriteria(new WhereBrandEnabled(), new WhereProductTypeEnabled(), new WhereEnabled(true), new WhereHasStockInventory($request))
                             ->setDefaultSort()
                             ->get();

        return new ProductCollection($products);
    }

    public function all(Request $request)
    {
        $products = Datatable::setRequest($request)
            ->setModel(new Product())
            ->setDefaultSort()
            ->get();

        return new ProductCollection($products);
    }

    /**
     * @param ProductCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(ProductCreateRequest $request)
    {
        $payload = $request->all();

        $images = Arr::pull($payload, 'images');

        $product = $this->productService->create($payload, $images);

        $product->load('brand','type', 'gender');

        return response()->json(new ProductResource($product));
    }

    /**
     * @param ProductUpdateRequest $request
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        $product->load(['categories', 'images']);

        $this->productService->update(
            $product,
            $request->except(['images', 'deleted_images']),
            $request->file('images') ?? [],
            $request->get('uploaded_images', []),
            $request->get('deleted_images', []));

        $product->load('images', 'variants');

        return response()->json(new ProductResource($product));
    }

    public function getVariants()
    {
        $options = [
            'options' => function (HasMany $builder) {
                $builder->withCount('products');
            }
        ];

        $variants = Variant::with($options)
                           ->withCount('products')
                           ->get();

        return response()->json($variants);
    }

    public function createVariant(VariantCreateRequest $request)
    {
        $variant = Variant::create($request->all());

        $options = [
            'options' => function (HasMany $builder) {
                $builder->withCount('products');
            }
        ];

        $variant->load($options)
                ->loadCount('products')
                ->setAttribute('removable', true);

        return response()->json($variant);
    }

    public function updateVariant(VariantUpdateRequest $request, Variant $variant)
    {
        $options = [
            'options' => function (HasMany $builder) {
                $builder->withCount('products');
            }
        ];

        $updated = $variant->updateVariant($request->all());

        $updated->load($options)->loadCount('products');

        return response()->json($updated);
    }

    /**
     * @param Variant $variant
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteVariant(Variant $variant)
    {
        $result = $variant->safeDelete();

        return response()->json(['result' => $result]);
    }

    public function getVariantCombinationOptions(Product $product)
    {
        $combinations = $product->availableVariantCombination();

        return response()->json($combinations);
    }

    public function getStocks(Product $product)
    {
        $stocks = $product->stocks()->get();

        return response()->json(ProductStockResource::collection($stocks));
    }

    public function createStock(ProductStockCreateRequest $request, Product $product)
    {
        $stock = $this->productService->createStock($product, $request->all());

        return response()->json(new ProductStockResource($stock));
    }

    public function updateStock(ProductStockUpdateRequest $request, ProductStock $stock)
    {
        $this->productService->updateStocks($stock, $request->all());

        $stock->refresh();

        return response()->json(new ProductStockResource($stock));
    }

    /**
     * @param ProductDeleteStockRequest $request
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws ModelHasAssociate
     */
    public function deleteStocks(ProductDeleteStockRequest $request, Product $product)
    {
        $result = $this->productService->deleteStocks($product, $request->get('ids'));

        return response()->json(['result' => $result]);
    }

    public function getTags(ProductStock $stock)
    {
        $tags = $stock->inventories()->get();

        $tags->load('status', 'warehouse', 'shippedOutTracks');

        return response()->json(ProductStockInventoryResource::collection($tags));
    }

    public function restock(ProductRestockRequest $request, ProductStock $stock)
    {
        $tags = $stock->restocks(
            $request->get('tags'),
            $request->get('warehouse_id'),
            $request->get('stock_status_id'));

        $tags->load('status', 'warehouse','shippedOutTracks');

        return response()->json(ProductStockInventoryResource::collection($tags));
    }

    public function updateTag(ProductTagUpdateRequest $request, ProductStockInventory $tag)
    {
        $tag->fill($request->all())->save();

        $tag->load('status', 'warehouse' , 'shippedOutTracks');

        return response()->json(new ProductStockInventoryResource($tag));
    }

    /**
     * @param ProductTagDeleteRequest $request
     * @param ProductStock $stock
     * @return \Illuminate\Http\JsonResponse
     * @throws ModelHasAssociate
     */
    public function deleteTags(ProductTagDeleteRequest $request, ProductStock $stock)
    {
        $result = $stock->deleteTags($request->get('ids'));

        return response()->json(['result' => $result]);
    }

    public function getFilterableOptions()
    {
        $result = Product::filterOptions();

        return response()->json($result);
    }

    public function getFeaturedProducts(Request $request)
    {
        $featured = Product::getFeaturedByCategories($request->get('limit', 10));

        $featured->load('images');

        return response()->json(new ProductFeaturedCollection($featured));
    }

    /**
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ModelHasAssociate
     */
    public function delete(Product $product)
    {
        $result = $product->safeDelete();

        return response()->json(['result' => $result]);
    }

    public function getVariantOptions()
    {
        return response()->json(VariantOptionResource::collection(VariantOption::all()));
    }

    public function notify(Request $request, ProductStock $stock)
    {
        /** @var User $user */
        $user = $request->user();

        $user->queue($stock);

        return response()->json();
    }
}
