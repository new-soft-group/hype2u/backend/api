<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\CategoryUpdateRequest;
use App\Http\Resources\CategoryResource;
use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * @var CategoryService
     */
    private $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index(Request $request)
    {
        $businessType = $request->query('business_type_id');

        $categories = Category::all();

        if($businessType) $categories = Category::where('business_type_id', $businessType)->get();

        return response()->json(CategoryResource::collection($categories));
    }

    public function all()
    {
        $categories = Category::withCount('products')->get();

        $categories->load('businessType');

        return response()->json(CategoryResource::collection($categories));
    }

    public function create(CategoryRequest $request)
    {
        $category = $this->categoryService->create($request->all());

        $category->load('businessType');

        return response()->json(new CategoryResource($category));
    }

    public function update(CategoryUpdateRequest $request)
    {
        $categories = $this->categoryService->update($request);

        $categories->loadCount('products');

        $categories->load('businessType');

        return response()->json(CategoryResource::collection($categories));
    }

    /**
     * @param Category $category
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\ModelHasAssociate
     */
    public function delete(Category $category)
    {
        $result = $category->safeDelete();

        return response()->json(['result' => $result]);
    }
}
