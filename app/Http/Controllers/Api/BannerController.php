<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Banner\BannerCreateRequest;
use App\Http\Requests\Banner\BannerDeleteRequest;
use App\Http\Requests\Banner\BannerUpdateRequest;
use App\Http\Requests\PositionsUpdateRequest;
use App\Http\Requests\PositionUpdateRequest;
use App\Http\Resources\BannerResource;
use App\Models\Banner;
use App\Models\BannerPosition;
use App\Services\BannerService;

class BannerController extends Controller
{
    /**
     * @var BannerService
     */
    private $bannerService;

    public function __construct(BannerService $bannerService)
    {
        $this->bannerService = $bannerService;
    }

    public function index()
    {
        $banners = Banner::sortOrder()->get();

        return response()->json(BannerResource::collection($banners));
    }

    public function create(BannerCreateRequest $request)
    {
        $data = $request->all()['banners'];

        $banners = $this->bannerService->create($data);

        return response()->json(BannerResource::collection($banners));
    }

    public function update(BannerUpdateRequest $request)
    {
        $this->bannerService->update($request->get('banners'));

        $banners = Banner::sortOrder()->get();

        return response()->json(BannerResource::collection($banners));
    }

    public function delete(BannerDeleteRequest $request)
    {
        $result = $this->bannerService->delete($request->get('ids'));

        return response()->json(['result' => $result]);
    }

    public function positions()
    {
        $positions = BannerPosition::all();

        return response()->json(['positions' => $positions]);
    }

    public function updatePosition(PositionUpdateRequest $request, BannerPosition $position)
    {
        $position->rename($request->get('name'));

        return response()->json(['result' => true]);
    }

    public function updatePositions(PositionsUpdateRequest $request)
    {
        $attributes = $request->get('positions', []);

        foreach ($attributes as $attribute) {
            $position = BannerPosition::find($attribute['id']);

            $position->rename($attribute['name']);
        }

        return response()->json(['result' => true]);
    }
}
