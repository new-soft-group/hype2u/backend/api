<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\Gender;
use App\Http\Resources\GenderResource;

class GenderController extends Controller
{
    public function index()
    {
        $genders = Gender::all();
        
        return response()->json(GenderResource::collection($genders));
    }
}
