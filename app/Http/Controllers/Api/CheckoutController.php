<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CheckoutRequest;
use App\Http\Requests\ShopCheckoutRequest;
use App\Http\Resources\OrderResource;
use App\Models\BusinessType;
use App\Models\User;
use App\Options\OrderOptions as Options;
use App\Services\CheckoutService;
use App\Services\OrderService;
use Illuminate\Support\Facades\Log;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;

class CheckoutController extends Controller
{
    /**
     * @var OrderService
     */
    private $orderService;

    /**
     * @var CheckoutService
     */
    private $checkoutService;

    /**
     * CheckoutController constructor.
     * @param OrderService $orderService
     * @param CheckoutService $checkoutService
     */
    public function __construct(OrderService $orderService, CheckoutService $checkoutService)
    {
        $this->orderService = $orderService;
        $this->checkoutService = $checkoutService;
    }

    /**
     * @param CheckoutRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\EmptyCartException
     * @throws \App\Exceptions\InsufficientStockException
     * @throws \App\Exceptions\OutOfStockException
     * @throws \App\Exceptions\ShippingAddressNotFound
     * @throws \App\Exceptions\DuplicateTimeslotException
     */
    public function checkout(CheckoutRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $cart = $user->cart;
        $plan = $user->defaultSubscription()->plan;

        $exclude = $plan->policies->primeTargetIds();
        $totalAmount = $cart->totalAmount($exclude);
        $surcharges = $plan->getSurchargeAmount($totalAmount);

        $option = new Options($request->only([
            'currency',
            'shipping_address',
            'timeslot',
            'timeslot_date'
        ]));
        $option->setCharge($surcharges);

        // Update default shipping address for existing hype2u customer if address exist
        $this->orderService->updateDefaultShippingAddress($user);

        $order = $this->orderService->create($user, $cart->processCheckout(), $option);

        $order->load('timeslots');

        return response()->json(new OrderResource($order));
    }

    public function shopCheckout(ShopCheckoutRequest $request)
    {
        /** @var User $user */
        $user = auth()->user();
        $cart = $user->shopCart;

        $option = new Options($request->only([
            'currency',
            'shipping_address',
        ]));

        // Update default shipping address for existing hype2u customer if address exist
        $this->orderService->updateDefaultShippingAddress($user);

        $order = $this->orderService->create($user, $cart->processCheckout(), $option, BusinessType::SHOP);

        try {
            $method = $request->get('payment_method');

            $this->checkoutService->shopCheckout($order, $method);

            return response()->json(new OrderResource($order));
        }
        catch (ApiErrorException $e) {

            return response()->json(['message' => $e->getMessage()], 400);
        }
    }
}
