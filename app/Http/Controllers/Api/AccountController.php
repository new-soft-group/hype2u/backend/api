<?php

namespace App\Http\Controllers\Api;

use App\Criteria\WhereExcept;
use App\Criteria\WhereHasSubscription;
use App\Criteria\WhereInvoiceBelongsToUser;
use App\Criteria\WhereUser;
use App\Http\Controllers\Controller;
use App\Http\Requests\AccountCreateRequest;
use App\Http\Requests\AccountUpdateRequest;
use App\Http\Resources\AddressResource;
use App\Http\Resources\Invoice\InvoiceResource;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\PaymentMethodResource;
use App\Http\Resources\RevenueCollection;
use App\Http\Resources\SubscriptionResource;
use App\Http\Resources\UserCollection;
use App\Http\Resources\UserResource;
use App\Http\Resources\WishlistResource;
use App\Models\Affiliate;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\Subscription;
use App\Models\User;
use App\Services\AffiliateService;
use App\Services\UserService;
use Datatable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class AccountController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;
    /**
     * @var AffiliateService
     */
    private $affiliateService;

    /**
     * RoleController constructor.
     * @param UserService $userService
     * @param AffiliateService $affiliateService
     */
    public function __construct(UserService $userService, AffiliateService $affiliateService)
    {
        $this->userService = $userService;
        $this->affiliateService = $affiliateService;
    }

    public function show(User $user)
    {
        $user->load('profile', 'roles', 'affiliate.discountCode');

        return response()->json(new UserResource($user));
    }

    /**
     * @param Request $request
     * @return UserCollection
     */
    public function index(Request $request)
    {
        $users = Datatable::setRequest($request)
                          ->setModel(new User())
                          ->setDefaultSort()
                          ->setCriteria(new WhereExcept($request->user()))
                          ->get();

        return new UserCollection($users);
    }

    public function revenue(Request $request)
    {
        $users = Datatable::setRequest($request)
                          ->setModel(new User())
                          ->setDefaultSort()
                          ->setCriteria(new WhereExcept($request->user()), new WhereHasSubscription())
                          ->get();

        return new RevenueCollection($users);
    }

    public function create(AccountCreateRequest $request)
    {
        $user = $this->userService->create(
            $request->except('affiliate'),
            $request->get('role_id'));

        if ($user->hasRole([Affiliate::REQUIRED_ROLE])) {
            $this->affiliateService->create($user, $request->get('affiliate'));
        }

        return response()->json(new UserResource($user->load(['affiliate'])));
    }

    public function update(AccountUpdateRequest $request, User $user)
    {
        $user->profile->update($request->except(['affiliate', 'role_id']));

        if ($request->has('role_id')) {
            $this->userService->updateRole($user, $request->get('role_id'));
        }

        if ($request->has('affiliate') and $user->affiliate) {
            $this->affiliateService->update($user->affiliate, $request->get('affiliate'));
        }

        return response()->json(new UserResource($user));
    }

    public function getPaymentMethods(User $user)
    {
        $methods = $user->paymentMethods();

        return response()->json(PaymentMethodResource::collection($methods));
    }

    public function getAddresses(User $user)
    {
        return AddressResource::collection($user->profile->addresses)
                              ->additional(['meta' => [
                                  'default_shipping' => $user->profile->shipping_address_id,
                                  'default_billing' => $user->profile->billing_address_id
                              ]]);
    }

    public function general(User $user)
    {
        /** @var Subscription $subscription */
        $subscription = $user->defaultSubscription();

        return response()->json([
            'total_order' => $user->orders()->count(),
            'total_sales' => $user->totalSales(),
            'created_at' => $user->created_at,
            'subscribed' => $subscription ? new SubscriptionResource($subscription) : null,
            'next_billing_cycle' => $subscription ? $subscription->nextBillingCycle() : null
        ]);
    }

    public function getWishlist(User $user)
    {
        $wishlist = $user->getWishlist()->load('items.product.images');

        return response()->json(new WishlistResource($wishlist));
    }

    /**
     * @param Request $request
     * @param User $user
     * @return OrderCollection
     */
    public function getOrders(Request $request, User $user)
    {
        $orders = Datatable::setRequest($request)
                           ->setModel(new Order())
                           ->setDefaultSort()
                           ->setCriteria(new WhereUser($user))
                           ->get();

        return new OrderCollection($orders);
    }

    public function getInvoices(Request $request, User $user)
    {
        $invoices = Datatable::setRequest($request)
                             ->setModel(new Invoice())
                             ->setCriteria(new WhereInvoiceBelongsToUser($user))
                             ->setDefaultSort()
                             ->get();

        return InvoiceResource::collection($invoices);
    }
}
