<?php

namespace App\Http\Controllers\Api;

use App\Criteria\WithAllocationCount;
use App\Facades\Datatable\Datatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Timeslot\TimeslotAllocateRequest;
use App\Http\Requests\Timeslot\TimeslotCreateRequest;
use App\Http\Requests\Timeslot\TimeslotRescheduleRequest;
use App\Http\Requests\Timeslot\TimeslotUpdateRequest;
use App\Http\Resources\Timeslot\TimeslotAllocationResource;
use App\Http\Resources\Timeslot\TimeslotEventResource;
use App\Http\Resources\Timeslot\TimeslotResource;
use App\Http\Resources\Timeslot\TimeslotTypeResource;
use App\Models\Timeslot;
use App\Models\TimeslotAllocation;
use App\Models\TimeslotType;
use Illuminate\Http\Request;


class TimeslotController extends Controller
{
    public function index(Request $request)
    {
        $timeslots = Datatable::setRequest($request)
                              ->setModel(new Timeslot())
                              ->setCriteria(new WithAllocationCount())
                              ->get();

        return TimeslotResource::collection($timeslots);
    }

    public function show(Timeslot $timeslot)
    {
        $timeslot->load('allocations');

        return response()->json(new TimeslotResource($timeslot));
    }

    public function create(TimeslotCreateRequest $request)
    {
        $timeslot = new Timeslot($request->all());
        $timeslot->save();

        return response()->json(new TimeslotResource($timeslot));
    }

    /**
     * @param TimeslotUpdateRequest $request
     * @param Timeslot $timeslot
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(TimeslotUpdateRequest $request, Timeslot $timeslot)
    {
        $this->authorize('update', $timeslot);

        $timeslot->fill($request->all())
                 ->update();

        return response()->json(new TimeslotResource($timeslot));
    }


    /**
     * @param Timeslot $timeslot
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Auth\Access\AuthorizationException
     * @throws \Exception
     */
    public function delete(Timeslot $timeslot)
    {
        $this->authorize('delete', $timeslot);

        $result = $timeslot->delete();

        return response()->json(['result' => $result]);
    }

    /**
     * @param TimeslotAllocateRequest $request
     * @param Timeslot $timeslot
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\DuplicateTimeslotException
     */
    public function allocate(TimeslotAllocateRequest $request, Timeslot $timeslot)
    {
        $order = $request->get('order_id');
        $date = $request->get('date');

        $allocated = $timeslot->allocate($order, $date);

        return response()->json(new TimeslotAllocationResource($allocated));
    }

    public function reschedule(TimeslotRescheduleRequest $request, TimeslotAllocation $allocated)
    {
        $allocated->reschedule($request->get('timeslot'), $request->get('date'));

        return response()->json(new TimeslotEventResource($allocated));
    }

    public function types()
    {
        $types = TimeslotType::all();

        return response()->json(TimeslotTypeResource::collection($types));
    }

    public function events(Request $request)
    {
        $events = Datatable::setRequest($request)
                           ->setModel(new TimeslotAllocation())
                           ->get();

        return TimeslotEventResource::collection($events);
    }
}
