<?php


namespace App\Http\Controllers\Api;


use App\Http\Controllers\Controller;
use App\Http\Requests\ContactUsRequest;
use App\Jobs\NotifyContactUs;

class ContactUsController extends Controller
{
    public function contact(ContactUsRequest $request)
    {
        NotifyContactUs::dispatch($request->all());

        return response()->json();
    }
}
