<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\VendorCreateRequest;
use App\Http\Requests\VendorUpdateRequest;
use App\Services\VendorService;
use Illuminate\Http\Request;
use Datatable;
use App\Http\Resources\VendorCollection;
use App\Http\Resources\VendorResource;
use App\Models\Vendor;

class VendorController extends Controller
{
    /**
     * @var VendorService
     */
    private $vendorService;

    public function __construct(VendorService $vendorService)
    {
        $this->vendorService = $vendorService;
    }

    public function show(Vendor $vendor)
    {
        return response()->json(new VendorResource($vendor));
    }

    public function index(Request $request)
    {
        $vendor = Datatable::setRequest($request)
                           ->setModel(new Vendor())
                           ->setDefaultSort()
                           ->get();

        return new VendorCollection($vendor);
    }

    /**
     * @param VendorCreateRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function create(VendorCreateRequest $request)
    {
        $payload = $request->all();

        $vendor = $this->vendorService->create($payload);

        return response()->json(new VendorResource($vendor));
    }


    /**
     * @param VendorUpdateRequest $request
     * @param Vendor $vendor
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VendorUpdateRequest $request, Vendor $vendor)
    {
        $this->vendorService->update($vendor, $request->all());

        return response()->json(new VendorResource($vendor));
    }

    /**
     * @param Vendor $vendor
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Vendor $vendor)
    {
        $result = $vendor->safeDelete();

        return response()->json(['result' => $result]);
    }
}
