<?php

namespace App\Http\Controllers\Api;

use App\Criteria\GroupStockInventories;
use App\Http\Controllers\Controller;
use App\Http\Resources\WarehouseInventoryResource;
use App\Models\ProductStockInventory;
use Datatable;
use Illuminate\Http\Request;

class WarehouseInventoryController extends Controller
{
    public function index(Request $request)
    {
        $items = Datatable::setRequest($request)
                          ->setModel(new ProductStockInventory())
                          ->setCriteria(new GroupStockInventories())
                          ->get();

        return WarehouseInventoryResource::collection($items);
    }
}
