<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ReplacePaymentMethodRequest;
use App\Http\Resources\PaymentMethodResource;
use App\Models\User;

class PaymentMethodController extends Controller
{
    public function defaultMethod()
    {
        /** @var User $user */
        $user = auth()->user();

        $method = $user->defaultPaymentMethod();

        return $method ? response()->json(new PaymentMethodResource($method)) : null ;
    }

    public function replace(ReplacePaymentMethodRequest $request, User $user)
    {
        /** Retrieve current default payment method */
        $default = $user->defaultPaymentMethod();
        /** Update new payment method as default */
        $user->updateDefaultPaymentMethod($request->get('payment'));
        /** Remove existing old payment method */
        if(!is_null($default)) {
            $user->removePaymentMethod($default->id);
        }

        return response()->json(['result' => true]);
    }

    public function methods()
    {
        /** @var User $user */
        $user = auth()->user();

        $methods = $user->paymentMethods();

        return response()->json(PaymentMethodResource::collection($methods));
    }

    public function remove(User $user, string $id)
    {
        $user->removePaymentMethod($id);

        return response()->json(['result' => true]);
    }
}
