<?php

namespace App\Http\Controllers\Api;

use App\Facades\Datatable\Datatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Holiday\HolidayCreateRequest;
use App\Http\Requests\Holiday\HolidayUpdateRequest;
use App\Http\Resources\HolidayResource;
use App\Models\Holiday;
use Illuminate\Http\Request;

class HolidayController extends Controller
{
    public function index(Request $request)
    {
        $holidays = Datatable::setRequest($request)
                             ->setModel(new Holiday())
                             ->get();

        return HolidayResource::collection($holidays);
    }

    public function show(Holiday $holiday)
    {
        return response()->json(new HolidayResource($holiday));
    }

    public function create(HolidayCreateRequest $request)
    {
        $holiday = new Holiday($request->all());
        $holiday->save();

        return response()->json(new HolidayResource($holiday));
    }

    public function update(HolidayUpdateRequest $request, Holiday $holiday)
    {
        $holiday->fill($request->all())
                ->update();

        return response()->json(new HolidayResource($holiday));
    }

    /**
     * @param Holiday $holiday
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function delete(Holiday $holiday)
    {
        $result = $holiday->delete();

        return response()->json(['result' => $result]);
    }
}
