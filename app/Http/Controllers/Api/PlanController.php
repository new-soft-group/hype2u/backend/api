<?php

namespace App\Http\Controllers\Api;

use Datatable;
use App\Http\Controllers\Controller;
use App\Http\Requests\Plan\PlanCreateRequest;
use App\Http\Requests\Plan\PlanStatusRequest;
use App\Http\Requests\Plan\PlanUpdateRequest;
use App\Http\Resources\Plan\PlanCollection;
use Illuminate\Http\Request;
use App\Services\PlanService;
use App\Http\Resources\Plan\PlanResource;
use App\Models\Plan;

class PlanController extends Controller
{
    /**
     * @var PlanService
     */
    private $planService;

    public function __construct(PlanService $planService)
    {
        $this->planService = $planService;
    }

    public function index(Request $request)
    {
        $plans = Datatable::setRequest($request)
                          ->setModel(new Plan())
                          ->setDefaultSort()
                          ->get();

        return new PlanCollection($plans);
    }

    public function all()
    {
        $plans = Plan::where('enabled', true)->get();

        return new PlanCollection($plans);
    }

    public function show(Plan $plan)
    {
        $plan->load('surcharges', 'policies.rules');

        return response()->json(new PlanResource($plan));
    }

    public function create(PlanCreateRequest $request)
    {
        $plan = $this->planService->create($request->all());

        return response()->json(new PlanResource($plan));
    }

    /**
     * @param PlanUpdateRequest $request
     * @param Plan $plan
     * @return \Illuminate\Http\JsonResponse
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(PlanUpdateRequest $request, Plan $plan)
    {
        if($request->has('surcharges')) {
            $plan->validateThreshold($request->get('surcharges'));
        }

        $plan = $this->planService->update($plan, $request->all());

        return response()->json(new PlanResource($plan));
    }

    public function status(PlanStatusRequest $request, Plan $plan)
    {
        $this->planService->status($plan, $request->get('enabled'));

        return response()->json(new PlanResource($plan));
    }

    public function sort(Plan $plan, Request $request)
    {
        $plan = $this->planService->updateSort($plan, $request->input('sort'));

        return response()->json(new PlanResource($plan));
    }

    /**
     * @param Plan $plan
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deleteSurcharge(Plan $plan, int $id)
    {
        $surcharge = $plan->surcharges()->find($id);

        if(is_null($surcharge)) {
            return response()->json(['result' => false]);
        }

        $surcharge->delete();

        return response()->json(['result' => true]);
    }

    /**
     * @param Plan $plan
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     * @throws \Exception
     */
    public function deletePolicies(Plan $plan, int $id)
    {
        $policies = $plan->policies()->find($id);

        if(is_null($policies)) {
            return response()->json(['result' => false]);
        }

        $policies->delete();

        return response()->json(['result' => true]);
    }
}
