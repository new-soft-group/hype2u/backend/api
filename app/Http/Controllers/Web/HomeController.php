<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Http\Resources\UserResource;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Show the application dashboard page.
     *
     * @param Request $request
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request) {
        /** @var User $user */
        $user = Auth::user();
        
        $url = $request->getRequestUri();
        
        $user->load(['roles.permissions', 'profile']);
        
        return view('dashboard.layouts.app', [
            'url' => $url,
            'user' => new UserResource($user)
        ]);
    }
}
