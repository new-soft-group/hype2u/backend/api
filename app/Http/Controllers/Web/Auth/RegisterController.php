<?php

namespace App\Http\Controllers\Web\Auth;

use App\Constants\Role;
use App\Http\Controllers\Controller;
use App\Http\Requests\EnrollRequest;
use App\Providers\RouteServiceProvider;
use App\Services\AffiliateService;
use App\Services\UserService;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    
    use RegistersUsers;
    
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
    /**
     * @var AffiliateService
     */
    private $affiliateService;
    /**
     * @var UserService
     */
    private $userService;
    
    /**
     * Create a new controller instance.
     *
     * @param UserService $userService
     * @param AffiliateService $affiliateService
     */
    public function __construct(UserService $userService, AffiliateService $affiliateService)
    {
        $this->middleware('guest');
        $this->userService = $userService;
        $this->affiliateService = $affiliateService;
    }
    
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, (new EnrollRequest())->rules());
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\Models\User
     * @throws \Exception
     */
    protected function create(array $data)
    {
        $user = $this->userService->create($data, Role::AGENT);
    
        $this->affiliateService->createByCode($user, $data['code']);
        
        return $user;
    }
}
