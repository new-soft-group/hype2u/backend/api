<?php


namespace App\Http\Controllers\Web;

use App\Events\NewSubscriptionEvent;
use App\Events\SubscriptionRenewEvent;
use App\Events\UpdateSubscriptionEvent;
use App\Models\Coupon;
use App\Models\Subscription;
use App\Models\User;
use App\Services\PaymentSummaryService;
use Illuminate\Support\Facades\Log;
use \Laravel\Cashier\Http\Controllers\WebhookController as CashierController;
use Stripe\Invoice;
use Stripe\Discount;
use Symfony\Component\HttpFoundation\Response;

class WebhookController extends CashierController
{
    public function handleInvoicePaymentSucceeded($payload)
    {
        /** @var Invoice $stripeInvoice */
        $stripeInvoice = convertToStripeObject($payload);

        if(!is_null($stripeInvoice->subscription)) {
            /** @var Subscription $subscription */
            $subscription = Subscription::findByStripeId($stripeInvoice->subscription);

            if(!is_null($subscription)) {
                $invoice = $subscription->newInvoice($stripeInvoice);

                $subscription->invoices()->save($invoice);

                /** Send SMS Notification based on billing reason */
                if(!is_null($stripeInvoice->billing_reason)){
                    if($stripeInvoice->billing_reason == "subscription_create"){
                        event(new NewSubscriptionEvent($subscription));
                    }elseif($stripeInvoice->billing_reason == "subscription_cycle"){
                        event(new SubscriptionRenewEvent($subscription));
                    }
                }
            }
        }
    }

    public function handleCustomerDiscountCreated($payload)
    {
        /** @var Discount $stripeDiscount */
        $stripeDiscount = convertToStripeObject($payload);

        if(!is_null($stripeDiscount->coupon)) {
            /** @var Coupon $coupon */
            $coupon = Coupon::findByStripeId($stripeDiscount->coupon->id);

            if(!is_null($coupon)) {
                $coupon->syncWithStripeCoupon($stripeDiscount->coupon);
            }
        }
    }

    /**
     * Handle charge succeeded
     *
     * @param array $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleChargeSucceeded($payload)
    {
        $object = $payload['data']['object'];

        $payment = new PaymentSummaryService();
        $input = $this->createData($payload);

        try{
            $result = $payment->create($input);
        }catch (\Throwable $th) {
            Log::debug($th->getMessage());
            return new Response($th->getMessage(), 422);
        }

        return $this->successMethod();
    }

    /**
     * Handle charge failed
     *
     * @param array $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleChargeFailed($payload)
    {
        $payment = new PaymentSummaryService();
        $input = $this->createData($payload);

        try{
            $result = $payment->create($input);
        }catch (\Throwable $th) {
            Log::debug($th->getMessage());
            return new Response($th->getMessage(), 422);
        }

        return $this->successMethod();
    }

    /**
     * Handle charge refunded
     *
     * @param array $payload
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function handleChargeRefunded($payload)
    {
        $payment = new PaymentSummaryService();
        $input = $this->createData($payload);

        try{
            $result = $payment->create($input);
        }catch (\Throwable $th) {
            Log::debug($th->getMessage());

            return new Response($th->getMessage(), 422);
        }

        return $this->successMethod();
    }

    /**
     * Handle successful calls on the controller.
     *
     * @param  array  $parameters
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function successMethod($parameters = [])
    {
        return new Response('Webhook Handled', 200);
    }


    /**
     * Create data for table payment
     *
     * @param array $payload
     */
    protected function createData($payload)
    {
        // create data
        $object = $payload['data']['object'];

        $user = $this->getUserByStripeId($object['customer']);

        $input = [
            'charge_id' =>  $object['id'],
            'invoice_id' => $object['invoice'],
            'payment_intent' => $object['payment_intent'],
            'amount' => $this->reformatNumber($object['amount']),
            'amount_refunded' => $this->reformatNumber($object['amount_refunded']),
            'application_fee' => $this->reformatNumber($object['application']),
            'application_fee_amount' => $this->reformatNumber($object['application_fee_amount']),
            'dispute' => $this->reformatNumber($object['dispute']),
            'disputed' => $object['disputed'],
            'type' => $payload['type'],
            'failure_code' => $object['failure_code'],
            'failure_message' => $object['failure_message'],
            'customer_stripe_id' => $object['customer'],
            'payment_date' => $object['created'],
            'status' => $object['status'],
            'currency' => $object['currency'],
        ];

        if($user){
            $input['user_id'] = $user->id;
        }

        return $input;
    }

    /**
     * Reformat number
     *
     * @param number $number
     * @return mixed
     */
    protected function reformatNumber($number)
    {
        return ($number / 100);
    }

}
