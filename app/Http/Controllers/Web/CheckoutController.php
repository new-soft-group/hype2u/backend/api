<?php

namespace App\Http\Controllers\web;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Services\CheckoutService;
use Stripe\Exception\ApiErrorException;
use Stripe\PaymentIntent;

class CheckoutController extends Controller
{
    /**
     * @var CheckoutService
     */
    private $checkoutService;
    
    public function __construct(CheckoutService $checkoutService)
    {
        $this->checkoutService = $checkoutService;
    }
}
