<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;

class LanguageController extends Controller
{
    public function switchLanguage (Request $request, $locale = 'en') {

        Session::put('lang', $locale);

        return redirect()->back();
    }

}
