<?php

namespace App\Policies;

use App\Models\Timeslot;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class TimeslotPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Timeslot $timeslot
     * @return \Illuminate\Auth\Access\Response
     */
    public function update(User $user, Timeslot $timeslot)
    {
        $allowed = $user->hasPermissionTo('system.editor') && !$timeslot->allocations()->exists();

        return $allowed ? $this->allow() : $this->deny(__('timeslot.has.allocation'), 403);
    }

    /**
     * @param User $user
     * @param Timeslot $timeslot
     * @return \Illuminate\Auth\Access\Response
     */
    public function delete(User $user, Timeslot $timeslot)
    {
        $allowed = $user->hasPermissionTo('system.editor') && !$timeslot->allocations()->exists();

        return $allowed ? $this->allow() : $this->deny(__('timeslot.has.allocation'), 403);
    }
}
