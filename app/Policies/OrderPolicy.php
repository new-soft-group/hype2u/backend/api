<?php

namespace App\Policies;

use App\Models\Order;
use App\Models\OrderStatus;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class OrderPolicy
{
    use HandlesAuthorization;

    public function view(User $user, Order $order)
    {
        return (int)$order->user_id === $user->id;
    }

    public function update(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canEdit();

        return $allowed
            ? $this->allow()
            : $this->deny(__('order.failed.update', ['status' => $order->status->name]), 403);
    }

    public function cancel(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canCancel();

        return $allowed
            ? $this->allow()
            : $this->deny(__('order.failed.cancel', ['status' => $order->status->name]), 403);
    }

    public function delete(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canDelete();

        $hasCharge = !is_null($order->charge);

        $message = $hasCharge
            ? $this->deny(__('order.has.charge'), 403)
            : $this->deny(__('order.failed.delete', ['status' => $order->status->name]), 403);

        return $allowed && !$hasCharge ? $this->allow() : $message;
    }

    public function reject(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canReject();

        return $allowed
            ? $this->allow()
            : $this->deny(__('order.failed.update', ['status' => $order->status->name]), 403);
    }

    public function deliver(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canDeliver();

        return $allowed
            ? $this->allow()
            : $this->deny(__('order.failed.update', ['status' => $order->status->name]), 403);
    }

    public function approve(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canApprove();

        return $allowed
            ? $this->allow()
            : $this->deny(__('order.failed.update', ['status' => $order->status->name]), 403);
    }

    public function ship(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canShip();

        return $allowed
            ? $this->allow()
            : $this->deny(__('order.failed.update', ['status' => $order->status->name]), 403);
    }

    public function reserveTags(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canReserveTags();

        return $allowed
            ? $this->allow()
            : $this->deny(__('order.failed.update', ['status' => $order->status->name]), 403);
    }

    public function removeReserveTag(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canRemoveReserveTag();

        return $allowed
            ? $this->allow()
            : $this->deny(__('order.failed.update', ['status' => $order->status->name]), 403);
    }

    public function returnBack(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canReturn();

        return $allowed
            ? $this->allow()
            : $this->deny(__('order.failed.update', ['status' => $order->status->name]), 403);
    }

    public function deleteItem(User $user, Order $order)
    {
        $allowed = $user->hasPermissionTo('order.editor') && $order->canDelete();

        return $allowed
            ? $this->allow()
            : $this->deny(__('order.failed.delete_item', ['status' => $order->status->name]), 403);
    }
}
