<?php

namespace App\Policies;

use App\Models\Address;
use App\Models\Coupon;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CouponPolicy
{
    use HandlesAuthorization;

    /**
     * @param User $user
     * @param Coupon $coupon
     * @return \Illuminate\Auth\Access\Response
     */
    public function update(User $user, Coupon $coupon)
    {
        $allowed = $user->hasPermissionTo('system.editor') && empty($coupon->invoices()->count());

        return $allowed ? $this->allow() : $this->deny(__('coupon.failed.update'), 403);
    }
}
