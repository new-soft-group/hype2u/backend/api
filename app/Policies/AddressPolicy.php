<?php

namespace App\Policies;

use App\Models\Address;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class AddressPolicy
{
    use HandlesAuthorization;
    
    /**
     * Determine whether the user can update the model.
     *
     * @param \App\Models\User $user
     * @param Address $address
     * @return mixed
     */
    public function update(User $user, Address $address)
    {
        return $user->profile->id === (int)$address->addressable_id;
    }
    
    /**
     * Determine whether the user can delete the model.
     *
     * @param \App\Models\User $user
     * @param Address $address
     * @return mixed
     */
    public function delete(User $user, Address $address)
    {
        return $user->profile->id === (int)$address->addressable_id;
    }
}
