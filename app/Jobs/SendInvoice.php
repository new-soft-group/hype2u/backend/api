<?php

namespace App\Jobs;

use App\Mail\InvoicePaid;
use App\Models\Guest;
use App\Models\Transaction;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Mail;

class SendInvoice implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var Transaction
     */
    private $transaction;

    /**
     * Create a new job instance.
     *
     * @param Transaction $transaction
     */
    public function __construct(Transaction $transaction)
    {
        $this->transaction = $transaction;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->transaction->load('order.orderable');

        /** @var User|Guest $customer */
        $customer = $this->transaction->order->orderable;

        \Log::info("Processing customer $customer->email invoice.");

        Mail::to($customer)->send(new InvoicePaid($this->transaction));
    }
}
