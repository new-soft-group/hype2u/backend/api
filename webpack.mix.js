const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

if (mix.inProduction()) {
    mix.setPublicPath('public')
        .js('resources/js/dashboard.js', 'public/js')
        .js('resources/js/login.js', 'public/js')
        .js('resources/js/checkout.js', 'public/js')
        .sass('resources/sass/dashboard.scss', 'public/css')
        .sass('resources/sass/login.scss', 'public/css')
        .sass('resources/sass/checkout.scss', 'public/css')
        .version();
}
else {
    mix.js('resources/js/dashboard.js', 'public/js')
        .sass('resources/sass/dashboard.scss', 'public/css');

    mix.js('resources/js/login.js', 'public/js')
        .sass('resources/sass/login.scss', 'public/css');

    mix.js('resources/js/checkout.js', 'public/js')
        .sass('resources/sass/checkout.scss', 'public/css');

    mix.sass('resources/sass/editor.scss', 'public/css');
}
