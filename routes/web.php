<?php

use App\Http\Controllers\Web\WebhookController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')
     ->middleware('auth');

Route::group(['middleware' => 'web'], function () {
    Route::get('/locale/{locale}', 'LanguageController@switchLanguage')->name('locale');
});

Route::prefix('dashboard')->group(function () {
    Route::get('/', 'HomeController@index')->middleware('auth');
    Route::post('/logout', 'Auth\LoginController@logout');
});

Route::post('stripe/webhooks', [WebhookController::class, 'handleWebhook']);

