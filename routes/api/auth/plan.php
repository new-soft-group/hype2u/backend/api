<?php

use App\Http\Controllers\Api\PlanController;

Route::prefix('plans')->group(function () {
    Route::get('{plan}', [PlanController::class, 'show']);

    Route::post('/', [PlanController::class, 'create'])
         ->middleware('permission:plan.create|permission:plan.edit');

    Route::put('{plan}', [PlanController::class, 'update'])
         ->middleware('permission:plan.edit');

    Route::put('{plan}/status', [PlanController::class, 'status'])
         ->middleware('permission:plan.edit');

    Route::put('{plan}/sort', [PlanController::class, 'sort'])
         ->middleware('permission:plan.edit');

    Route::delete('{plan}/surcharges/{id}', [PlanController::class, 'deleteSurcharge'])
         ->middleware('permission:plan.edit');

    Route::delete('{plan}/policies/{id}', [PlanController::class, 'deletePolicies'])
        ->middleware('permission:plan.edit');
});
