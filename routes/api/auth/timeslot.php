<?php

use App\Http\Controllers\Api\TimeslotController;

Route::prefix('timeslots')->group(function () {
    Route::post('/', [TimeslotController::class, 'create'])
         ->middleware('permission:system.editor');

    Route::get('events', [TimeslotController::class, 'events'])
         ->middleware('permission:system.editor');

    Route::get('{timeslot}', [TimeslotController::class, 'show']);
    Route::put('{timeslot}', [TimeslotController::class, 'update']);
    Route::delete('{timeslot}', [TimeslotController::class, 'delete']);
    Route::post('{timeslot}/allocate', [TimeslotController::class, 'allocate'])
         ->middleware('timeslot');

    Route::put('{allocated}/reschedule', [TimeslotController::class, 'reschedule'])
         ->middleware(['permission:system.editor', 'timeslot']);
});
