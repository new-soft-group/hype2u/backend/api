<?php

use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\ProductTypeController;

Route::prefix('products')->group(function () {
    Route::get('/all', [ProductController::class, 'all'])
        ->middleware('permission:product.view');
    Route::post('/', [ProductController::class, 'create'])
         ->middleware('permission:product.create');
    Route::put('{product}', [ProductController::class, 'update'])
         ->middleware('permission:product.edit');
    Route::delete('{product}', [ProductController::class, 'delete'])
         ->middleware('permission:product.delete');

    Route::get('variants', [ProductController::class, 'getVariants'])
         ->middleware('permission:product.create|product.edit');
    Route::post('variants', [ProductController::class, 'createVariant'])
         ->middleware('permission:product.create|product.edit');
    Route::put('variants/{variant}', [ProductController::class, 'updateVariant'])
         ->middleware('permission:product.create|product.edit');
    Route::delete('variants/{variant}', [ProductController::class, 'deleteVariant'])
         ->middleware('permission:product.delete');

    Route::get('combinations/{product}', [ProductController::class, 'getVariantCombinationOptions'])
         ->middleware('permission:product.create|product.edit');

    Route::get('stocks/{product}', [ProductController::class, 'getStocks'])
         ->middleware('permission:product.view');
    Route::post('stocks/{product}', [ProductController::class, 'createStock'])
         ->middleware('permission:product.create');
    Route::put('stocks/{stock}', [ProductController::class, 'updateStock'])
         ->middleware('permission:product.edit');
    Route::delete('stocks/{product}', [ProductController::class, 'deleteStocks'])
         ->middleware('permission:product.delete');
    Route::post('stocks/{stock}/notify', [ProductController::class, 'notify']);

    Route::get('tags/{stock}', [ProductController::class, 'getTags'])
         ->middleware('permission:product.view');
    Route::post('tags/{stock}', [ProductController::class, 'restock'])
         ->middleware('permission:product.create');
    Route::put('tags/{tag}', [ProductController::class, 'updateTag'])
         ->middleware('permission:product.edit');
    Route::delete('tags/{stock}', [ProductController::class, 'deleteTags'])
         ->middleware('permission:product.delete');

    Route::get('types/all', [ProductTypeController::class , 'all'])
        ->middleware('permission:product.view');
    Route::post('types', [ProductTypeController::class, 'create'])
         ->middleware('permission:product.create');
    Route::patch('types', [ProductTypeController::class, 'update'])
         ->middleware('permission:product.edit');
    Route::delete('types/{type}', [ProductTypeController::class, 'delete'])
         ->middleware('permission:product.delete');
});
