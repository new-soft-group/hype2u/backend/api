<?php

Route::namespace('Auth')->group(function () {
    Route::post('logout', 'LoginController@logout');
});
