<?php

use App\Http\Controllers\Api\UserController;

Route::prefix('users')->group(function () {
    Route::post('password', [UserController::class, 'updatePassword'])
         ->middleware('permission:profile.edit');

    Route::get('/', [UserController::class, 'getProfile'])
         ->middleware('permission:profile.view');

    Route::post('/', [UserController::class, 'updateProfile'])
         ->middleware('permission:profile.edit');

    Route::prefix('addresses')->group(function () {
        Route::get('/', [UserController::class, 'getAddresses'])
             ->middleware('permission:profile.view');

        Route::middleware('permission:profile.edit')->group(function () {
            Route::post('/', [UserController::class, 'createAddress']);
            Route::put('{address}', [UserController::class, 'updateAddress']);
            Route::put('shipping/{address}', [UserController::class, 'setShippingAddress']);
            Route::put('billing/{address}', [UserController::class, 'setBillingAddress']);
        });

        Route::delete('{address}', [UserController::class, 'deleteAddress'])
             ->middleware('permission:profile.delete');
    });

    Route::get('subscriptions', [UserController::class, 'subscriptions']);

    Route::get('cards/setup', [UserController::class, 'cardSetup']);

    Route::get('orders', [UserController::class, 'orders'])
         ->middleware('permission:order.view');

    Route::get('orders/{order}', [UserController::class, 'showOrder'])
         ->middleware('permission:order.view');

    Route::get('invoices', [UserController::class, 'invoices']);
});
