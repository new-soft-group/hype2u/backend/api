<?php

use App\Http\Controllers\Api\WishlistController;

Route::prefix('wishlists')->group(function () {
    Route::get('/', [WishlistController::class, 'index'])
         ->middleware('permission:wishlist.view');

    Route::put('{product}', [WishlistController::class, 'addItem'])
         ->middleware('permission:wishlist.create');

    Route::delete('{product}', [WishlistController::class, 'deleteItem'])
         ->middleware('permission:wishlist.delete');

    Route::get('/all', [WishlistController::class, 'all'])
         ->middleware('permission:wishlist.view');
});
