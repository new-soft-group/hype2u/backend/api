<?php

use App\Http\Controllers\Api\BrandController;

Route::prefix('brands')->group(function () {
    Route::get('/all', [BrandController::class, 'all']);

    Route::post('/', [BrandController::class, 'create'])
         ->middleware('permission:product.create');

    Route::patch('/', [BrandController::class, 'update'])
         ->middleware('permission:product.edit');

    Route::delete('{brand}', [BrandController::class, 'delete'])
         ->middleware('permission:product.delete');
});
