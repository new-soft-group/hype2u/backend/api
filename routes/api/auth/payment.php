<?php

use App\Http\Controllers\Api\PaymentMethodController;

Route::prefix('payments')->group(function () {
    Route::prefix('methods')->group(function () {
        Route::get('/', [PaymentMethodController::class, 'methods']);
        Route::get('default', [PaymentMethodController::class, 'defaultMethod']);
        Route::delete('{user}/{id}', [PaymentMethodController::class, 'remove'])
             ->middleware('permission:payment.methods.remove');
    });
});
