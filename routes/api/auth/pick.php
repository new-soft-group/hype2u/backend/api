<?php

use App\Http\Controllers\Api\PickController;

Route::prefix('picks')->group(function () {
    Route::get('/pending', [PickController::class, 'pendingPickCount']);
    Route::post('/', [PickController::class, 'create'])->middleware(['can.purchase']);
    Route::get('/', [PickController::class, 'index']);
    Route::get('/status', [PickController::class, 'status'])->middleware(['can.purchase']);
    Route::get('/{pick}', [PickController::class, 'show']);
    Route::put('/{pick}', [PickController::class, 'update']);
    Route::put('/{pick}/attended', [PickController::class, 'attended']);
    Route::put('/{pick}/pending', [PickController::class, 'pending']);
    Route::put('/{pick}/reject', [PickController::class, 'reject']);
    Route::put('/{pick}/close', [PickController::class, 'close']);
});





