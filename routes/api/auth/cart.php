<?php

use App\Http\Controllers\Api\CartController;

Route::prefix('carts')->middleware(['subscribed'])->group(function () {
    Route::get('surcharges', [CartController::class, 'surcharges']);
    Route::get('/', [CartController::class, 'index']);
    Route::delete('{id}', [CartController::class, 'remove']);

    Route::middleware(['can.purchase', 'policy'])->group(function () {
        Route::post('/', [CartController::class, 'add'])
             ->name('add.cart.items');

        Route::put('{id}', [CartController::class, 'update'])
             ->name('update.cart.items');
    });


});

Route::prefix('carts/shop')->group(function () {
    Route::get('/', [CartController::class, 'shopIndex']);
    Route::delete('{id}', [CartController::class, 'removeShopCart']);

    Route::middleware(['can.add.shop.cart'])->group(function () {
        Route::post('/', [CartController::class, 'addShopCart'])
            ->name('add.shop.cart.items');

        Route::put('{id}', [CartController::class, 'updateShopCart'])
            ->name('update.shop.cart.items');
    });

    Route::prefix('items')->group(function () {
        Route::post('increment/{item}', [CartController::class, 'incrementShopCartItem'])
            ->name('increment.shop.cart.item');

        Route::post('decrement/{item}', [CartController::class, 'decrementShopCartItem'])
            ->name('decrement.shop.cart.item');
    });

});


