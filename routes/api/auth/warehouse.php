<?php

use App\Http\Controllers\Api\WarehouseController;
use App\Http\Controllers\Api\WarehouseInventoryController;

Route::prefix('warehouses')->group(function () {
    Route::post('/', [WarehouseController::class, 'create']);
    Route::put('{warehouse}', [WarehouseController::class, 'update']);
    Route::delete('{warehouse}', [WarehouseController::class, 'delete']);

    Route::get('inventories/items', [WarehouseInventoryController::class, 'index']);
});
