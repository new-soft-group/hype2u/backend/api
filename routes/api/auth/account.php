<?php

use App\Http\Controllers\Api\AccountController;

Route::prefix('accounts')->group(function () {
    Route::get('/', [AccountController::class, 'index'])
         ->middleware('permission:account.view');

    Route::get('{user}', [AccountController::class, 'show'])
         ->middleware('permission:account.view');

    Route::post('/', [AccountController::class, 'create'])
         ->middleware('permission:account.create');

    Route::put('{user}', [AccountController::class, 'update'])
         ->middleware('permission:account.edit');

    Route::get('{user}/payments/methods', [AccountController::class, 'getPaymentMethods'])
         ->middleware('permission:account.view');

    Route::get('{user}/addresses', [AccountController::class, 'getAddresses'])
         ->middleware('permission:account.view');

    Route::get('{user}/general', [AccountController::class, 'general'])
         ->middleware('permission:account.view');

    Route::get('{user}/wishlists', [AccountController::class, 'getWishlist'])
         ->middleware('permission:account.view');

    Route::get('{user}/orders', [AccountController::class, 'getOrders'])
         ->middleware('permission:account.view');

    Route::get('{user}/invoices', [AccountController::class, 'getInvoices'])
         ->middleware('permission:account.view');
});

Route::get('/revenue', [AccountController::class, 'revenue'])
    ->middleware('permission:account.view');
