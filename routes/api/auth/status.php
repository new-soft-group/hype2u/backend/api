<?php

use App\Http\Controllers\Api\StockStatusController;

Route::prefix('stocks')->group(function() {
    Route::get('status', [StockStatusController::class, 'index']);
});
