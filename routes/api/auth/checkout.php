<?php

use App\Http\Controllers\Api\CheckoutController;

Route::middleware(['can.purchase', 'subscribed', 'timeslot'])->group(function () {
    Route::post('checkout', [CheckoutController::class, 'checkout']);
});

Route::prefix('shop/checkout')->group(function () {
    Route::post('/', [CheckoutController::class, 'shopCheckout']);
});

