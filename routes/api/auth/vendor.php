<?php

use App\Http\Controllers\Api\VendorController;

Route::prefix('vendors')->group(function () {
    Route::get('/', [VendorController::class, 'index']);
    Route::post('/', [VendorController::class, 'create']);
    Route::put('{vendor}', [VendorController::class, 'update']);
    Route::delete('{vendor}', [VendorController::class, 'delete']);
});
