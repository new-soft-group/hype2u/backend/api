<?php

use App\Http\Controllers\Api\CategoryController;

Route::prefix('categories')->group(function () {
    Route::get('/all', [CategoryController::class, 'all'])
        ->middleware('permission:category.view');

    Route::post('/', [CategoryController::class, 'create'])
         ->middleware('permission:category.create');

    Route::patch('/', [CategoryController::class, 'update'])
         ->middleware('permission:category.edit');

    Route::delete('{category}', [CategoryController::class, 'delete'])
         ->middleware('permission:category.delete');
});
