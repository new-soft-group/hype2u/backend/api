<?php

use App\Http\Controllers\Api\SubscriptionController;

Route::prefix('subscriptions')->group(function () {
    Route::post('/', [SubscriptionController::class, 'subscribe'])
         ->middleware('permission:subscription.create');

    Route::post('upgrade', [SubscriptionController::class, 'upgrade'])
         ->middleware('permission:subscription.edit');

    Route::post('downgrade', [SubscriptionController::class, 'downgrade'])
         ->middleware('permission:subscription.edit');

    Route::post('{user}/cancel', [SubscriptionController::class, 'cancel'])
         ->middleware('permission:subscription.cancel');

    Route::get('/', [SubscriptionController::class, 'index']);

});
