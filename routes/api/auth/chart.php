<?php

Route::prefix('charts')->group(function () {
    Route::middleware('permission:sales.view')->group(function () {
        Route::get('overall', 'ChartController@overall');
        Route::get('sales', 'ChartController@totalSales');
        Route::get('orders', 'ChartController@totalOrders');
    });

    Route::middleware('permission:affiliate.sales')->group(function () {
        Route::get('affiliates/overall', 'ChartController@affiliateOverall');
        Route::get('affiliates/sales', 'ChartController@affiliateSales');
    });

    Route::middleware('permission:affiliate.member.sales')->group(function () {
        Route::get('affiliates/member/sales', 'ChartController@affiliateMemberSales');
    });
});
