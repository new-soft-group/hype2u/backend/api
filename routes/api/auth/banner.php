<?php

use App\Http\Controllers\Api\BannerController;

Route::prefix('banners')->group(function () {
    Route::get('positions', [BannerController::class, 'positions'])
         ->middleware('permission:banner.create|permission:banner.edit');

    Route::put('positions/{position}', [BannerController::class, 'updatePosition'])
         ->middleware('permission:banner.create|permission:banner.edit');

    Route::patch('positions', [BannerController::class, 'updatePositions'])
         ->middleware('permission:banner.create|permission:banner.edit');

    Route::post('/', [BannerController::class, 'create'])
         ->middleware('permission:banner.create');

    Route::patch('/', [BannerController::class, 'update'])
         ->middleware('permission:banner.edit');

    Route::delete('/', [BannerController::class, 'delete'])
         ->middleware('permission:banner.delete');
});
