<?php

use App\Http\Controllers\Api\RoleController;
use App\Http\Controllers\Api\SettingController;

Route::middleware('permission:system.editor')->group(function () {

    Route::prefix('roles')->group(function () {
        Route::get('/', [RoleController::class, 'roles']);
        Route::get('permissions', [RoleController::class, 'permissions']);
        Route::post('permissions', [RoleController::class, 'update']);
    });

    Route::prefix('settings')->group(function () {
        Route::get('/', [SettingController::class, 'index']);
        Route::post('/', [SettingController::class, 'update']);
    });

    Route::put('affiliate/discount/code/{affiliate}', 'AffiliateController@updateDiscountCode');
});
