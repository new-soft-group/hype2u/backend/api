<?php

use App\Http\Controllers\Api\HolidayController;

Route::prefix('holidays')->group(function () {
    Route::get('{holiday}', [HolidayController::class, 'show']);

    Route::middleware('permission:system.editor')->group(function () {
        Route::post('/', [HolidayController::class, 'create']);
        Route::put('{holiday}', [HolidayController::class, 'update']);
        Route::delete('{holiday}', [HolidayController::class, 'delete']);
    });
});
