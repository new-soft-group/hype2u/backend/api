<?php

use App\Http\Controllers\Api\CouponController;

Route::prefix('coupons')
    ->middleware('permission:system.editor')
    ->group(function () {
        Route::get('/', [CouponController::class, 'index']);
        Route::get('{coupon}', [CouponController::class, 'show']);
        Route::post('/', [CouponController::class, 'create']);
        Route::put('{coupon}', [CouponController::class, 'update']);
        Route::delete('{coupon}', [CouponController::class, 'delete']);
    });

