<?php

use App\Http\Controllers\Api\OrderController;

Route::prefix('orders')->group(function() {
    Route::get('/pending', [OrderController::class, 'pendingOrderCount']);
    Route::get('/', [OrderController::class, 'index']);
    Route::post('{user}', [OrderController::class, 'create'])
         ->middleware('permission:order.editor');
    Route::get('{order}', [OrderController::class, 'show']);
    Route::delete('{order}', [OrderController::class, 'delete']);
    Route::put('{order}/approve', [OrderController::class, 'approve']);
    Route::put('{order}/ship', [OrderController::class, 'ship']);
    Route::put('{order}/deliver', [OrderController::class, 'deliver']);
    Route::put('{order}/reject', [OrderController::class, 'reject']);
    Route::put('{order}/return', [OrderController::class, 'returnBack']);
    Route::put('{order}/return/partial', [OrderController::class, 'returnBackPartial']);
    Route::post('{order}/reserve/tags', [OrderController::class, 'scanReservedTags']);
    Route::put('{order}/reserve/tag', [OrderController::class, 'removeScannedReservedTag']);

    Route::post('{order}/timeslot', [OrderController::class, 'attachTimeslot']);
});

