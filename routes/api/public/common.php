<?php

use App\Http\Controllers\Api\BannerController;
use App\Http\Controllers\Api\BrandController;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\BusinessTypeController;
use App\Http\Controllers\Api\GenderController;
use App\Http\Controllers\Api\HolidayController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\TimeslotController;
use App\Http\Controllers\Api\VendorController;
use App\Http\Controllers\Api\WarehouseController;
use App\Http\Controllers\Api\PlanController;
use App\Http\Controllers\Api\CouponController;

Route::get('banners', [BannerController::class, 'index']);
Route::get('categories', [CategoryController::class, 'index']);
Route::get('business-types', [BusinessTypeController::class, 'index']);
Route::get('brands', [BrandController::class, 'index']);
Route::get('genders', [GenderController::class, 'index']);
Route::get('warehouses', [WarehouseController::class, 'index']);
Route::get('variants/options', [ProductController::class, 'getVariantOptions']);
Route::get('warehouses/{warehouse}', [WarehouseController::class, 'show']);
Route::get('vendors/{vendor}', [VendorController::class, 'show']);
Route::get('plans', [PlanController::class, 'index']);
Route::get('plans/all', [PlanController::class, 'all']);
Route::get('timeslots', [TimeslotController::class, 'index']);
Route::get('timeslots/types', [TimeslotController::class, 'types']);
Route::get('holidays', [HolidayController::class, 'index']);
Route::post('coupons/validate', [CouponController::class, 'validateCoupon']);


