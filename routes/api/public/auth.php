<?php

Route::namespace('Auth')->group(function () {
    Route::post('register', 'RegisterController@register');
    Route::post('login', 'LoginController@login');
    Route::post('social/login', 'LoginController@socialLogin');
    Route::post('forgot/password', 'ForgotPasswordController@sendResetLink');
    Route::post('reset/password', 'ResetPasswordController@resetPassword');
});
