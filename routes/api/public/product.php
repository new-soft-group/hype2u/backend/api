<?php

use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\ProductTypeController;

Route::prefix('products')->group(function () {
    Route::get('/', [ProductController::class, 'index']);
    Route::get('featured', [ProductController::class, 'getFeaturedProducts']);
    Route::get('filterable', [ProductController::class, 'getFilterableOptions']);
    Route::get('types', [ProductTypeController::class, 'index']);
});

Route::get('product/{product}', [ProductController::class, 'show']);


