# Compile PHP
FROM composer:latest as vendor
WORKDIR /app
COPY database/ database/
COPY composer.json ./
COPY composer.lock ./
RUN composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist \
    --no-dev

# Compile NodeJS
FROM node:14-alpine as node_modules
WORKDIR /app
RUN mkdir -p ./public
COPY package*.json webpack.mix.js ./
COPY public/fonts ./public/fonts
COPY public/images ./public/images
COPY resources/ ./resources
RUN npm install && npm run production

# Combine
FROM webdevops/php-apache:7.4-alpine
WORKDIR /app
ENV WEB_DOCUMENT_ROOT=/app/public
ENV PHP_MEMORY_LIMIT=1G
ENV PHP_POST_MAX_SIZE=100M
ENV PHP_UPLOAD_MAX_FILESIZE=100M

COPY --chown=application:application . .
RUN rm -rf ./.env.*
ARG env
COPY --chown=application:application ./.env.${env} ./.env

COPY --from=vendor /app/vendor/ ./vendor/
COPY --from=node_modules /app/public/js/ ./public/js/
COPY --from=node_modules /app/public/css/ ./public/css/
COPY --from=node_modules /app/public/mix-manifest.json ./public/mix-manifest.json

COPY supervisor-*.conf /opt/docker/etc/supervisor.d/
RUN chmod +x ./docker-entrypoint.sh
CMD ["/bin/sh", "./docker-entrypoint.sh"]