<?php


namespace Tests\Unit\Jobs;

use App\Jobs\SendInvoice;
use App\Mail\InvoicePaid;
use App\Models\Transaction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

// class SendInvoiceTest extends TestCase
// {
//     use RefreshDatabase;

//     protected function setUp(): void
//     {
//         parent::setUp();

//         $this->seed();
//     }

//     /**
//      * @throws \Throwable
//      */
//     public function testInvoiceSent()
//     {
//         Mail::fake();

//         Storage::fake('product');

//         $transaction = factory(Transaction::class)->create();

//         $job = new SendInvoice($transaction);

//         $job->handle();

//         Mail::assertSent(InvoicePaid::class, 1);
//     }
// }
