<?php


namespace Tests\Unit\Listeners;


use App\Events\StockArrival;
use App\Listeners\SendStockArrivalNotification;
use App\Models\NotifyQueue;
use App\Models\ProductStock;
use App\Models\Queuer;
use App\Notifications\StockArrivalNotification;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class SendStockArrivalNotificationTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @throws \Exception
     */
    public function testSendNotificationToQueuer()
    {
        $queue = NotifyQueue::factory()
                            ->has(Queuer::factory()
                                        ->count(2));

        /** @var ProductStock $stock */
        $stock = ProductStock::factory()->has($queue)->create();

        $queuers = $stock->queuers()->get();

        $listener = new SendStockArrivalNotification();

        Notification::fake();

        $listener->handle(new StockArrival($stock));

        Notification::assertTimesSent(2, StockArrivalNotification::class);

        foreach ($queuers as $queuer) {
            Notification::assertSentTo(
                [$queuer->user],
                StockArrivalNotification::class,
                function ($notification, $channels, $notifiable) use ($stock) {
                    return $notification->stock->id === $stock->id;
                }
            );
        }

        $this->assertDatabaseCount('queuers', 0);
    }
}
