<?php


namespace Tests\Unit\Validators;


use App\Collections\PolicyCollection;
use App\Models\CartItem;
use App\Models\PlanPolicy;
use App\Models\Product;
use App\Models\ProductType;
use App\Validators\Plan\Policy\PolicyRule;
use App\Validators\Plan\Policy\PolicyValidator;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PolicyValidatorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testShouldPassWhenPolicyMatch()
    {
        $vip = $this->factoryPolicy('VIP', 1, PlanPolicy::LOGICAL_NONE);

        $items = CartItem::factory()
                         ->count(1)
                         ->state(new Sequence(
                             ['quantity' => 1, 'product_type_id' => 1]
                         ))
                         ->create();

        $validator = new PolicyValidator(new PolicyCollection([$vip]), $items);

        $this->assertTrue($validator->validate());
    }

    public function testShouldFailMultipleItemWithSameTypeExceedQuantity()
    {
        $vip = $this->factoryPolicy('VIP', 1, PlanPolicy::LOGICAL_NONE);

        $items = CartItem::factory()
                         ->count(2)
                         ->state(new Sequence(
                             ['quantity' => 1, 'product_type_id' => 1],
                             ['quantity' => 1, 'product_type_id' => 1]
                         ))
                         ->create();

        $validator = new PolicyValidator(new PolicyCollection([$vip]), $items);

        $this->assertFalse($validator->validate());
    }

    public function testShouldFailWhenExtraItemPresentNotInPolicy()
    {
        $vip = $this->factoryPolicy('VIP', 1, PlanPolicy::LOGICAL_NONE);

        $items = CartItem::factory()
                         ->count(2)
                         ->state(new Sequence(
                             ['quantity' => 1, 'product_type_id' => 1],
                             ['quantity' => 1, 'product_type_id' => 2]
                         ))
                         ->create();

        $validator = new PolicyValidator(new PolicyCollection([$vip]), $items);

        $this->assertFalse($validator->validate());
    }

    public function testShouldFailWhenOrRulePassButExtraItemIsPresent()
    {
        $vip = $this->factoryPolicy('VIP', 3, PlanPolicy::LOGICAL_OR);
        $street = $this->factoryPolicy('Street', 1, PlanPolicy::LOGICAL_NONE);

        $items = CartItem::factory()
                         ->count(3)
                         ->state(new Sequence(
                             ['quantity' => 1, 'product_type_id' => 2],
                             ['quantity' => 1, 'product_type_id' => 1],
                             ['quantity' => 2, 'product_type_id' => 1]
                         ))
                         ->create();

        $validator = new PolicyValidator(new PolicyCollection([$vip, $street]), $items);

        $this->assertFalse($validator->validate());
    }

    public function testShouldPassWhenOrRuleIsSpecifiedAndQuantityMatch()
    {
        $vip = $this->factoryPolicy('VIP', 1, PlanPolicy::LOGICAL_OR);
        $street = $this->factoryPolicy('Street', 4, PlanPolicy::LOGICAL_NONE);

        $items = CartItem::factory()
                         ->count(2)
                         ->state(new Sequence(
                             ['quantity' => 2, 'product_type_id' => 2],
                             ['quantity' => 2, 'product_type_id' => 2]
                         ))
                         ->create();

        $validator = new PolicyValidator(new PolicyCollection([$vip, $street]), $items);

        $this->assertTrue($validator->validate());
    }

    public function testShouldFailedWhenQuantityAndTypeIsMatchButWithExtraItemPresent()
    {
        $vip = $this->factoryPolicy('VIP', 2, PlanPolicy::LOGICAL_OR);
        $streetOrPremium = $this->factoryPolicy(['Street', 'Premium'], 2, PlanPolicy::LOGICAL_NONE);

        $items = CartItem::factory()
                         ->count(3)
                         ->state(new Sequence(
                             ['quantity' => 1, 'product_type_id' => 2],
                             ['quantity' => 1, 'product_type_id' => 3],
                             ['quantity' => 1, 'product_type_id' => 1]
                         ))
                         ->create();

        $validator = new PolicyValidator(new PolicyCollection([$vip, $streetOrPremium]), $items);

        $this->assertFalse($validator->validate());
    }

    public function testShouldPassWhenQuantityAndTypeWithCombinationRuleApplied()
    {
        $vip = $this->factoryPolicy('VIP', 2, PlanPolicy::LOGICAL_OR);
        $streetOrPremium = $this->factoryPolicy(['Street', 'Premium'], 2, PlanPolicy::LOGICAL_NONE);

        $items = CartItem::factory()
                         ->count(2)
                         ->state(new Sequence(
                             ['quantity' => 1, 'product_type_id' => 2],
                             ['quantity' => 1, 'product_type_id' => 3]
                         ))
                         ->create();

        $validator = new PolicyValidator(new PolicyCollection([$vip, $streetOrPremium]), $items);

        $this->assertTrue($validator->validate());
    }

    public function testShouldPassWhenItemMatchMultipleCombinationRules()
    {
        $vip = $this->factoryPolicy('VIP', 2, PlanPolicy::LOGICAL_OR);
        $street = $this->factoryPolicy('Street', 3, PlanPolicy::LOGICAL_AND);
        $premium = $this->factoryPolicy('Premium', 4, PlanPolicy::LOGICAL_NONE);

        $items = CartItem::factory()
                         ->count(3)
                         ->state(new Sequence(
                             ['quantity' => 3, 'product_type_id' => 3],
                             ['quantity' => 1, 'product_type_id' => 3],
                             ['quantity' => 1, 'product_type_id' => 2],
                             ['quantity' => 1, 'product_type_id' => 2],
                             ['quantity' => 1, 'product_type_id' => 2]
                         ))
                         ->create();

        $validator = new PolicyValidator(new PolicyCollection([$vip, $street, $premium]), $items);

        $this->assertTrue($validator->validate());
    }

    public function testShouldPassWhenSpecifiedPolicyProductNotExist()
    {
        $types = ProductType::factory()
                            ->count(3)
                            ->state(new Sequence(
                                ['name' => 'VIP'],
                                ['name' => 'Street'],
                                ['name' => 'Premium']
                            ));

        $policy = PlanPolicy::factory()
                            ->hasAttached($types, [], 'rules')
                            ->state([
                                'quantity' => 1,
                                'logical' => PlanPolicy::LOGICAL_NONE
                            ])
                            ->create();

        $items = CartItem::factory()
                         ->count(1)
                         ->state(new Sequence(
                             ['quantity' => 1, 'product_type_id' => 1]
                         ))
                         ->create();

        $validator = new PolicyValidator(new PolicyCollection([$policy]), $items);

        $this->assertTrue($validator->validate());
    }

    public function testShouldFailWhenNoRuleApply()
    {
        $items = CartItem::factory()
                         ->count(2)
                         ->state(new Sequence(
                             ['quantity' => 2, 'product_type_id' => 3],
                             ['quantity' => 1, 'product_type_id' => 3]
                         ))
                         ->create();

        $validator = new PolicyValidator(new PolicyCollection(), $items);

        $this->assertFalse($validator->validate());
    }

    protected function factoryPolicy($name, $quantity, $logical, $pivot = []): PlanPolicy
    {
        if(is_array($name)) {
            $names = array_map(function ($v) {
                return ['name' => $v];
            }, $name);

            $type = ProductType::factory()
                               ->count(count($name))
                               ->has(Product::factory())
                               ->state(new Sequence(...$names));
        }
        else {
            $type = ProductType::factory()
                               ->has(Product::factory())
                               ->state(['name' => $name]);
        }

        return PlanPolicy::factory()
                         ->hasAttached($type, $pivot, 'rules')
                         ->state([
                             'quantity' => $quantity,
                             'logical' => $logical
                         ])
                         ->create();
    }
}
