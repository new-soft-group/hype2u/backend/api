<?php


namespace Tests\Unit\Validators;


use App\Models\Plan;
use App\Models\Surcharge;
use App\Validators\Plan\PlanValidator;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PlanValidatorTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testValidatedMessage()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->state(['purchase_amount' => 1000, 'exchange_limit' => 1])
                    ->create();

        $validator = new PlanValidator($plan);

        $validator->setExchange(2)
                  ->setPurchaseAmount(2000);

        $this->assertFalse($validator->validate());
        $this->assertNotNull($validator->getMessage());
    }

    public function testFailWhenPurchaseAmountExceedThreshold()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->state(['purchase_amount' => 500, 'exchange_limit' => 1])
                    ->has(Surcharge::factory()
                                   ->count(2)
                                   ->state(new Sequence(
                                       ['threshold' => 1000],
                                       ['threshold' => 1500]
                                   )))
                    ->create();

        $validator = new PlanValidator($plan);

        $validator->setExchange(1)
                  ->setPurchaseAmount(2000);

        $this->assertFalse($validator->validate());
        $this->assertNotNull($validator->getMessage());
    }

    public function testPassWhenPurchaseAmountWithinThreshold()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->state(['purchase_amount' => 500, 'exchange_limit' => 1])
                    ->has(Surcharge::factory()
                                   ->count(3)
                                   ->state(new Sequence(
                                       ['threshold' => 1000],
                                       ['threshold' => 1500],
                                       ['threshold' => 2000]
                                   )))
                    ->create();

        $validator = new PlanValidator($plan);

        $validator->setExchange(1)
                  ->setPurchaseAmount(1500);

        $this->assertTrue($validator->validate());
        $this->assertEquals('', $validator->getMessage());
    }
}
