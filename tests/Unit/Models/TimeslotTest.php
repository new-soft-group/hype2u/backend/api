<?php


namespace Tests\Unit\Models;


use App\Exceptions\DuplicateTimeslotException;
use App\Models\Order;
use App\Models\Timeslot;
use App\Models\TimeslotAllocation;
use App\Models\TimeslotType;
use Carbon\Carbon;
use Database\Seeders\TimeslotSeeder;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class TimeslotTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();

        $this->seed(TimeslotSeeder::class);
    }

    public function testCreateTimeslot()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->create();

        $this->assertTrue(is_string($timeslot->start_time));
        $this->assertTrue(is_string($timeslot->end_time));
        $this->assertInstanceOf(TimeslotType::class, $timeslot->type);
        $this->assertDatabaseCount('timeslots', 1);
    }

    public function testAllocateTimeslot()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->create();
        /** @var Order $order */
        $order = Order::factory()->create();

        $date = Carbon::now()->format('Y-m-d');

        $allocated = $timeslot->allocate($order, $date);

        $startTimestamp = Carbon::parse(strtotime("$date $timeslot->start_time"))
                                ->format('Y-m-d H:i:s');

        $endTimestamp = Carbon::parse(strtotime("$date $timeslot->end_time"))
                              ->format('Y-m-d H:i:s');

        $this->assertEquals($allocated->start_at, $startTimestamp);
        $this->assertEquals($allocated->end_at, $endTimestamp);
        $this->assertDatabaseHas('timeslot_allocations', [
            'timeslot_id' => $timeslot->id,
            'order_id' => $order->id,
            'start_at' => $startTimestamp,
            'end_at' => $endTimestamp
        ]);
    }

    public function testShouldFailWhenAllocateTimeslotToOrderTwice()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()
                            ->has(TimeslotAllocation::factory(), 'allocations')
                            ->create();

        /** @var Order $order */
        $order = Order::find(1);

        $date = Carbon::now()->format('Y-m-d');

        $this->expectException(DuplicateTimeslotException::class);

        $timeslot->allocate($order, $date);
    }

    public function testShouldFailWhenAllocateTimeslotToOrderTwiceUsingKey()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()
                            ->has(TimeslotAllocation::factory(), 'allocations')
                            ->create();

        /** @var Order $order */
        $order = Order::find(1);

        $date = Carbon::now()->format('Y-m-d');

        $this->expectException(DuplicateTimeslotException::class);

        $timeslot->allocate($order->id, $date);
    }

    public function testFullyBook()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()
                            ->has(TimeslotAllocation::factory()->count(1), 'allocations')
                            ->state(['limit' => 1])
                            ->create();

        $this->assertTrue($timeslot->fullyBooked());
    }

    public function testFilterByMonth()
    {
        TimeslotAllocation::factory()
                          ->count(3)
                          ->state(new Sequence(
                              [
                                  'start_at' => now()->subMonth()->toDateTimeString(),
                                  'end_at' => now()->subMonth()->toDateTimeString()
                              ],
                              [
                                  'start_at' => now()->toDateTimeString(),
                                  'end_at' => now()->toDateTimeString()
                              ],
                              [
                                  'start_at' => now()->subMonth()->toDateTimeString(),
                                  'end_at' => now()->subMonth()->toDateTimeString()
                              ]
                          ))
                          ->create();

        $this->assertCount(1, TimeslotAllocation::month(now()->format('Y-m'))->get());
    }

    public function testFilterByWeek()
    {
        TimeslotAllocation::factory()
                          ->count(3)
                          ->state(new Sequence(
                              [
                                  'start_at' => now()->subMonth()->toDateTimeString(),
                                  'end_at' => now()->subMonth()->toDateTimeString()
                              ],
                              [
                                  'start_at' => now()->toDateTimeString(),
                                  'end_at' => now()->toDateTimeString()
                              ],
                              [
                                  'start_at' => now()->addDay()->toDateTimeString(),
                                  'end_at' => now()->addDay()->toDateTimeString()
                              ]
                          ))
                          ->create();

        $this->assertCount(2, TimeslotAllocation::week(now()->toDateString())->get());
    }

    public function testFilterByDay()
    {
        TimeslotAllocation::factory()
                          ->count(3)
                          ->state(new Sequence(
                              [
                                  'start_at' => now()->toDateTimeString(),
                                  'end_at' => now()->toDateTimeString()
                              ],
                              [
                                  'start_at' => now()->addDay()->toDateTimeString(),
                                  'end_at' => now()->addDay()->toDateTimeString()
                              ],
                              [
                                  'start_at' => now()->addDays(2)->toDateTimeString(),
                                  'end_at' => now()->addDays(2)->toDateTimeString()
                              ]
                          ))
                          ->create();

        $this->assertCount(1, TimeslotAllocation::day(now()->toDateString())->get());
    }
}
