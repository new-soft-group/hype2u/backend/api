<?php


namespace Tests\Unit\Models;


use App\Models\Plan;
use App\Models\Surcharge;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class SurchargeTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testGetSurchargeAmount()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->has(Surcharge::factory()->count(2)->state(new Sequence(
                        ['threshold' => 3200, 'charge' => 200],
                        ['threshold' => 3500, 'charge' => 500]
                    )))
                    ->create(['amount' => 3000]);

        $this->assertEquals(500, $plan->getSurchargeAmount(3400));
    }
}
