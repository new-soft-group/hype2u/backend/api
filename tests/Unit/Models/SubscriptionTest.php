<?php


namespace Tests\Unit\Models;


use App\Models\Invoice;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\MockSubscription;

class SubscriptionTest extends TestCase
{
    use RefreshDatabase, MockSubscription;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testMockSubscription()
    {
        $user = $this->subscribedUser();

        $this->assertTrue($user->hasStripeId());
        $this->assertCount(1, $user->subscriptions);
    }
}
