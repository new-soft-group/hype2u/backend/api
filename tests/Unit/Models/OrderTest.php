<?php


namespace Tests\Unit\Models;


use App\Models\Order;
use App\Models\ProductStockInventory;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class OrderTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testRetrieveTrashedTrackingTags()
    {
        /** @var Order $order */
        $order = Order::factory()
                      ->hasAttached(ProductStockInventory::factory()
                                                         ->state([
                                                             'deleted_at' => now()
                                                         ]),
                          [], 'tracks')
                      ->create();

        $this->assertCount(1, $order->tracks);
    }
}
