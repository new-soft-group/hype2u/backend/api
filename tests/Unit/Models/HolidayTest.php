<?php

namespace Tests\Unit\Models;


use App\Models\Holiday;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HolidayTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testGivenDateCrashOnHoliday()
    {
        Holiday::factory()->state([
            'from' => '2020-01-01',
            'to' => '2020-01-03',
        ])->create();

        $this->assertTrue(Holiday::crashOn('2020-01-03'));
    }

    public function testGivenDateCrashOnHolidayBoundary()
    {
        Holiday::factory()->state([
            'from' => '2020-01-01',
            'to' => '2020-01-03',
        ])->create();

        $this->assertTrue(Holiday::crashOn('2020-01-01'));
    }

    public function testGivenDateCrashOnHolidayBoundary2()
    {
        Holiday::factory()->state([
            'from' => '2020-01-01',
            'to' => '2020-01-01',
        ])->create();

        $this->assertTrue(Holiday::crashOn('2020-01-01'));
    }

    public function testGivenDateCrashOnHolidayBoundary3()
    {
        Holiday::factory()->state([
            'from' => '2020-01-01',
            'to' => '2020-01-02',
        ])->create();

        $this->assertTrue(Holiday::crashOn('2020-01-02'));
    }
}
