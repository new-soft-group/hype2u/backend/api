<?php


namespace Tests\Unit\Events;


use App\Events\UserLoggedIn;
use App\Listeners\MergeWithGuestCart;
use App\Models\Cart;
use App\Models\Product;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

// class UserLoggedInEventTest extends TestCase
// {
//     use RefreshDatabase;
    
//     protected function setUp(): void
//     {
//         parent::setUp();
        
//         $this->seed([\VariantSeeder::class]);
//     }
    
//     /**
//      * @throws \Throwable
//      */
//     public function testMergeCartAfterUserLoggedInEventFired()
//     {
//         Storage::fake('product');
        
//         $product = factory(Product::class)->state('option')->create();
        
//         $cart = factory(Cart::class)->create();
//         $cart->addToCart([
//             'product_id' => $product->id,
//             'product_stock_id' => $product->stocks->get(0)->id,
//             'quantity' => 1
//         ]);
    
//         $request = new Request();
//         $request->headers->set(Cart::SESSION_HEADER, $cart->session_id);
        
//         $user = factory(User::class)->state('user')->create();
        
//         $listener = new MergeWithGuestCart();
//         $listener->handle(new UserLoggedIn($request, $user));
        
//         $this->assertDatabaseCount('carts', 1);
//         $this->assertDatabaseHas('carts', [
//             'user_id' => $user->id,
//             'session_id' => null
//         ]);
//         $this->assertDatabaseCount('cart_items', 1);
//         $this->assertDatabaseHas('cart_items', [
//             'cart_id' => $user->cart->id,
//             'product_id' => $product->id,
//             'product_stock_id' => $product->stocks->get(0)->id,
//             'quantity' => 1
//         ]);
//     }
// }
