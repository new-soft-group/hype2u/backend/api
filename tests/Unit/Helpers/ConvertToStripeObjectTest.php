<?php


namespace Tests\Unit\Helpers;


use Stripe\PaymentIntent;
use Tests\TestCase;

class ConvertToStripeObjectTest extends TestCase
{
    public function testConvertToStripeObject()
    {
        $payload = [
            'id' => 'evt_1Hs09rJmw7lQtYWq3Jw9DJYC',
            'object' => 'event',
            'api_version' => '2020-03-02',
            'created' => 1606457415,
            'data' => [
                'object' => [
                    'id' => 'pi_1Hs09qJmw7lQtYWqThARuEQV',
                    'object' => 'payment_intent',
                    'amount' => 50000,
                    'amount_capturable' => 0,
                    'amount_received' => 50000,
                    'application' => NULL,
                    'application_fee_amount' => NULL,
                    'canceled_at' => NULL,
                    'cancellation_reason' => NULL,
                    'capture_method' => 'automatic',
                    'charges' => [
                        'object' => 'list',
                        'data' => [
                            0 => [
                                'id' => 'ch_1Hs09qJmw7lQtYWql9s2TeSI',
                                'object' => 'charge',
                                'amount' => 50000,
                                'amount_captured' => 50000,
                                'amount_refunded' => 0,
                                'application' => NULL,
                                'application_fee' => NULL,
                                'application_fee_amount' => NULL,
                                'balance_transaction' => 'txn_1Hs09qJmw7lQtYWq9n1pO8PY',
                                'billing_details' => [
                                    'address' => [
                                        'city' => 'Over 9 levels deep, aborting normalization',
                                        'country' => 'Over 9 levels deep, aborting normalization',
                                        'line1' => 'Over 9 levels deep, aborting normalization',
                                        'line2' => 'Over 9 levels deep, aborting normalization',
                                        'postal_code' => 'Over 9 levels deep, aborting normalization',
                                        'state' => 'Over 9 levels deep, aborting normalization',
                                    ],
                                    'email' => NULL,
                                    'name' => 'Zoie Nitzsche',
                                    'phone' => NULL,
                                ],
                                'calculated_statement_descriptor' => 'Stripe',
                                'captured' => true,
                                'created' => 1606457414,
                                'currency' => 'myr',
                                'customer' => 'cus_ISufqtZXrIDAdM',
                                'description' => NULL,
                                'destination' => NULL,
                                'dispute' => NULL,
                                'disputed' => false,
                                'failure_code' => NULL,
                                'failure_message' => NULL,
                                'fraud_details' => [
                                ],
                                'invoice' => NULL,
                                'livemode' => false,
                                'metadata' => [
                                ],
                                'on_behalf_of' => NULL,
                                'order' => NULL,
                                'outcome' => [
                                    'network_status' => 'approved_by_network',
                                    'reason' => NULL,
                                    'risk_level' => 'normal',
                                    'risk_score' => 14,
                                    'seller_message' => 'Payment complete.',
                                    'type' => 'authorized',
                                ],
                                'paid' => true,
                                'payment_intent' => 'pi_1Hs09qJmw7lQtYWqThARuEQV',
                                'payment_method' => 'pm_1HrypBJmw7lQtYWqtRjYJ8Yl',
                                'payment_method_details' => [
                                    'card' => [
                                        'brand' => 'Over 9 levels deep, aborting normalization',
                                        'checks' => 'Over 9 levels deep, aborting normalization',
                                        'country' => 'Over 9 levels deep, aborting normalization',
                                        'exp_month' => 'Over 9 levels deep, aborting normalization',
                                        'exp_year' => 'Over 9 levels deep, aborting normalization',
                                        'fingerprint' => 'Over 9 levels deep, aborting normalization',
                                        'funding' => 'Over 9 levels deep, aborting normalization',
                                        'installments' => 'Over 9 levels deep, aborting normalization',
                                        'last4' => 'Over 9 levels deep, aborting normalization',
                                        'network' => 'Over 9 levels deep, aborting normalization',
                                        'three_d_secure' => 'Over 9 levels deep, aborting normalization',
                                        'wallet' => 'Over 9 levels deep, aborting normalization',
                                    ],
                                    'type' => 'card',
                                ],
                                'receipt_email' => NULL,
                                'receipt_number' => NULL,
                                'receipt_url' => 'https://pay.stripe.com/receipts/acct_1H9n4UJmw7lQtYWq/ch_1Hs09qJmw7lQtYWql9s2TeSI/rcpt_ISw17ghFc2Ke3vso8FSt4t28dipja1o',
                                'refunded' => false,
                                'refunds' => [
                                    'object' => 'list',
                                    'data' => [
                                    ],
                                    'has_more' => false,
                                    'total_count' => 0,
                                    'url' => '/v1/charges/ch_1Hs09qJmw7lQtYWql9s2TeSI/refunds',
                                ],
                                'review' => NULL,
                                'shipping' => NULL,
                                'source' => NULL,
                                'source_transfer' => NULL,
                                'statement_descriptor' => NULL,
                                'statement_descriptor_suffix' => NULL,
                                'status' => 'succeeded',
                                'transfer_data' => NULL,
                                'transfer_group' => NULL,
                            ],
                        ],
                        'has_more' => false,
                        'total_count' => 1,
                        'url' => '/v1/charges?payment_intent=pi_1Hs09qJmw7lQtYWqThARuEQV',
                    ],
                    'client_secret' => 'pi_1Hs09qJmw7lQtYWqThARuEQV_secret_0d5mHY7zGj6LGfDBMiU9PqMRT',
                    'confirmation_method' => 'automatic',
                    'created' => 1606457414,
                    'currency' => 'myr',
                    'customer' => 'cus_ISufqtZXrIDAdM',
                    'description' => NULL,
                    'invoice' => NULL,
                    'last_payment_error' => NULL,
                    'livemode' => false,
                    'metadata' => [
                    ],
                    'next_action' => NULL,
                    'on_behalf_of' => NULL,
                    'payment_method' => 'pm_1HrypBJmw7lQtYWqtRjYJ8Yl',
                    'payment_method_options' => [
                        'card' => [
                            'installments' => NULL,
                            'network' => NULL,
                            'request_three_d_secure' => 'automatic',
                        ],
                    ],
                    'payment_method_types' => [
                        0 => 'card',
                    ],
                    'receipt_email' => NULL,
                    'review' => NULL,
                    'setup_future_usage' => NULL,
                    'shipping' => NULL,
                    'source' => NULL,
                    'statement_descriptor' => NULL,
                    'statement_descriptor_suffix' => NULL,
                    'status' => 'succeeded',
                    'transfer_data' => NULL,
                    'transfer_group' => NULL,
                ],
            ],
            'livemode' => false,
            'pending_webhooks' => 1,
            'request' => [
                'id' => 'req_VDnJBoIeWr6pUT',
                'idempotency_key' => NULL,
            ],
            'type' => 'payment_intent.succeeded',
        ];

        $object = convertToStripeObject($payload);

        $this->assertInstanceOf(PaymentIntent::class, $object);
    }
}
