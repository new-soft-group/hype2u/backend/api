<?php


namespace Tests\Unit\Seeders;


use App\Models\Role;
use App\Models\User;
use Database\Seeders\AdminSeeder;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AdminSeederTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed([
            PermissionSeeder::class,
            AdminSeeder::class
        ]);
    }

    public function testAdminShouldExist()
    {
        $user = User::find(1);

        $this->assertNotNull($user);
        $this->assertTrue($user->hasRole(Role::ADMIN));
        $this->assertDatabaseCount('users', 1);
    }
}
