<?php


namespace Tests\Unit\Collections;


use App\Collections\PolicyCollection;
use App\Models\PlanPolicy;
use App\Models\ProductType;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class PolicyCollectionTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testGetPrimeTargetIds()
    {
        /** @var PolicyCollection $policies */
        $policies = PlanPolicy::factory()
                              ->count(1)
                              ->hasAttached(ProductType::factory(), ['prime' => true], 'rules')
                              ->create();

        $this->assertCount(1, $policies->getPrimeRules());
    }

    public function testGetPrimeTargetIdsShouldReturnEmptyArray()
    {
        /** @var PolicyCollection $policies */
        $policies = PlanPolicy::factory()
                              ->count(1)
                              ->hasAttached(ProductType::factory(), [], 'rules')
                              ->create();

        $this->assertEmpty($policies->getPrimeRules());
    }
}
