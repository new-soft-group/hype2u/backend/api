<?php


namespace Tests\Unit\Middlewares;


use App\Exceptions\InvalidSubscriptionException;
use App\Http\Middleware\Subscribed;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Http\Request;
use Tests\TestCase;
use Tests\Traits\MockSubscription;

class SubscribedTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @throws InvalidSubscriptionException
     * @throws \App\Exceptions\RequireSubscriptionException
     */
    public function testShouldThrowExceptionWhenInvalidStatus()
    {
        $request = new Request();
        $request->setUserResolver(function () {
            $factory = Subscription::factory()->state(['stripe_status' => 'past_due']);
            return User::factory()->has($factory)->create();
        });

        $middleware = new Subscribed();

        $this->expectException(InvalidSubscriptionException::class);

        $middleware->handle($request, function () {});
    }
}
