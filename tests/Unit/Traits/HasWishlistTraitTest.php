<?php

namespace Tests\Unit\Traits;

use App\Models\Product;
use App\Models\User;
use App\Models\Wishlist;
use App\Models\WishlistItem;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class HasWishlistTraitTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testCreateWishlist()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $wishlist = $user->getWishlist();

        $this->assertNotNull($wishlist);
        $this->assertCount(0, $wishlist->items);
        $this->assertDatabaseHas('wishlists', [
            'id' => $wishlist->id,
            'user_id' => $user->id
        ]);
    }

    public function testAddItemToWishlist()
    {
        /** @var Product $product */
        $product = Product::factory()->create();

        /** @var Wishlist $wishlist */
        $wishlist = Wishlist::factory()->create();

        $item = $wishlist->addItem($product);

        $this->assertNotNull($item);
        $this->assertDatabaseCount('wishlist_items', 1);
        $this->assertDatabaseHas('wishlist_items', [
            'wishlist_id' => $wishlist->id,
            'product_id' => $product->id,
        ]);
    }

    public function testAddDuplicateWishlistItem()
    {
        /** @var Wishlist $wishlist */
        $wishlist = Wishlist::factory()
                            ->has(WishlistItem::factory(), 'items')
                            ->create();

        $item = $wishlist->addItem($wishlist->items->first()->product_id);

        $this->assertNotNull($item);
        $this->assertEquals($item->toArray(), $wishlist->items->first()->toArray());
        $this->assertDatabaseCount('wishlist_items', 1);
    }

    public function testAppendNewWishlistItem()
    {
        /** @var Wishlist $wishlist */
        $wishlist = Wishlist::factory()
                            ->has(WishlistItem::factory(), 'items')
                            ->create();

        $product = Product::factory()->create();

        $item = $wishlist->addItem($product);

        $this->assertNotNull($item);
        $this->assertDatabaseCount('wishlist_items', 2);
    }

    public function testDeleteWishlistItem()
    {
        /** @var Wishlist $wishlist */
        $wishlist = Wishlist::factory()
                            ->has(WishlistItem::factory()->count(3), 'items')
                            ->create();

        $item = $wishlist->items->first();

        $result = $wishlist->removeItem($item->product_id);

        $this->assertTrue($result);
        $this->assertCount(2, $wishlist->items()->get());
        $this->assertDatabaseCount('wishlists', 1);
        $this->assertDatabaseCount('wishlist_items', 2);
        $this->assertDatabaseMissing('wishlist_items', [
            'id' => $item->id,
            'wishlist_id' => $wishlist->id
        ]);
    }
}
