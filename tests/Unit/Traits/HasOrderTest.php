<?php


namespace Tests\Unit\Traits;


use App\Collections\Carry\CarryForward;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStatus;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\User;
use Database\Seeders\OrderStatusSeeder;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class HasOrderTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(OrderStatusSeeder::class);
    }

    public function testHasIncompleteOrderShouldReturnFalse()
    {
        $orders = Order::factory()
                       ->count(4)
                       ->state(new Sequence(
                           ['order_status_id' => OrderStatus::FAILED],
                           ['order_status_id' => OrderStatus::CANCELLED],
                           ['order_status_id' => OrderStatus::PENDING],
                           ['order_status_id' => OrderStatus::REJECTED]
                       ));

        /** @var User $user */
        $user = User::factory()->has($orders)->create();

        $this->assertCount(1, $user->ongoingOrders()->get());
    }

    public function testHasIncompleteOrderShouldReturnTrue()
    {
        $orders = Order::factory()
                       ->count(3)
                       ->state(new Sequence(
                           ['order_status_id' => OrderStatus::PENDING],
                           ['order_status_id' => OrderStatus::DELIVERED, 'returned_at' => now()],
                           ['order_status_id' => OrderStatus::DELIVERED]
                       ));

        /** @var User $user */
        $user = User::factory()->has($orders)->create();

        $this->assertCount(2, $user->ongoingOrders()->get());
    }

    public function testGetPartialReturnOrder()
    {
        /** @var User $user */
        $user = User::factory()->create();
        /**
         * Completed order
         */
        Order::factory()
             ->hasAttached(ProductStockInventory::factory()->count(2), ['scanned_at' => now()], 'tracks')
             ->hasAttached(ProductStockInventory::factory(), [], 'tracks')
             ->state([
                 'user_id' => $user->id,
                 'order_status_id' => OrderStatus::PARTIAL_RETURNED
             ])
             ->create();

        Order::factory()
             ->count(2)
             ->hasAttached(ProductStockInventory::factory()->count(2), ['scanned_at' => now()], 'tracks')
             ->state([
                 'user_id' => $user->id,
                 'order_status_id' => OrderStatus::COMPLETED,
                 'returned_at' => now()
             ])
             ->create();

        $this->assertDatabaseCount('orders', 3);
        $this->assertCount(1, $user->partialReturnedOrders()->get());
    }

    public function testRemainingExchangeShouldReturnOrdersHappenOnThisMonth()
    {
        $factory = ProductStockInventory::factory();

        /** @var User $user */
        $user = User::factory()->create();

        Order::factory()
             ->state([
                 'user_id' => $user->id,
                 'order_status_id' => OrderStatus::REJECTED
             ])->create();

        Order::factory()
             ->hasAttached($factory, ['scanned_at' => now()], 'tracks')
             ->state([
                 'user_id' => $user->id,
                 'order_status_id' => OrderStatus::COMPLETED,
                 'returned_at' => now()
             ])->create();

        $this->assertCount(1, $user->ongoingMonthlyOrders()->get());
    }

    public function testRemainingExchangeShouldIncludePartialReturned()
    {
        $factory = ProductStockInventory::factory();

        /** @var User $user */
        $user = User::factory()->create();

        Order::factory()
             ->hasAttached($factory, [], 'tracks')
             ->state([
                 'user_id' => $user->id,
                 'order_status_id' => OrderStatus::COMPLETED,
                 'returned_at' => now()
             ])->create();

        Order::factory()
             ->hasAttached($factory->count(2), ['scanned_at' => now()->subMonth()], 'tracks')
             ->hasAttached($factory, [], 'tracks')
             ->state([
                 'user_id' => $user->id,
                 'order_status_id' => OrderStatus::PARTIAL_RETURNED,
                 'created_at' => now()->subMonth()
             ])->create();

        Order::factory()
             ->hasAttached($factory, ['scanned_at' => now()->subMonth()], 'tracks')
             ->state([
                 'user_id' => $user->id,
                 'order_status_id' => OrderStatus::COMPLETED,
                 'returned_at' => now()->subMonth(),
                 'created_at' => now()->subMonth()
             ])->create();

        $this->assertCount(2, $user->ongoingMonthlyOrders()->get());
    }

    public function testItemHaveNotBeenReturnYet()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $inventory = ProductStockInventory::factory();

        $factory = ProductStock::factory()
                               ->product()
                               ->has($inventory->count(2), 'inventories');

        /** @var Product $product */
        $product = Product::factory()
                          ->has($factory, 'stocks')
                          ->create();

        /** @var Order $order */
        $order = Order::factory()
                      ->has(OrderItem::factory()
                                     ->state([
                                         'product_id' => $product->id,
                                         'product_stock_id' => 1
                                     ]),
                          'items')
                      ->state([
                          'user_id' => $user->id,
                          'order_status_id' => OrderStatus::COMPLETED
                      ])->create();

        $order->tracks()->sync([1 => ['scanned_at' => now()]]);

        /** @var Order $order2 */
        $order2 = Order::factory()
                      ->has(OrderItem::factory()
                                     ->state([
                                         'product_id' => $product->id,
                                         'product_stock_id' => 1
                                     ]),
                          'items')
                      ->state([
                          'user_id' => $user->id,
                          'order_status_id' => OrderStatus::COMPLETED
                      ])->create();

        $order2->tracks()->sync([2]);

        $items = $user->carryForwardItems();

        $this->assertCount(1, $items);

        /** @var CarryForward $carryItem */
        $carryItem = $items->get(0);
        $this->assertEquals($product->id, $carryItem->getProduct()->id);
        $this->assertEquals(1, $carryItem->getStockKey());
        $this->assertEquals($product->type->id, $carryItem->getProduct()->product_type_id);
        $this->assertEquals(1, $carryItem->getQuantity());
    }

    public function testItemHaveNotBeenReturnYetShouldGroupStockKeys()
    {
        /** @var User $user */
        $user = User::factory()->create();

        $stock = ProductStock::factory()->product();
        $inventory = ProductStockInventory::factory();

        Product::factory()
               ->has($stock->has($inventory, 'inventories'), 'stocks')
               ->create();

        Product::factory()
               ->has($stock->has($inventory, 'inventories'), 'stocks')
               ->create();

        Product::factory()
               ->has($stock->has($inventory->count(2), 'inventories'), 'stocks')
               ->create();

        /** @var Order $order */
        $order = Order::factory()
                      ->has(OrderItem::factory()
                                     ->count(4)
                                     ->state(new Sequence(
                                         ['product_id' => 1, 'product_stock_id' => 1],
                                         ['product_id' => 2, 'product_stock_id' => 2],
                                         ['product_id' => 3, 'product_stock_id' => 3],
                                         ['product_id' => 3, 'product_stock_id' => 3]
                                     )), 'items')
                      ->state([
                          'user_id' => $user->id,
                          'order_status_id' => OrderStatus::DELIVERED
                      ])->create();

        $order->tracks()->sync([1 => ['scanned_at' => now()], 2, 3, 4]);

        $items = $user->carryForwardItems();

        $this->assertCount(2, $items);

        /** @var CarryForward $carryItem1 */
        $carryItem1 = $items->get(0);

        $this->assertEquals($order->id, $carryItem1->getOrderKey());
        $this->assertEquals(2, $carryItem1->getProduct()->id);
        $this->assertEquals(2, $carryItem1->getStockKey());
        $this->assertEquals(2, $carryItem1->getProduct()->product_type_id);
        $this->assertEquals(1, $carryItem1->getQuantity());
        /** @var CarryForward $carryItem2 */
        $carryItem2 = $items->get(1);
        $this->assertEquals($order->id, $carryItem2->getOrderKey());
        $this->assertEquals(3, $carryItem2->getProduct()->id);
        $this->assertEquals(3, $carryItem2->getStockKey());
        $this->assertEquals(3, $carryItem2->getProduct()->product_type_id);
        $this->assertEquals(2, $carryItem2->getQuantity());
    }
}
