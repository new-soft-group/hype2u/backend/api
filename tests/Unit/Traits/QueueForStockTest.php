<?php


namespace Tests\Unit\Traits;


use App\Exceptions\DuplicateQueueEntryException;
use App\Models\NotifyQueue;
use App\Models\ProductStock;
use App\Models\Queuer;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class QueueForStockTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testQueueForStock()
    {
        /** @var User $user */
        $user = User::factory()->create();

        /** @var ProductStock $stock */
        $stock = ProductStock::factory()->create();

        $queuer = $user->queue($stock);

        $this->assertDatabaseCount('notify_queues', 1)
             ->assertDatabaseHas('notify_queues', [
                 'id' => $queuer->queue_id,
                 'stock_id' => $stock->id
             ]);
        $this->assertDatabaseCount('queuers', 1)
             ->assertDatabaseHas('queuers', [
                 'user_id' => $user->id
             ]);
        $this->assertCount(1, $stock->queuers);
    }

    public function testRequeueForStock()
    {
        $queue = NotifyQueue::factory()
                            ->has(Queuer::factory());

        $stock = ProductStock::factory()
                             ->has($queue)
                             ->create();

        $user = Queuer::first()->user;

        $this->expectException(DuplicateQueueEntryException::class);
        $user->queue($stock);
    }

    public function testDequeueForStock()
    {
        $queue = NotifyQueue::factory()
                            ->has(Queuer::factory()->count(3));

        $stock = ProductStock::factory()
                             ->has($queue)
                             ->create();

        $user = User::find(1);

        $result = $user->dequeue($stock);

        $this->assertTrue($result);
        $this->assertDatabaseCount('queuers', 2)
             ->assertDatabaseMissing('queuers', [
                 'user_id' => $user->id
             ]);
    }

    public function testIsQueueingForStock()
    {
        /** @var User $user */
        $user = User::factory()
                    ->has(Queuer::factory())
                    ->create();

        $this->assertTrue($user->isQueueing(ProductStock::first()));
    }

    public function testQueueing()
    {
        /** @var User $user */
        $user = User::factory()
                    ->has(Queuer::factory()->count(4))
                    ->create();

        $this->assertCount(4, $user->queues);
    }
}
