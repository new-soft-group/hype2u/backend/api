<?php

namespace Tests\Unit\Services;

use App\Events\StockArrival;
use App\Exceptions\ModelHasAssociate;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Gender;
use App\Models\Image;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\ProductVariantOption;
use App\Models\StockStatus;
use App\Models\Variant;
use App\Models\Vendor;
use App\Models\Warehouse;
use App\Services\ProductService;
use Database\Seeders\StockStatusSeeder;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Arr;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Tests\Traits\FakeSetting;
use Tests\Traits\FakeImage;

class ProductServiceTest extends TestCase
{
    use RefreshDatabase,
        WithFaker,
        FakeImage,
        FakeSetting;

    /**
     * @var ProductService
     */
    private $service;

    /**
     * @throws BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('product');

        $this->seed(StockStatusSeeder::class);

        $this->setupSetting();

        $this->service = $this->app->make(ProductService::class);
    }

    /**
     * Test create product without any variants apply
     */
    public function testCreateProductWithoutVariant()
    {
        $categories = Category::factory()
                              ->count(1)
                              ->create()
                              ->pluck('id')
                              ->toArray();

        /** @var Product $product */
        $product = Product::factory()
                          ->enableFeatured()
                          ->make(['category_ids' => $categories]);

        $created = $this->service->create($product->toArray(), [$this->fakeUpload('product.png')]);

        Storage::disk('product')->assertExists($created->images->first()->path);

        $this->assertNotNull($created->sku);
        $this->assertNotNull($created->slug);
        $this->assertEquals($product->name, $created->name);
        $this->assertEquals($product->description, $created->description);
        $this->assertTrue($created->featured);
        $this->assertDatabaseHas('products', [
            'id' => $created->id,
            'sku' => $created->sku,
            'brand_id' => $product->brand_id,
            'gender_id' => $product->gender_id,
            'featured' => true
        ]);
        $this->assertDatabaseHas('images', [
            'imageable_id' => $created->id,
            'sort' => 0
        ]);
        $this->assertDatabaseHas('product_categories', [
            'product_id' => $created->id,
            'category_id' => $categories[0]
        ]);
    }

    /**
     * Test create product with default disable state
     */
    public function testCreateProductWithDisableState()
    {
        $product = Product::factory()->disabled()->make();

        $product = $this->service->create($product->toArray(), []);

        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'enabled' => false
        ]);
    }

    /**
     * Test create product with single variants combination
     */
    public function testCreateProductWithSingleVariant()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small'],
                ]
            ]
        ]);

        /** @var Vendor $vendor */
        $vendor = Vendor::factory()->create();

        /** @var Product $product */
        $product = Product::factory()->make([
            'variants' => [
                [
                    'variant_id' => $variants->getVariantIdByName('Sizes'),
                    'options' => $variants->getOptionIdByName(['Large', 'Small'], true)
                ]
            ],
            'vendor' => $vendor->id
        ]);

        $product = $this->service->create($product->toArray(), []);

        $this->assertCount(1, $product->variants);
        $this->assertCount(2, $product->options);
        $this->assertEquals($variants->getAllOptionName(), $product->getOptionNames());
    }

    /**
     * Test create product with multiple variants
     */
    public function testCreateProductWithMultipleVariants()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small'],
                ]
            ],
            [
                'name' => 'Colors',
                'options' => [
                    ['name' => 'Black'],
                    ['name' => 'White'],
                ],
            ]
        ]);

        /** @var Vendor $vendor */
        $vendor = Vendor::factory()->create();

        /** @var Product $product */
        $product = Product::factory()->make([
            'variants' => [
                [
                    'variant_id' => $variants->getVariantIdByName('Sizes'),
                    'options' => $variants->getOptionIdByName(['Large', 'Small'], true)
                ],
                [
                    'variant_id' => $variants->getVariantIdByName('Colors'),
                    'options' => $variants->getOptionIdByName(['Black', 'White'], true)
                ]
            ],
            'vendor' => $vendor->id
        ]);

        $product = $this->service->create($product->toArray(), []);

        $this->assertCount(2, $product->variants);
        $this->assertCount(4, $product->options);
        $this->assertEquals($variants->getAllOptionName(), $product->getOptionNames());
    }

    /**
     * Test create product with variants should generate stock with all the possible combinations
     */
    public function testCreateProductWithMultipleVariantShouldGenerateStocks()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small'],
                ]
            ],
            [
                'name' => 'Colors',
                'options' => [
                    ['name' => 'Black'],
                    ['name' => 'White'],
                ],
            ]
        ]);

        /** @var Vendor $vendor */
        $vendor = Vendor::factory()->create();

        /** @var Product $product */
        $product = Product::factory()->make([
            'variants' => [
                [
                    'variant_id' => $variants->getVariantIdByName('Sizes'),
                    'options' => $variants->getOptionIdByName(['Large', 'Small'], true)
                ],
                [
                    'variant_id' => $variants->getVariantIdByName('Colors'),
                    'options' => $variants->getOptionIdByName(['Black', 'White'], true)
                ]
            ],
            'vendor' => $vendor->id
        ]);

        $product = $this->service->create($product->toArray(), []);

        /** @var Collection $stocks */
        $stocks = $product->stocks()->get();

        $this->assertCount(4, $stocks);

        $combinations = [
            ['Large', 'Black'],
            ['Large', 'White'],
            ['Small', 'Black'],
            ['Small', 'White']
        ];

        foreach ($combinations as $index => $combination) {
            /** @var ProductStock $stock */
            $stock = $stocks->get($index);

            $this->assertTrue($stock->hasVendor($vendor));
            $this->assertTrue($stock->hasCombination($combination));
        }
    }

    /**
     * Test update product image should truncate old images
     */
    public function testUpdateProductImageShouldTruncateOldImages()
    {
        /** @var Product $product */
        $product = Product::factory()
                          ->has(Image::factory()->product())
                          ->create();

        $deletedImages = $product->images;
        $deleteImageIds = $deletedImages->pluck('id')->toArray();
        $uploadedImages = $this->fakeUpload('product.png', 2);

        $data = ['sku' => 'test sku'];

        $updated = $this->service->update($product, $data, $uploadedImages, [], $deleteImageIds);

        $this->assertTrue($updated);
        $this->assertDatabaseCount('products', 1);
        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'sku' => $data['sku']
        ]);
        $this->assertDatabaseCount('images', 2);
        foreach ($deletedImages as $image) {
            Storage::disk('product')->assertMissing($image->path);
        }
        foreach ($product->images as $image) {
            Storage::disk('product')->assertExists($image->path);
        }
    }

    /**
     * Test update product variants should update the combinations accordingly
     */
    public function testUpdateProductVariants()
    {
        $product = Product::factory()->create();

        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small']
                ]
            ]
        ]);

        $updated = $this->service->update($product, [
            'variants' => [
                [
                    'variant_id' => $variants->getVariantIdByName('Sizes'),
                    'options' => $variants->getOptionIdByName(['Large', 'Small'], true)
                ]
            ]
        ]);

        $this->assertTrue($updated);
        $this->assertDatabaseCount('products', 1);
        $this->assertDatabaseCount('product_variants', 1);
        $this->assertDatabaseCount('product_variant_options', 2);
        $this->assertDatabaseCount('product_stocks', 0);
        $this->assertDatabaseCount('stock_variant_option_combinations', 0);
    }

    /**
     * Test update product state
     */
    public function testUpdateProductState()
    {
        /** @var Product $product */
        $product = Product::factory()->create();

        $updated = $this->service->update($product, ['enabled' => false]);

        $this->assertTrue($updated);
        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'enabled' => false
        ]);
    }

    /**
     * Test update product category
     */
    public function testUpdateProductCategory()
    {
        /** @var \Illuminate\Database\Eloquent\Collection $categories */
        $categories = Category::factory()
                              ->count(2)
                              ->state(new Sequence(
                                  ['name' => 'Tops'],
                                  ['name' => 'Bottoms']
                              ))
                              ->create();

        $categoryIds = $categories->pluck('id')->toArray();

        /** @var Product $product */
        $product = Product::factory()->create();
        $product->assignCategories($categoryIds[0]);

        $updated = $this->service->update($product, ['category_ids' => $categoryIds]);

        $this->assertTrue($updated);
        $this->assertDatabaseCount('product_categories', 2);
        $this->assertEquals($categories->toArray(), $product->categories->toArray());
    }

    /**
     * Test update product gender
     */
    public function testUpdateProductGender()
    {
        /** @var Gender $gender */
        $gender = Gender::factory()->create(['name' => 'Men']);

        /** @var Product $product */
        $product = Product::factory()->create();

        $updated = $this->service->update($product, ['gender_id' => $gender->id]);

        $this->assertTrue($updated);
        $this->assertEquals($gender->name, $product->gender->name);
        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'gender_id' => $gender->id
        ]);
    }

    /**
     * Test update product brand
     */
    public function testUpdateProductBrand()
    {
        /** @var Brand $brand */
        $brand = Brand::factory()->create();

        /** @var Product $product */
        $product = Product::factory()->create();

        $updated = $this->service->update($product, ['brand_id' => $brand->id]);

        $this->assertTrue($updated);
        $this->assertEquals($brand->name, $product->brand->name);
        $this->assertDatabaseHas('products', [
            'id' => $product->id,
            'brand_id' => $brand->id
        ]);
    }

    /**
     * Sync new variant
     *
     * Before
     * ------------------------------------
     * Empty Variant/Options
     *
     * After
     * ------------------------------------
     * Colors: Black, White
     *
     * @throws \Exception
     */
    public function testSyncNewVariant()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Colors',
                'options' => [
                    ['name' => 'Black'],
                    ['name' => 'White'],
                ]
            ]
        ]);

        $product = Product::factory()->create();

        /** Update Variants
         *
         *  Colors(!Added) - Black, White
         */
        $updated = $this->service->update($product, [
            'variants' => [
                [
                    'variant_id' => $variants->getVariantIdByName('Colors'),
                    'options' => $variants->getOptionIdByName(['Black', 'White'], true)
                ]
            ]
        ]);

        $this->assertTrue($updated);
        $this->assertEquals($variants->getAllOptionName(), $product->getOptionNames());
        $this->assertDatabaseCount('product_variants', 1);
        $this->assertDatabaseCount('product_variant_options', 2);
        $this->assertDatabaseCount('product_stocks', 0);
        $this->assertDatabaseCount('stock_variant_option_combinations', 0);
    }

    /**
     * Sync new variant with existing variant attached
     *
     * Before
     * ------------------------------------
     * Sizes - Large, Small
     *
     * After
     * ------------------------------------
     * Sizes - Large, Small
     * Colors - Black, White
     *
     */
    public function testShouldPassSyncVariantWithExistingVariantAttached()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small'],
                ]
            ],
            [
                'name' => 'Colors',
                'options' => [
                    ['name' => 'Black'],
                    ['name' => 'White'],
                ]
            ]
        ]);

        /** @var Vendor $vendor */
        $vendor = Vendor::factory()->create();

        /** Prepare Variants
         *
         *  Sizes - Large, Small
         */
        /** @var Product $product */
        $product = Product::factory()->create();
        $product->assignVariants([
            [
                'variant_id' => $variants->getVariantIdByName('Sizes'),
                'options' => $variants->getOptionIdByName(['Large', 'Small'], true)
            ]
        ]);
        $product->initializeStocks($product->variants, $vendor->id);

        /** Update Variants
         *
         *  Sizes(!Existing) - Large, Small
         *  Colors(!New) - Black, White
         */
        $updated = $this->service->update($product, [
            'variants' => [
                [
                    'id' => $product->variants->first()->id,
                    'variant_id' => $variants->getVariantIdByName('Sizes'),
                    'options' => [
                        [
                            'id' => 1,
                            'variant_option_id' => $variants->getOptionIdByName('Large')
                        ],
                        [
                            'id' => 2,
                            'variant_option_id' => $variants->getOptionIdByName('Small')
                        ]
                    ]
                ],
                [
                    'variant_id' => $variants->getVariantIdByName('Colors'),
                    'options' => $variants->getOptionIdByName(['Black', 'White'], true)
                ]
            ]
        ]);

        $this->assertTrue($updated);

        $combinations = Arr::pluck($product->availableVariantCombination(), 'name');

        $this->assertEquals($variants->getAllOptionName(), $product->getOptionNames());
        $this->assertEquals(
            [
                'Large/Black',
                'Large/White',
                'Small/Black',
                'Small/White'
            ],
            $combinations
        );
        $this->assertDatabaseCount('product_variants', 2);
        $this->assertDatabaseCount('product_variant_options', 4);
        $this->assertDatabaseCount('product_stocks', 2);
        $this->assertDatabaseCount('stock_variant_option_combinations', 2);

        $combinations = [['Large'], ['Small']];

        $stocks = $product->stocks()->get();

        foreach ($combinations as $index => $combination) {
            /** @var ProductStock $stock */
            $stock = $stocks->get($index);

            $this->assertTrue($stock->hasCombination($combination));
        }
    }

    /**
     * Sync new variant with new options and has existing variant attached
     *
     * Before
     * ------------------------------------
     * Sizes - Large, Small
     *
     * After
     * ------------------------------------
     * Sizes - Large, Small, Medium
     * Colors - Black, White
     *
     */
    public function testSyncVariantWithExtraOptions()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small'],
                    ['name' => 'Medium'],
                ]
            ],
            [
                'name' => 'Colors',
                'options' => [
                    ['name' => 'Black'],
                    ['name' => 'White'],
                ]
            ]
        ]);

        /** Prepare Variants
         *
         *  Sizes - Large, Small
         */
        /** @var Product $product */
        $product = Product::factory()->create();
        $product->assignVariants([
            [
                'variant_id' => $variants->getVariantIdByName('Sizes'),
                'options' => $variants->getOptionIdByName(['Large', 'Small'], true)
            ]
        ]);
        $product->initializeStocks($product->variants);

        /** Update Variants
         *
         *  Sizes(!Remove) - Large, Small
         *  Colors(!Added) - Black, White
         */
        $this->service->update($product, [
            'variants' => [
                [
                    'variant_id' => $variants->getVariantIdByName('Sizes'),
                    'options' => $variants->getOptionIdByName(['Large', 'Small', 'Medium'], true)
                ],
                [
                    'variant_id' => $variants->getVariantIdByName('Colors'),
                    'options' => $variants->getOptionIdByName(['Black', 'White'], true)
                ]
            ]
        ]);

        $combinations = Arr::pluck($product->availableVariantCombination(), 'name');

        $this->assertEquals(
            [
                'Large/Black',
                'Large/White',
                'Small/Black',
                'Small/White',
                'Medium/Black',
                'Medium/White'
            ],
            $combinations
        );
    }

    /**
     * Sync new variant with new options and has existing variant attached
     *
     * Before
     * ------------------------------------
     * Sizes - Large, Small
     *
     * After
     * ------------------------------------
     * Sizes - Large
     * Colors - Black, White
     *
     * @throws \Exception
     */
    public function testShouldThrowExceptionWhenSyncVariantWithDifferentOption()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small'],
                    ['name' => 'Medium'],
                ]
            ],
            [
                'name' => 'Colors',
                'options' => [
                    ['name' => 'Black'],
                    ['name' => 'White'],
                ]
            ]
        ]);

        /** Prepare Variants
         *
         *  Sizes - Large, Small
         */
        /** @var Product $product */
        $product = Product::factory()->create();
        $product->assignVariants([
            [
                'variant_id' => $variants->getVariantIdByName('Sizes'),
                'options' => $variants->getOptionIdByName(['Large', 'Small'], true)
            ]
        ]);
        $product->initializeStocks($product->variants);

        $this->expectException(ModelHasAssociate::class);
        /** Update Variants
         *
         *  Sizes(!Remove) - Large, Small
         *  Colors(!Added) - Black, White
         */
        $this->service->update($product, [
            'variants' => [
                [
                    'variant_id' => $variants->getVariantIdByName('Sizes'),
                    'options' => $variants->getOptionIdByName(['Large'], true)
                ]
            ]
        ]);
    }

    public function testShouldThrowExceptionWhenDeleteStockWithAssociateTags()
    {
        /** @var ProductStock $stock */
        $stock = ProductStock::factory()
                             ->has(ProductStockInventory::factory(), 'inventories')
                             ->create();

        $this->expectException(ModelHasAssociate::class);

        $this->service->deleteStocks($stock->product, $stock->id);
    }

    /**
     * Test delete multiple product's stock at once
     */
    public function testDeleteMultipleStock()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small'],
                ]
            ]
        ]);

        /** Prepare variants
         *
         *  Sizes - Large, Small
         */
        /** @var Product $product */
        $product = Product::factory()->create();
        $product->assignVariants([
            [
                'variant_id' => $variants->get(0)->id,
                'options' => [
                    ['variant_option_id' => 1],
                    ['variant_option_id' => 2]
                ]
            ]
        ]);
        $stocks = $product->initializeStocks($product->variants);

        $result = $this->service->deleteStocks($product, $stocks->pluck('id')->toArray());

        $this->assertTrue($result);
        $this->assertDatabaseCount('product_stocks', 0);
        $this->assertDatabaseCount('stock_variant_option_combinations', 0);
    }

    /**
     * Test delete single product's stock
     */
    public function testDeleteSingleStock()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small'],
                ]
            ]
        ]);

        /** Prepare variants
         *
         *  Sizes - Large, Small
         */
        /** @var Product $product */
        $product = Product::factory()->create();
        $product->assignVariants([
            [
                'variant_id' => $variants->get(0)->id,
                'options' => [
                    ['variant_option_id' => 1],
                    ['variant_option_id' => 2]
                ]
            ]
        ]);
        $stocks = $product->initializeStocks($product->variants);

        $result = $this->service->deleteStocks($product, $stocks->first()->id);

        $this->assertTrue($result);
        $this->assertDatabaseCount('product_stocks', 1);
        $this->assertDatabaseCount('stock_variant_option_combinations', 1);
    }

    /**
     * Test manually create product stock with selected variant combination
     */
    public function testCreateStockWithVariantCombination()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small'],
                ]
            ],
            [
                'name' => 'Colors',
                'options' => [
                    ['name' => 'Black'],
                    ['name' => 'White']
                ]
            ]
        ]);

        /** @var Vendor $vendor */
        $vendor = Vendor::factory()->create();

        /** Prepare variants
         *
         *  Sizes - Large, Small
         *  Colors - Black, White
         */
        /** @var Product $product */
        $product = Product::factory()->create();
        $product->assignVariants([
            [
                'variant_id' => $variants->getVariantIdByName('Sizes'),
                'options' => $variants->getOptionIdByName(['Large', 'Small'], true)
            ],
            [
                'variant_id' => $variants->getVariantIdByName('Colors'),
                'options' => $variants->getOptionIdByName(['Black', 'White'], true)
            ]
        ]);

        $variantCombination = $product->availableVariantCombination()[0];

        $stock = $this->service->createStock($product, [
            'vendor' => $vendor->id,
            'price' => 100,
            'product_variant_option_ids' => $variantCombination['ids'],
            'enabled' => false
        ]);

        $this->assertNotNull($stock);
        $this->assertEquals($variantCombination['ids'], $stock->combination_ids);
        $this->assertEquals($variantCombination['name'], $stock->combination);
        $this->assertDatabaseCount('product_stocks', 1);
        $this->assertDatabaseHas('product_stocks', [
            'id' => $stock->id,
            'price' => 100,
            'product_id' => $product->id,
            'enabled' => false,
            'vendor_id' => $vendor->id
        ]);
        $this->assertDatabaseCount('stock_variant_option_combinations', 2);
    }

    /**
     * Test add tag code to product's stock
     */
    public function testAddTagCodeToProductStock()
    {
        /** @var ProductStock $stock */
        $stock = ProductStock::factory()
                             ->hasAttached(ProductVariantOption::factory(), [], 'combinations')
                             ->create();

        Event::fake();

        $item = $stock->restock($this->faker->uuid, Warehouse::factory()->create(), StockStatus::IN_STOCK);

        Event::assertDispatched(StockArrival::class, 1);
        $this->assertDatabaseCount('product_stock_inventories', 1);
        $this->assertDatabaseHas('product_stock_inventories', [
            'id' => $item->id,
            'product_stock_id' => $stock->id,
            'warehouse_id' => $item->warehouse_id,
            'tag' => $item->tag,
            'stock_status_id' => StockStatus::IN_STOCK
        ]);
    }

    /**
     * @throws \Exception
     */
    public function testAttachMultipleTagToProductStock()
    {
        /** @var ProductStock $stock */
        $stock = ProductStock::factory()
                             ->hasAttached(ProductVariantOption::factory(), [], 'combinations')
                             ->create();

        Event::fake();

        $items = $stock->restocks(
            [$this->faker->uuid, $this->faker->uuid],
            Warehouse::factory()->create()->id,
            StockStatus::IN_STOCK
        );

        Event::assertDispatched(StockArrival::class, 1);
        $this->assertDatabaseCount('product_stock_inventories', 2);
        foreach ($items as $item) {
            $this->assertDatabaseHas('product_stock_inventories', [
                'id' => $item->id,
                'product_stock_id' => $item->product_stock_id,
                'warehouse_id' => $item->warehouse_id,
                'tag' => $item->tag,
                'stock_status_id' => StockStatus::IN_STOCK
            ]);
        }
    }

    /**
     * Update product tag code
     */
    public function testUpdateProductTagCode()
    {
        /** @var ProductStockInventory $tag */
        $tag = ProductStockInventory::factory()->create();

        $newTag = $this->faker->uuid;

        $updated = $tag->setTag($newTag)->save();

        $this->assertTrue($updated);
        $this->assertDatabaseCount('product_stock_inventories', 1);
        $this->assertDatabaseHas('product_stock_inventories', [
            'id' => $tag->id,
            'tag' => $newTag
        ]);
    }

    /**
     * Delete product tag from stock
     */
    public function testDeleteProductStockSingleTagId()
    {
        /** @var ProductStockInventory $tag */
        $tag = ProductStockInventory::factory()->create();

        $deleted = $tag->delete();

        $this->assertTrue($deleted);
        $this->assertCount(1, ProductStockInventory::onlyTrashed()->get());
        $this->assertDatabaseCount('product_stock_inventories', 1);
    }

    public function testDeleteProductStockMultipleTagId()
    {
        /** @var ProductStock $stock */
        $stock = ProductStock::factory()
                             ->has(ProductStockInventory::factory()->count(3), 'inventories')
                             ->create();

        $deleted = $stock->deleteTags([1, 2]);

        $this->assertTrue($deleted);
        $this->assertCount(2, ProductStockInventory::onlyTrashed()->get());
        $this->assertDatabaseCount('product_stock_inventories', 3);
    }
}
