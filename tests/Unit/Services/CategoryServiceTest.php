<?php

namespace Tests\Unit\Services;

use App\Models\Category;
use App\Services\CategoryService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CategoryServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @var CategoryService
     */
    private $service;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->service = $this->app->make(CategoryService::class);
    }

    public function testCreateCategory()
    {
        $category = $this->service->create('name');

        $this->assertNotNull($category);
        $this->assertDatabaseHas('categories', ['id' => $category->id]);
    }

    public function testUpdateCategory()
    {
        /** @var Category $category */
        $category = Category::factory()->create();

        $updated = $this->service->update($category, 'Updated Category');

        $this->assertTrue($updated);
        $this->assertDatabaseCount('categories', 1);
        $this->assertDatabaseHas('categories', [
            'id' => $category->id,
            'name' => 'Updated Category'
        ]);
    }
}
