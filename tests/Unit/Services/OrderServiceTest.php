<?php

namespace Tests\Unit\Services;

use App\Exceptions\InsufficientStockException;
use App\Exceptions\OutOfStockException;
use App\Models\Address;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStatus;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\ProductVariantOption;
use App\Models\Profile;
use App\Models\Timeslot;
use App\Models\User;
use App\Notifications\OrderConfirmationNotification;
use App\Options\OrderOptions;
use App\Services\OrderService;
use Database\Seeders\OrderStatusSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Mockery\MockInterface;
use Stripe\Invoice;
use Tests\TestCase;
use Tests\Traits\FakeSetting;
use Illuminate\Support\Facades\Notification;

class OrderServiceTest extends TestCase
{
    use RefreshDatabase, FakeSetting;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(OrderStatusSeeder::class);

        $this->setupSetting();

        Storage::fake('product');
    }

    /**
     * Create an order by guest
     *
     * @return void
     * @throws \Exception
     */
    public function testCreateOrder()
    {
        Notification::fake();

        /** @var User $user */
        $user = User::factory()
                    ->has(Profile::factory()->has(Address::factory()))
                    ->create();

        $user->profile->defaultShippingAddress()->associate(1)->save();

        $items = CartItem::factory()->count(2)->make();

        $this->partialMock(OrderService::class, function (MockInterface $mock) {
            $mock->shouldNotHaveReceived('processCharges');
        });

        $order = resolve(OrderService::class)->create($user, $items, new OrderOptions());

        $this->assertNotNull($order);
        $this->assertEquals(OrderStatus::PENDING, $order->status->id);
        $this->assertDatabaseHas('addresses', [
            'addressable_id' => $order->id,
            'addressable_type' => Order::class
        ]);
        $this->assertDatabaseCount('order_items', 2);

        Notification::assertSentTo(
            $user, function(OrderConfirmationNotification $notification, $channels) use ($order){
                return $notification->order->user->profile->name === $order->user->profile->name;
            }
        );

        $order->items->each(function (OrderItem $item) {
            $this->assertDatabaseHas('product_stocks', [
                'id' => $item->product_stock_id,
                'reserved' => $item->quantity
            ]);
        });
    }

    /**
     * Create an order by guest
     *
     * @return void
     * @throws \Exception
     */
    public function testCreateOrderWithTimeslot()
    {
        Notification::fake();

        /** @var User $user */
        $user = User::factory()
                    ->has(Profile::factory()->has(Address::factory()))
                    ->create();

        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->create();

        $user->profile->defaultShippingAddress()->associate(1)->save();

        $items = CartItem::factory()->count(1)->make();

        $order = resolve(OrderService::class)->create($user, $items,
            new OrderOptions([
                'currency' => 'myr',
                'charge' => 0,
                'shipping_address' => null,
                'timeslot' => $timeslot->id,
                'timeslot_date' => now()->addDay()->format('Y-m-d')
            ]));

        $this->assertCount(1, $order->timeslots);
        $this->assertDatabaseHas('timeslot_allocations', [
            'timeslot_id' => $timeslot->id,
            'order_id' => $order->id
        ]);
    }

    /**
     * Create an order by guest
     *
     * @return void
     * @throws \Exception
     */
    public function testCreateOrderWithShippingAddress()
    {
        Notification::fake();

        /** @var User $user */
        $user = User::factory()
                    ->has(Profile::factory())
                    ->create();

        $items = CartItem::factory()->count(1)->make();

        $address = Address::factory()
                          ->make()
                          ->toArray();

        $order = resolve(OrderService::class)->create($user, $items,
            new OrderOptions([
                'currency' => 'myr',
                'charge' => 0,
                'shipping_address' => Arr::except($address, 'addressable_type')
            ]));

        $this->assertDatabaseCount('addresses', 1)
             ->assertDatabaseHas('addresses', [
                 'addressable_id' => $order->id,
                 'addressable_type' => Order::class
             ]);
    }

    /**
     * Create an order by guest
     *
     * @return void
     * @throws \Exception
     */
    public function testOrderCreateWithCharge()
    {
        /** @var User $user */
        $user = User::factory()
                    ->has(Profile::factory()->has(Address::factory()))
                    ->create();

        $user->profile->defaultShippingAddress()->associate(1)->save();

        $items = CartItem::factory()->count(2)->make();

        $invoice = Invoice::constructFrom([
            'id' => 'in_' . Str::random(),
            'payment_intent' => 'pm_intent_' . Str::random(),
            'amount_paid' => amountToCent(300),
            'currency' => 'myr',
            'status' => 'paid',
            'invoice_pdf' => null,
            'discount' => null,
        ]);
        $this->partialMock(OrderService::class, function (MockInterface $mock) use ($user, $invoice) {
            $mock->shouldReceive('processCharges')
                 ->once()
                 ->andReturn($invoice);
        });

        $order = resolve(OrderService::class)->create($user, $items, new OrderOptions(['charge' => 300]));

        $this->assertDatabaseHas('orders', [
            'id' => $order->id,
            'order_status_id' => OrderStatus::PENDING
        ]);
        $this->assertDatabaseCount('invoices', 1);
        $this->assertDatabaseHas('invoices', [
            'stripe_id' => $invoice->id,
            'payment_intent' => $invoice->payment_intent,
            'amount' => centToAmount($invoice->amount_paid),
            'currency' => $invoice->currency,
            'status' => $invoice->status,
        ]);
    }

    /**
     * Create an order by guest
     *
     * @return void
     * @throws \Exception
     */
    public function testCreateOrderFailedWithOutOfStock()
    {
        /** @var User $user */
        $user = User::factory()
                    ->has(Profile::factory()->has(Address::factory()))
                    ->create();

        $user->profile->defaultShippingAddress()->associate(1)->save();

        /** @var ProductStock $stock */
        $stock = ProductStock::factory()
                             ->hasAttached(
                                 ProductVariantOption::factory()->product(),
                                 [],
                                 'combinations'
                             )->create();

        $this->partialMock(OrderService::class, function (MockInterface $mock) {
            $mock->shouldNotHaveReceived('processCharges');
        });

        /** @var Cart $cart
         * Prepare user checkout items
         */
        $cart = Cart::factory()
                    ->state(['user_id' => $user->id])
                    ->create();
        $cart->items()->create([
            'product_id' => $stock->product_id,
            'product_stock_id' => $stock->id,
            'product_type_id' => 1,
            'quantity' => 1
        ]);

        $this->expectException(OutOfStockException::class);
        resolve(OrderService::class)->create($user, $cart->items, new OrderOptions());

        $this->assertDatabaseHas('product_stocks', [
            'id' => $stock->id,
            'reserved' => 0
        ]);
        $this->assertDatabaseCount('orders', 0)
             ->assertDatabaseCount('order_items', 0);
    }

    /**
     * Create an order by guest
     *
     * @return void
     * @throws \Exception
     */
    public function testCreateOrderFailedWithInsufficientStock()
    {
        /** @var User $user */
        $user = User::factory()
                    ->has(Profile::factory()->has(Address::factory()))
                    ->create();

        $user->profile->defaultShippingAddress()->associate(1)->save();

        $stocks = ProductStock::factory()
                              ->count(2)
                              ->hasAttached(
                                  ProductVariantOption::factory()->product(),
                                  [],
                                  'combinations'
                              )
                              ->has(ProductStockInventory::factory(), 'inventories')
                              ->create();

        $this->partialMock(OrderService::class, function (MockInterface $mock) {
            $mock->shouldNotHaveReceived('processCharges');
        });

        /** @var Cart $cart
         * Prepare user checkout items
         */
        $cart = Cart::factory()
                    ->state(['user_id' => $user->id])
                    ->create();
        $cart->items()->createMany([
            [
                'product_id' => 1,
                'product_stock_id' => 1,
                'product_type_id' => 1,
                'quantity' => 1
            ],
            [
                'product_id' => 2,
                'product_stock_id' => 2,
                'product_type_id' => 1,
                'quantity' => 2
            ]
        ]);

        $this->expectException(InsufficientStockException::class);
        resolve(OrderService::class)->create($user, $cart->items, new OrderOptions());

        $stocks->each(function (ProductStock $stock) {
            $this->assertDatabaseHas('product_stocks', [
                'id' => $stock->id,
                'reserved' => 0
            ]);
        });

        $this->assertDatabaseCount('orders', 0)
             ->assertDatabaseCount('order_items', 0);
    }
}
