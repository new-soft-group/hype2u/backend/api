<?php


namespace Tests\Unit\Services;


use App\Gateways\PaymentGateway;
use App\Models\Coupon;
use App\Models\Invoice;
use App\Models\Subscription;
use Carbon\Carbon;
use Illuminate\Support\Str;
use Stripe\Coupon as StripeCoupon;
use App\Services\CouponService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery\MockInterface;
use Tests\TestCase;

class CouponServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var CouponService|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $service;

    protected function setUp(): void
    {
        parent::setUp();
    }

    /**
     * @throws \Stripe\Exception\ApiErrorException
     */
    public function testCreate()
    {
        $stripeId = Str::random();
        /** @var Coupon $attributes */
        $attributes = Coupon::factory()->amount()->make();

        $this->mock(PaymentGateway::class,
            function (MockInterface $mock) use ($attributes, $stripeId) {
                $mock->shouldReceive('request')
                     ->with('post', '/v1/coupons', $attributes->getStripeOptions(), null)
                     ->once()
                     ->andReturn(StripeCoupon::constructFrom([
                         'id' => $stripeId
                     ]));
            });

        $coupon = resolve(CouponService::class)->create(
            $attributes->name,
            $attributes->code,
            $attributes->duration,
            $attributes->amount_off,
            $attributes->currency,
            ['redeem_by' => $attributes->redeem_by]
        );

        $this->assertDatabaseHas('coupons', [
            'id' => $coupon->id,
            'stripe_id' => $stripeId,
            'duration' => Coupon::DURATION_ONCE,
            'amount_off' => $coupon->amount_off,
            'currency' => $coupon->currency,
            'percent_off' => null,
            'redeem_by' => $coupon->redeem_by,
            'enabled' => $coupon->enabled,
        ]);
    }

    public function testUpdate()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->create();

        /** @var Coupon $attributes */
        $attributes = Coupon::factory()->make();

        $this->mock(PaymentGateway::class,
            function (MockInterface $mock) use ($coupon, $attributes) {
                $mock->shouldReceive('request')
                     ->with('post', "/v1/coupons/$coupon->stripe_id", [
                         'name' => $attributes->name
                     ], null)
                     ->once();
            });

        $coupon = resolve(CouponService::class)->update($coupon, $attributes->toArray());

        $this->assertDatabaseHas('coupons', [
            'id' => $coupon->id,
            'name' => $attributes->name,
            'code' => $attributes->code,
            'enabled' => $attributes->enabled
        ]);
    }

    public function testUpdateCodeOnlyShouldNotTriggerStripeApi()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->create();

        $newCode = Str::random();

        $coupon = resolve(CouponService::class)->update($coupon, [
            'code' => $newCode
        ]);

        $this->assertDatabaseHas('coupons', [
            'id' => $coupon->id,
            'code' => $newCode
        ]);
    }

    /**
     * @throws \Exception
     */
    public function testForceDeleteWhenNoInvoice()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->create();

        $this->mock(PaymentGateway::class,
            function (MockInterface $mock) use ($coupon) {
                $mock->shouldReceive('request')
                     ->with('delete', "/v1/coupons/$coupon->stripe_id", null, null)
                     ->once();
            });

        $result = resolve(CouponService::class)->delete($coupon);

        $this->assertTrue($result);
        $this->assertDatabaseCount('coupons', 0);
    }

    /**
     * @throws \Exception
     */
    public function testSoftDelete()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->create();
        Subscription::factory()
                    ->has(Invoice::factory()->state([
                        'stripe_coupon' => $coupon->stripe_id
                    ]))
                    ->create();

        $this->mock(PaymentGateway::class,
            function (MockInterface $mock) use ($coupon) {
                $mock->shouldReceive('request')
                     ->with('delete', "/v1/coupons/$coupon->stripe_id", null, null)
                     ->once();
            });

        $result = resolve(CouponService::class)->delete($coupon);

        $this->assertTrue($result);
        $this->assertDatabaseCount('coupons', 1);
    }
}
