<?php

namespace Tests\Unit\Services;

use App\Models\Role;
use App\Services\UserService;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class UserServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @var UserService
     */
    private $service;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(PermissionSeeder::class);

        $this->service = $this->app->make(UserService::class);
    }

    public function testCreateUser()
    {
        $data = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->email,
            'password' => 'password',
            'mobile' => $this->faker->e164PhoneNumber
        ];

        $user = $this->service->create($data, Role::ADMIN);

        $this->assertNotNull($user);
        $this->assertTrue($user->hasRole(Role::ADMIN));
        $this->assertDatabaseHas('users', [
            'id' => $user->id,
            'email' => $data['email']
        ]);
        $this->assertDatabaseHas('profiles', [
            'id' => $user->profile->id,
            'first_name' => $data['first_name'],
            'last_name' => $data['last_name'],
            'mobile' => $data['mobile'],
        ]);
    }
}
