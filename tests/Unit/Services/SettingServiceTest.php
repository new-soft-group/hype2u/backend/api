<?php

namespace Tests\Unit\Services;

use App\Services\SettingService;
use Database\Seeders\SettingSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\FakeSetting;

class SettingServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker, FakeSetting;

    /**
     * @var SettingService
     */
    private $service;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(SettingSeeder::class);

        $this->setupSetting();

        $this->service = $this->app->make(SettingService::class);
    }

    public function testUpdateSetting()
    {
        $this->service->update([
            ['key' => 'currencies.rate.myr', 'value' => 2]
        ]);

        $this->assertDatabaseHas('settings', [
            'key' => 'currencies.rate.myr',
            'value' => 2
        ]);
    }
}
