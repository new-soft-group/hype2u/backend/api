<?php

namespace Tests\Unit\Services;


use App\Gateways\PaymentGateway;
use App\Models\PlanPolicy;
use App\Models\ProductType;
use App\Models\Surcharge;
use App\Services\PlanService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Mockery\MockInterface;
use Tests\TestCase;
use App\Models\Plan;
use Tests\Traits\MockStripeResponse;

class PlanServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker, MockStripeResponse;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testCreatePlan()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()->state(['interval' => 'week'])->make();

        $mockResponse = $this->mockStripeResponse([
            'id' => 'plan_'.Str::random(),
            'product' => 'prod_'.Str::random()
        ]);
        $this->mock(PaymentGateway::class, function (MockInterface $mock) use ($plan, $mockResponse) {
            $mock->shouldReceive('request')
                 ->with('post', '/v1/plans', $plan->getStripeOption(), null)
                 ->once()
                 ->andReturn($mockResponse);
        });
        $created = resolve(PlanService::class)->create($plan->toArray());

        $this->assertDatabaseCount('plans', 1);
        $this->assertDatabaseHas('plans', [
            'id' => $created->id,
            'stripe_plan' => $mockResponse->id,
            'interval' => $plan->interval,
            'stripe_product' => $mockResponse->product,
        ]);
    }

    public function testCreatePlanWithPolicies()
    {
        /** @var ProductType $type */
        $type = ProductType::factory()
                           ->state(['name' => 'VIP'])
                           ->create();

        /** @var Plan $plan */
        $plan = Plan::factory()->make(['policies' => [
            [
                'quantity' => 1,
                'rules' => [
                    [
                        'id' => $type->id,
                        'prime' => false
                    ]
                ]
            ]
        ]]);

        $mockResponse = $this->mockStripeResponse([
            'id' => 'plan_'.Str::random(),
            'product' => 'prod_'.Str::random()
        ]);
        $this->mock(PaymentGateway::class, function (MockInterface $mock) use ($plan, $mockResponse) {
            $mock->shouldReceive('request')
                 ->with('post', '/v1/plans', $plan->getStripeOption(), null)
                 ->once()
                 ->andReturn($mockResponse);
        });

        $created = resolve(PlanService::class)->create($plan->toArray());

        $this->assertDatabaseCount('plan_policies', 1)
             ->assertDatabaseHas('plan_policies', [
                 'plan_id' => $created->id,
                 'quantity' => 1,
                 'logical' => 'none'
             ]);
        $this->assertDatabaseCount('policy_rules', 1);
        foreach ($created->policies as $policy) {
            $this->assertDatabaseHas('policy_rules', [
                'plan_policy_id' => $policy->id,
                'product_type_id' => $type->id,
                'prime' => false
            ]);
        }
    }

    public function testCreatePlanWithCharges()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()->make(['surcharges' => [
            ['threshold' => 4000, 'charge' => 100]
        ]]);

        $mockResponse = $this->mockStripeResponse([
            'id' => 'plan_'.Str::random(),
            'product' => 'prod_'.Str::random()
        ]);
        $this->mock(PaymentGateway::class, function (MockInterface $mock) use ($plan, $mockResponse) {
            $mock->shouldReceive('request')
                 ->with('post', '/v1/plans', $plan->getStripeOption(), null)
                 ->once()
                 ->andReturn($mockResponse);
        });
        $created = resolve(PlanService::class)->create($plan->toArray());

        $this->assertDatabaseCount('plans', 1);
        $this->assertDatabaseHas('plans', [
            'id' => $created->id,
            'stripe_plan' => $mockResponse->id,
            'stripe_product' => $mockResponse->product,
        ]);
        $this->assertDatabaseCount('surcharges', 1);
        $this->assertDatabaseHas('surcharges', [
            'plan_id' => $created->id,
            'threshold' => 4000,
            'charge' => 100,
            'default' => false
        ]);
    }

    public function testUpdatePlan()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()->create();

        /** @var Plan $attributes */
        $attributes = Plan::factory()->make();

        $this->mock(PaymentGateway::class, function (MockInterface $mock) use ($plan, $attributes) {
            $mock->shouldReceive('request')
                 ->with('post', '/v1/products/'.$plan->stripe_product, [
                     'name' => $attributes->name
                 ], null)
                 ->once();
        });
        resolve(PlanService::class)->update($plan, $attributes->toArray());

        $this->assertDatabaseCount('plans', 1);
        $this->assertDatabaseHas('plans', [
            'id' => $plan->id,
            'name' => $attributes->name,
            'description' => $attributes->description,
            'amount' => $plan->amount,
            'purchase_amount' => $attributes->purchase_amount,
            'exchange_limit' => $attributes->exchange_limit
        ]);
    }

    public function testUpdatePlanWithPolicies()
    {
        $rules = ProductType::factory()->count(2);

        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->has(PlanPolicy::factory()
                                    ->hasAttached($rules, [], 'rules'),
                        'policies')
                    ->create();

        /** @var ProductType $type */
        $type = ProductType::factory()->create();
        $attributes = array_merge($plan->toArray(), [
            'policies' => [
                [
                    'id' => 1,
                    'quantity' => 1,
                    'rules' => [
                        [
                            'id' => $type->id,
                            'prime' => false
                        ]
                    ]
                ]
            ]
        ]);
        resolve(PlanService::class)->update($plan, $attributes);

        $this->assertDatabaseCount('plan_policies', 1)
             ->assertDatabaseCount('policy_rules', 1);
    }

    public function testUpdatePlanWithNewPolicies()
    {
        $rules = ProductType::factory();

        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->has(PlanPolicy::factory()
                                    ->hasAttached($rules, [], 'rules'),
                        'policies')
                    ->create();

        /** @var ProductType $type */
        $type = ProductType::factory()->create();
        $attributes = array_merge($plan->toArray(), [
            'policies' => [
                [
                    'quantity' => 1,
                    'rules' => [
                        [
                            'id' => $type->id,
                            'prime' => false
                        ]
                    ]
                ]
            ]
        ]);
        resolve(PlanService::class)->update($plan, $attributes);

        $this->assertDatabaseCount('plan_policies', 2)
             ->assertDatabaseCount('policy_rules', 2);
    }

    public function testUpdatePlanWithSurcharge()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()->create();

        $attributes = [
            'surcharges' => [
                Surcharge::factory()->make()->toArray()
            ]
        ];

        resolve(PlanService::class)->update($plan, $attributes);

        $this->assertDatabaseCount('surcharges', 1);
        $this->assertDatabaseHas('surcharges', $attributes['surcharges'][0]);
    }

    public function testUpdatePlanExistingSurcharge()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()->has(Surcharge::factory())->create();

        $attributes = [
            'surcharges' => [
                Surcharge::factory()->make([
                    'id' => $plan->surcharges->first()->id
                ])->toArray()
            ]
        ];

        resolve(PlanService::class)->update($plan, $attributes);

        $this->assertDatabaseCount('surcharges', 1);
        $this->assertDatabaseHas('surcharges', [
            'id' => $attributes['surcharges'][0]['id'],
            'threshold' => $plan->surcharges->first()->threshold,
            'charge' => $attributes['surcharges'][0]['charge'],
        ]);
    }

    public function testUpdateStatus()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()->create();

        $this->mock(PaymentGateway::class, function (MockInterface $mock) use ($plan) {
            $mock->shouldReceive('request')
                 ->with('post', '/v1/plans/'.$plan->stripe_plan, [
                     'active' => false
                 ], null)
                 ->once();
        });
        $result = resolve(PlanService::class)->deactivate($plan);

        $this->assertTrue($result);
        $this->assertDatabaseHas('plans', [
            'id' => $plan->id,
            'enabled' => false
        ]);
    }

    public function testDeletePricing()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()->create();

        $this->mock(PaymentGateway::class, function (MockInterface $mock) use ($plan) {
            $mock->shouldReceive('request')
                 ->with('delete', '/v1/plans/'.$plan->stripe_plan, [], null)
                 ->once();
        });
        $result = resolve(PlanService::class)->deletePricing($plan);

        $this->assertTrue($result);
        $this->assertDatabaseHas('plans', [
            'id' => $plan->id,
            'stripe_plan' => ''
        ]);
    }

    public function testUpdateSort()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()->create();

        $service = $this->app->make(PlanService::class);
        $service->updateSort($plan, $plan->sort + 1);

        $this->assertDatabaseHas('plans', [
            'id' => $plan->id,
            'sort' => $plan->sort
        ]);
    }
}
