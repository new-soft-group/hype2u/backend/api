<?php

namespace Tests\Unit\Services;

use App\Services\RolePermissionService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\Models\Role as Constant;
use Tests\TestCase;

class RoleServiceTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var RolePermissionService
     */
    private $service;

    protected function setUp(): void
    {
        parent::setUp();

        $this->service = new RolePermissionService();
    }

    /**
     * A basic test example.
     *
     * @return void
     */
    public function testUpdateRolePermissions()
    {
        Permission::create(['name' => 'unit.test']);

        /** @var Role $role */
        $role = Role::create(['name' => Constant::USER]);
        $role->givePermissionTo(Permission::all());

        $newPermission = Permission::create(['name' => 'integration.test']);

        $rolePermissions = [
            [
                'id' => $role->id,
                'name' => Constant::USER,
                'permissions' => [
                    [
                        'id' => $newPermission->id,
                        'name' => $newPermission->name
                    ]
                ]
            ]
        ];

        $this->service->updateRolePermissions($rolePermissions);

        $role = Role::findByName(Constant::USER, 'web');

        $this->assertTrue($role->hasPermissionTo('integration.test'));
        $this->assertEquals(1, $role->getAllPermissions()->count());
    }
}
