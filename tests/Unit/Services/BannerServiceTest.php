<?php

namespace Tests\Unit\Services;

use App\Models\Banner;
use App\Models\BannerPosition;
use App\Models\Image;
use App\Services\BannerService;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Tests\Traits\FakeImage;

class BannerServiceTest extends TestCase
{
    use RefreshDatabase, WithFaker, FakeImage;
    /**
     * @var BannerService
     */
    private $service;

    /**
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('banner');

        $this->service = $this->app->make(BannerService::class);
    }

    public function testCreateBanner()
    {
        /** @var BannerPosition $position */
        $position = BannerPosition::factory()->create();

        $banners = $this->service->create([
            [
                'viewport' => Banner::DESKTOP_VIEWPORT,
                'banner_position_id' => $position->id,
                'image' => $this->fakeUpload('banner1.png')
            ],
            [
                'viewport' => Banner::MOBILE_VIEWPORT,
                'banner_position_id' => $position->id,
                'image' => $this->fakeUpload('banner2.png')
            ]
        ]);

        $this->assertNotEmpty($banners);
        $this->assertEquals($banners->first()->position->id, $position->id);
        $this->assertDatabaseCount('banners', 2);
        $this->assertDatabaseHas('banners', [
            'id' => 1,
            'banner_position_id' => $position->id,
        ]);
        $this->assertDatabaseHas('banners', [
            'id' => 2,
            'banner_position_id' => $position->id,
        ]);

        foreach ($banners as $banner) {
            Storage::disk('banner')->assertExists($banner->image->path);
        }
    }

    public function testUpdateBannerSortingAndPosition()
    {
        $banners = Banner::factory()->count(3)->create();

        foreach ($banners as $index => $banner) {
            $banner->sort = $index + 1;
            $banner->banner_position_id = 2;
        }

        $result = $this->service->update($banners->toArray());

        $this->assertTrue($result);
        $this->assertDatabaseCount('banners', 3);

        foreach ($banners as $banner) {
            $this->assertDatabaseHas('banners', [
                'id' => $banner->id,
                'sort' => $banner->sort,
                'banner_position_id' => 2
            ]);
        }
    }

    public function testDeleteBanners()
    {
        $banners = Banner::factory()
                         ->count(3)
                         ->has(Image::factory()->banner(), 'image')
                         ->create();

        $images = $banners->pluck('image');

        $result = $this->service->delete($banners->pluck('id')->toArray());

        $this->assertTrue($result);
        $this->assertDatabaseCount('banners', 0);

        foreach ($images as $image) {
            Storage::disk('banner')->assertMissing($image->path);
            $this->assertDatabaseMissing('images', [
                'path' => $image->path
            ]);
        }
    }
}
