<?php


namespace Tests\Unit\Options;


use App\Models\Address;
use App\Options\OrderOptions as Option;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class OrderOptionTest extends TestCase
{
    use WithFaker;

    public function testConstructOption()
    {
        $option = [
            'currency' => $this->faker->currencyCode,
            'shipping_address' => Address::factory()->make()->toArray(),
            'timeslot' => 1,
            'timeslotDate' => $this->faker->date(),
            'charge' => $this->faker->randomNumber(2)
        ];

        $optionObj = new Option($option);

        $this->assertEquals($option['currency'], $optionObj->getCurrency());
        $this->assertInstanceOf(Address::class, $optionObj->getShippingAddress());
        $this->assertEquals($option['timeslot'], $optionObj->getTimeslot());
        $this->assertEquals($option['timeslotDate'], $optionObj->getTimeslotDate());
        $this->assertEquals($option['charge'], $optionObj->getCharge());
    }
}
