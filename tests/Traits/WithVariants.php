<?php


namespace Tests\Traits;


use App\Collections\VariantCollection;
use App\Models\Variant;

trait WithVariants
{
    public function generateVariants(): VariantCollection
    {
        Variant::reguard();

        return Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Medium'],
                    ['name' => 'Small'],
                ]
            ],
            [
                'name' => 'Colors',
                'options' => [
                    ['name' => 'Black'],
                    ['name' => 'White']
                ]
            ]
        ]);
    }
}
