<?php


namespace Tests\Traits;


use App\Models\Setting;
use Cache;
use Database\Seeders\SettingSeeder;
use Schema;

trait FakeSetting
{
    protected function setupSetting()
    {
        $this->seed(SettingSeeder::class);

        $this->cacheSetting();
    }

    protected function cacheSetting() {
        if(Schema::hasTable('settings')) {
            $settings = Cache::rememberForever('settings', function() {
                return Setting::all()->pluck('value', 'key')->toArray();
            });

            config()->set($settings);
        }
    }
}
