<?php


namespace Tests\Traits;


use App\Models\Profile;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Support\Str;

trait MockSubscription
{
    public function subscribedUser(): User
    {
        return User::factory()
                   ->has(Profile::factory())
                   ->has(Subscription::factory(), 'subscriptions')
                   ->create([
                       'stripe_id' => 'cus_'.Str::random(),
                       'card_brand' => 'visa',
                       'card_last_four' => 1234
                   ]);
    }
}
