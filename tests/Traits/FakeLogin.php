<?php


namespace Tests\Traits;


use App\Models\Role;
use App\Models\User;
use Database\Seeders\PermissionSeeder;
use Laravel\Passport\Passport;

trait FakeLogin
{
    public function setupPermission()
    {
        $this->seed(PermissionSeeder::class);
    }

    public function login($role)
    {
        /** @var User $user */
        $user = User::factory()->create();
        $user->assignRole($role);

        Passport::actingAs($user);
    }

    public function loginAs(User $user, string $role = Role::USER)
    {
        $user->assignRole($role);
        Passport::actingAs($user);
    }
}
