<?php


namespace Tests\Traits;


use Illuminate\Http\Testing\FileFactory;
use Illuminate\Http\UploadedFile;

trait FakeImage
{
    /**
     * The Fake FileFactory instance.
     *
     * @var FileFactory
     */
    protected $uploaded;
    
    /**
     * Setup up the Fake FileFactory instance.
     *
     * @return void
     */
    protected function setUpFileFactory() {
        $this->uploaded = UploadedFile::fake();
    }
    
    /**
     * Initialize Fake Upload Properties
     * @param $name
     * @param int $count
     * @param int $width
     * @param int $height
     * @return \Illuminate\Http\Testing\File|array
     */
    protected function fakeUpload($name, $count = 0, $width = 100, $height = 200) {
        if($count === 0) {
            return [
                'file' => UploadedFile::fake()->image($name, $width, $height),
                'sort' => 0
            ];
        }
    
        $uploaded = [];
        for ($i = 0; $i < $count; $i++) {
            $_name = explode('.', $name);
            $uploaded[] = [
                'file' => UploadedFile::fake()->image("{$_name[0]}_{$i}.{$_name[1]}", $width, $height),
                'sort' => 0
            ];
        }
    
        return $uploaded;
    }
}
