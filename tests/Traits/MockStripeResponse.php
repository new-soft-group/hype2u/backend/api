<?php


namespace Tests\Traits;


use Stripe\StripeObject;

trait MockStripeResponse
{
    public function mockStripeResponse(array $attributes)
    {
        return StripeObject::constructFrom($attributes);
    }
}
