<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Notifications\ResetPasswordLink;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;

class ForgotPasswordApi extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();
    }

    /**
     * List all account
     *
     * @return void
     * @throws \Exception
     */
    public function testForgotPassword()
    {
        Notification::fake();

        /** @var User $user */
        $user = User::factory()->create();

        $response = $this->json('post', 'api/forgot/password', [
            'email' => $user->email
        ]);

        $response->assertStatus(200);

        Notification::assertSentTo([$user], ResetPasswordLink::class);

        $this->assertDatabaseHas('password_resets', [
            'email' => $user->email
        ]);
    }
}
