<?php

namespace Tests\Feature\Api;

use App\Gateways\PaymentGateway;
use App\Models\Address;
use App\Models\BusinessType;
use App\Models\Cart;
use App\Models\CartItem;
use App\Models\Order;
use App\Models\Plan;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\ProductVariantOption;
use App\Models\Profile;
use App\Models\Subscription;
use App\Models\Surcharge;
use App\Models\Timeslot;
use App\Models\TimeslotAllocation;
use App\Models\User;
use App\Services\OrderService;
use Database\Seeders\OrderStatusSeeder;
use Database\Seeders\PermissionSeeder;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Mockery\MockInterface;
use Tests\TestCase;
use Tests\Traits\FakeLogin;
use Tests\Traits\FakeSetting;
use Tests\Traits\MockSubscription;

class CheckoutApiTest extends TestCase
{
    use RefreshDatabase, WithFaker, FakeSetting, FakeLogin, MockSubscription;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed([PermissionSeeder::class, OrderStatusSeeder::class]);

        $this->cacheSetting();

        Storage::fake('product');
    }

    /**
     *
     * @return void
     * @throws \Throwable
     */
    public function testCheckout()
    {
        $user = $this->subscribedUser();

        /** Prepare subscribed plan */
        Plan::factory()->state([
            'stripe_plan' => $user->defaultSubscription()->stripe_plan,
            'amount' => 3000
        ])->create();

        /** Prepare product stocks */
        $stock = ProductStock::factory()
                             ->product()
                             ->state(['price' => 0])
                             ->hasAttached(
                                 ProductVariantOption::factory()->product(), [],
                                 'combinations'
                             )
                             ->has(ProductStockInventory::factory(), 'inventories');

        /** Prepare product */
        Product::factory()
               ->count(2)
               ->state(['price' => 500])
               ->has($stock, 'stocks')
               ->create();

        /** Prepare cart items */
        Cart::factory()
            ->state(['user_id' => $user->id])
            ->has(CartItem::factory()
                          ->count(2)
                          ->state(new Sequence(
                              ['product_id' => 1, 'product_stock_id' => 1, 'product_type_id' => 1, 'quantity' => 1],
                              ['product_id' => 2, 'product_stock_id' => 2, 'product_type_id' => 2, 'quantity' => 1]
                          )), 'items')
            ->create();

        $this->mock(PaymentGateway::class, function (MockInterface $mock) {
            $mock->shouldNotHaveReceived('request');
        });

        $this->loginAs($user);

        $response = $this->json('post', 'api/checkout', [
            'shipping_address' => Address::factory()->make()->toArray(),
            'currency' => 'myr'
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'amount',
            'currency',
            'items' => [
                '*' => [
                    'unit_price',
                    'quantity',
                    'product'
                ]
            ]
        ]);
        $response->assertJsonMissing(['charge']);
    }

    /**
     *
     * @return void
     * @throws \Throwable
     */
    public function testCheckoutWithTimeslot()
    {
        $user = $this->subscribedUser();

        /** Prepare subscribed plan */
        Plan::factory()->state([
            'stripe_plan' => $user->defaultSubscription()->stripe_plan,
            'amount' => 3000
        ])->create();

        /** Prepare product stocks */
        $stock = ProductStock::factory()
                             ->product()
                             ->state(['price' => 0])
                             ->hasAttached(
                                 ProductVariantOption::factory()->product(), [],
                                 'combinations'
                             )
                             ->has(ProductStockInventory::factory(), 'inventories');

        /** Prepare product */
        Product::factory()
               ->count(2)
               ->state(['price' => 500])
               ->has($stock, 'stocks')
               ->create();

        /** Prepare cart items */
        Cart::factory()
            ->state(['user_id' => $user->id])
            ->has(CartItem::factory()
                          ->count(2)
                          ->state(new Sequence(
                              ['product_id' => 1, 'product_stock_id' => 1, 'product_type_id' => 1, 'quantity' => 1],
                              ['product_id' => 2, 'product_stock_id' => 2, 'product_type_id' => 2, 'quantity' => 1]
                          )), 'items')
            ->create();

        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->create();

        $this->mock(PaymentGateway::class, function (MockInterface $mock) {
            $mock->shouldNotHaveReceived('request');
        });

        $this->loginAs($user);

        $response = $this->json('post', 'api/checkout', [
            'shipping_address' => Address::factory()->make()->toArray(),
            'currency' => 'myr',
            'timeslot' => $timeslot->id,
            'timeslot_date' => now()->addDay()->format('Y-m-d')
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'amount',
            'currency',
            'items' => [
                '*' => [
                    'unit_price',
                    'quantity',
                    'product'
                ]
            ],
            'timeslots' => [
                '*' => [
                    'id',
                    'timeslot' => [
                        'type' => [
                            'id',
                            'name'
                        ]
                    ],
                    'start_at',
                    'end_at'
                ]
            ]
        ]);
        $response->assertJsonMissing(['charge']);
    }

    /**
     *
     * @return void
     * @throws \Throwable
     */
    public function testCheckoutWithFullyBookedTimeslot()
    {
        $user = $this->subscribedUser();

        /** Prepare subscribed plan */
        Plan::factory()->state([
            'stripe_plan' => $user->defaultSubscription()->stripe_plan,
            'amount' => 3000
        ])->create();

        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()
                            ->has(TimeslotAllocation::factory(), 'allocations')
                            ->state(['limit' => 1])
                            ->create();

        $this->loginAs($user);

        $response = $this->json('post', 'api/checkout', [
            'shipping_address' => Address::factory()->make()->toArray(),
            'currency' => 'myr',
            'timeslot' => $timeslot->id,
            'timeslot_date' => now()->addDay()->format('Y-m-d')
        ]);

        $response->assertStatus(400)
                 ->assertJson([
                     'message' => __('timeslot.full')
                 ]);
    }

    /**
     *
     * @return void
     * @throws \Throwable
     */
    public function testCheckoutWithoutDefaultShippingAddress()
    {
        $user = $this->subscribedUser();

        /** Prepare subscribed plan */
        Plan::factory()->state([
            'stripe_plan' => $user->defaultSubscription()->stripe_plan,
            'amount' => 3000
        ])->create();

        /** Prepare product stocks */
        $stock = ProductStock::factory()
                             ->product()
                             ->state(['price' => 0])
                             ->hasAttached(
                                 ProductVariantOption::factory()->product(), [],
                                 'combinations'
                             )
                             ->has(ProductStockInventory::factory(), 'inventories');

        /** Prepare product */
        Product::factory()
               ->count(2)
               ->state(['price' => 500])
               ->has($stock, 'stocks')
               ->create();

        /** Prepare cart items */
        Cart::factory()
            ->state(['user_id' => $user->id])
            ->has(CartItem::factory()
                          ->count(2)
                          ->state(new Sequence(
                              ['product_id' => 1, 'product_stock_id' => 1, 'product_type_id' => 1, 'quantity' => 1],
                              ['product_id' => 2, 'product_stock_id' => 2, 'product_type_id' => 2, 'quantity' => 1]
                          )), 'items')
            ->create();

        $this->mock(PaymentGateway::class, function (MockInterface $mock) {
            $mock->shouldNotHaveReceived('request');
        });

        $this->loginAs($user);

        $response = $this->json('post', 'api/checkout', [
            'currency' => 'myr'
        ]);

        $response->assertStatus(400)
                 ->assertJson([
                     'message' => __('profile.not_found.shipping_address')
                 ]);
    }

    /**
     *
     * @return void
     * @throws \Throwable
     */
    public function testCheckoutFailedDueToOutOfStock()
    {
        $user = $this->subscribedUser();

        $user->profile->addresses()->save(Address::factory()->make());
        $user->profile->defaultShippingAddress()->associate(1)->save();

        /** Prepare subscribed plan */
        Plan::factory()->state([
            'stripe_plan' => $user->defaultSubscription()->stripe_plan,
            'amount' => 3000
        ])->create();

        /** Prepare product stocks */
        $stock = ProductStock::factory()
                             ->product()
                             ->state(['price' => 0])
                             ->hasAttached(
                                 ProductVariantOption::factory()->product(), [],
                                 'combinations'
                             );

        /** Prepare product */
        Product::factory()
               ->count(2)
               ->state(['price' => 500])
               ->has($stock, 'stocks')
               ->create();

        /** Prepare cart items */
        Cart::factory()
            ->state(['user_id' => $user->id])
            ->has(CartItem::factory()
                          ->count(2)
                          ->state(new Sequence(
                              ['product_id' => 1, 'product_stock_id' => 1, 'product_type_id' => 1, 'quantity' => 1],
                              ['product_id' => 2, 'product_stock_id' => 2, 'product_type_id' => 1, 'quantity' => 1]
                          )), 'items')
            ->create();

        $this->mock(PaymentGateway::class, function (MockInterface $mock) {
            $mock->shouldNotHaveReceived('request');
        });

        $this->loginAs($user);

        $response = $this->json('post', 'api/checkout', ['currency' => 'myr']);

        $response->assertStatus(400);
    }

    /**
     * Checkout with Surcharge Policies Applied
     *
     * @return void
     * @throws \Throwable
     */
    public function testCheckoutWithCharges()
    {
        $user = $this->subscribedUser();

        /** Prepare product stocks */
        $stock = ProductStock::factory()
                             ->product()
                             ->state(['price' => 0])
                             ->hasAttached(
                                 ProductVariantOption::factory()->product(), [],
                                 'combinations'
                             )
                             ->has(ProductStockInventory::factory(), 'inventories');

        /** Prepare product */
        Product::factory()
               ->count(2)
               ->state(['price' => 1000])
               ->has($stock, 'stocks')
               ->create();

        /** Prepare surcharges */
        $surcharges = Surcharge::factory()
                              ->count(2)
                              ->state(new Sequence(
                                  ['threshold' => 1500, 'charge' => 150],
                                  ['threshold' => 3000, 'charge' => 300]
                              ));

        /** Prepare subscribed plan */
        Plan::factory()->state([
            'stripe_plan' => $user->defaultSubscription()->stripe_plan,
            'purchase_amount' => 1000
        ])->has($surcharges, 'surcharges')->create();

        /** Prepare cart items */
        Cart::factory()
            ->state(['user_id' => $user->id])
            ->has(CartItem::factory()
                          ->count(2)
                          ->state(new Sequence(
                              ['product_id' => 1, 'product_stock_id' => 1, 'product_type_id' => 1, 'quantity' => 1],
                              ['product_id' => 2, 'product_stock_id' => 2, 'product_type_id' => 2, 'quantity' => 1]
                          )), 'items')
            ->create();

        $this->loginAs($user);

        $invoice = \Stripe\Invoice::constructFrom([
            'id' => 'in_' . Str::random(),
            'payment_intent' => 'pm_intent_' . Str::random(),
            'amount_paid' => amountToCent(300),
            'currency' => 'myr',
            'status' => 'paid',
            'invoice_pdf' => null,
            'discount' => null,
        ]);
        $this->partialMock(OrderService::class, function (MockInterface $mock) use ($user, $invoice) {
            $mock->shouldReceive('processCharges')
                 ->once()
                 ->andReturn($invoice);
        });

        $response = $this->json('post', 'api/checkout', [
            'shipping_address' => Address::factory()->make()->toArray(),
            'currency' => 'myr'
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'amount',
            'currency',
            'items' => [
                '*' => [
                    'unit_price',
                    'quantity',
                    'product'
                ]
            ],
            'invoice' => [
                'stripe_id',
                'payment_intent',
                'amount',
                'currency',
                'status'
            ]
        ]);
    }


    /**
     * Checkout with Surcharge Policies Applied
     *
     * @return void
     * @throws \Throwable
     */
    public function testShopCheckoutWithCharges()
    {
        $user = User::factory()
            ->has(Profile::factory())
            ->create();

        /** Prepare product stocks */
        $stock = ProductStock::factory()
            ->product()
            ->state(['price' => 0])
            ->hasAttached(
                ProductVariantOption::factory()->product(), [],
                'combinations'
            )
            ->has(ProductStockInventory::factory(), 'inventories');

        /** Prepare product */
        Product::factory()
            ->count(2)
            ->state(['price' => 1000, 'business_type_id'=> BusinessType::SHOP])
            ->has($stock, 'stocks')
            ->create();


        /** Prepare cart items */
        Cart::factory()
            ->state(['user_id' => $user->id, 'business_type_id' => BusinessType::SHOP])
            ->has(CartItem::factory()
                ->count(2)
                ->state(new Sequence(
                    ['product_id' => 1, 'product_stock_id' => 1, 'product_type_id' => 1, 'quantity' => 1],
                    ['product_id' => 2, 'product_stock_id' => 2, 'product_type_id' => 2, 'quantity' => 1]
                )), 'items')
            ->create();

        $this->loginAs($user);

        $invoice = \Stripe\Invoice::constructFrom([
            'id' => 'in_' . Str::random(),
            'payment_intent' => 'pm_intent_' . Str::random(),
            'amount_paid' => amountToCent(300),
            'currency' => 'myr',
            'status' => 'paid',
            'invoice_pdf' => null,
            'discount' => null,
        ]);

        $response = $this->json('post', 'api/shop/checkout', [
            'shipping_address' => Address::factory()->make()->toArray(),
            'currency' => 'myr',
            'payment_method' => ['id' => 'pm_card_visa']
        ]);

        \Log::debug(json_encode($response));

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'amount',
            'currency',
            'items' => [
                '*' => [
                    'unit_price',
                    'quantity',
                    'product'
                ]
            ],
            'invoice' => [
                'stripe_id',
                'payment_intent',
                'amount',
                'currency',
                'status'
            ]
        ]);
    }

    /**
     *
     * @return void
     * @throws \Throwable
     */
    public function testCheckoutFailWhenExistingOrderIsPending()
    {
        $user = $this->subscribedUser();

        Order::factory()
             ->state(['user_id' => $user->id])
             ->create();

        $this->mock(PaymentGateway::class, function (MockInterface $mock) {
            $mock->shouldNotHaveReceived('request');
        });

        $this->loginAs($user);

        $response = $this->json('post', 'api/checkout', [
            'shipping_address' => Address::factory()->make()->toArray(),
            'currency' => 'myr'
        ]);

        $response->assertStatus(400)
                 ->assertJson([
                     'message' => __('order.failed.ongoing', ['count' => 1])
                 ]);
    }
}
