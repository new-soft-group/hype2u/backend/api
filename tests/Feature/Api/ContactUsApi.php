<?php

namespace Tests\Feature\Api;

use App\Mail\ContactUs;
use App\Models\Role;
use App\Models\User;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Laravel\Passport\Passport;
use Tests\TestCase;

class ContactUsApi extends TestCase
{
    use RefreshDatabase, WithFaker;

    public function setUp(): void
    {
        parent::setUp();

        Mail::fake();

        $this->seed(PermissionSeeder::class);

        $user = User::factory()->create();
        $user->assignRole(Role::ADMIN);

        Passport::actingAs($user);
    }

    public function testShouldBroadcastEmailToAdmin()
    {
        $response = $this->json('post', 'api/contact/us', [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->email,
            'question' => implode(' ', $this->faker->sentences),
        ]);

        $response->assertStatus(200);

        Mail::assertSent(ContactUs::class, 1);
    }

    public function testShouldBroadcastEmailToAllAdmin()
    {
        User::factory()->count(2)
                       ->create()
                       ->each(function (User $user) {
                           $user->assignRole(Role::ADMIN);
                       });

        $response = $this->json('post', 'api/contact/us', [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->email,
            'question' => implode(' ', $this->faker->sentences),
        ]);

        $response->assertStatus(200);

        Mail::assertSent(ContactUs::class, 3);
    }
}
