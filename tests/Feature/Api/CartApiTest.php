<?php


namespace Tests\Feature\Api;


use App\Models\Cart;
use App\Models\Image;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStatus;
use App\Models\Plan;
use App\Models\PlanPolicy;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\ProductType;
use App\Models\ProductVariantOption;
use App\Models\Subscription;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\FakeSetting;

 class CartApiTest extends TestCase
 {
     use RefreshDatabase, FakeSetting;

     protected function setUp(): void
     {
         parent::setUp();

         $this->setupSetting();

         Storage::fake('storage');

         $this->artisan('passport:install');
     }

     public function testShowCartWithoutSubscription()
     {
         $user = User::factory()
                     ->has(Cart::factory())
                     ->create();

         Passport::actingAs($user);

         $response = $this->json('get', 'api/carts');

         $response->assertStatus(400);
     }

     public function testShowCart()
     {
         /** @var User $user */
         $user = User::factory()
                     ->has(Subscription::factory())
                     ->has(Cart::factory())
                     ->create();

         $stocks = ProductStock::factory()
                                 ->count(2)
                                 ->hasAttached(
                                     ProductVariantOption::factory()->product(), [],
                                     'combinations'
                                 )->create();

         foreach ($stocks as $stock) {
             $user->getCart()->items()->create([
                 'product_id' => $stock->product_id,
                 'product_stock_id' => $stock->id,
                 'product_type_id' => 1,
                 'quantity' => 1
             ]);
         }

         Passport::actingAs($user);

         $response = $this->json('get', 'api/carts');

         $response->assertStatus(200);

         $response->assertJsonStructure([
             'items' => [
                 '*' => [
                     'id',
                     'quantity',
                     'product' => [
                         'name',
                         'description',
                         'sku',
                         'slug',
                         'images',
                         'pricing' => [
                             '*' => [
                                 'currency_code',
                                 'price'
                             ]
                         ],
                         'brand',
                         'categories',
                         'gender',
                         'type',
                         'featured',
                         'enabled',
                         'new_arrival',
                         'most_popular'
                     ],
                     'option' => [
                         'name',
                         'pricing' => [
                             '*' => [
                                 'currency_code',
                                 'price'
                             ]
                         ]
                     ]
                 ]
             ]
         ]);
     }

     public function testShowCartWithItemHaveNotBeenReturnYet()
     {
         Storage::fake('product');

         $stock = ProductStock::factory()
                              ->for(Product::factory()
                                           ->has(Image::factory()->product()));
         /** @var ProductStockInventory $inventory1 */
         $inventory1 = ProductStockInventory::factory()
                                             ->for($stock, 'stock')
                                             ->create();
         /** @var ProductStockInventory $inventory2 */
         $inventory2 = ProductStockInventory::factory()->create();
         /** @var User $user */
         $user = User::factory()
                     ->has(Subscription::factory())
                     ->create();
         /** @var Order $order */
         $order = Order::factory()
                       ->has(OrderItem::factory()
                                      ->count(2)
                                      ->state(new Sequence(
                                          ['product_stock_id' => $inventory1->product_stock_id],
                                          ['product_stock_id' => $inventory2->product_stock_id]
                                      ))
                           , 'items')
                       ->state([
                           'user_id' => $user->id,
                           'order_status_id' => OrderStatus::DELIVERED
                       ])->create();
         $order->tracks()->sync([$inventory1->id => ['scanned_at' => now()], $inventory2->id]);

         Passport::actingAs($user);

         $response = $this->json('get', 'api/carts');

         $response->assertStatus(200)
                  ->assertJson(['items' => []])
                  ->assertJsonCount(1, 'carry_forward')
                  ->assertJsonStructure([
                      'carry_forward' => [
                          '*' => [
                              'order_id',
                              'product' => [
                                  'id',
                                  'name',
                                  'description',
                                  'slug',
                                  'images' => [
                                      '*' => [
                                          'id',
                                          'url',
                                          'width',
                                          'height',
                                          'sort'
                                      ]
                                  ],
                                  'gender',
                                  'categories',
                                  'brand',
                                  'type'
                              ],
                              'variant',
                              'variant_price',
                              'unit_price',
                              'quantity',
                              'amount'
                          ]
                      ]
                  ]);
     }

     public function testShowCartWithEmptyItem()
     {
         $user = User::factory()
                     ->has(Subscription::factory())
                     ->create();

         Passport::actingAs($user);

         $response = $this->json('get', 'api/carts');

         $response->assertStatus(200);

         $response->assertJson([
             'items' => []
         ]);
     }

     public function testAddItem()
     {
         $types = ProductType::factory()
                             ->count(3)
                             ->state(new Sequence(
                                 ['name' => 'VIP'],
                                 ['name' => 'Street'],
                                 ['name' => 'Premium']
                             ));

         /** @var Plan $plan */
         $plan = Plan::factory()
                     ->has(PlanPolicy::factory()
                                     ->hasAttached($types, [], 'rules'),
                         'policies')
                     ->state(['purchase_amount' => 2000, 'exchange_limit'=> 1])
                     ->create();

         $user = User::factory()
                     ->has(Subscription::factory()->state(['stripe_plan' => $plan->stripe_plan]))
                     ->create();

         /** @var Product $product */
         $product = Product::factory()
                           ->has(ProductStock::factory()
                                             ->product()
                                             ->state(['price' => 0])
                                             ->hasAttached(
                                                 ProductVariantOption::factory()->product(), [],
                                                 'combinations'
                                             ), 'stocks')
                           ->state(['price' => 1000, 'product_type_id' => 1])
                           ->create();

         Passport::actingAs($user);

         $response = $this->json('post', 'api/carts', [
             'product_id' => $product->id,
             'product_stock_id' => 1,
             'product_type_id' => 1,
             'quantity' => 1
         ]);

         $response->assertStatus(200);

         $response->assertJsonStructure([
             'id',
             'quantity',
             'product' => [
                 'name',
                 'description',
                 'sku',
                 'slug',
                 'images',
                 'pricing' => [
                     '*' => [
                         'currency_code',
                         'price'
                     ]
                 ],
                 'brand',
                 'categories',
                 'gender',
                 'type',
                 'featured',
                 'enabled',
                 'new_arrival',
                 'most_popular'
             ],
             'option' => [
                 'name',
                 'pricing' => [
                     '*' => [
                         'currency_code',
                         'price'
                     ]
                 ]
             ]
         ]);
     }

     public function testShouldFailAddSameItemTwiceButPlanOnlyAllowOneItem()
     {
         $type = ProductType::factory()->state(['name' => 'VIP']);

         /** @var Plan $plan */
         $plan = Plan::factory()
                     ->has(PlanPolicy::factory()
                                     ->logicalNone()
                                     ->state(['quantity' => 1])
                                     ->hasAttached($type, [], 'rules'),
                         'policies')
                     ->state(['purchase_amount' => 2000])
                     ->create();

         /** @var User $user */
         $user = User::factory()
                     ->has(Cart::factory())
                     ->has(Subscription::factory()->state(['stripe_plan' => $plan->stripe_plan]))
                     ->create();

         /** @var Product $product */
         $product = Product::factory()
                           ->has(ProductStock::factory()
                                             ->product()
                                             ->state(['price' => 0])
                                             ->hasAttached(
                                                 ProductVariantOption::factory()->product(), [],
                                                 'combinations'
                                             ), 'stocks')
                           ->state(['price' => 1000])
                           ->create();

         $user->getCart()->addItem([
             'product_id' => $product->id,
             'product_stock_id' => 1,
             'product_type_id' => 1,
             'quantity' => 1
         ]);

         Passport::actingAs($user);

         $response = $this->json('post', 'api/carts', [
             'product_id' => $product->id,
             'product_stock_id' => 1,
             'product_type_id' => 1,
             'quantity' => 1
         ]);

         $response->assertStatus(400);
     }

     public function testShouldPassIfPolicyAllowTwoItems()
     {
         $type = ProductType::factory()->state(['name' => 'VIP']);

         /** @var Plan $plan */
         $plan = Plan::factory()
                     ->has(PlanPolicy::factory()
                                     ->logicalNone()
                                     ->state(['quantity' => 2])
                                     ->hasAttached($type, [], 'rules'),
                         'policies')
                     ->state(['purchase_amount' => 2000])
                     ->create();

         /** @var User $user */
         $user = User::factory()
                     ->has(Cart::factory())
                     ->has(Subscription::factory()->state(['stripe_plan' => $plan->stripe_plan]))
                     ->create();

         /** @var Product $product */
         $product = Product::factory()
                           ->has(ProductStock::factory()
                                             ->product()
                                             ->state(['price' => 0])
                                             ->hasAttached(
                                                 ProductVariantOption::factory()->product(), [],
                                                 'combinations'
                                             ), 'stocks')
                           ->state(['price' => 1000])
                           ->create();

         $user->getCart()->addItem([
             'product_id' => $product->id,
             'product_stock_id' => 1,
             'product_type_id' => 1,
             'quantity' => 1
         ]);

         Passport::actingAs($user);

         $response = $this->json('post', 'api/carts', [
             'product_id' => $product->id,
             'product_stock_id' => 1,
             'product_type_id' => 1,
             'quantity' => 1
         ]);

         $response->assertStatus(200);
     }

     public function testShouldFailWhenUpdateCartItemQuantityExceedPolicy()
     {
         $type = ProductType::factory()->state(['name' => 'VIP']);

         /** @var Plan $plan */
         $plan = Plan::factory()
                     ->has(PlanPolicy::factory()
                                     ->logicalNone()
                                     ->state(['quantity' => 2])
                                     ->hasAttached($type, [], 'rules'),
                         'policies')
                     ->state(['purchase_amount' => 2000])
                     ->create();

         /** @var User $user */
         $user = User::factory()
                     ->has(Cart::factory())
                     ->has(Subscription::factory()->state(['stripe_plan' => $plan->stripe_plan]))
                     ->create();

         /** @var Product $product */
         $product = Product::factory()
                           ->has(ProductStock::factory()
                                             ->product()
                                             ->state(['price' => 0])
                                             ->hasAttached(
                                                 ProductVariantOption::factory()->product(), [],
                                                 'combinations'
                                             ), 'stocks')
                           ->state(['price' => 1000])
                           ->create();

         $user->getCart()->addItem([
             'product_id' => $product->id,
             'product_stock_id' => 1,
             'product_type_id' => 1,
             'quantity' => 2
         ]);

         Passport::actingAs($user);

         $response = $this->put("api/carts/1", ['quantity' => 3]);

         $response->assertStatus(400);
     }

     public function testShouldPassWhenUpdateCartItemQuantityWithinPolicy()
     {
         $type = ProductType::factory()->state(['name' => 'VIP']);

         /** @var Plan $plan */
         $plan = Plan::factory()
                     ->has(PlanPolicy::factory()
                                     ->logicalNone()
                                     ->state(['quantity' => 3])
                                     ->hasAttached($type, [], 'rules'),
                         'policies')
                     ->state(['purchase_amount' => 3000])
                     ->create();

         /** @var User $user */
         $user = User::factory()
                     ->has(Cart::factory())
                     ->has(Subscription::factory()->state(['stripe_plan' => $plan->stripe_plan]))
                     ->create();

         /** @var Product $product */
         $product = Product::factory()
                           ->has(ProductStock::factory()
                                             ->product()
                                             ->state(['price' => 0])
                                             ->hasAttached(
                                                 ProductVariantOption::factory()->product(), [],
                                                 'combinations'
                                             ), 'stocks')
                           ->state(['price' => 1000])
                           ->create();

         $user->getCart()->addItem([
             'product_id' => $product->id,
             'product_stock_id' => 1,
             'product_type_id' => 1,
             'quantity' => 2
         ]);

         Passport::actingAs($user);

         $response = $this->put("api/carts/1", ['quantity' => 3]);

         $response->assertStatus(200);
     }

     public function testDeleteCartItem()
     {
         /** @var User $user */
         $user = User::factory()
                     ->has(Subscription::factory())
                     ->create();

         /** @var ProductStock $stock */
         $stock = ProductStock::factory()
                              ->hasAttached(
                                  ProductVariantOption::factory()->product(), [],
                                  'combinations'
                              )->create();

         $item = $user->getCart()->addItem([
             'product_id' => $stock->product_id,
             'product_stock_id' => $stock->id,
             'product_type_id' => 1,
             'quantity' => 1
         ]);

         Passport::actingAs($user);

         $response = $this->delete("api/carts/$item->id");

         $response->assertStatus(200);

         $response->assertJson(['result' => true]);
     }

     public function testDeleteCartItemShouldFail()
     {
         /** @var User $user */
         $user = User::factory()
                     ->has(Subscription::factory())
                     ->create();

         Passport::actingAs($user);

         $response = $this->delete("api/carts/1");

         $response->assertStatus(200);

         $response->assertJson(['result' => false]);
     }

     public function testShouldIncludeItemNotReturnIntoValidation()
     {
         $type = ProductType::factory()->state(['name' => 'VIP']);
         /** @var Plan $plan */
         $plan = Plan::factory()
                     ->has(PlanPolicy::factory()
                                     ->logicalNone()
                                     ->state(['quantity' => 2])
                                     ->hasAttached($type, [], 'rules'),
                         'policies')
                     ->state(['purchase_amount' => 2000])
                     ->create();
         /** @var User $user */
         $user = User::factory()
                     ->has(Cart::factory())
                     ->has(Subscription::factory()->state(['stripe_plan' => $plan->stripe_plan]))
                     ->create();
         /** @var Product $product */
         $product = Product::factory()
                           ->has(ProductStock::factory()
                                             ->product()
                                             ->state(['price' => 0])
                                             ->has(ProductStockInventory::factory()->count(2), 'inventories')
                                             ->hasAttached(
                                                 ProductVariantOption::factory()->product(), [],
                                                 'combinations'
                                             ), 'stocks')
                           ->state(['price' => 1000])
                           ->create();
         /**
          * @var Order $exist
          * Simulate existing order
          */
         $exist = Order::factory()
                       ->state([
                           'user_id' => $user->id,
                           'order_status_id' => OrderStatus::PARTIAL_RETURNED
                       ])
                       ->has(OrderItem::factory()
                                      ->count(2)
                                      ->state([
                                          'product_id' => $product->id,
                                          'product_stock_id' => 1,
                                          'variant' => 'Black'
                                      ]), 'items')
                       ->create();
         /**
          * Simulate existing partial returned order
          */
         $exist->tracks()->sync([1 => ['scanned_at' => now()], 2]);
         /**
          * Login test user
          */
         Passport::actingAs($user);

         $response = $this->json('post', 'api/carts', [
             'product_id' => $product->id,
             'product_stock_id' => 1,
             'product_type_id' => 1,
             'quantity' => 1
         ]);

         $response->assertStatus(400)
                  ->assertJson([
                      'message' => __('plan.policies', [
                          'message' => '2 VIP'
                      ])
                  ]);
     }

     public function testShouldValidateDeliveredItemsAmount()
     {
         $type = ProductType::factory()->state(['name' => 'VIP']);
         /** @var Plan $plan */
         $plan = Plan::factory()
                     ->has(PlanPolicy::factory()
                                     ->logicalNone()
                                     ->state(['quantity' => 2])
                                     ->hasAttached($type, [], 'rules'),
                         'policies')
                     ->state(['purchase_amount' => 1000])
                     ->create();
         /** @var User $user */
         $user = User::factory()
                     ->has(Cart::factory())
                     ->has(Subscription::factory()->state(['stripe_plan' => $plan->stripe_plan]))
                     ->create();
         /** @var Product $product */
         $product = Product::factory()
                           ->has(ProductStock::factory()
                                             ->product()
                                             ->state(['price' => 0])
                                             ->has(ProductStockInventory::factory(), 'inventories')
                                             ->hasAttached(
                                                 ProductVariantOption::factory()->product(), [],
                                                 'combinations'
                                             ), 'stocks')
                           ->state(['price' => 1000, 'product_type_id' => 1])
                           ->create();
         /**
          * @var Order $exist
          * Simulate existing order
          */
         $exist = Order::factory()
                       ->state([
                           'user_id' => $user->id,
                           'order_status_id' => OrderStatus::PARTIAL_RETURNED
                       ])
                       ->has(OrderItem::factory()
                                      ->count(2)
                                      ->state([
                                          'product_id' => $product->id,
                                          'product_stock_id' => 1,
                                          'variant' => 'Black'
                                      ]), 'items')
                       ->create();
         /**
          * Simulate existing partial returned order
          */
         $exist->tracks()->sync([1]);
         /**
          * Login test user
          */
         Passport::actingAs($user);

         $response = $this->json('post', 'api/carts', [
             'product_id' => $product->id,
             'product_stock_id' => 1,
             'product_type_id' => 1,
             'quantity' => 1
         ]);

         $response->assertStatus(400)
                  ->assertJson([
                      'message' => __('plan.exceed.amount', [
                          'plan' => $plan->name,
                          'amount' => $plan->getThreshold()
                      ])
                  ]);
     }

     public function testAddToCartShouldFailWhenUserHasOngoingOrders()
     {
         /** @var User $user */
         $user = User::factory()
                     ->has(Cart::factory())
                     ->has(Subscription::factory())
                     ->create();

         /**
          * Simulate existing order
          */
         $orders = Order::factory()
              ->count(2)
              ->state([
                  'user_id' => $user->id,
                  'order_status_id' => OrderStatus::PENDING
              ])
              ->create();
         /**
          * Login test user
          */
         Passport::actingAs($user);
         /** @var Product $product */
         $product = Product::factory()
                           ->has(ProductStock::factory()->product(), 'stocks')
                           ->create();

         $response = $this->json('post', 'api/carts', [
             'product_id' => $product->id,
             'product_stock_id' => 1,
             'product_type_id' => $product->type->id,
             'quantity' => 1
         ]);

         $response->assertStatus(400)
                  ->assertJson([
                      'message' => __('order.failed.ongoing', ['count' => $orders->count()])
                  ]);
     }
 }
