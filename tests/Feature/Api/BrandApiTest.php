<?php


namespace Tests\Feature\Api;


use App\Models\Brand;
use App\Models\Product;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\FakeLogin;

class BrandApiTest extends TestCase
{
    use RefreshDatabase, FakeLogin, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setupPermission();
    }

    public function testIndex()
    {
        Brand::factory()->count(2)->create();

        $response = $this->json('get', 'api/brands');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'name'
            ]
        ]);
    }

    public function testCreate()
    {
        $this->login(Role::ADMIN);

        $attributes = Brand::factory()->make()->toArray();

        $response = $this->json('post', 'api/brands', $attributes);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'name'
        ]);
    }

    public function testUpdate()
    {
        $this->login(Role::ADMIN);

        $brands = Brand::factory()->count(2)->create();

        $attributes = $brands->map(function (Brand $brand) {
            return ['id' => $brand->id, 'name' => $this->faker->name];
        })->toArray();

        $response = $this->json('patch', 'api/brands', [
            'brands' => $attributes
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'name'
            ]
        ]);
    }

    public function testDelete()
    {
        $this->login(Role::ADMIN);

        /** @var Brand $brand */
        $brand = Brand::factory()->create();

        $response = $this->json('delete', "api/brands/$brand->id");

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);
    }

    public function testDeleteFail()
    {
        $this->login(Role::ADMIN);

        /** @var Brand $brand */
        $brand = Brand::factory()->has(Product::factory())->create();

        $response = $this->json('delete', "api/brands/$brand->id");

        $response->assertStatus(400);

        $response->assertJson([
            'message' => __('brand.delete.failed'),
            'status' => 400
        ]);
    }
}
