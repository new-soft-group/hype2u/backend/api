<?php

namespace Tests\Feature\Api;

use App\Gateways\PaymentGateway;
use App\Models\Plan;
use App\Models\PlanPolicy;
use App\Models\ProductType;
use App\Models\Role;
use App\Models\Surcharge;
use App\Models\User;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Laravel\Passport\Passport;
use Mockery\MockInterface;
use Tests\TestCase;
use Tests\Traits\FakeImage;
use Tests\Traits\MockStripeResponse;

class PlanApiTest extends TestCase
{
    use RefreshDatabase, WithFaker, FakeImage, MockStripeResponse;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(PermissionSeeder::class);

        $user = User::factory()->create();
        $user->assignRole(Role::ADMIN);

        Passport::actingAs($user);
    }

    public function testShow()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->has(PlanPolicy::factory()
                                    ->hasAttached(
                                        ProductType::factory(),
                                        [],
                                        'rules'),
                        'policies')
                    ->has(Surcharge::factory())
                    ->create();

        $response = $this->get("api/plans/$plan->id");

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'id',
                     'name',
                     'stripe_plan',
                     'amount',
                     'slug',
                     'description',
                     'exchange_limit',
                     'sort',
                     'enabled',
                     'policies' => [
                         '*' => [
                             'logical',
                             'quantity',
                             'rules' => [
                                 '*' => [
                                     'id',
                                     'name',
                                     'prime'
                                 ]
                             ]
                         ]
                     ]
                 ]);
    }

    public function testCreatePlan()
    {
        ProductType::factory()->create();

        /** @var Plan $plan */
        $plan = Plan::factory()->make([
            'policies' => [
                [
                    'quantity' => 1,
                    'logical' => PlanPolicy::LOGICAL_NONE,
                    'rules' => [
                        [
                            'id' => 1,
                            'prime' => false
                        ]
                    ]
                ]
            ]
        ]);

        $mockResponse = $this->mockStripeResponse([
            'id' => 'plan_'.Str::random(),
            'product' => 'prod_'.Str::random()
        ]);
        $this->mock(PaymentGateway::class, function (MockInterface $mock) use ($plan, $mockResponse) {
            $mock->shouldReceive('request')
                 ->with('post', '/v1/plans', $plan->getStripeOption(), null)
                 ->once()
                 ->andReturn($mockResponse);
        });

        $response = $this->json('post', 'api/plans', $plan->toArray());

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'name',
            'stripe_plan',
            'amount',
            'slug',
            'description',
            'exchange_limit',
            'sort',
            'enabled'
        ]);
    }

    public function testCreatePlanWithInvalidThreshold()
    {
        $plan = Plan::factory()
                    ->make([
                        'amount' => 2000,
                        'surcharges' => [
                            ['threshold' => 1000, 'charge' => 100]
                        ]
                    ]);

        $response = $this->json('post', 'api/plans', $plan->toArray());

        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'surcharges.0.threshold' => [
                    __('plan.threshold.invalid')
                ]
            ]
        ]);
    }

    public function testCreatePlanWithSurcharges()
    {
        ProductType::factory()->create();

        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->make([
                        'amount' => 2000,
                        'policies' => [
                            [
                                'quantity' => 1,
                                'logical' => PlanPolicy::LOGICAL_NONE,
                                'rules' => [
                                    [
                                        'id' => 1,
                                        'prime' => false
                                    ]
                                ]
                            ]
                        ],
                        'surcharges' => [
                            ['threshold' => 2500, 'charge' => 100]
                        ]
                    ]);

        $mockResponse = $this->mockStripeResponse([
            'id' => 'plan_'.Str::random(),
            'product' => 'prod_'.Str::random()
        ]);
        $this->mock(PaymentGateway::class, function (MockInterface $mock) use ($plan, $mockResponse) {
            $mock->shouldReceive('request')
                 ->with('post', '/v1/plans', $plan->getStripeOption(), null)
                 ->once()
                 ->andReturn($mockResponse);
        });

        $response = $this->json('post', 'api/plans', $plan->toArray());

        $response->assertStatus(200);
    }

    public function testUpdatePlan()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()->create();
        /** @var Plan $attributes */
        $attributes = Plan::factory()->make();

        $this->mock(PaymentGateway::class, function (MockInterface $mock) use ($plan, $attributes) {
            $mock->shouldReceive('request')
                 ->with('post', '/v1/products/'.$plan->stripe_product, [
                     'name' => $attributes->name
                 ], null)
                 ->once();
        });

        $response = $this->json('put', "api/plans/$plan->id", $attributes->toArray());

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'name',
            'stripe_plan',
            'amount',
            'slug',
            'description',
            'exchange_limit',
            'sort',
            'enabled'
        ]);
    }

    public function testUpdatePlanWithExistingSurchargeUsingInvalidThreshold()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->has(Surcharge::factory())
                    ->state(['amount' => 2000])
                    ->create();

        $attributes = [
            'surcharges' => [
                ['threshold' => 1000, 'charge' => 100]
            ]
        ];

        $response = $this->json('put', "api/plans/$plan->id", $attributes);

        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
                'surcharges.0.threshold' => [
                    __('plan.threshold.invalid')
                ]
            ]
        ]);
    }

    public function testUpdatePlanWithExistingSurcharge()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->has(Surcharge::factory())
                    ->state(['amount' => 2000])
                    ->create();

        $attributes = [
            'surcharges' => [
                ['threshold' => 3000, 'charge' => 100]
            ]
        ];

        $response = $this->json('put', "api/plans/$plan->id", $attributes);

        $response->assertStatus(200);
    }

    public function testUpdateStatus()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()
                    ->state(['enabled' => true])
                    ->create();

        $this->mock(PaymentGateway::class, function (MockInterface $mock) use ($plan) {
            $mock->shouldReceive('request')
                 ->with('post', '/v1/plans/'.$plan->stripe_plan, [
                     'active' => false
                 ], null)
                 ->once();
        });

        $response = $this->json('put', "api/plans/$plan->id/status", ['enabled' => false]);

        $response->assertStatus(200);
        $response->assertJson(['enabled' => false]);
        $response->assertJsonStructure([
            'id',
            'name',
            'stripe_plan',
            'amount',
            'slug',
            'description',
            'exchange_limit',
            'sort',
            'enabled'
        ]);
    }

    public function testUpdateSort()
    {
        /** @var Plan $plan */
        $plan = Plan::factory()->create();

        $response = $this->json('put', "api/plans/$plan->id/sort", [
            'sort' => 1
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'name',
            'stripe_plan',
            'amount',
            'slug',
            'description',
            'exchange_limit',
            'sort',
            'enabled'
        ]);
    }
}
