<?php


namespace Tests\Feature\Api;


use App\Models\Coupon;
use App\Models\Plan;
use App\Models\Role;
use App\Models\User;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Mockery\MockInterface;
use Str;
use Stripe\Charge;
use Stripe\Customer;
use Stripe\PaymentMethod;
use Stripe\Stripe;
use Stripe\Subscription;
use Tests\TestCase;
use Tests\Traits\MockSubscription;

class SubscriptionApiTest extends TestCase
{
    use RefreshDatabase, MockSubscription;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(PermissionSeeder::class);
    }

    public function getMockPayment(): string
    {
        return 'pm_'.Str::random();
    }

    public function testSubscription()
    {
        $user = $this->subscribedUser();
        /** @var Plan $plan */
        $plan = Plan::factory()->create();
        $payment = $this->getMockPayment();

        $this->mock('overload:Laravel\Cashier\SubscriptionBuilder',
            function (MockInterface $mock) use ($user, $payment) {
                $mock->shouldReceive('create')
                     ->with($payment)
                     ->once()
                     ->andReturn($user->subscriptions->first());
            });
        $user->assignRole(Role::USER);
        Passport::actingAs($user);

        $response = $this->json('post', 'api/subscriptions', [
            'stripe_plan' => $plan->stripe_plan,
            'payment' => $payment,
        ]);

        $response->assertStatus(200);
        $response->assertJson(['result' => true]);
    }

    public function testSubscriptionWithCoupon()
    {
        $user = $this->subscribedUser();
        /** @var Plan $plan */
        $plan = Plan::factory()->create();
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->create();
        /** Mock stripe payment */
        $payment = $this->getMockPayment();

        $this->mock('overload:Laravel\Cashier\SubscriptionBuilder',
            function (MockInterface $mock) use ($user, $payment, $coupon) {
                $mock->shouldReceive('create')
                     ->with($payment)
                     ->once()
                     ->andReturn($user->subscriptions->first());

                $mock->shouldReceive('withCoupon')
                     ->with($coupon->stripe_id)
                     ->once();
            });
        $user->assignRole(Role::USER);
        Passport::actingAs($user);

        $response = $this->json('post', 'api/subscriptions', [
            'stripe_plan' => $plan->stripe_plan,
            'payment' => $payment,
            'coupon' => $coupon->code
        ]);

        $response->assertStatus(200);
        $response->assertJson(['result' => true]);
    }
}
