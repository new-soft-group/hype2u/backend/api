<?php

namespace Tests\Feature\Api;

use App\Models\Address;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Profile;
use App\Models\Role;
use App\Models\Subscription;
use App\Models\User;
use Database\Seeders\PermissionSeeder;
use Hash;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\FakeSetting;

class UserApiTest extends TestCase
{
    use RefreshDatabase, WithFaker, FakeSetting;

    /**
     * @var User|User[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model
     */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(PermissionSeeder::class);

        $this->cacheSetting();

        $this->user = User::factory()
                          ->has(Profile::factory())
                          ->create();

        $this->user->assignRole(Role::USER);

        Passport::actingAs($this->user);
    }

    /**
     * Get user's profile
     *
     * @return void
     * @throws \Throwable
     */
    public function testGetUserProfile()
    {
        $response = $this->json('get', '/api/users');

        $response->assertStatus(200);

        $response->assertJson([
            'id' => $this->user->id,
            'email' => $this->user->email,
            'profile' => [
                'first_name' => $this->user->profile->first_name,
                'last_name' => $this->user->profile->last_name,
                'name' => $this->user->profile->name,
                'mobile' => $this->user->profile->mobile,
            ]
        ]);
    }

    /**
     * Update user's profile
     */
    public function testUpdateProfile()
    {
        $response = $this->json('post', '/api/users', [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'mobile' => $this->faker->e164PhoneNumber
        ]);

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);
    }

    /**
     * Create user's address
     *
     * @return void
     * @throws \Throwable
     */
    public function testCreateAddress()
    {
        $data = [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'mobile' => $this->faker->e164PhoneNumber,
            'address_1' => $this->faker->address,
            'city' => $this->faker->city,
            'state' => $this->faker->state,
            'country' => $this->faker->countryCode,
            'postcode' => $this->faker->postcode
        ];

        $response = $this->json('post', '/api/users/addresses', $data);

        $response->assertStatus(200);
        $response->assertJson($data);
    }

    /**
     * List user's addresses
     */
    public function testListAddress()
    {
        Address::factory()
               ->count(2)
               ->create([
                   'addressable_id' => $this->user->profile->id,
                   'addressable_type' => Profile::class
               ]);

        $response = $this->json('get', '/api/users/addresses');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'first_name',
                    'last_name',
                    'mobile',
                    'company',
                    'address_1',
                    'address_2',
                    'city',
                    'state',
                    'country',
                    'postcode'
                ]
            ],
            'meta' => [
                'default_shipping',
                'default_billing'
            ]
        ]);
    }

    /**
     * Update user's address
     */
    public function testUpdateAddress()
    {
        /** @var Address $address */
        $address = Address::factory()
                          ->create([
                              'addressable_id' => $this->user->profile->id,
                              'addressable_type' => Profile::class
                          ]);

        $data = ['first_name' => $this->faker->firstName];

        $response = $this->json('put', '/api/users/addresses/'.$address->id, $data);

        $response->assertStatus(200);
        $response->assertJson([
            'id' => $address->id,
            'first_name' => $data['first_name']
        ]);
    }

    /**
     * Update other user's address
     */
    public function testUpdateAddressNotBelongToUser()
    {
        /** @var Address $address */
        $address = Address::factory()
                          ->create([
                              'addressable_id' => $this->user->profile->id,
                              'addressable_type' => Profile::class
                          ]);

        /** @var User $otherUser */
        $otherUser = User::factory()
                         ->has(Profile::factory()->has(Address::factory()))
                         ->create();
        $otherUser->assignRole(Role::USER);
        Passport::actingAs($otherUser);

        $response = $this->json('put', '/api/users/addresses/'.$address->id, [
            'first_name' => $this->faker->firstName
        ]);

        $response->assertStatus(403);
    }

    /**
     * Delete other user's address
     */
    public function testDeleteAddressNotBelongToUser()
    {
        /** @var Address $address */
        $address = Address::factory()
                          ->create([
                              'addressable_id' => $this->user->profile->id,
                              'addressable_type' => Profile::class
                          ]);

        /** @var User $otherUser */
        $otherUser = User::factory()
                         ->has(Profile::factory()->has(Address::factory()))
                         ->create();
        $otherUser->assignRole(Role::USER);
        Passport::actingAs($otherUser);

        $response = $this->json('delete', '/api/users/addresses/'.$address->id);

        $response->assertStatus(403);
    }

    /**
     * Delete user's address
     */
    public function testDeleteAddress()
    {
        /** @var Address $address */
        $address = Address::factory()
                          ->count(2)
                          ->create([
                              'addressable_id' => $this->user->profile->id,
                              'addressable_type' => Profile::class
                          ]);

        $address = $address->first();

        $response = $this->json('delete', '/api/users/addresses/'.$address->id);

        $response->assertStatus(200);
        $response->assertJson(['result' => true]);
    }

    /**
     * Set user's default shipping address
     */
    public function testSetDefaultShippingAddress()
    {
        /** @var Address $address */
        $address = Address::factory()
                          ->create([
                              'addressable_id' => $this->user->profile->id,
                              'addressable_type' => Profile::class
                          ]);

        $response = $this->json('put', "/api/users/addresses/shipping/$address->id");

        $response->assertStatus(200);
        $response->assertJson(['result' => true]);
        $this->assertDatabaseHas('profiles', [
            'id' => $this->user->profile->id,
            'shipping_address_id' => $address->id
        ]);
    }

    /**
     * Set user's default billing address
     */
    public function testSetDefaultBillingAddress()
    {
        /** @var Address $address */
        $address = Address::factory()
                          ->create([
                              'addressable_id' => $this->user->profile->id,
                              'addressable_type' => Profile::class
                          ]);

        $response = $this->json('put', "/api/users/addresses/billing/$address->id");

        $response->assertStatus(200);
        $response->assertJson(['result' => true]);
        $this->assertDatabaseHas('profiles', [
            'id' => $this->user->profile->id,
            'billing_address_id' => $address->id
        ]);
    }

    /**
     * Set user's default billing address
     */
    public function testUpdatePassword()
    {
        $newPassword = 'newPassword';

        $response = $this->json('post', '/api/users/password', [
            'old_password' => 'password',
            'password' => $newPassword,
            'password_confirmation' => $newPassword
        ]);

        $response->assertStatus(200);
        $response->assertJson(['result' => true]);
        $this->assertTrue(Hash::check($newPassword, $this->user->password));
    }

    /**
     * Set user's default billing address
     */
    public function testGetSubscriptionsWithoutAnySubscribedPlan()
    {
        $response = $this->json('get', '/api/users/subscriptions');

        $response->assertStatus(200)
                 ->assertJson([]);
    }

    public function testGetOrders()
    {
        Order::factory()
             ->count(5)
             ->has(OrderItem::factory(), 'items')
             ->state(['user_id' => $this->user->id])
             ->create();

        $response = $this->json('get', 'api/users/orders');

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'data' => [
                         '*' => [
                             'amount',
                             'currency',
                             'items' => [
                                 '*' => [
                                     'variant'
                                 ]
                             ]
                         ]
                     ]
                 ]);
    }

    public function testShowOrder()
    {
        /** @var Order $order */
        $order = Order::factory()
                      ->has(OrderItem::factory(), 'items')
                      ->state(['user_id' => $this->user->id])
                      ->create();

        $response = $this->json('get', "api/users/orders/$order->id");

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'amount',
                     'currency',
                     'items' => [
                         '*' => [
                             'variant'
                         ]
                     ],
                     'shipping_address',
                     'invoice'
                 ]);
    }

    public function testGetInvoices()
    {
        Subscription::factory()
                    ->state(['user_id' => $this->user->id])
                    ->create();

        Order::factory()
             ->state(['user_id' => $this->user->id])
             ->has(Invoice::factory())
             ->has(OrderItem::factory(), 'items')
             ->create();

        $this->user->defaultSubscription()
                   ->invoices()
                   ->save(Invoice::factory()->make());

        $response = $this->json('get', "api/users/invoices");

        $response->assertStatus(200)
                 ->assertJsonCount(2, 'data')
                 ->assertJsonStructure([
                     'data' => [
                         '*' => [
                             'id',
                             'amount',
                             'currency',
                             'status',
                             'pdf'
                         ]
                     ]
                 ]);
    }
}
