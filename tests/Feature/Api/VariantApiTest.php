<?php


namespace Tests\Feature\Api;


use App\Models\Product;
use App\Models\Role;
use App\Models\User;
use App\Models\Variant;
use App\Models\VariantOption;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;

class VariantApiTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(PermissionSeeder::class);

        $user = User::factory()
                    ->create();
        $user->assignRole(Role::ADMIN);

        Passport::actingAs($user);
    }

    public function testUpdateVariantWithNewOption()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small']
                ]
            ]
        ]);

        $response = $this->json('put', "api/products/variants/".$variants->get(0)->id, [
            'name' => 'Size',
            'options' => [
                [
                    'name' => 'Medium'
                ]
            ]
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'id' => 1,
            'name' => 'Size',
            'removable' => true,
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Large',
                    'removable' => true,
                ],
                [
                    'id' => 2,
                    'name' => 'Small',
                    'removable' => true,
                ],
                [
                    'id' => 3,
                    'name' => 'Medium',
                    'removable' => true,
                ]
            ]
        ]);
    }

    public function testUpdateVariantWithOption()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small']
                ]
            ]
        ]);

        $response = $this->json('put', 'api/products/variants/'.$variants->get(0)->id, [
            'name' => 'Size',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'S'
                ]
            ]
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'id' => 1,
            'name' => 'Size',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'S'
                ],
                [
                    'id' => 2,
                    'name' => 'Small'
                ]
            ]
        ]);
    }

    public function testRemoveVariantOption()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small']
                ]
            ]
        ]);

        $response = $this->json('put', 'api/products/variants/'.$variants->get(0)->id, [
            'name' => 'Size',
            'removed_options' => [1]
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'id' => 1,
            'name' => 'Size',
            'options' => [
                [
                    'id' => 2,
                    'name' => 'Small',
                ]
            ]
        ]);
    }

    public function testRemoveVariantOptionWithProductAssociate()
    {
        Storage::fake('product');

        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small']
                ]
            ]
        ]);

        /** @var Product $product */
        $product = Product::factory()->create();
        $product->assignVariants([
            [
                'variant_id' => $variants->getVariantIdByName('Sizes'),
                'options' => [
                    [
                        'variant_option_id' => $variants->getOptionIdByName('Large')
                    ]
                ]
            ]
        ]);

        $response = $this->json('put', 'api/products/variants/'.$variants->get(0)->id, [
            'name' => 'Size',
            'removed_options' => [1]
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'id' => 1,
            'name' => 'Size',
            'options' => [
                [
                    'id' => 1,
                    'name' => 'Large'
                ],
                [
                    'id' => 2,
                    'name' => 'Small'
                ]
            ]
        ]);
    }

    public function testRemoveVariant()
    {
        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small']
                ]
            ]
        ]);

        $response = $this->json('delete', 'api/products/variants/'.$variants->get(0)->id);

        $response->assertStatus(200);
        $response->assertJson(['result' => true]);
    }

    public function testRemoveVariantWithProductAssociate()
    {
        Storage::fake('product');

        $variants = Variant::createMany([
            [
                'name' => 'Sizes',
                'options' => [
                    ['name' => 'Large'],
                    ['name' => 'Small']
                ]
            ]
        ]);

        /** @var Product $product */
        $product = Product::factory()->create();
        $product->assignVariants([
            [
                'variant_id' => $variants->getVariantIdByName('Sizes'),
                'options' => [
                    [
                        'variant_option_id' => $variants->getOptionIdByName('Large')
                    ]
                ]
            ]
        ]);

        $response = $this->json('delete', 'api/products/variants/'.$variants->get(0)->id);

        $response->assertStatus(200);
        $response->assertJson(['result' => false]);
    }
}
