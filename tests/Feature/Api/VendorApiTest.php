<?php


namespace Tests\Feature\Api;


use App\Models\Role;
use App\Models\Vendor;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\FakeLogin;

class VendorApiTest extends TestCase
{
    use RefreshDatabase, FakeLogin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setupPermission();
    }

    public function testShow()
    {
        /** @var Vendor $vendor */
        $vendor = Vendor::factory()->create();

        $response = $this->json('get', "api/vendors/$vendor->id");

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'company_name',
            'contact_name',
            'contact_title',
            'address1',
            'address2',
            'city',
            'country',
            'phone',
            'fax',
            'email',
            'site_url'
        ]);
    }

    public function testIndex()
    {
        $this->login(Role::ADMIN);

        Vendor::factory()->count(2)->create();

        $response = $this->json('get', 'api/vendors');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'company_name',
                    'contact_name',
                    'contact_title',
                    'address1',
                    'address2',
                    'city',
                    'country',
                    'phone',
                    'fax',
                    'email',
                    'site_url'
                ]
            ],
            'meta'
        ]);
    }

    public function testCreate()
    {
        $this->login(Role::ADMIN);

        $attributes = Vendor::factory()->make()->toArray();

        $response = $this->json('post', 'api/vendors', $attributes);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'company_name',
            'contact_name',
            'contact_title',
            'address1',
            'address2',
            'city',
            'country',
            'phone',
            'fax',
            'email',
            'site_url'
        ]);
    }

    public function testUpdate()
    {
        $this->login(Role::ADMIN);

        /** @var Vendor $vendor */
        $vendor = Vendor::factory()->create();

        $attributes = Vendor::factory()->make()->toArray();

        $response = $this->json('put', "api/vendors/$vendor->id", $attributes);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'company_name',
            'contact_name',
            'contact_title',
            'address1',
            'address2',
            'city',
            'country',
            'phone',
            'fax',
            'email',
            'site_url'
        ]);
    }

    public function testDelete()
    {
        $this->login(Role::ADMIN);

        /** @var Vendor $vendor */
        $vendor = Vendor::factory()->create();

        $response = $this->json('delete', "api/vendors/$vendor->id");

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);
    }
}
