<?php


namespace Tests\Feature\Api;


use App\Gateways\PaymentGateway;
use App\Models\Coupon;
use App\Models\Invoice;
use App\Models\Role;
use App\Models\Subscription;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery\MockInterface;
use Stripe\Coupon as StripeCoupon;
use Tests\TestCase;
use Tests\Traits\FakeLogin;

class CouponApiTest extends TestCase
{
    use RefreshDatabase, FakeLogin;

    public function setUp(): void
    {
        parent::setUp();

        $this->setupPermission();

        $this->login(Role::ADMIN);
    }

    public function testShow()
    {
        Coupon::factory()->count(2)->amount()->create();
        Coupon::factory()->count(2)
                         ->amount()
                         ->state(['deleted_at' => now()])
                         ->create();

        $response = $this->json('get', "api/coupons");

        $response->assertStatus(200)
                 ->assertJsonCount(4, 'data')
                 ->assertJsonStructure([
                     'data' => [
                         '*' => [
                             'name',
                             'stripe_id',
                             'code',
                             'amount_off',
                             'currency',
                             'percent_off',
                             'duration',
                             'duration_in_months',
                             'max_redemptions',
                             'times_redeemed',
                             'redeem_by',
                             'enabled',
                             'deleted_at',
                             'created_at'
                         ]
                     ]
                 ]);
    }

    public function testIndex()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->amount()->create();

        $response = $this->json('get', "api/coupons/$coupon->id");

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'name',
                     'stripe_id',
                     'code',
                     'amount_off',
                     'currency',
                     'percent_off',
                     'duration',
                     'duration_in_months',
                     'max_redemptions',
                     'times_redeemed',
                     'redeem_by',
                     'enabled',
                     'deleted_at',
                     'created_at'
                 ]);
    }

    public function testCreateWithoutDiscountAmount()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->make();

        $response = $this->json('post', 'api/coupons', [
            'name' => $coupon->name,
            'code' => $coupon->code,
            'duration' => $coupon->duration
        ]);

        $response->assertStatus(422)
                 ->assertJsonStructure([
                     'message',
                     'errors' => [
                         'amount_off',
                         'percent_off'
                     ]
                 ]);
    }

    public function testCreate()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->amount()->make();

        $this->mock(PaymentGateway::class,
            function (MockInterface $mock) use ($coupon) {
                $mock->shouldReceive('request')
                     ->with('post', '/v1/coupons', $coupon->getStripeOptions(), null)
                     ->once()
                     ->andReturn(StripeCoupon::constructFrom([
                         'id' => $coupon->stripe_id
                     ]));
            });

        $response = $this->json('post', 'api/coupons', $coupon->toArray());

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'name',
                     'stripe_id',
                     'code',
                     'amount_off',
                     'currency',
                     'percent_off',
                     'duration',
                     'duration_in_months',
                     'max_redemptions',
                     'times_redeemed',
                     'redeem_by',
                     'enabled',
                     'deleted_at',
                     'created_at'
                 ]);
    }

    public function testUpdate()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->create();

        /** @var Coupon $attributes */
        $attributes = Coupon::factory()->make();

        $response = $this->json('put', "api/coupons/$coupon->id", [
            $attributes->code
        ]);

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'name',
                     'stripe_id',
                     'code',
                     'amount_off',
                     'currency',
                     'percent_off',
                     'duration',
                     'duration_in_months',
                     'max_redemptions',
                     'times_redeemed',
                     'redeem_by',
                     'enabled',
                     'deleted_at',
                     'created_at'
                 ]);
    }

    public function testUpdateFail()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->create();
        Subscription::factory()
                    ->has(Invoice::factory()->state([
                        'stripe_coupon' => $coupon->stripe_id
                    ]))
                    ->create();

        /** @var Coupon $attributes */
        $attributes = Coupon::factory()->make();

        $response = $this->json('put', "api/coupons/$coupon->id", [
            $attributes->code
        ]);

        $response->assertStatus(403);
    }

    public function testForceDelete()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->create();
        Subscription::factory()
                    ->has(Invoice::factory()->state([
                        'stripe_coupon' => $coupon->stripe_id
                    ]))
                    ->create();

        $this->mock(PaymentGateway::class,
            function (MockInterface $mock) use ($coupon) {
                $mock->shouldReceive('request')
                     ->with('delete', "/v1/coupons/$coupon->stripe_id", null, null)
                     ->once();
            });

        $response = $this->json('delete', "api/coupons/$coupon->id");

        $response->assertStatus(200)
                 ->assertJson(['result' => true]);
    }

    public function testDelete()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->create();

        $this->mock(PaymentGateway::class,
            function (MockInterface $mock) use ($coupon) {
                $mock->shouldReceive('request')
                     ->with('delete', "/v1/coupons/$coupon->stripe_id", null, null)
                     ->once();
            });

        $response = $this->json('delete', "api/coupons/$coupon->id");

        $response->assertStatus(200)
                 ->assertJson(['result' => true]);
    }
}
