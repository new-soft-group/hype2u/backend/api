<?php

namespace Tests\Feature\Api;


use App\Models\Category;
use App\Models\Image;
use App\Models\NotifyQueue;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\ProductVariantOption;
use App\Models\Queuer;
use App\Models\Role;
use App\Models\StockStatus;
use App\Models\User;
use Database\Seeders\PermissionSeeder;
use Database\Seeders\StockStatusSeeder;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Laravel\Passport\Passport;
use Storage;
use Tests\TestCase;
use Tests\Traits\FakeSetting;
use Tests\Traits\FakeImage;
use App\Models\Vendor;
use App\Models\Warehouse;
use Tests\Traits\WithVariants;

class ProductApiTest extends TestCase
{
    use RefreshDatabase,
        WithFaker,
        FakeImage,
        WithVariants,
        FakeSetting;

    protected function setUp(): void
    {
        parent::setUp(); // TODO: Change the autogenerated stub

        $this->seed([
            StockStatusSeeder::class,
            PermissionSeeder::class,
        ]);

        Storage::fake('product');

        $this->setupSetting();

        $user = User::factory()->create();
        $user->assignRole(Role::ADMIN);

        Passport::actingAs($user);
    }

    public function testShowProduct()
    {
        /** @var Product $product */
        $product = Product::factory()
                          ->has(Image::factory()->product())
                          ->has(ProductStock::factory()
                                            ->hasAttached(
                                                ProductVariantOption::factory()->product(),
                                                [], 'combinations'
                                            )
                                            ->has(ProductStockInventory::factory(), 'inventories'),
                              'stocks'
                          )
                          ->create();

        $response = $this->json('get', "api/product/$product->id");

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'name',
            'description',
            'sku',
            'slug',
            'images' => [
                '*' => [
                    'id',
                    'url',
                    'width',
                    'height'
                ]
            ],
            'pricing' => [
                '*' => [
                    'currency_code',
                    'price'
                ]
            ],
            'featured',
            'categories' => [
                '*' => [
                    'id',
                    'name'
                ]
            ],
            'brand' => [
                'id',
                'name'
            ],
            'gender' => [
                'id',
                'name'
            ],
            'variants' => [
                '*' => [
                    'id',
                    'variant_id',
                    'name',
                    'options' => [
                        '*' => [
                            'id',
                            'variant_option_id',
                            'name',
                        ]
                    ]
                ]
            ],
            'stocks' => [
                '*' => [
                    'id',
                    'sku',
                    'price',
                    'vendor_id',
                    'product_variant_option_ids',
                    'option_combination',
                    'enabled',
                    'inventories_count'
                ]
            ]
        ]);
    }

    public function testListProduct()
    {
        Product::factory()
               ->count(2)
               ->has(Image::factory()->product())
               ->has(ProductStock::factory()
                                 ->hasAttached(
                                     ProductVariantOption::factory(),
                                     [],
                                     'combinations'
                                 )
                                 ->has(ProductStockInventory::factory(), 'inventories'),
                   'stocks')
               ->create();

        $response = $this->json('get', 'api/products');

        $response->assertStatus(200);

        $response->assertJsonMissingExact([
            'data' => [
                '*' => [
                    'options'
                ]
            ]
        ]);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'name',
                    'description',
                    'sku',
                    'slug',
                    'images' => [
                        '*' => [
                            'id',
                            'url',
                            'width',
                            'height'
                        ]
                    ],
                    'pricing' => [
                        '*' => [
                            'currency_code',
                            'price'
                        ]
                    ],
                    'featured',
                    'categories' => [
                        '*' => [
                            'id',
                            'name'
                        ]
                    ],
                    'brand' => [
                        'id',
                        'name'
                    ],
                    'gender' => [
                        'id',
                        'name'
                    ],
                    'stocks' => [
                        '*' => [
                            'inventories_count'
                        ]
                    ]
                ]
            ],
            'links',
            'meta'
        ]);
    }

    public function testListProductFilterByVariant()
    {
        $options = ProductVariantOption::factory()->count(2)->create();

        $ids = $options->pluck('id')->toArray();

        $response = $this->json('get', 'api/products', [
            'filter' => json_encode([
                'variants' => $ids
            ])
        ]);

        $response->assertStatus(200);
    }

    public function testCreateProduct()
    {
        /** @var Category $category */
        $category = Category::factory()->create();

        $variants = $this->generateVariants();

        $product = Product::factory()->make([
            'images' => $this->fakeUpload('product.png', 1),
            'category_ids' => [$category->id],
            'variants' => [
                [
                    'variant_id' => $variants->getVariantIdByName('Colors'),
                    'options' => $variants->getOptionIdByName(['Black', 'White'], true)
                ]
            ]
        ]);

        $response = $this->json('post', 'api/products', $product->toArray());

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'name',
            'description',
            'sku',
            'slug',
            'images' => [
                '*' => [
                    'id',
                    'url',
                    'width',
                    'height',
                    'sort'
                ]
            ],
            'pricing' => [
                '*' => [
                    'currency_code',
                    'price'
                ]
            ],
            'featured',
            'categories' => [
                '*' => [
                    'id',
                    'name'
                ]
            ],
            'brand' => [
                'id',
                'name'
            ],
            'gender' => [
                'id',
                'name'
            ],
            'variants' => [
                '*' => [
                    'id',
                    'variant_id',
                    'name',
                    'options' => [
                        '*' => [
                            'id',
                            'variant_option_id',
                            'name'
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function testUpdateProduct()
    {
        $categories = Category::factory()->count(2)->create();

        /** @var Product $product */
        $product = Product::factory()
                          ->hasAttached(Category::factory())
                          ->create();

        $response = $this->json('put', "api/products/$product->id", [
            'name' => $product->name,
            'description' => $product->description,
            'categories_id' => $categories->pluck('id')->toArray(),
            'images' => [$this->fakeUpload('product.jpg')]
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'name',
            'description',
            'sku',
            'slug',
            'images' => [
                '*' => [
                    'id',
                    'url',
                    'width',
                    'height',
                    'sort'
                ]
            ],
            'pricing' => [
                '*' => [
                    'currency_code',
                    'price'
                ]
            ],
            'featured',
            'categories' => [
                '*' => [
                    'id',
                    'name'
                ]
            ],
            'brand' => [
                'id',
                'name'
            ],
            'gender' => [
                'id',
                'name'
            ],
            'variants' => [
                '*' => [
                    'id',
                    'variant_id',
                    'name',
                    'options' => [
                        '*' => [
                            'id',
                            'variant_option_id',
                            'name'
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function testUpdateProductVariants()
    {
        /** @var Product $product */
        $product = Product::factory()->create();

        $variants = $this->generateVariants();

        $response = $this->json('put', "api/products/$product->id", [
            'name' => $product->name,
            'description' => $product->description,
            'variants' => [
                [
                    'variant_id' => $variants->getVariantIdByName('Sizes'),
                    'options' => [$variants->getOptionIdByName('Large', true)]
                ]
            ]
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'name',
            'description',
            'sku',
            'slug',
            'images' => [
                '*' => [
                    'id',
                    'url',
                    'width',
                    'height',
                    'sort'
                ]
            ],
            'pricing' => [
                '*' => [
                    'currency_code',
                    'price'
                ]
            ],
            'featured',
            'categories' => [
                '*' => [
                    'id',
                    'name'
                ]
            ],
            'brand' => [
                'id',
                'name'
            ],
            'gender' => [
                'id',
                'name'
            ],
            'variants' => [
                '*' => [
                    'id',
                    'variant_id',
                    'name',
                    'options' => [
                        '*' => [
                            'id',
                            'variant_option_id',
                            'name'
                        ]
                    ]
                ]
            ]
        ]);
    }

    public function testCreateProductStock()
    {
        /** @var Product $product */
        $product = Product::factory()->create();
        $variants = $this->generateVariants();

        $product->assignVariants([
            [
                'variant_id' => $variants->getVariantIdByName('Colors'),
                'options' => [$variants->getOptionIdByName('Black', true)],
            ]
        ]);

        $combination = $product->availableVariantCombination()[0];
        /** @var Vendor $vendor */
        $vendor = Vendor::factory()->create();

        $response = $this->json('post', "api/products/stocks/$product->id", [
            'vendor_id' => $vendor->id,
            'price' => 99.99,
            'product_variant_option_ids' => $combination['ids']
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'sku',
            'price',
            'vendor_id',
            'product_variant_option_ids',
            'option_combination',
            'enabled'
        ]);
    }

    public function testUpdateProductStock()
    {
        /** @var Product $product */
        $product = Product::factory()->create();

        $variants = $this->generateVariants();
        $product->assignVariants([
            [
                'variant_id' => $variants->getVariantIdByName('Colors'),
                'options' => [$variants->getOptionIdByName('Black', true)],
            ]
        ]);
        $vendor = Vendor::factory()->create();
        $product->initializeStocks($product->variants, $vendor);
        $product->assignVariants([
            [
                'variant_id' => $variants->getVariantIdByName('Sizes'),
                'options' => [$variants->getOptionIdByName('Large', true)],
            ]
        ]);

        $combination = $product->availableVariantCombination()[0];

        /** @var ProductStock $stock */
        $stock = $product->stocks()->first();

        $response = $this->json('put', "api/products/stocks/$stock->id", [
            'vendor_id' => $vendor->id,
            'price' => 99.99,
            'variant_option_ids' => $combination['ids']
        ]);

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'id',
            'sku',
            'price',
            'vendor_id',
            'product_variant_option_ids',
            'option_combination',
            'enabled',
        ]);
    }

    public function testDeleteProductStocks()
    {
        $product = Product::factory()->create();
        $variants = $this->generateVariants();
        /** @var Product $product */
        $product->assignVariants([
            [
                'variant_id' => $variants->getVariantIdByName('Colors'),
                'options' => [$variants->getOptionIdByName('Black', true)],
            ]
        ]);
        $vendor = Vendor::factory()->create();
        $stocks = $product->initializeStocks($product->variants, $vendor);

        $response = $this->json('delete', "api/products/stocks/$product->id", [
            'ids' => $stocks->pluck('id')->toArray()
        ]);

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);
    }

    public function testViewProductTag()
    {
        /** @var ProductStockInventory $tag */
        $tag = ProductStockInventory::factory()->create();

        $response = $this->json('get', "api/products/tags/$tag->product_stock_id");

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' =>  [
                'id',
                'product_stock_id',
                'warehouse',
                'tag',
                'status'
            ]
        ]);
    }

    public function testCreateProductTag()
    {
        /** @var ProductStock $stock */
        $stock = ProductStock::factory()
                             ->hasAttached(ProductVariantOption::factory(), [], 'combinations')
                             ->create();

        /** @var Warehouse $warehouse */
        $warehouse = Warehouse::factory()->create();

        $response = $this->json('post', "api/products/tags/$stock->id", [
            'tags' => [$this->faker->uuid],
            'warehouse_id' => $warehouse->id,
            'stock_status_id' => StockStatus::IN_STOCK
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'product_stock_id',
                'warehouse',
                'tag',
                'status'
            ]
        ]);
    }

    public function testUpdateProductTag()
    {
        /** @var ProductStockInventory $tag */
        $tag = ProductStockInventory::factory()->create();

        /** @var ProductStockInventory $newItem */
        $newItem = ProductStockInventory::factory()
                                        ->state(['stock_status_id' => StockStatus::OUT_OF_STOCK])
                                        ->make();

        $response = $this->json('put', "api/products/tags/$tag->id", [
            'tag' => $newItem->tag,
            'warehouse_id' => $newItem->warehouse_id,
            'stock_status_id' => $newItem->stock_status_id
        ]);

        $response->assertStatus(200);

        $response->assertJson([
            'id' => $tag->id,
            'warehouse' => [
                'id' => $newItem->warehouse_id
            ],
            'tag' => $newItem->tag
        ]);
    }

    public function testDeleteProductTag()
    {
        /** @var ProductStockInventory $tag */
        $tag = ProductStockInventory::factory()->create();

        $deletedResponse = $this->json('delete', "api/products/tags/$tag->product_stock_id", [
            'ids' => [$tag->id]
        ]);

        $deletedResponse->assertStatus(200);

        $deletedResponse->assertJson(['result' => true]);
    }

    public function testGetProductFilterableOptionsProductCountShouldOnlyIncludeEnabled()
    {
        $sizes = ['Large', 'Small'];
        $colors = ['White', 'Black'];

        $variants = $this->generateVariants();

        Product::factory()
               ->count(2)
               ->hasAttached(Category::factory())
               ->disabled()
               ->create()
               ->each(function (Product $product, $index) use ($variants, $sizes, $colors) {
                   $product->assignVariants([
                       [
                           'variant_id' => $variants->getVariantIdByName('Sizes'),
                           'options' => [$variants->getOptionIdByName($sizes[$index], true)]
                       ],
                       [
                           'variant_id' => $variants->getVariantIdByName('Colors'),
                           'options' => [$variants->getOptionIdByName($colors[$index], true)]
                       ]
                   ]);
               });

        $response = $this->json('get', 'api/products/filterable');

        $json = $response->decodeResponseJson();

        foreach ($json['categories'] as $category) {
            $this->assertEquals(0, $category['products_count']);
        }

        foreach ($json['brands'] as $brand) {
            $this->assertEquals(0, $brand['products_count']);
        }

        foreach ($json['genders'] as $gender) {
            $this->assertEquals(0, $gender['products_count']);
        }

        foreach ($json['variants'] as $variant) {
            $this->assertEquals(0, $variant['products_count']);

            foreach ($variant['options'] as $option) {
                if($option['name'] === 'Medium') {
                    $this->assertEquals(0, $option['products_count']);
                }
                else {
                    $this->assertEquals(1, $option['products_count']);
                }
            }
        }
    }

    public function testGetProductFilterableOptions()
    {
        $sizes = ['Large', 'Small'];
        $colors = ['White', 'Black'];

        $variants = $this->generateVariants();

        Product::factory()
               ->count(2)
               ->hasAttached(Category::factory())
               ->create()
               ->each(function (Product $product, $index) use ($variants, $sizes, $colors) {
                   $product->assignVariants([
                       [
                           'variant_id' => $variants->getVariantIdByName('Sizes'),
                           'options' => [$variants->getOptionIdByName($sizes[$index], true)]
                       ],
                       [
                           'variant_id' => $variants->getVariantIdByName('Colors'),
                           'options' => [$variants->getOptionIdByName($colors[$index], true)]
                       ]
                   ]);
               });

        $response = $this->json('get', 'api/products/filterable');

        $response->assertJsonStructure([
            'categories' => [
                '*' => [
                    'id',
                    'name',
                    'products_count'
                ]
            ],
            'brands' => [
                '*' => [
                    'id',
                    'name',
                    'products_count'
                ]
            ],
            'genders' => [
                '*' => [
                    'id',
                    'name',
                    'products_count'
                ]
            ],
            'variants' => [
                '*' => [
                    'id',
                    'name',
                    'products_count',
                    'options' => [
                        '*' => [
                            'id',
                            'variant_id',
                            'name',
                            'products_count',
                        ]
                    ]
                ]
            ]
        ]);
    }

    /**
     * @throws \Throwable
     */
    public function testGetEveryFeaturedProductFromCategoriesShouldOnlyGetEnabled()
    {
        Product::factory()
               ->count(5)
               ->state(new Sequence(
                   ['enabled' => true],
                   ['enabled' => true],
                   ['enabled' => true],
                   ['enabled' => false],
                   ['enabled' => false]
               ))
               ->hasAttached(Category::factory())
               ->enableFeatured()
               ->create();

        $response = $this->json('get', 'api/products/featured', [
            'limit' => 5
        ]);

        $json = $response->decodeResponseJson();

        $response->assertStatus(200);

        $this->assertCount(3, $json);
    }

    public function testGetEveryFeaturedProductFromCategories()
    {
        /** Prepare Categories */
        $categories = Category::factory()->count(3)->create();

        $structure = [];

        foreach ($categories as $category) {

            /** Construct Json Structure */
            $structure[$category->name] = [
                '*' => [
                    'id',
                    'name',
                    'images' => [
                        '*' => [
                            'id',
                            'url',
                            'width',
                            'height'
                        ]
                    ],
                    'brand',
                    'gender',
                    'categories'
                ]
            ];

            Product::factory()
                   ->count(10)
                   ->create(['featured' => true])
                   ->each(function (Product $product) use ($category) {
                       $product->assignCategories($category->id);
                   });
        }

        $response = $this->json('get', 'api/products/featured', [
            'limit' => 5
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure($structure);
    }

    public function testGetProductVariants()
    {
        $this->generateVariants();

        $response = $this->get('api/products/variants');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'name',
                'products_count',
                'options' => [
                    '*' => [
                        'id',
                        'variant_id',
                        'name',
                        'products_count'
                    ]
                ]
            ]
        ]);
    }
}
