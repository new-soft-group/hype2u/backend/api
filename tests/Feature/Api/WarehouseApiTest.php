<?php


namespace Tests\Feature\Api;


use App\Models\Role;
use App\Models\Warehouse;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\FakeLogin;

class WarehouseApiTest extends TestCase
{
    use RefreshDatabase, FakeLogin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setupPermission();
    }

    public function testShow()
    {
        /** @var Warehouse $warehouse */
        $warehouse = Warehouse::factory()->create();

        $response = $this->json('get', "api/warehouses/$warehouse->id");

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'country',
            'name',
            'slug',
            'address1',
            'address2',
            'city',
            'phone',
            'fax',
            'default',
            'enabled'
        ]);
    }

    public function testIndex()
    {
        $this->login(Role::ADMIN);

        Warehouse::factory()->count(2)->create();

        $response = $this->json('get', 'api/warehouses');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'country',
                    'name',
                    'slug',
                    'address1',
                    'address2',
                    'city',
                    'phone',
                    'fax',
                    'default',
                    'enabled'
                ]
            ],
            'meta'
        ]);
    }

    public function testCreate()
    {
        $this->login(Role::ADMIN);

        $attributes = Warehouse::factory()->make()->toArray();

        $response = $this->json('post', 'api/warehouses', $attributes);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'country',
            'name',
            'slug',
            'address1',
            'address2',
            'city',
            'phone',
            'fax',
            'default',
            'enabled'
        ]);
    }

    public function testUpdate()
    {
        $this->login(Role::ADMIN);

        /** @var Warehouse $warehouse */
        $warehouse = Warehouse::factory()->create();

        $attributes = Warehouse::factory()->make()->toArray();

        $response = $this->json('put', "api/warehouses/$warehouse->id", $attributes);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'country',
            'name',
            'slug',
            'address1',
            'address2',
            'city',
            'phone',
            'fax',
            'default',
            'enabled'
        ]);
    }

    public function testDelete()
    {
        $this->login(Role::ADMIN);

        /** @var Warehouse $warehouse */
        $warehouse = Warehouse::factory()->create();

        $response = $this->json('delete', "api/warehouses/$warehouse->id");

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);
    }
}
