<?php

namespace Tests\Feature\Api;

use App\Events\UserLoggedIn;
use App\Facades\Datatable;
use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Laravel\Passport\Passport;
use Laravel\Socialite\Facades\Socialite;
use Laravel\Socialite\Two\AbstractProvider;
use Laravel\Socialite\Two\User as SocialUser;
use Mockery;
use Tests\TestCase;

class LoginApiTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(PermissionSeeder::class);

        $this->artisan('passport:install');
    }

    public function testShouldGetPersonalTokenAndRole()
    {
        Event::fake();

        /** @var User $user */
        $user = User::factory()->create();
        $user->assignRole(Role::USER);

        $response = $this->post('api/login', [
            'email' => $user->email,
            'password' => 'password'
        ]);

        $response->assertStatus(200);

        Event::assertDispatched(function (UserLoggedIn $event) use ($user) {
            return $event->user->id === $user->id;
        });

        $this->assertNotNull($response->offsetGet('roles'));
        $this->assertNotNull($response->offsetGet('token'));
        $this->assertDatabaseCount('oauth_access_tokens', 1);
    }

    public function testShouldFailWithInvalidSocialToken()
    {
        Event::fake();

        $abstractUser = Mockery::mock(SocialUser::class);
        $abstractUser->shouldReceive('getEmail')
                     ->andReturn('facebook@gmail.com');

        $provider = Mockery::mock(AbstractProvider::class);
        $provider->shouldReceive('userFromToken')
                 ->with('facebook_token')
                 ->andThrow(\Exception::class);

        Socialite::shouldReceive('driver')
                 ->with('facebook')
                 ->andReturn($provider);

        $response = $this->post('api/social/login', [
            'provider' => 'facebook',
            'token' => 'facebook_token'
        ]);

        $response->assertStatus(400);
        $response->assertJson(['message' => trans('auth.provider.failed')]);
    }

    public function testShouldGetTokenWithValidSocialToken()
    {
        Event::fake();

        $user = User::factory()
                    ->has(Profile::factory())
                    ->create([
                        'email' => 'facebook@gmail.com',
                        'provider' => 'facebook'
                    ]);

        $abstractUser = Mockery::mock(SocialUser::class);
        $abstractUser->shouldReceive('getEmail')
                     ->andReturn($user->email);

        $provider = Mockery::mock(AbstractProvider::class);
        $provider->shouldReceive('fields')
                 ->with(['email', 'name', 'first_name', 'last_name'])
                 ->andReturn($provider)
                 ->shouldReceive('userFromToken')
                 ->with('facebook_token')
                 ->andReturn($abstractUser);

        Socialite::shouldReceive('driver')
                 ->with('facebook')
                 ->andReturn($provider);

        $response = $this->post('api/social/login', [
            'provider' => 'facebook',
            'token' => 'facebook_token'
        ]);

        $response->assertStatus(200);
        $this->assertNotNull($response->offsetGet('token'));
    }

    public function testShouldCreateSocialAccountIfNotExist()
    {
        Event::fake();

        $abstractUser = Mockery::mock(SocialUser::class);
        $abstractUser->shouldReceive('getEmail')
                     ->andReturn('facebook@gmail.com')
                     ->shouldReceive('offsetExists')
                     ->with('first_name')
                     ->andReturn(true)
                     ->shouldReceive('offsetGet')
                     ->with('first_name')
                     ->andReturn('James')
                     ->shouldReceive('offsetExists')
                     ->with('last_name')
                     ->andReturn(true)
                     ->shouldReceive('offsetGet')
                     ->with('last_name')
                     ->andReturn('Bones');

        $provider = Mockery::mock(AbstractProvider::class);
        $provider->shouldReceive('fields')
                 ->with(['email', 'name', 'first_name', 'last_name'])
                 ->andReturn($provider)
                 ->shouldReceive('userFromToken')
                 ->with('facebook_token')
                 ->andReturn($abstractUser);

        Socialite::shouldReceive('driver')
                 ->with('facebook')
                 ->andReturn($provider);

        $response = $this->json('post', '/api/social/login', [
            'provider' => 'facebook',
            'token' => 'facebook_token'
        ]);

        $response->assertStatus(200);
        $this->assertNotNull($response->offsetGet('token'));
        $this->assertDatabaseHas('users', [
            'email' => 'facebook@gmail.com',
            'provider' => 'facebook'
        ]);
        $this->assertDatabaseHas('profiles', [
            'first_name' => 'James',
            'last_name' => 'Bones'
        ]);

        $user = User::find($response->offsetGet('id'));

        Event::assertDispatched(function (UserLoggedIn $event) use ($user) {
            return $event->user->id === $user->id;
        });
    }

    public function testShouldAbleToLoginUsingExistingSocialAccount()
    {
        Event::fake();

        /** @var User $user */
        $user = User::factory()
                    ->has(Profile::factory())
                    ->create(['provider' => 'facebook']);

        $abstractUser = Mockery::mock(SocialUser::class);
        $abstractUser->shouldReceive('getEmail')
                     ->andReturn($user->email)
                     ->shouldReceive('offsetExists')
                     ->with('first_name')
                     ->andReturn(true)
                     ->shouldReceive('offsetGet')
                     ->with('first_name')
                     ->andReturn($user->profile->first_name)
                     ->shouldReceive('offsetExists')
                     ->with('last_name')
                     ->andReturn(true)
                     ->shouldReceive('offsetGet')
                     ->with('last_name')
                     ->andReturn($user->profile->last_name);

        $provider = Mockery::mock(AbstractProvider::class);
        $provider->shouldReceive('fields')
                 ->with(['email', 'name', 'first_name', 'last_name'])
                 ->andReturn($provider)
                 ->shouldReceive('userFromToken')
                 ->with('facebook_token')
                 ->andReturn($abstractUser);

        Socialite::shouldReceive('driver')
                 ->with('facebook')
                 ->andReturn($provider);

        $response = $this->json('post', '/api/social/login', [
            'provider' => 'facebook',
            'token' => 'facebook_token'
        ]);

        $response->assertStatus(200);
        $this->assertDatabaseCount('users', 1);
        $this->assertNotNull($response->offsetGet('token'));

        Event::assertDispatched(function (UserLoggedIn $event) use ($user) {
            return $event->user->id === $user->id;
        });
    }

    public function testDeleteAccessTokenWhenLogout()
    {
        /** @var User $user */
        $user = User::factory()->create();
        $user->assignRole(Role::USER);

        Passport::actingAs($user);

        $response = $this->post('api/logout');

        $response->assertStatus(200);
        $response->assertJson(['message' => __('auth.logout')]);
        $this->assertDatabaseCount('oauth_access_tokens', 0);
    }
}
