<?php


namespace Tests\Feature\Api;


use App\Models\NotifyQueue;
use App\Models\ProductStock;
use App\Models\Queuer;
use App\Models\Role;
use App\Models\User;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;

class StockNotifyApiTest extends TestCase
{
    use RefreshDatabase;

    /**
     * @var User
     */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(PermissionSeeder::class);

        $this->user = User::factory()->create();
        $this->user->assignRole(Role::USER);
        Passport::actingAs($this->user);
    }

    public function testQueueStockNotify()
    {
        /** @var ProductStock $stock */
        $stock = ProductStock::factory()->create();

        $response = $this->json('post', "api/products/stocks/$stock->id/notify");

        $response->assertStatus(200);

        $this->assertDatabaseCount('notify_queues', 1)
             ->assertDatabaseCount('queuers', 1);
    }

    public function testRequeueStockNotify()
    {
        $queue = NotifyQueue::factory()
                            ->has(Queuer::factory()
                                        ->state(['user_id' => $this->user->id]));

        $stock = ProductStock::factory()
                             ->has($queue)
                             ->create();

        $response = $this->json('post', "api/products/stocks/$stock->id/notify");

        $response->assertStatus(400)
                 ->assertJson(['message' => __('queue.duplicate')]);
    }
}
