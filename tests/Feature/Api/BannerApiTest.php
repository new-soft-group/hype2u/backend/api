<?php

namespace Tests\Feature\Api;

use App\Models\Banner;
use App\Models\BannerPosition;
use App\Models\Image;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;
use Tests\Traits\FakeImage;
use Tests\Traits\FakeLogin;

class BannerApiTest extends TestCase
{
    use RefreshDatabase, WithFaker, FakeLogin, FakeImage;

    protected function setUp(): void
    {
        parent::setUp();

        Storage::fake('banner');

        $this->setupPermission();

        $this->login(Role::ADMIN);
    }

    public function testGetBanners()
    {
        Banner::factory()
              ->count(3)
              ->has(Image::factory()->banner())
              ->create();

        $response = $this->json('get', 'api/banners');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'viewport',
                'sort',
                'image' => [
                    'id',
                    'url',
                    'width',
                    'height'
                ],
                'position' => [
                    'id',
                    'name'
                ]
            ]
        ]);
    }

    public function testCreateBanner()
    {
        /** @var BannerPosition $position */
        $position = BannerPosition::factory()->create([
            'name' => 'Top Section'
        ]);

        $response = $this->json('post', 'api/banners', [
            'banners' => [
                [
                    'viewport' => Banner::DESKTOP_VIEWPORT,
                    'banner_position_id' => $position->id,
                    'image' => $this->fakeUpload('banner1.png')
                ]
            ]
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'viewport',
                'sort',
                'image' => [
                    'id',
                    'url',
                    'width',
                    'height',
                    'sort'
                ],
                'position' => [
                    'id',
                    'name'
                ]
            ]
        ]);
    }

    public function testUpdateBanner()
    {
        $banners = Banner::factory()->count(3)->create();

        $response = $this->json('patch', 'api/banners', [
            'banners' => [
                [
                    'id' => $banners->first()->id,
                    'sort' => 1
                ],
            ]
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('banners', [
            'id' => 1,
            'sort' => 1
        ]);

        $this->assertDatabaseHas('banners', [
            'id' => 2,
            'sort' => 0
        ]);

        $this->assertDatabaseHas('banners', [
            'id' => 3,
            'sort' => 0
        ]);
    }

    public function testUpdateBannerPosition()
    {
        /** @var Banner $banner */
        $banner = Banner::factory()->create();

        /** @var BannerPosition $position */
        $position = BannerPosition::factory()->create();

        $response = $this->json('patch', 'api/banners', [
            'banners' => [
                [
                    'id' => $banner->id,
                    'banner_position_id' => $position->id
                ],
            ]
        ]);

        $response->assertStatus(200);

        $response->assertJson([
            [
                'id' => $banner->id,
                'viewport' => $banner->viewport
            ]
        ]);

        $this->assertDatabaseHas('banners', [
            'id' => $banner->id,
            'sort' => $banner->sort,
            'banner_position_id' => $position->id
        ]);
    }

    public function testDeleteBanner()
    {
        $banners = Banner::factory()
                         ->count(3)
                         ->has(Image::factory()->banner())
                         ->create();

        $response = $this->json('delete', 'api/banners', [
            'ids' => $banners->pluck('id')->toArray()
        ]);

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);

        $this->assertDatabaseCount('banners', 0);
        $this->assertDatabaseCount('images', 0);
    }

    public function testRenameBannerPosition()
    {
        /** @var BannerPosition $position */
        $position = BannerPosition::factory()->create();

        $response = $this->json('put', "api/banners/positions/$position->id", [
            'name' => 'New Position'
        ]);

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);

        $this->assertDatabaseHas('banner_positions', [
            'id' => $position->id,
            'name' => 'New Position'
        ]);
    }
}
