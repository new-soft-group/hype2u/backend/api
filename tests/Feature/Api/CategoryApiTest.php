<?php


namespace Tests\Feature\Api;


use App\Models\Category;
use App\Models\Product;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\FakeLogin;

class CategoryApiTest extends TestCase
{
    use RefreshDatabase, FakeLogin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setupPermission();
    }

    public function testIndex()
    {
        Category::factory()->count(2)->create();

        $response = $this->json('get', 'api/categories');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'name'
            ]
        ]);
    }

    public function testCreate()
    {
        $this->login(Role::ADMIN);

        $attributes = Category::factory()->create()->toArray();

        $response = $this->json('post', 'api/categories', $attributes);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'name'
        ]);
    }

    public function testUpdate()
    {
        $this->login(Role::ADMIN);

        /** @var Category $category */
        $category = Category::factory()->create();

        $attributes = Category::factory()->make()->toArray();

        $response = $this->json('put', "api/categories/$category->id", $attributes);

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);
    }

    public function testDelete()
    {
        $this->login(Role::ADMIN);

        /** @var Category $category */
        $category = Category::factory()->create();

        $response = $this->json('delete', "api/categories/$category->id");

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);
    }

    public function testDeleteFail()
    {
        $this->login(Role::ADMIN);

        /** @var Category $category */
        $category = Category::factory()->hasAttached(Product::factory())->create();

        $response = $this->json('delete', "api/categories/$category->id");

        $response->assertStatus(400);

        $response->assertJson([
            'message' => __('category.delete.failed'),
            'status' => 400
        ]);
    }
}
