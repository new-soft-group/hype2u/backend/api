<?php

namespace Tests\Feature\Api;

use App\Collections\ProductStockInventoryCollection;
use App\Events\StockArrival;
use App\Models\Address;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\OrderStatus;
use App\Models\Product;
use App\Models\ProductStock;
use App\Models\ProductStockInventory;
use App\Models\ProductVariantOption;
use App\Models\Profile;
use App\Models\Role;
use App\Models\StockStatus;
use App\Models\TimeslotAllocation;
use App\Models\User;
use App\Notifications\ShippingConfirmationNotification;
use App\Rules\AllowedCurrencies;
use Database\Seeders\OrderStatusSeeder;
use Database\Seeders\PermissionSeeder;
use Database\Seeders\StockStatusSeeder;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Notification;
use Tests\TestCase;
use Tests\Traits\FakeLogin;

class OrderApiTest extends TestCase
{
    use RefreshDatabase, WithFaker, FakeLogin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed([PermissionSeeder::class, OrderStatusSeeder::class, StockStatusSeeder::class]);

        $user = User::factory()->create();

        $this->loginAs($user, Role::ADMIN);
    }

    public function testShowOrders()
    {
        Order::factory()
             ->count(3)
             ->has(OrderItem::factory()->count(3), 'items')
             ->has(Invoice::factory())
             ->create();

        $response = $this->json('get', 'api/orders');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'id',
                    'status',
                    'amount',
                    'currency',
                    'items' => [
                        '*' => [
                            'unit_price',
                            'quantity',
                            'variant' => [
                                'name',
                                'price'
                            ]
                        ]
                    ],
                    'order_at'
                ]
            ]
        ]);
    }

    public function testShowOrder()
    {
        /** @var Order $order */
        $order = Order::factory()
                      ->has(OrderItem::factory()->count(3), 'items')
                      ->has(Address::factory(), 'shippingAddress')
                      ->has(Invoice::factory())
                      ->has(TimeslotAllocation::factory(), 'timeslots')
                      ->create();

        $response = $this->json('get', "api/orders/$order->id");

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'id',
                     'status',
                     'amount',
                     'currency',
                     'items' => [
                         '*' => [
                             'unit_price',
                             'quantity',
                             'variant' => [
                                 'name',
                                 'price'
                             ]
                         ]
                     ],
                     'order_at',
                     'invoice',
                     'shipping_address',
                     'timeslots' => [
                         '*' => [
                             'id',
                             'timeslot' => [
                                 'type' => [
                                     'id',
                                     'name'
                                 ]
                             ],
                             'start_at',
                             'end_at'
                         ]
                     ]
                 ]);
    }

    public function testDeleteOrder()
    {
        $items = OrderItem::factory()
                          ->state(['quantity' => 2])
                          ->for(ProductStock::factory()
                                            ->hasAttached(ProductVariantOption::factory(), [], 'combinations')
                                            ->has(ProductStockInventory::factory()->count(5), 'inventories'),
                              'stock');

        /** @var Order $order */
        $order = Order::factory()
                      ->has($items, 'items')
                      ->has(Address::factory(), 'shippingAddress')
                      ->state(['order_status_id' => OrderStatus::PENDING])
                      ->create();

        $items = $order->items()->get();
        /**
         * Assume Stock have been reserved 3 items
         */
        ProductStock::find(1)->reserved(3);

        $response = $this->json('delete', "api/orders/$order->id");

        $response->assertStatus(200)
                 ->assertJson(['result' => true]);

        $items->each(function (OrderItem $item) {
            $this->assertDatabaseHas('product_stocks', [
                'id' => $item->product_stock_id,
                'reserved' => 1
            ]);
        });
    }

    public function testCreateOrderOnBehalfUsingDifferentAddress()
    {
        $items = OrderItem::factory()
                          ->state(['quantity' => 2])
                          ->for(ProductStock::factory()
                                            ->hasAttached(ProductVariantOption::factory(), [], 'combinations')
                                            ->has(ProductStockInventory::factory()->count(5), 'inventories'),
                              'stock')
                          ->make();

        /** @var User $user */
        $user = User::factory()
                    ->has(Profile::factory()->has(Address::factory()))
                    ->create();

        $response = $this->json('post', "api/orders/$user->id", [
            'currency' => AllowedCurrencies::MYR,
            'items' => [$items->toArray()],
            'shipping_address' => Address::factory()->make()
        ]);

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'id',
                     'amount',
                     'currency',
                     'items' => [
                         '*' => [
                             'unit_price',
                             'quantity',
                             'product'
                         ]
                     ]
                 ]);
    }

    public function testCreateOrderOnBehalf()
    {
        $items = OrderItem::factory()
                          ->state(['quantity' => 2])
                          ->for(ProductStock::factory()
                                            ->hasAttached(ProductVariantOption::factory(), [], 'combinations')
                                            ->has(ProductStockInventory::factory()->count(5), 'inventories'),
                              'stock')
                          ->make();

        /** @var User $user */
        $user = User::factory()
                    ->has(Profile::factory()->has(Address::factory()))
                    ->create();
        $user->profile->defaultShippingAddress()->associate(1)->save();

        $response = $this->json('post', "api/orders/$user->id", [
            'currency' => AllowedCurrencies::MYR,
            'items' => [$items->toArray()]
        ]);

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'id',
                     'amount',
                     'currency',
                     'items' => [
                         '*' => [
                             'unit_price',
                             'quantity',
                             'product'
                         ]
                     ]
                 ]);
    }

    public function testRejectOrder()
    {
        /** @var Order $order */
        $order = Order::factory()
                      ->has(OrderItem::factory(), 'items')
                      ->create();

        $order->items()->each(function (OrderItem $item) {
            $item->stock->reserved($item->quantity);
        });

        $response = $this->json('put', "api/orders/$order->id/reject");

        $response->assertStatus(200);
        $this->assertDatabaseHas('product_stocks', [
            'id' => 1,
            'reserved' => 0
        ]);
    }

    public function testApproveOrder()
    {
        /** @var Order $order */
        $order = Order::factory()
            ->has(OrderItem::factory(), 'items')
            ->create();

        $order->items->load('stock.inventoryOnHand');

        $response = $this->json('put', "api/orders/$order->id/approve");

        $response->assertStatus(200);

        $this->assertDatabaseHas('orders', [
            'order_status_id' => OrderStatus::APPROVED
        ]);

    }

    public function testScanReserveTags()
    {
        /** @var Order $order */
        $order = Order::factory()
            ->has(OrderItem::factory(), 'items')
            ->create();

        $order->items->load('stock.inventories');

        $tags = [];

        foreach ($order->items as $item){
            $inventoryTags = $item->stock->inventories->first()->take($item->quantity)->pluck('tag');
            array_push($tags, $inventoryTags);
        }

        $response = $this->json('post', "api/orders/$order->id/reserve/tags", [
            'tags' => $tags
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('product_stock_inventories', [
            'stock_status_id' => StockStatus::RESERVED
        ]);

        $inventories = json_decode($response->getContent(), true);

        foreach ($inventories as $inventory) {
            $this->assertDatabaseHas('product_stock_inventories', [
                'tag' => $inventory['tag'],
                'stock_status_id' => StockStatus::RESERVED
            ]);
            $this->assertDatabaseHas('inventory_order_tracks', [
                'product_stock_inventory_id' => $inventory['id'],
                'order_id' => $order->id
            ]);
        }

    }

    public function testShipOrder()
    {
        Notification::fake();

        /** @var Order $order */
        $order = Order::factory()
            ->has(OrderItem::factory(), 'items')
            ->has(ProductStockInventory::factory(), 'tracks')
            ->create();

        $order->items->load('stock.inventoryOnHand');


        $response = $this->json('put', "api/orders/$order->id/ship");

        $response->assertStatus(200);

        $this->assertDatabaseHas('orders', [
            'order_status_id' => OrderStatus::SHIPPING
        ]);

        Notification::assertSentTo(
            $order->user, function(ShippingConfirmationNotification $notification, $channels) use ($order){
            return $notification->order->user->profile->name === $order->user->profile->name;
        }
        );
    }

    public function testDeliverOrder()
    {
        /** @var Order $order */
        $order = Order::factory()
            ->has(OrderItem::factory(), 'items')
            ->has(ProductStockInventory::factory(), 'tracks')
            ->create();

        $order->items->load('stock.inventoryOnHand');

        $response = $this->json('put', "api/orders/$order->id/deliver");

        $response->assertStatus(200);

        $this->assertDatabaseHas('orders', [
            'order_status_id' => OrderStatus::DELIVERED
        ]);


    }

//    public function testDeliverOrder()
//    {
//        Notification::fake();
//
//        /** @var Order $order */
//        $order = Order::factory()
//                      ->has(OrderItem::factory(), 'items')
//                      ->create();
//
//        $order->items->load('stock.inventoryOnHand');
//
//        $tags = $order->items->pluck('stock.inventoryOnHand')
//                             ->flatten()
//                             ->map(function (ProductStockInventory $inventory) {
//                                 return $inventory->tag;
//                             })
//                             ->toArray();
//
//        $response = $this->json('put', "api/orders/$order->id/deliver", [
//            'tags' => $tags
//        ]);
//
//        $response->assertStatus(200)
//                 ->assertJsonCount($order->items->sum('quantity'), '*');
//
//        $inventories = json_decode($response->getContent(), true);
//
//        foreach ($inventories as $inventory) {
//            $this->assertDatabaseHas('product_stock_inventories', [
//                'tag' => $inventory['tag'],
//                'stock_status_id' => StockStatus::SHIPPED_OUT
//            ]);
//            $this->assertDatabaseHas('inventory_order_tracks', [
//                'product_stock_inventory_id' => $inventory['id'],
//                'order_id' => $order->id
//            ]);
//        }
//
//        Notification::assertSentTo(
//                $order->user, function(ShippingConfirmationNotification $notification, $channels) use ($order){
//                return $notification->order->user->profile->name === $order->user->profile->name;
//            }
//        );
//    }

    public function testReturnBackOrder()
    {
        $inventories = ProductStockInventory::factory()
                                            ->count(3)
                                            ->shippedOut();
        /** Simulate others available stocks present */
        ProductStock::factory()->has($inventories, 'inventories')->create();
        /** @var ProductStock $stock */
        $stock = ProductStock::factory()
                             ->has($inventories, 'inventories')
                             ->state(['reserved' => 3])
                             ->create();
        /** @var Order $order */
        $order = Order::factory()
                      ->state(['order_status_id' => OrderStatus::DELIVERED])
                      ->create();
        /** Attach stock's inventory tags to order */
        $order->tracks()->syncWithoutDetaching($stock->inventories);
        /** Map payload to specified format */
        $tags = $order->tracks->map(function (ProductStockInventory $inventory) {
            return [
                'tag' => $inventory->tag,
                'status' => StockStatus::IN_STOCK
            ];
        });

        Event::fake();

        $response = $this->json('put', "api/orders/$order->id/return", [
            'tags' => $tags->toArray()
        ]);

        $response->assertStatus(200)
                 ->assertJsonStructure(['tracks' => [
                     '*' => [
                         'tag',
                         'scanned_at'
                     ]
                 ]]);

        $this->assertCount(3, $stock->inventoryOnHand()->get());
        $this->assertDatabaseHas('orders', ['order_status_id' => OrderStatus::COMPLETED])
             ->assertDatabaseCount('inventory_order_tracks', 3)
             ->assertDatabaseHas('product_stocks', [
                 'id' => $stock->id,
                 'reserved' => 0
             ]);

        $order->tracks()->each(function ($track) {
           $this->assertNotNull($track->pivot->scanned_at);
           $this->assertDatabaseHas('inventory_order_tracks', [
                'product_stock_inventory_id' => $track->pivot->product_stock_inventory_id,
                'order_id' => $track->pivot->order_id
           ]);
        });

        Event::assertDispatched(StockArrival::class, 1);
    }

    public function testReturnBackOrderShouldFireTwoEvents()
    {
        /** @var ProductStock $stock1 */
        $stock1 = ProductStock::factory()
                              ->for(Product::factory())
                              ->hasAttached(
                                  ProductVariantOption::factory()->product(), [],
                                  'combinations')
                              ->has(ProductStockInventory::factory()->count(2)->shippedOut(), 'inventories')
                              ->state(['reserved' => 2])
                              ->create();

        /** @var ProductStock $stock2 */
        $stock2 = ProductStock::factory()
                              ->for(Product::factory())
                              ->hasAttached(
                                  ProductVariantOption::factory()->product(), [],
                                  'combinations')
                              ->has(ProductStockInventory::factory()->count(1)->shippedOut(), 'inventories')
                              ->state(['reserved' => 1])
                              ->create();

        /** @var Order $order */
        $order = Order::factory()
                      ->has(OrderItem::factory()
                                     ->count(3)
                                     ->state(new Sequence(
                                         [
                                             'product_id' => 1,
                                             'product_stock_id' => $stock1->id
                                         ],
                                         [
                                             'product_id' => 1,
                                             'product_stock_id' => $stock1->id
                                         ],
                                         [
                                             'product_id' => 2,
                                             'product_stock_id' => $stock2->id
                                         ]
                                     )),
                          'items')
                      ->state(['order_status_id' => OrderStatus::DELIVERED])
                      ->create();

        $order->tracks()->syncWithoutDetaching($stock1->inventories);
        $order->tracks()->syncWithoutDetaching($stock2->inventories);

        Event::fake();

        $inventories1 = $stock1->inventories->map(function ($inventory) {
            return ['tag' => $inventory->tag, 'status' => 1];
        })->toArray();

        $inventories2 = $stock2->inventories->map(function ($inventory) {
            return ['tag' => $inventory->tag, 'status' => 1];
        })->toArray();

        $response = $this->json('put', "api/orders/$order->id/return", [
            'tags' => array_merge($inventories1, $inventories2)
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('product_stocks', [
            'id' => $stock1->id,
            'reserved' => 0
        ])->assertDatabaseHas('product_stocks', [
            'id' => $stock2->id,
            'reserved' => 0
        ]);

        Event::assertDispatchedTimes(StockArrival::class, 2);
    }

    public function testReturnBackShouldFailIfTagNotMatch()
    {
        /** @var ProductStockInventory $tag */
        $inventory = ProductStockInventory::factory()->create();
        /** @var ProductStock $stock */
        $stock = ProductStock::factory()
                             ->for(Product::factory())
                             ->hasAttached(
                                 ProductVariantOption::factory()->product(), [],
                                 'combinations')
                             ->has(ProductStockInventory::factory()->count(2)->shippedOut(), 'inventories')
                             ->state(['reserved' => 1])
                             ->create();

        /** @var Order $order */
        $order = Order::factory()
                      ->has(OrderItem::factory()
                                     ->state([
                                         'product_id' => 1,
                                         'product_stock_id' => 1
                                     ]),
                          'items')
                      ->state(['order_status_id' => OrderStatus::DELIVERED])
                      ->create();

        $order->tracks()->syncWithoutDetaching($stock->inventories);

        Event::fake();

        $response = $this->json('put', "api/orders/$order->id/return", [
            'tags' => [
                [
                    'tag' => $inventory->tag,
                    'status' => 1
                ]
            ]
        ]);

        $response->assertStatus(400)
                 ->assertJson(['message' => __('tag.not_match', [
                     'tags' => $inventory->tag
                 ])]);

        Event::assertNotDispatched(StockArrival::class);
    }

    public function testShouldNotCompleteWhenPartialTagsReturned()
    {
        /** @var ProductStock $stock */
        $stock = ProductStock::factory()
                             ->for(Product::factory())
                             ->hasAttached(
                                 ProductVariantOption::factory()->product(), [],
                                 'combinations')
                             ->has(ProductStockInventory::factory()->count(2)->shippedOut(), 'inventories')
                             ->state(['reserved' => 2])
                             ->create();

        /** @var Order $order */
        $order = Order::factory()
                      ->has(OrderItem::factory()
                                     ->count(2)
                                     ->state([
                                         'product_id' => 1,
                                         'product_stock_id' => 1,
                                     ]),
                          'items')
                      ->state(['order_status_id' => OrderStatus::DELIVERED])
                      ->create();

        $order->tracks()->syncWithoutDetaching($stock->inventories);

        Event::fake();

        $inventories = $stock->inventories->map(function ($inventory) {
            return ['tag' => $inventory->tag, 'status' => 1];
        })->toArray();

        $response = $this->json('put', "api/orders/$order->id/return", [
            'tags' => [
                $inventories[0]
            ]
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseHas('product_stocks', [
            'id' => $stock->id,
            'reserved' => 1
        ])->assertDatabaseHas('orders', [
            'id' => $order->id,
            'order_status_id' => OrderStatus::PARTIAL_RETURNED
        ]);

        Event::assertDispatchedTimes(StockArrival::class);
    }

    public function testShouldThrowExceptionWhenDoubleScanOccur()
    {
        /** @var ProductStockInventory $inventory */
        $inventory = ProductStockInventory::factory()->create();

        /** @var Order $order */
        $order = Order::factory()
                      ->state(['order_status_id' => OrderStatus::DELIVERED])
                      ->create();

        $order->tracks()->attach($inventory, ['scanned_at' => now()]);

        Event::fake();

        $response = $this->json('put', "api/orders/$order->id/return", [
            'tags' => [
                [
                    'tag' => $inventory->tag,
                    'status' => StockStatus::IN_STOCK
                ]
            ]
        ]);

        $response->assertStatus(400)
                 ->assertJson(['message' => __('tag.scanned', ['tag' => $inventory->tag])]);

        Event::assertNotDispatched(StockArrival::class);
    }
}
