<?php


namespace Tests\Feature\Api;


use App\Models\Product;
use App\Models\ProductType;
use App\Models\Role;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\FakeLogin;

class ProductTypeApiTest extends TestCase
{
    use RefreshDatabase, FakeLogin, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setupPermission();

        $this->login(Role::ADMIN);
    }

    public function testIndex()
    {
        ProductType::factory()->count(2)->create();

        $response = $this->json('get', 'api/products/types');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'name'
            ]
        ]);
    }

    public function testCreate()
    {
        $attributes = ProductType::factory()->make()->toArray();

        $response = $this->json('post', '/api/products/types', $attributes);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'id',
            'name'
        ]);
    }

    public function testUpdate()
    {
        $types = ProductType::factory()->count(2)->create();

        $attributes = $types->map(function (ProductType $type) {
            return [
                'id' => $type->id,
                'name' => $this->faker->name
            ];
        })->toArray();

        $response = $this->json('patch', "api/products/types", [
            'types' => $attributes
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'name'
            ]
        ]);
    }

    public function testDelete()
    {
        /** @var ProductType $type */
        $type = ProductType::factory()->create();

        $response = $this->json('delete', "api/products/types/$type->id");

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);
    }

    public function testDeleteFail()
    {
        /** @var ProductType $type */
        $type = ProductType::factory()->has(Product::factory())->create();

        $response = $this->json('delete', "api/products/types/$type->id");

        $response->assertStatus(400);

        $response->assertJson([
            'message' => __('product.type.delete.failed'),
            'status' => 400
        ]);
    }
}
