<?php

namespace Tests\Feature\Api;


use App\Models\Holiday;
use App\Models\Order;
use App\Models\Role;
use App\Models\Timeslot;
use App\Models\TimeslotAllocation;
use App\Models\TimeslotType;
use Carbon\Carbon;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\FakeLogin;

class TimeslotApiTest extends TestCase
{
    use RefreshDatabase, FakeLogin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setupPermission();

        $this->login(Role::ADMIN);
    }

    public function testIndex()
    {
        Timeslot::factory()
                ->count(5)
                ->create();

        $response = $this->json('get', 'api/timeslots');

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'data' => [
                         '*' => [
                             'start_time',
                             'end_time',
                             'type' => [
                                 'id',
                                 'name'
                             ],
                             'full'
                         ]
                     ]
                 ]);
    }

    public function testShow()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()
                            ->has(TimeslotAllocation::factory(), 'allocations')
                            ->create();

        $response = $this->json('get', "api/timeslots/$timeslot->id");

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'start_time',
                     'end_time',
                     'type_id',
                     'limit',
                     'enabled',
                     'allocations' => [
                         '*' => [
                             'id',
                             'order_id',
                             'start_at',
                             'end_at'
                         ]
                     ]
                 ]);
    }

    public function testCreate()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->make();

        $response = $this->json('post', 'api/timeslots', [
            'start_time' => $timeslot->start_time,
            'end_time' => $timeslot->end_time,
            'type_id' => $timeslot->type_id,
            'limit' => $timeslot->limit,
            'enabled' => $timeslot->enabled,
        ]);

        $response->assertStatus(200);

        $response->assertJson([
            'id' => 1,
            'start_time' => $timeslot->start_time,
            'end_time' => $timeslot->end_time,
            'type_id' => $timeslot->type_id,
            'limit' => $timeslot->limit,
            'enabled' => $timeslot->enabled
        ]);
    }

    public function testUpdate()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->create();
        /** @var Timeslot $attribute */
        $attribute = Timeslot::factory()->make();

        $response = $this->json('put', "api/timeslots/$timeslot->id", [
            'start_time' => $attribute->start_time,
            'end_time' => $attribute->end_time,
            'type_id' => $attribute->type_id,
            'limit' => $attribute->limit,
            'enabled' => false
        ]);

        $response->assertStatus(200);

        $response->assertJson([
            'id' => $timeslot->id,
            'start_time' => $attribute->start_time,
            'end_time' => $attribute->end_time,
            'type_id' => $attribute->type_id,
            'limit' => $attribute->limit,
            'enabled' => false
        ]);
    }

    public function testUpdateShouldFail()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()
                            ->has(TimeslotAllocation::factory(), 'allocations')
                            ->create();
        /** @var Timeslot $attribute */
        $attribute = Timeslot::factory()->make();

        $response = $this->json('put', "api/timeslots/$timeslot->id", [
            'start_time' => $attribute->start_time,
            'end_time' => $attribute->end_time
        ]);

        $response->assertStatus(403);
    }

    public function testDeleteShouldFail()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()
                            ->has(TimeslotAllocation::factory(), 'allocations')
                            ->create();

        $response = $this->json('delete', "api/timeslots/$timeslot->id");

        $response->assertStatus(403);
    }

    public function testDelete()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->create();

        $response = $this->json('delete', "api/timeslots/$timeslot->id");

        $response->assertStatus(200)
                 ->assertJson(['result' => true]);
    }

    public function testAllocate()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->create();
        /** @var Order $order */
        $order = Order::factory()->create();

        $response = $this->json('post', "api/timeslots/$timeslot->id/allocate", [
            'order_id' => $order->id,
            'date' => Carbon::now()->addDay()->format('Y-m-d')
        ]);

        $response->assertStatus(200)
                 ->assertJson(['order_id' => $order->id])
                 ->assertJsonStructure([
                     'order_id',
                     'start_at',
                     'end_at'
                 ]);
    }

    public function testAllocateBoundaryLimit()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()
                            ->has(TimeslotAllocation::factory()->count(2), 'allocations')
                            ->state(['limit' => 3])
                            ->create();
        /** @var Order $order */
        $order = Order::factory()->create();

        $response = $this->json('post', "api/timeslots/$timeslot->id/allocate", [
            'order_id' => $order->id,
            'date' => Carbon::now()->addDay()->format('Y-m-d')
        ]);

        $response->assertStatus(200)
                 ->assertJson(['order_id' => $order->id])
                 ->assertJsonStructure([
                     'order_id',
                     'start_at',
                     'end_at'
                 ]);
    }

    public function testShouldFailWhenTimeslotFullyBooked()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()
                            ->has(TimeslotAllocation::factory()->count(2), 'allocations')
                            ->state(['limit' => 2])
                            ->create();
        /** @var Order $order */
        $order = Order::factory()->create();

        $response = $this->json('post', "api/timeslots/$timeslot->id/allocate", [
            'order_id' => $order->id,
            'date' => Carbon::now()->addDay()->format('Y-m-d')
        ]);

        $response->assertStatus(400)
                 ->assertJson(['message' => __('timeslot.full')]);
    }

    public function testShouldFailWhenTimeslotDateIsWeekend()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->state(['limit' => 1])->create();
        /** @var Order $order */
        $order = Order::factory()->create();

        $response = $this->json('post', "api/timeslots/$timeslot->id/allocate", [
            'order_id' => $order->id,
            'date' => Carbon::now()->nextWeekendDay()->format('Y-m-d')
        ]);

        $response->assertStatus(400)
                 ->assertJson(['message' => __('timeslot.weekend')]);
    }

    public function testShouldFailWhenTimeslotDateIsHoliday()
    {
        $today = Carbon::now()->format('Y-m-d');

        Holiday::factory()->state([
            'from' => $today,
            'to' => $today
        ])->create();

        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->state(['limit' => 1])->create();
        /** @var Order $order */
        $order = Order::factory()->create();

        $response = $this->json('post', "api/timeslots/$timeslot->id/allocate", [
            'order_id' => $order->id,
            'date' => $today
        ]);

        $response->assertStatus(400)
                 ->assertJson(['message' => __('timeslot.holiday')]);
    }

    public function testGetTypeListing()
    {
        TimeslotType::factory()->count(5)->create();

        $response = $this->json('get', 'api/timeslots/types');

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     '*' => [
                         'id',
                         'name'
                     ]
                 ]);
    }

    public function testGetEvents()
    {
        TimeslotAllocation::factory()->count(5)->create();

        $response = $this->json('get', 'api/timeslots/events');

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'data' => [
                         '*' => [
                             'name',
                             'start',
                             'end'
                         ]
                     ]
                 ]);
    }

    public function testRescheduleTimeslot()
    {
        /** @var Timeslot $timeslot */
        $timeslot = Timeslot::factory()->create();
        /** @var TimeslotAllocation $allocated */
        $allocated = TimeslotAllocation::factory()->create();

        $date = now()->addDay()->toDateString();

        $response = $this->json('put', "api/timeslots/$allocated->id/reschedule", [
            'timeslot' => $timeslot->id,
            'date' => $date
        ]);

        $start = Carbon::parse("$date $timeslot->start_time")->toJSON();

        $end = Carbon::parse("$date $timeslot->end_time")->toJSON();

        $response->assertStatus(200)
                 ->assertJson([
                     'id' => 1,
                     'timeslot_id' => $timeslot->id,
                     'order_id' => $allocated->order_id,
                     'name' => $timeslot->type->name,
                     'date' => $date,
                     'start' => $start,
                     'end' => $end,
                 ]);
    }
}
