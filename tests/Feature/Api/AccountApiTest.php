<?php

namespace Tests\Feature\Api;

use App\Models\Address;
use App\Models\Coupon;
use App\Models\Invoice;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Plan;
use App\Models\Profile;
use App\Models\Role;
use App\Models\User;
use Google\Auth\Cache\Item;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use Tests\Traits\FakeLogin;
use Tests\Traits\MockSubscription;

class AccountApiTest extends TestCase
{
    use RefreshDatabase, WithFaker, FakeLogin, MockSubscription;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setupPermission();

        $this->login(Role::ADMIN);
    }

    /**
     * List all account
     *
     * @return void
     */
    public function testShowAccounts()
    {
        User::factory()
            ->count(2)
            ->has(Profile::factory())
            ->create();

        $response = $this->json('get', 'api/accounts');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'data' => [
                '*' => [
                    'email',
                    'profile' => [
                        'first_name',
                        'last_name',
                        'mobile'
                    ]
                ]
            ],
            'links',
            'meta'
        ]);
    }

    /**
     * Update existing account
     *
     * @return void
     * @throws \Throwable
     */
    public function testUpdateAccount()
    {
        /** @var User $account */
        $account = User::factory()->has(Profile::factory())->create();
        $account->assignRole(Role::USER);
        $role = Role::findByName(Role::ADMIN, 'web');

        /** @var Profile $profile */
        $profile = Profile::factory()->make();

        $response = $this->json('put', "api/accounts/$account->id", [
            'first_name' => $profile->first_name,
            'last_name' => $profile->last_name,
            'mobile' => $profile->mobile,
            'role_id' => $role->id
        ]);

        $response->assertStatus(200);

        $response->assertJson(['profile' => [
            'first_name' => $profile->first_name,
            'last_name' => $profile->last_name,
            'mobile' => $profile->mobile,
        ]]);

        $account->refresh();

        $this->assertTrue($account->hasRole(Role::ADMIN));
    }

    /**
     * Update existing account
     *
     * @return void
     * @throws \Throwable
     */
    public function testGetAccountAddresses()
    {
        /** @var User $user */
        $user = User::factory()
                    ->has(Profile::factory()->has(Address::factory()))
                    ->create();

        $response = $this->json('get', "api/accounts/$user->id/addresses");

        $response->assertStatus(200);
    }

    public function testGetInvoices()
    {
        $user1 = $this->subscribedUser();
        $user2 = $this->subscribedUser();

        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->amount()->create();

        $user1->defaultSubscription()
             ->invoices()
             ->saveMany(
                 Invoice::factory()
                        ->count(2)
                        ->state(['stripe_coupon' => $coupon->stripe_id])
                        ->make()
             );

        $user2->defaultSubscription()
              ->invoices()
              ->saveMany(
                  Invoice::factory()
                         ->count(2)
                         ->state(['stripe_coupon' => $coupon->stripe_id])
                         ->make()
              );

        $response = $this->json('get', "api/accounts/$user1->id/invoices");

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'data' => [
                         '*' => [
                             'stripe_id',
                             'payment_intent',
                             'currency',
                             'amount',
                             'status',
                             'created_at',
                             'updated_at',
                             'coupon' => [
                                 'id',
                                 'name',
                                 'stripe_id',
                                 'code',
                                 'amount_off',
                                 'currency',
                                 'percent_off',
                                 'duration',
                                 'duration_in_months',
                                 'max_redemptions',
                                 'times_redeemed',
                                 'redeem_by',
                                 'enabled',
                                 'deleted_at',
                                 'created_at'
                             ]
                         ]
                     ]
                 ]);
    }

    public function testGetInvoicesShouldIncludeChargePayment()
    {
        $user = $this->subscribedUser();

        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->amount()->create();

        Order::factory()
             ->state(['user_id' => $user->id])
             ->has(Invoice::factory()->state(['amount' => 1000]))
             ->has(OrderItem::factory(), 'items')
             ->create();

        $user->defaultSubscription()
              ->invoices()
              ->saveMany(
                  Invoice::factory()
                         ->count(2)
                         ->state([
                             'amount' => 2000,
                             'stripe_coupon' => $coupon->stripe_id
                         ])
                         ->make()
              );

        $response = $this->json('get', "api/accounts/$user->id/invoices");

        $response->assertStatus(200)
                 ->assertJsonCount(3, 'data')
                 ->assertJsonStructure([
                     'data' => [
                         '*' => [
                             'stripe_id',
                             'payment_intent',
                             'currency',
                             'amount',
                             'status',
                             'created_at',
                             'updated_at',
                             'coupon'
                         ]
                     ]
                 ]);
    }
}
