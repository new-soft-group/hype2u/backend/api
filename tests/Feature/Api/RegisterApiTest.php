<?php

namespace Tests\Feature\Api;

use App\Events\UserLoggedIn;
use App\Models\User;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Event;
use Tests\TestCase;

class RegisterApiTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(PermissionSeeder::class);

        $this->artisan('passport:install');
    }

    public function testRegister()
    {
        Event::fake();

        $response = $this->json('post', '/api/register', [
            'first_name' => $this->faker->firstName,
            'last_name' => $this->faker->lastName,
            'email' => $this->faker->email,
            'password' => 'password',
            'password_confirmation' => 'password',
            'mobile' => $this->faker->e164PhoneNumber
        ]);

        $response->assertStatus(201);

        $user = User::find($response->offsetGet('id'));

        $this->assertNotNull($response->offsetGet('token'));

        Event::assertDispatched(function (UserLoggedIn $event) use ($user) {
            return $event->user->id === $user->id;
        });
    }
}
