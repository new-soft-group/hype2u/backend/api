<?php

namespace Tests\Feature\Api;

use App\Models\Role;
use App\Models\User;
use Database\Seeders\PermissionSeeder;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Laravel\Passport\Passport;
use Tests\TestCase;
use Tests\Traits\FakeLogin;

class RoleApiTest extends TestCase
{
    use RefreshDatabase, FakeLogin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setupPermission();

        $this->login(Role::ADMIN);
    }

    /**
     * Get all available roles.
     *
     * @return void
     */
    public function testGetRoles()
    {
        $response = $this->json('get', '/api/roles');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'name'
            ]
        ]);
    }

    /**
     * Get all available roles & permissions.
     *
     * @return void
     */
    public function testGetRolePermissions()
    {
        $response = $this->json('get', '/api/roles/permissions');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'name',
                'permissions' => [
                    '*' => [
                        'id',
                        'name'
                    ]
                ]
            ]
        ]);
    }

    /**
     * Update role's permissions with the given payload
     *
     * @return void
     */
    public function testUpdateRolePermissions()
    {
        $response = $this->json('post', '/api/roles/permissions', [
            'role_permissions' => [
                [
                    'id' => 2,
                    'name' => Role::USER,
                    'permissions' => [
                        ['id' => 1, 'name' => 'access.dashboard']
                    ]
                ]
            ]
        ]);

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'name',
                'permissions' => [
                    '*' => [
                        'id',
                        'name'
                    ]
                ]
            ]
        ]);
    }
}
