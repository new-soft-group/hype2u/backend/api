<?php


namespace Tests\Feature\Api;


use App\Models\Holiday;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Tests\Traits\FakeLogin;

class HolidayApiTest extends TestCase
{
    use RefreshDatabase, FakeLogin;

    protected function setUp(): void
    {
        parent::setUp();

        $this->setupPermission();

        $this->login(Role::ADMIN);
    }

    public function testIndex()
    {
        Holiday::factory()->count(3)->create();

        $response = $this->json('get', 'api/holidays');

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'data' => [
                         '*' => [
                             'id',
                             'label',
                             'from',
                             'to'
                         ]
                     ]
                 ]);
    }

    public function testIndexFilterByWeek()
    {
        Holiday::factory()
               ->count(3)
               ->state(new Sequence(
                   ['from' => now()->subWeek()->toDateString(), 'to' => now()->subDay()->toDateString()],
                   ['from' => now()->addMonth()->toDateString(), 'to' => now()->addMonth()->toDateString()],
                   ['from' => now()->addDay()->toDateString(), 'to' => now()->addWeek()->toDateString()]
               ))
               ->create();

        $response = $this->json('get', 'api/holidays', [
            'filter' => json_encode(['week' => now()->toDateString()])
        ]);

        $response->assertStatus(200)
                 ->assertJsonCount(1, 'data');
    }

    public function testIndexFilterByMonth()
    {
        Holiday::factory()
               ->count(3)
               ->state(new Sequence(
                   ['from' => now()->subDay()->toDateString(), 'to' => now()->subDay()->toDateString()],
                   ['from' => now()->toDateString(), 'to' => now()->toDateString()],
                   ['from' => now()->addMonth()->toDateString(), 'to' => now()->addMonth()->toDateString()]
               ))
               ->create();

        $response = $this->json('get', 'api/holidays', [
            'filter' => json_encode(['month' => now()->toDateString()])
        ]);

        $response->assertStatus(200)
                 ->assertJsonCount(2, 'data');
    }

    public function testIndexFilterByYear()
    {
        $today = Carbon::createFromDate(2020, 1, 10);

        Holiday::factory()
               ->count(4)
               ->state(new Sequence(
                   ['from' => $today->subDay()->toDateTimeString(), 'to' => $today->subDay()->toDateTimeString()],
                   ['from' => $today->toDateTimeString(), 'to' => $today->toDateTimeString()],
                   ['from' => $today->addWeek()->toDateTimeString(), 'to' => $today->addWeek()->toDateTimeString()],
                   ['from' => $today->addYear()->toDateTimeString(), 'to' => $today->addYear()->toDateTimeString()]
               ))
               ->create();

        $response = $this->json('get', 'api/holidays', [
            'filter' => json_encode(['year' => "2020"])
        ]);

        $response->assertStatus(200)
                 ->assertJsonCount(3, 'data');
    }

    public function testShow()
    {
        /** @var Holiday $holiday */
        $holiday = Holiday::factory()->create();

        $response = $this->json('get', "api/holidays/$holiday->id");

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'id',
                     'label',
                     'from',
                     'to'
                 ]);
    }

    public function testCreate()
    {
        /** @var Holiday $holiday */
        $holiday = Holiday::factory()->make();

        $response = $this->json('post', "api/holidays", $holiday->toArray());

        $response->assertStatus(200)
                 ->assertJsonStructure([
                     'id',
                     'label',
                     'from',
                     'to'
                 ]);
    }

    public function testUpdate()
    {
        /** @var Holiday $holiday */
        $holiday = Holiday::factory()->create();

        /** @var Holiday $attributes */
        $attributes = Holiday::factory()
                             ->state([
                                 'from' => now()->addDay()->format('Y-m-d'),
                                 'to' => now()->addDays(2)->format('Y-m-d')
                             ])->make();

        $response = $this->json('put', "api/holidays/$holiday->id", $attributes->toArray());

        $response->assertStatus(200)
                 ->assertJson([
                     'id' => $holiday->id,
                     'label' => $attributes->label,
                     'from' => $attributes->from,
                     'to' => $attributes->to
                 ]);
    }

    public function testDelete()
    {
        /** @var Holiday $holiday */
        $holiday = Holiday::factory()->create();

        $response = $this->json('delete', "api/holidays/$holiday->id");

        $response->assertStatus(200)
                 ->assertJson(['result' => true]);
    }
}
