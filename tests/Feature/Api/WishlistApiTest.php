<?php

namespace Tests\Feature\Api;

use App\Models\Image;
use App\Models\Product;
use App\Models\Role;
use App\Models\User;
use App\Models\Wishlist;
use App\Models\WishlistItem;
use Database\Seeders\PermissionSeeder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Laravel\Passport\Passport;
use Tests\TestCase;

class WishlistApiTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @var User|User[]|Collection|\Illuminate\Database\Eloquent\Model
     */
    private $user;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed(PermissionSeeder::class);

        $this->user = User::factory()->create();

        $this->user->assignRole(Role::USER);

        Passport::actingAs($this->user);
    }

    public function testGetWishlists()
    {
        Storage::fake('product');

        Wishlist::factory()
                ->state(['user_id' => $this->user->id])
                ->has(WishlistItem::factory()
                                  ->has(Product::factory()
                                               ->count(2)
                                               ->has(Image::factory()->product())), 'items')
                ->create();

        $response = $this->json('get', 'api/wishlists');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            'items' => [
                '*' => [
                    'id',
                    'product' => [
                        'images'
                    ]
                ]
            ]
        ]);
    }

    public function testCreateWishlist()
    {
        $product = Product::factory()->create();

        $response = $this->json('put', "api/wishlists/$product->id");

        $response->assertStatus(200);

        $response->assertJson([
            'id' => 1,
            'product' => [
                'id' => $product->id
            ]
        ]);
    }

    public function testCreateWishlistWithDuplicateItem()
    {
        /** @var Wishlist $wishlist */
        $wishlist = Wishlist::factory()
                            ->has(WishlistItem::factory(), 'items')
                            ->state(['user_id' => $this->user->id])
                            ->create();

        $product = $wishlist->items->first()->product_id;

        $response = $this->json('put', "api/wishlists/$product");

        $response->assertStatus(200);

        $response->assertJson([
            'id' => 1,
            'product' => [
                'id' => $product
            ]
        ]);
    }

    public function testAppendNewWishlistItem()
    {
        Wishlist::factory()
                ->has(WishlistItem::factory(), 'items')
                ->state(['user_id' => $this->user->id])
                ->create();

        /** @var Product $product */
        $product = Product::factory()->create();

        $response = $this->json('put', "api/wishlists/$product->id");

        $response->assertStatus(200);

        $response->assertJson([
            'id' => 2,
            'product' => [
                'id' => $product->id
            ]
        ]);
    }

    /**
     * @throws \Throwable
     */
    public function testDeleteNonExistItem()
    {
        Wishlist::factory()
                ->has(WishlistItem::factory(), 'items')
                ->state(['user_id' => $this->user->id])
                ->create();

        /** @var Product $product */
        $product = Product::factory()->create();

        $response = $this->json('delete', "api/wishlists/$product->id");

        $response->assertStatus(200);

        $response->assertJson(['result' => false]);
    }

    /**
     * @throws \Throwable
     */
    public function testDeleteItem()
    {
        /** @var Wishlist $wishlist */
        $wishlist = Wishlist::factory()
                            ->has(WishlistItem::factory()->count(3), 'items')
                            ->state(['user_id' => $this->user->id])
                            ->create();

        /** @var WishlistItem $item */
        $item = $wishlist->getItem(1);

        $response = $this->json('delete', "api/wishlists/$item->product_id");

        $response->assertStatus(200);

        $response->assertJson(['result' => true]);
    }
}
