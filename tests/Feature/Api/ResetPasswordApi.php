<?php

namespace Tests\Feature\Api;

use App\Models\User;
use App\Notifications\ResetPasswordLink;
use Illuminate\Auth\Events\PasswordReset;
use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Auth\Passwords\PasswordBrokerManager;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;
use Tests\TestCase;

class ResetPasswordApi extends TestCase
{
    use RefreshDatabase, WithFaker;

    protected function setUp(): void
    {
        parent::setUp();

        $this->seed();
    }

    /**
     * @param string $email
     * @return string
     * @throws \Illuminate\Contracts\Container\BindingResolutionException
     */
    protected function fakeToken(string $email)
    {
        $key = $this->app['config']['app.key'];

        $token = hash_hmac('sha256', Str::random(40), $key);

        $hashedToken = Hash::make($token);

        \DB::table('password_resets')->insert([
            ['email' => $email, 'token' => $hashedToken]
        ]);

        return $token;
    }


    /**
     * List all account
     *
     * @return void
     * @throws \Exception
     */
    public function testResetPassword()
    {
        Event::fake();

        /** @var User $user */
        $user = User::factory()->create();

        $token = $this->fakeToken($user->email);

        $response = $this->json('post', 'api/reset/password', [
            'token' => $token,
            'email' => $user->email,
            'password' => 'new_password',
            'password_confirmation' => 'new_password'
        ]);

        $response->assertStatus(200);

        $this->assertDatabaseCount('password_resets', 0);
    }
}
