<?php


namespace Tests\Feature\Api;


use App\Models\Gender;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class GenderApiTest extends TestCase
{
    use RefreshDatabase;

    protected function setUp(): void
    {
        parent::setUp();
    }

    public function testIndex()
    {
        Gender::factory()->count(2)->create();

        $response = $this->json('get', 'api/genders');

        $response->assertStatus(200);

        $response->assertJsonStructure([
            '*' => [
                'id',
                'name'
            ]
        ]);
    }
}
