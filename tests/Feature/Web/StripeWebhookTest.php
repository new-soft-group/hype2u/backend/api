<?php


namespace Tests\Feature\Web;


use App\Models\Coupon;
use App\Models\Subscription;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Str;
use Tests\TestCase;

class StripeWebhookTest extends TestCase
{
    use RefreshDatabase, WithoutMiddleware, WithFaker;

    protected function fixtureInvoicePaymentSucceedPayload(Subscription $subscription, Coupon $coupon = null)
    {
        return [
            'id' => 'evt_'.Str::random(),
            'object' => 'event',
            'data' => [
                'object' => [
                    'id' => 'in_'.Str::random(),
                    'object' => 'invoice',
                    'payment_intent' => 'pm_intent_' . Str::random(),
                    'amount_paid' => amountToCent(300),
                    'currency' => 'myr',
                    'status' => 'paid',
                    'subscription' => $subscription->stripe_id,
                    'invoice_pdf' => $this->faker->url,
                    'billing_reason' => $this->faker->sentence,
                    'discount' => !is_null($coupon) ? [
                        'id' => 'di_'.Str::random(),
                        'object' => 'discount',
                        'coupon' => [
                            'id' => $coupon->stripe_id
                        ]
                    ] : null,
                ]
            ],
            'type' => 'invoice.payment_succeeded'
        ];
    }

    protected function fixtureCustomerDiscountCreated(Coupon $coupon)
    {
        return [
            'id' => 'evt_'.Str::random(),
            'object' => 'event',
            'data' => [
                'object' => [
                    'id' => 'di_'.Str::random(),
                    'object' => 'discount',
                    'coupon' => [
                        'id' => $coupon->stripe_id,
                        'object' => 'coupon',
                        'duration_in_months' => 1,
                        'max_redemptions' => null,
                        'redeem_by' => null,
                        'times_redeemed' => 1
                    ],
                ]
            ],
            'type' => 'customer.discount.created'
        ];
    }

    public function testHandleSubscriptionInvoice()
    {
        /** @var Subscription $subscription */
        $subscription = Subscription::factory()->create();

        $fixture = $this->fixtureInvoicePaymentSucceedPayload($subscription);

        $response = $this->call('post', 'stripe/webhooks', [], [], [], [], json_encode($fixture));

        $response->assertStatus(200);

        $this->assertDatabaseHas('invoices', [
            'invoiceable_id' => $subscription->id,
            'invoiceable_type' => Subscription::class,
            'stripe_id' => $fixture['data']['object']['id'],
            'payment_intent' => $fixture['data']['object']['payment_intent'],
            'amount' => centToAmount($fixture['data']['object']['amount_paid']),
            'currency' => $fixture['data']['object']['currency'],
            'status' => $fixture['data']['object']['status']
        ]);
    }

    public function testHandleSubscriptionInvoiceWithCoupon()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->amount()->create();

        /** @var Subscription $subscription */
        $subscription = Subscription::factory()->create();

        $fixture = $this->fixtureInvoicePaymentSucceedPayload($subscription, $coupon);

        $response = $this->call('post', 'stripe/webhooks', [], [], [], [], json_encode($fixture));

        $response->assertStatus(200);

        $this->assertTrue($coupon->has('invoices')->exists());
        $this->assertDatabaseHas('invoices', [
            'invoiceable_id' => $subscription->id,
            'invoiceable_type' => Subscription::class,
            'stripe_id' => $fixture['data']['object']['id'],
            'payment_intent' => $fixture['data']['object']['payment_intent'],
            'amount' => centToAmount($fixture['data']['object']['amount_paid']),
            'currency' => $fixture['data']['object']['currency'],
            'status' => $fixture['data']['object']['status'],
            'stripe_coupon' => $coupon->stripe_id
        ]);
    }

    public function testHandleCouponApplied()
    {
        /** @var Coupon $coupon */
        $coupon = Coupon::factory()->amount()->create();

        $fixture = $this->fixtureCustomerDiscountCreated($coupon);

        $response = $this->call('post', 'stripe/webhooks', [], [], [], [], json_encode($fixture));

        $response->assertStatus(200);

        $this->assertDatabaseHas('coupons', [
            'stripe_id' => $coupon->stripe_id,
            'duration_in_months' => $fixture['data']['object']['coupon']['duration_in_months'],
            'max_redemptions' => null,
            'redeem_by' => $fixture['data']['object']['coupon']['redeem_by'],
            'times_redeemed' => $fixture['data']['object']['coupon']['times_redeemed']
        ]);
    }
}
