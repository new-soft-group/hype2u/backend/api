<?php

return [
    'prefix' => 'RM0.00 HYPE2U - ',
    'endpoint' => 'http://110.4.44.41:11009/cgi-bin/',
    'auth' => [
        'username' => env('SMS_USERNAME', 'none'),
        'password' => env('SMS_PASSWORD', 'none')
    ],
    'options' => [
        'from' => env('SMS_FROM', 'HYPE2U')
    ],
];
