<?php

return [
    'key' => env('STRIPE_KEY', null),
    'secret' => env('STRIPE_SECRET', null),
];
