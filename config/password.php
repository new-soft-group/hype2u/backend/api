<?php

return [
    
    'reset' => [
        'domain' => env('WEB_DOMAIN').'/password/reset'
    ]
];
