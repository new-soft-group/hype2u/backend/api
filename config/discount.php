<?php

return [
    'code' => [
        'length' => env('DISCOUNT_CODE_LENGTH', 10)
    ]
];
