<?php
	return [
        'mail' => [
            'url' => env('MAIL_URL')
        ],
        'admin' => [
            'email' => env('P4M_EMAIL')
        ]
    ];

