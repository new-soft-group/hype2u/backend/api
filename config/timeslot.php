<?php

return [
    'ignore' => [
        /*
        |--------------------------------------------------------------------------
        | Weekend
        |--------------------------------------------------------------------------
        |
        | This value is used to determine whether timeslot can be reserve on weekend
        |
        */
        'weekend' => false,
        /*
        |--------------------------------------------------------------------------
        | Holiday
        |--------------------------------------------------------------------------
        |
        | You may specify list of holiday and set this value to 'true'. When selected
        | timeslot date within specified holidays date range will be forbidden
        |
        */
        'holiday' => false
    ]
];
